 
package datos;

/* IMPORTS */

import java.io.Serializable;
import java.util.*;
import javax.persistence.*;

/* CLASS ANNOTATIONS*/

@Entity 

	   
/* CLASS DECLARATION */
	
public class User
	extends com.jgoodies.binding.beans.Model implements Serializable{
 
		    /* ATTRIBUTES FROM ENTITY */
 			@Id
		@GeneratedValue
		@Column(name="Id_User")
		private Long id;
	  
/* ATTRIBUTES FROM ASSOCIATIONS*/

			 			@OneToOne//
			private BillingInfo billing;  
		 	 	 	 	  
/* OPERATIONS */
//access fields
		    private java.lang.String userId;
		    private java.lang.String email;
     public User (){
        //constructor por defecto
    }    
 			public Long getId(){
			return id;
		}
		
		public void setId( Long id){
			this.id = id;
		}		
	  	
	public java.lang.String getUserId () {
		return userId;
	} 

	public void setUserId(java.lang.String userId ) {
		java.lang.String oldValue = this.userId;
		this.userId = userId;
		firePropertyChange("userId", oldValue, this.userId );
	}
	public java.lang.String getEmail () {
		return email;
	} 

	public void setEmail(java.lang.String email ) {
		java.lang.String oldValue = this.email;
		this.email = email;
		firePropertyChange("email", oldValue, this.email );
	}
 
/* OPERATIONS FROM ASSOCIATIONS*/

			 			public BillingInfo getBilling (){
				return billing;
			}			

			public void setBilling ( BillingInfo billing ){
				this.billing = billing;
			}
		 	 	 	 	  
	public Object get(int index ){
		switch(index){
				 					case 0:
							return userId;
								case 1:
							return email;
					 			}
			return "";
	}	
}