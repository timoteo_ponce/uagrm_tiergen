 
package datos;

/* IMPORTS */

import java.io.Serializable;
import java.util.*;
import javax.persistence.*;

/* CLASS ANNOTATIONS*/

@Entity 

	   
/* CLASS DECLARATION */
	
public class BillingInfo
	extends com.jgoodies.binding.beans.Model implements Serializable{
 
		    /* ATTRIBUTES FROM ENTITY */
 			@Id
		@GeneratedValue
		@Column(name="Id_BillingInfo")
		private Long id;
	  
/* ATTRIBUTES FROM ASSOCIATIONS*/

						@OneToOne
			private User user;
		 	 	 	 	  
/* OPERATIONS */
//access fields
		    private java.lang.String creditCardType;
		    private java.lang.String creditCardNumber;
		    private java.lang.String nameOnCreditCard;
		    private java.util.Date expiration;
		    private java.lang.String bakAccountNumber;
		    private java.lang.String bankName;
     public BillingInfo (){
        //constructor por defecto
    }    
 			public Long getId(){
			return id;
		}
		
		public void setId( Long id){
			this.id = id;
		}		
	  	
	public java.lang.String getCreditCardType () {
		return creditCardType;
	} 

	public void setCreditCardType(java.lang.String creditCardType ) {
		java.lang.String oldValue = this.creditCardType;
		this.creditCardType = creditCardType;
		firePropertyChange("creditCardType", oldValue, this.creditCardType );
	}
	public java.lang.String getCreditCardNumber () {
		return creditCardNumber;
	} 

	public void setCreditCardNumber(java.lang.String creditCardNumber ) {
		java.lang.String oldValue = this.creditCardNumber;
		this.creditCardNumber = creditCardNumber;
		firePropertyChange("creditCardNumber", oldValue, this.creditCardNumber );
	}
	public java.lang.String getNameOnCreditCard () {
		return nameOnCreditCard;
	} 

	public void setNameOnCreditCard(java.lang.String nameOnCreditCard ) {
		java.lang.String oldValue = this.nameOnCreditCard;
		this.nameOnCreditCard = nameOnCreditCard;
		firePropertyChange("nameOnCreditCard", oldValue, this.nameOnCreditCard );
	}
	public java.util.Date getExpiration () {
		return expiration;
	} 

	public void setExpiration(java.util.Date expiration ) {
		java.util.Date oldValue = this.expiration;
		this.expiration = expiration;
		firePropertyChange("expiration", oldValue, this.expiration );
	}
	public java.lang.String getBakAccountNumber () {
		return bakAccountNumber;
	} 

	public void setBakAccountNumber(java.lang.String bakAccountNumber ) {
		java.lang.String oldValue = this.bakAccountNumber;
		this.bakAccountNumber = bakAccountNumber;
		firePropertyChange("bakAccountNumber", oldValue, this.bakAccountNumber );
	}
	public java.lang.String getBankName () {
		return bankName;
	} 

	public void setBankName(java.lang.String bankName ) {
		java.lang.String oldValue = this.bankName;
		this.bankName = bankName;
		firePropertyChange("bankName", oldValue, this.bankName );
	}
 
/* OPERATIONS FROM ASSOCIATIONS*/

						public User getUser (){
				return user;
			}			

			public void setUser ( User user ){
				this.user = user;
			}
		 	 	 	 	  
	public Object get(int index ){
		switch(index){
				 					case 0:
							return creditCardType;
								case 1:
							return creditCardNumber;
								case 2:
							return nameOnCreditCard;
								case 3:
							return expiration;
								case 4:
							return bakAccountNumber;
								case 5:
							return bankName;
					 			}
			return "";
	}	
}