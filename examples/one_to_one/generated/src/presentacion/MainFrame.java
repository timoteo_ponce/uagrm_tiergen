/*
 * Created by JFormDesigner on Thu May 28 10:26:38 BOT 2009
 */

package presentacion;

import datos.*;

import java.awt.Container;

import javax.swing.*;

import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.uif_lite.panel.SimpleInternalFrame;

/**
 * @author Timoteo Ponce
 */
public class MainFrame extends JFrame {
	public MainFrame() {
		initComponents();
		addEntityPanels();
	}	

	private void initComponents() {
		// JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
		panelContainer = new SimpleInternalFrame();
		tabPanel = new JTabbedPane();
		CellConstraints cc = new CellConstraints();

		//======== this ========
		setAlwaysOnTop(true);
		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		setTitle("Aplicaci\u00f3n generada");
		Container contentPane = getContentPane();
		contentPane.setLayout(new FormLayout(
			"default, $lcgap, 173dlu:grow, $lcgap, default",
			"default, $lgap, 126dlu:grow, $lgap, default"));

		//======== panelContainer ========
		{
			panelContainer.setTitle("Entidades");
			Container panelContainerContentPane = panelContainer.getContentPane();
			panelContainerContentPane.setLayout(new FormLayout(
				"default:grow",
				"default:grow"));

			//======== tabPanel ========
			{
				tabPanel.setTabPlacement(SwingConstants.LEFT);
			}
			panelContainerContentPane.add(tabPanel, cc.xywh(1, 1, 1, 1, CellConstraints.FILL, CellConstraints.FILL));
		}
		contentPane.add(panelContainer, cc.xywh(3, 3, 1, 1, CellConstraints.FILL, CellConstraints.FILL));
		setSize(660, 565);
		setLocationRelativeTo(getOwner());
		// JFormDesigner - End of component initialization  //GEN-END:initComponents
	}

	// JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
	private SimpleInternalFrame panelContainer;
	private JTabbedPane tabPanel;
	// JFormDesigner - End of variables declaration  //GEN-END:variables
	
	private void addEntityPanels(){
			PanelUser panelUser = new PanelUser();
		tabPanel.add(panelUser.getTitle(), panelUser);	
		PanelBillingInfo panelBillingInfo = new PanelBillingInfo();
		tabPanel.add(panelBillingInfo.getTitle(), panelBillingInfo);	
	}
	
	/**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
            	try {
					UIManager.setLookAndFeel( new com.jgoodies.looks.plastic.Plastic3DLookAndFeel());
				} catch (UnsupportedLookAndFeelException e) {					
					e.printStackTrace();
				}
                MainFrame frame = new MainFrame();
                frame.setVisible(true);
            }
        });
    }

}