 
package datos;

/* IMPORTS */

import java.io.Serializable;
import java.util.*;
import javax.persistence.*;

/* CLASS ANNOTATIONS*/

						  @Entity 
						  @Embeddable 

														  								  						  								  						   							      
/* CLASS DECLARATION */
	
public class Empleado
							  extends Persona implements Serializable{
 

		    /* ATTRIBUTES FROM ENTITY */
 
		    /* ATTRIBUTES FROM ASSOCIATIONS*/

							   							   							  				    @ManyToOne
				    private Empresa empresa ;
							   							    
		    /* OPERATIONS */
			    //access fields
							  							      private java.lang.Long sueldo;
						     public Empleado (){
        //constructor por defecto
    }    
 	
			  public java.lang.Long getSueldo () {
      return sueldo;
  } 

  public void setSueldo(java.lang.Long sueldo ) {
      java.lang.Long oldValue = this.sueldo;
      this.sueldo = sueldo;
      firePropertyChange("sueldo", oldValue, this.sueldo );
  }
 
		    /* OPERATIONS FROM ASSOCIATIONS*/

							   							   							  				    public Empresa getEmpresa (){
					    		    return empresa;
				    }

				    public void setEmpresa ( Empresa empresa ){
					    		    this.empresa = empresa;
				    }
							   							    
			    public Object get(int index ){
				    		    switch(index){
				 							    		    		    case 0:
						  						    		    		    return sueldo;
						  		 				    		    }
				    		    return "";
			    }	
}