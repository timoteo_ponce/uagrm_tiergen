 
package datos;

/* IMPORTS */

import java.io.Serializable;
import java.util.*;
import javax.persistence.*;

/* CLASS ANNOTATIONS*/

						  @Entity 
						  @Embeddable 

							   
/* CLASS DECLARATION */
	
public class Telefono
							  extends com.jgoodies.binding.beans.Model implements Serializable{
 

		    /* ATTRIBUTES FROM ENTITY */
 							  				    @Id
				    @GeneratedValue
				    @Column(name="Id_Telefono")
				    private Long id;
							    
		    /* ATTRIBUTES FROM ASSOCIATIONS*/

							   							   							  				    @ManyToOne
				    private Persona propietario ;
							   							    
		    /* OPERATIONS */
			    //access fields
							  							      private java.lang.String secuencia;
						     public Telefono (){
        //constructor por defecto
    }    
 							  				    public Long getId(){
					    		    return id;
				    }
		
				    public void setId( Long id){
					    		    this.id = id;
				    }		
							    	
			  public java.lang.String getSecuencia () {
      return secuencia;
  } 

  public void setSecuencia(java.lang.String secuencia ) {
      java.lang.String oldValue = this.secuencia;
      this.secuencia = secuencia;
      firePropertyChange("secuencia", oldValue, this.secuencia );
  }
 
		    /* OPERATIONS FROM ASSOCIATIONS*/

							   							   							  				    public Persona getPropietario (){
					    		    return propietario;
				    }

				    public void setPropietario ( Persona propietario ){
					    		    this.propietario = propietario;
				    }
							   							    
			    public Object get(int index ){
				    		    switch(index){
				 							    		    		    case 0:
						  						    		    		    return secuencia;
						  		 				    		    }
				    		    return "";
			    }	
}