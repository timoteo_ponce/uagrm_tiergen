 
package datos;

/* IMPORTS */

import java.io.Serializable;
import java.util.*;
import javax.persistence.*;

/* CLASS ANNOTATIONS*/

						  @Entity 
						  @Embeddable 

							   
/* CLASS DECLARATION */
	
public class Empresa
							  extends com.jgoodies.binding.beans.Model implements Serializable{
 

		    /* ATTRIBUTES FROM ENTITY */
 							  				    @Id
				    @GeneratedValue
				    @Column(name="Id_Empresa")
				    private Long id;
							    
		    /* ATTRIBUTES FROM ASSOCIATIONS*/

							   							  				    @OneToMany
				    private Set<Empleado> empleados = new HashSet<Empleado>();		
							   							   							   							   							  				    @OneToMany
				    private Set<Cliente> clientes = new HashSet<Cliente>();		
							   							   							    
		    /* OPERATIONS */
			    //access fields
							  							      private java.lang.String nombre;
						     public Empresa (){
        //constructor por defecto
    }    
 							  				    public Long getId(){
					    		    return id;
				    }
		
				    public void setId( Long id){
					    		    this.id = id;
				    }		
							    	
			  public java.lang.String getNombre () {
      return nombre;
  } 

  public void setNombre(java.lang.String nombre ) {
      java.lang.String oldValue = this.nombre;
      this.nombre = nombre;
      firePropertyChange("nombre", oldValue, this.nombre );
  }
 
		    /* OPERATIONS FROM ASSOCIATIONS*/

							   							  				    public Set<Empleado> getEmpleados (){
					    		    return empleados;
				    }
							   							   							   							   							  				    public Set<Cliente> getClientes (){
					    		    return clientes;
				    }
							   							   							    
			    public Object get(int index ){
				    		    switch(index){
				 							    		    		    case 0:
						  						    		    		    return nombre;
						  		 				    		    }
				    		    return "";
			    }	
}