 
package datos;

/* IMPORTS */

import java.io.Serializable;
import java.util.*;
import javax.persistence.*;

/* CLASS ANNOTATIONS*/

						  @Entity 
						  @Embeddable 

														  								  						  								  						   							      
/* CLASS DECLARATION */
	
public class Cliente
							  extends Persona implements Serializable{
 

		    /* ATTRIBUTES FROM ENTITY */
 
		    /* ATTRIBUTES FROM ASSOCIATIONS*/

							   							   							  				    @ManyToOne
				    private Empresa empresa ;
							   							    
		    /* OPERATIONS */
			    //access fields
							  							      private java.lang.Boolean permanente;
						     public Cliente (){
        //constructor por defecto
    }    
 	
			  public java.lang.Boolean getPermanente () {
      return permanente;
  } 

  public void setPermanente(java.lang.Boolean permanente ) {
      java.lang.Boolean oldValue = this.permanente;
      this.permanente = permanente;
      firePropertyChange("permanente", oldValue, this.permanente );
  }
 
		    /* OPERATIONS FROM ASSOCIATIONS*/

							   							   							  				    public Empresa getEmpresa (){
					    		    return empresa;
				    }

				    public void setEmpresa ( Empresa empresa ){
					    		    this.empresa = empresa;
				    }
							   							    
			    public Object get(int index ){
				    		    switch(index){
				 							    		    		    case 0:
						  						    		    		    return permanente;
						  		 				    		    }
				    		    return "";
			    }	
}