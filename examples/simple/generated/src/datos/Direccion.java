 
package datos;

/* IMPORTS */

import java.io.Serializable;
import java.util.*;
import javax.persistence.*;

/* CLASS ANNOTATIONS*/

						  @Entity 

							   
/* CLASS DECLARATION */
	
public class Direccion
							  extends com.jgoodies.binding.beans.Model implements Serializable{
 

		    /* ATTRIBUTES FROM ENTITY */
 							  				    @Id
				    @GeneratedValue
				    @Column(name="Id_Direccion")
				    private Long id;
							    
		    /* ATTRIBUTES FROM ASSOCIATIONS*/

							   							   							  				    @ManyToOne
				    private Persona persona ;
							   							    
		    /* OPERATIONS */
			    //access fields
							  							      private java.lang.String calle;
													  							      private java.lang.Integer numero;
						     public Direccion (){
        //constructor por defecto
    }    
 							  				    public Long getId(){
					    		    return id;
				    }
		
				    public void setId( Long id){
					    		    this.id = id;
				    }		
							    	
			  public java.lang.String getCalle () {
      return calle;
  } 

  public void setCalle(java.lang.String calle ) {
      java.lang.String oldValue = this.calle;
      this.calle = calle;
      firePropertyChange("calle", oldValue, this.calle );
  }
			  public java.lang.Integer getNumero () {
      return numero;
  } 

  public void setNumero(java.lang.Integer numero ) {
      java.lang.Integer oldValue = this.numero;
      this.numero = numero;
      firePropertyChange("numero", oldValue, this.numero );
  }
 
		    /* OPERATIONS FROM ASSOCIATIONS*/

							   							   							  				    public Persona getPersona (){
					    		    return persona;
				    }

				    public void setPersona ( Persona persona ){
					    		    this.persona = persona;
				    }
							   							    
			    public Object get(int index ){
				    		    switch(index){
				 							    		    		    case 0:
						  						    		    		    return calle;
						  							    		    		    case 1:
						  						    		    		    return numero;
						  		 				    		    }
				    		    return "";
			    }	
}