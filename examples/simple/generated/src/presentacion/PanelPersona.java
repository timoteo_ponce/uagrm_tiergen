

package presentacion;

import com.jgoodies.binding.PresentationModel;
import com.jgoodies.binding.adapter.AbstractTableAdapter;
import com.jgoodies.binding.adapter.BasicComponentFactory;
import com.jgoodies.binding.adapter.SingleListSelectionAdapter;
import com.jgoodies.binding.list.ArrayListModel;
import com.jgoodies.binding.list.SelectionInList;
import com.jgoodies.binding.value.ValueModel;
import datos.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;
import javax.swing.*;
import java.text.DecimalFormat;

/**
 *
 * @author  $autor
 */

public class PanelPersona extends javax.swing.JPanel {
    ArrayListModel<Persona> arrayListModel;
    SelectionInList<Persona> selection;
    PresentationModel<Persona> presentationModel;
    JTable table;
    Persona currentPersona;
    
    /** Creates new form PanelPersona */
    public PanelPersona() {        
        initComponents();
        initBindings();        
        updateTable();
    }
    
    private void initBindings(){
        arrayListModel = new ArrayListModel();
        
        currentPersona = new Persona();        
        selection = new SelectionInList((ListModel)arrayListModel);        
		
		        table = new JTable( new PersonaTableAdapter((ListModel)selection, new String[]{ "id","nombre","apellido","edad" }) );
        table.setSelectionModel(new SingleListSelectionAdapter(
                selection.getSelectionIndexHolder()));        
        panelTable.setViewportView( table );
        
        presentationModel = new PresentationModel<Persona>( currentPersona );
		
						JLabel lblnombre = new JLabel( "nombre" );
			panelFields.add( lblnombre );
		ValueModel nombreModel = presentationModel.getModel("nombre");
						JTextField txtnombre = BasicComponentFactory.createTextField( nombreModel );
		txtnombre.setColumns( 5 );
		panelFields.add( txtnombre );
				
		
						JLabel lblapellido = new JLabel( "apellido" );
			panelFields.add( lblapellido );
		ValueModel apellidoModel = presentationModel.getModel("apellido");
						JTextField txtapellido = BasicComponentFactory.createTextField( apellidoModel );
		txtapellido.setColumns( 5 );
		panelFields.add( txtapellido );
				
		
						JLabel lbledad = new JLabel( "edad" );
			panelFields.add( lbledad );
		ValueModel edadModel = presentationModel.getModel("edad");
						JFormattedTextField integeredad = BasicComponentFactory.createIntegerField( edadModel );
		integeredad.setColumns( 5 );
		panelFields.add( integeredad );
				
		         
        ActionListener listener = new PersonaActionListener();
        btnAdd.addActionListener(listener);
        btnDelete.addActionListener(listener);
        btnNew.addActionListener(listener);
    }
    
    private void updateTable(){
        try {
            List<Persona> list = PersistenceManager.getInstance().selectAll(Persona.class);
            arrayListModel.clear();            
            for (Persona item: list) {
                arrayListModel.add(item);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }
    
    public String getTitle(){
        return Persona.class.toString();
    }
    
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnAdd = new javax.swing.JButton();
        btnDelete = new javax.swing.JButton();
        btnNew = new javax.swing.JButton();
        panelTable = new javax.swing.JScrollPane();
        panelFields = new javax.swing.JPanel();

        btnAdd.setText("Add/Update");

        btnDelete.setText("Delete");

        btnNew.setText("New");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(panelFields, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 384, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addComponent(btnAdd)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnDelete)
                        .addGap(18, 18, 18)
                        .addComponent(btnNew))
                    .addComponent(panelTable, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 384, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panelFields, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnAdd)
                    .addComponent(btnDelete)
                    .addComponent(btnNew))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panelTable, javax.swing.GroupLayout.DEFAULT_SIZE, 305, Short.MAX_VALUE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents
    
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAdd;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnNew;
    private javax.swing.JPanel panelFields;
    private javax.swing.JScrollPane panelTable;
    // End of variables declaration//GEN-END:variables
    
    private class PersonaTableAdapter extends AbstractTableAdapter {
        public PersonaTableAdapter(ListModel listModel, String[] columnNames) {
            super(listModel, columnNames);
        }

        public Object getValueAt(int rowIndex, int columnIndex) {
            Persona display = (Persona) getRow(rowIndex);
            presentationModel.setBean(display);
            return display.get( columnIndex );
        }
    }
    
    private class PersonaActionListener implements ActionListener{        
        public void actionPerformed(ActionEvent e) {
            if( e.getSource().equals( btnAdd ) ){
                try {
                    PersistenceManager.getInstance().persist(currentPersona);
                    updateTable();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                return;
            }
            
            if( e.getSource().equals( btnDelete ) ){                
                int row = table.getSelectedRow();
                if( row > -1 ){
                    try {
                        PersonaTableAdapter model = (PanelPersona.PersonaTableAdapter) table.getModel();
                        Persona current = (Persona) model.getRow(row);
                        PersistenceManager.getInstance().delete(current);
                        updateTable();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
                return;
            }
            
            if( e.getSource().equals( btnNew ) ){
                currentPersona = new Persona();
                presentationModel.setBean(currentPersona);                
            }
        }
        
    }
}
