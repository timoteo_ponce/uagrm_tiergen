 
package datos;

/* IMPORTS */

import java.io.Serializable;
import java.util.*;
import javax.persistence.*;

/* CLASS ANNOTATIONS*/

						  @Entity 

							   
/* CLASS DECLARATION */
	
public class Item
							  extends com.jgoodies.binding.beans.Model implements Serializable{
 

		    /* ATTRIBUTES FROM ENTITY */
 							  		    @Id
								  						  									  						  						  									  						  						   					    						  @GeneratedValue(strategy=GenerationType.SEQUENCE)
								  						  								  						  				    private java.lang.Long id;
							    
		    /* ATTRIBUTES FROM ASSOCIATIONS*/

  				    @OneToMany(mappedBy="item")
				    private Set<Bid> bids = new HashSet<Bid>();		
							   							   							    
		    /* OPERATIONS */
			    //access fields
							  							      private java.lang.String title;
													  							      private java.lang.String description;
													  							      private java.util.Date postDate;
						     public Item (){
        //constructor por defecto
    }    
 							  		
				    public java.lang.Long getId () {
					    		    return id;
				    } 
		
				    public void setId(java.lang.Long id ) {
					    		    java.lang.Long oldValue = this.id;
					    		    this.id = id;
					    		    firePropertyChange("id", oldValue, this.id );
				    }
							    	
			  public java.lang.String getTitle () {
      return title;
  } 

  public void setTitle(java.lang.String title ) {
      java.lang.String oldValue = this.title;
      this.title = title;
      firePropertyChange("title", oldValue, this.title );
  }
			  public java.lang.String getDescription () {
      return description;
  } 

  public void setDescription(java.lang.String description ) {
      java.lang.String oldValue = this.description;
      this.description = description;
      firePropertyChange("description", oldValue, this.description );
  }
			  public java.util.Date getPostDate () {
      return postDate;
  } 

  public void setPostDate(java.util.Date postDate ) {
      java.util.Date oldValue = this.postDate;
      this.postDate = postDate;
      firePropertyChange("postDate", oldValue, this.postDate );
  }
 
		    /* OPERATIONS FROM ASSOCIATIONS*/

							   							  				    public Set<Bid> getBids (){
					    		    return bids;
				    }
							   							   							    
			    public Object get(int index ){
				    		    switch(index){
							    		    		    case 0:
						    		    		    		    return id;
						  					 							    		    		    case 1:
						  						    		    		    return title;
						  							    		    		    case 2:
						  						    		    		    return description;
						  							    		    		    case 3:
						  						    		    		    return postDate;
						  		 				    		    }
				    		    return "";
			    }	
}