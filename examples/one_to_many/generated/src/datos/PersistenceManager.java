package datos;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

/* Esta clase es generica para las operaciones CRUD dentro de una aplicacion
 * 
 */
public class PersistenceManager {

	private static PersistenceManager instance;

	private EntityManagerFactory managerFactory;
	private EntityManager entityManager;

	private PersistenceManager(){
		try{
			managerFactory = Persistence.createEntityManagerFactory( "defaultPU");
			entityManager = managerFactory.createEntityManager();
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public static PersistenceManager getInstance(){
		if( instance == null )
			instance = new PersistenceManager();
		return instance;
	}

	public Object persist(Object in) throws Exception{        
            EntityTransaction et = entityManager.getTransaction();
            et.begin();
            entityManager.persist(in);
            et.commit();

            return in;
    }
    
    public Object delete(Object in) throws Exception{    
            EntityTransaction et = entityManager.getTransaction();
            et.begin();
            entityManager.remove(in);
            et.commit();
            return in;    
    }
    
    public Object find(Class clase, Object in) throws Exception{    
            EntityTransaction et = entityManager.getTransaction();
            et.begin();
            Object out = entityManager.find(clase, in);
            et.commit();

            return out;
    }
    
    public List select(String in) throws Exception{
        List result = entityManager.createQuery(in).getResultList();
        return result;
    }
    
    public List selectAll(Class clase) throws Exception{
        List result = entityManager.createQuery( "SELECT e FROM " + clase.getSimpleName() + " e"
                ).getResultList();
        return result;
    }
}