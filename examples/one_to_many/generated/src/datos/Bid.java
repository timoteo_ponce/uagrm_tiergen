 
package datos;

/* IMPORTS */

import java.io.Serializable;
import java.util.*;
import javax.persistence.*;

/* CLASS ANNOTATIONS*/

						  @Entity 

							   
/* CLASS DECLARATION */
	
public class Bid
							  extends com.jgoodies.binding.beans.Model implements Serializable{
 

		    /* ATTRIBUTES FROM ENTITY */
 							  		    @Id
								  						  									  						  						  									  						  						   					    						  @GeneratedValue(strategy=GenerationType.SEQUENCE)
								  						  								  						  				    private java.lang.Long id;
							    
		    /* ATTRIBUTES FROM ASSOCIATIONS*/

					@ManyToOne
				    private Item item ;
							   							    
		    /* OPERATIONS */
			    //access fields
							  							      private java.lang.Long amount;
													  							      private java.util.Date timestamp;
						     public Bid (){
        //constructor por defecto
    }    
 							  		
				    public java.lang.Long getId () {
					    		    return id;
				    } 
		
				    public void setId(java.lang.Long id ) {
					    		    java.lang.Long oldValue = this.id;
					    		    this.id = id;
					    		    firePropertyChange("id", oldValue, this.id );
				    }
							    	
			  public java.lang.Long getAmount () {
      return amount;
  } 

  public void setAmount(java.lang.Long amount ) {
      java.lang.Long oldValue = this.amount;
      this.amount = amount;
      firePropertyChange("amount", oldValue, this.amount );
  }
			  public java.util.Date getTimestamp () {
      return timestamp;
  } 

  public void setTimestamp(java.util.Date timestamp ) {
      java.util.Date oldValue = this.timestamp;
      this.timestamp = timestamp;
      firePropertyChange("timestamp", oldValue, this.timestamp );
  }
 
		    /* OPERATIONS FROM ASSOCIATIONS*/

							   							   							  				    public Item getItem (){
					    		    return item;
				    }

				    public void setItem ( Item item ){
					    		    this.item = item;
				    }
							   							    
			    public Object get(int index ){
				    		    switch(index){
							    		    		    case 0:
						    		    		    		    return id;
						  					 							    		    		    case 1:
						  						    		    		    return amount;
						  							    		    		    case 2:
						  						    		    		    return timestamp;
						  		 				    		    }
				    		    return "";
			    }	
}