

package presentacion;

import com.jgoodies.binding.PresentationModel;
import com.jgoodies.binding.adapter.AbstractTableAdapter;
import com.jgoodies.binding.adapter.BasicComponentFactory;
import com.jgoodies.binding.adapter.SingleListSelectionAdapter;
import com.jgoodies.binding.list.ArrayListModel;
import com.jgoodies.binding.list.SelectionInList;
import com.jgoodies.binding.value.ValueModel;
import datos.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;
import javax.swing.*;
import java.text.DecimalFormat;

/**
 *
 * @author  $autor
 */

public class PanelCategory extends javax.swing.JPanel {
    ArrayListModel<Category> arrayListModel;
    SelectionInList<Category> selection;
    PresentationModel<Category> presentationModel;
    JTable table;
    Category currentCategory;
    
    /** Creates new form PanelCategory */
    public PanelCategory() {        
        initComponents();
        initBindings();        
        updateTable();
    }
    
    private void initBindings(){
        arrayListModel = new ArrayListModel();
        
        currentCategory = new Category();        
        selection = new SelectionInList((ListModel)arrayListModel);        
		
		        table = new JTable( new CategoryTableAdapter((ListModel)selection, new String[]{  }) );
        table.setSelectionModel(new SingleListSelectionAdapter(
                selection.getSelectionIndexHolder()));        
        panelTable.setViewportView( table );
        
        presentationModel = new PresentationModel<Category>( currentCategory );
		         
        ActionListener listener = new CategoryActionListener();
        btnAdd.addActionListener(listener);
        btnDelete.addActionListener(listener);
        btnNew.addActionListener(listener);
    }
    
    private void updateTable(){
        try {
            List<Category> list = PersistenceManager.getInstance().selectAll(Category.class);
            arrayListModel.clear();            
            for (Category item: list) {
                arrayListModel.add(item);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }
    
    public String getTitle(){
        return Category.class.toString();
    }
    
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnAdd = new javax.swing.JButton();
        btnDelete = new javax.swing.JButton();
        btnNew = new javax.swing.JButton();
        panelTable = new javax.swing.JScrollPane();
        panelFields = new javax.swing.JPanel();

        btnAdd.setText("Add/Update");

        btnDelete.setText("Delete");

        btnNew.setText("New");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(panelFields, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 384, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addComponent(btnAdd)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnDelete)
                        .addGap(18, 18, 18)
                        .addComponent(btnNew))
                    .addComponent(panelTable, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 384, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panelFields, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnAdd)
                    .addComponent(btnDelete)
                    .addComponent(btnNew))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panelTable, javax.swing.GroupLayout.DEFAULT_SIZE, 305, Short.MAX_VALUE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents
    
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAdd;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnNew;
    private javax.swing.JPanel panelFields;
    private javax.swing.JScrollPane panelTable;
    // End of variables declaration//GEN-END:variables
    
    private class CategoryTableAdapter extends AbstractTableAdapter {
        public CategoryTableAdapter(ListModel listModel, String[] columnNames) {
            super(listModel, columnNames);
        }

        public Object getValueAt(int rowIndex, int columnIndex) {
            Category display = (Category) getRow(rowIndex);
            presentationModel.setBean(display);
            return display.get( columnIndex );
        }
    }
    
    private class CategoryActionListener implements ActionListener{        
        public void actionPerformed(ActionEvent e) {
            if( e.getSource().equals( btnAdd ) ){
                try {
                    PersistenceManager.getInstance().persist(currentCategory);
                    updateTable();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                return;
            }
            
            if( e.getSource().equals( btnDelete ) ){                
                int row = table.getSelectedRow();
                if( row > -1 ){
                    try {
                        CategoryTableAdapter model = (PanelCategory.CategoryTableAdapter) table.getModel();
                        Category current = (Category) model.getRow(row);
                        PersistenceManager.getInstance().delete(current);
                        updateTable();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
                return;
            }
            
            if( e.getSource().equals( btnNew ) ){
                currentCategory = new Category();
                presentationModel.setBean(currentCategory);                
            }
        }
        
    }
}
