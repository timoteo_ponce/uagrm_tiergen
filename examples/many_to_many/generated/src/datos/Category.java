 
package datos;

/* IMPORTS */

import java.io.Serializable;
import java.util.*;
import javax.persistence.*;

/* CLASS ANNOTATIONS*/

						  @Entity 

							   
/* CLASS DECLARATION */
	
public class Category
							  extends com.jgoodies.binding.beans.Model implements Serializable{
 

		    /* ATTRIBUTES FROM ENTITY */
 							  				    @Id
				    @GeneratedValue
				    @Column(name="Id_Category")
				    private Long id;
							    
		    /* ATTRIBUTES FROM ASSOCIATIONS*/

							   							   							   							  		    @ManyToMany
				    private Set<Item> item = new HashSet<Item>();
							    
		    /* OPERATIONS */
			    //access fields
     public Category (){
        //constructor por defecto
    }    
 							  				    public Long getId(){
					    		    return id;
				    }
		
				    public void setId( Long id){
					    		    this.id = id;
				    }		
							    	
 
		    /* OPERATIONS FROM ASSOCIATIONS*/

							   							   							   							  				    public Set<Item> getItem (){
					    		    return item;
				    }

				    public void setItem ( Set<Item> item ){
					    		    this.item = item;
				    }			
							  		
							    
			    public Object get(int index ){
				    		    switch(index){
				 		 				    		    }
				    		    return "";
			    }	
}