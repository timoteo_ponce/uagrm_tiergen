 
package datos;

/* IMPORTS */

import java.io.Serializable;
import java.util.*;
import javax.persistence.*;

/* CLASS ANNOTATIONS*/

						  @Entity 

							   
/* CLASS DECLARATION */
	
public class Item
							  extends com.jgoodies.binding.beans.Model implements Serializable{
 

		    /* ATTRIBUTES FROM ENTITY */
 							  				    @Id
				    @GeneratedValue
				    @Column(name="Id_Item")
				    private Long id;
							    
		    /* ATTRIBUTES FROM ASSOCIATIONS*/

							   							   							   							  		    @ManyToMany
				    private Set<Category> category = new HashSet<Category>();
							    
		    /* OPERATIONS */
			    //access fields
     public Item (){
        //constructor por defecto
    }    
 							  				    public Long getId(){
					    		    return id;
				    }
		
				    public void setId( Long id){
					    		    this.id = id;
				    }		
							    	
 
		    /* OPERATIONS FROM ASSOCIATIONS*/

							   							   							   							  				    public Set<Category> getCategory (){
					    		    return category;
				    }

				    public void setCategory ( Set<Category> category ){
					    		    this.category = category;
				    }			
							  		
							    
			    public Object get(int index ){
				    		    switch(index){
				 		 				    		    }
				    		    return "";
			    }	
}