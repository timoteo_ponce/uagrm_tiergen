package test.controlador;

import java.beans.PropertyChangeEvent;

import modelo.Proyecto;
import modelo.repositorio.RepositorioImpl;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import vista.Vista;
import controlador.ControllerProyecto;

@Test(groups = { "controller" }, enabled = true)
public class ControllerProyectoTest {

	Proyecto project ;	
	ControllerProyecto controllerProyecto = ControllerProyecto.newInstance();

	Vista vista = new Vista(){

		@Override
		public String getTitulo() {
			return null;
		}

		@Override
		public void modelPropertyChange(final PropertyChangeEvent evt) {
			System.out.println( "Succesful : " + evt.getNewValue() );			
		}

		@Override
		public void setModelo(final Object modelo) {			
		}

		@Override
		public void updateVista() {		
		}

	};

	@BeforeClass
	public void create(){
		RepositorioImpl.getInstance().start();
		project = new Proyecto(RepositorioImpl.getInstance(), null);

		controllerProyecto.addModel( project );
		controllerProyecto.addView( vista );
	}	


	@Test
	public void changeModeloTest() {
		controllerProyecto.changeModelo( this );
	}

	@Test
	public void changeUmlPackageTest() {
		controllerProyecto.changeUmlPackage(this,this);
	}

	@Test
	public void changeNombreTest() {
		controllerProyecto.changeNombre("newName",this);
	}

	@Test
	public void changeAutorTest() {
		controllerProyecto.changeAutor("newAuthor",this);
	}

	@Test
	public void changePathTest() {
		controllerProyecto.changePath( "newPath" ,this);
	}

	@Test
	public void changeGenPathTest() {		
		controllerProyecto.changeGenPath( "newGenPath" ,this);
	}

	@Test
	public void changeTemplateTest() {		
		controllerProyecto.changeTemplate("newTemplate",this);
	}

	@Test
	public void changeDbmsTest() {		
		controllerProyecto.changeDbms("newDbms",this);
	}

	@Test
	public void changeDbNameTest() {
		controllerProyecto.changeDbName("newDbName",this);
	}

	@Test
	public void changeModificadoTest() {
		controllerProyecto.changeModificado(true,this);
	}
}
