package test.modelo;

import java.io.File;
import java.io.IOException;

import modelo.Proyecto;
import modelo.repositorio.Repositorio;
import modelo.repositorio.RepositorioImpl;
import modelo.types.State;

import org.omg.uml.foundation.core.UmlClass;
import org.omg.uml.modelmanagement.Model;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

@Test(groups = { "model" }, enabled = true,suiteName="modelo")
public class ProyectoTest {

	final Repositorio repository = RepositorioImpl.getInstance();
	final Proyecto project = new Proyecto(repository, null);

	@BeforeClass
	public void create() throws Exception {
		repository.start();
		project.init();
		project.newProyecto();
	}

	@Test(dependsOnMethods={"testSaveProperties"})
	public void testLoadProperties() throws IOException {	
		project.openProyecto( new File( "./test.project" ) );
		assert ( project.getAutor().equals( "Test NG") );
		assert ( project.getNombre().equals( "Test Project") );
		assert ( project.getVersion() == 0 );
	}

	@Test
	public void testSaveProperties() throws Exception {
		project.newProyecto();
		project.setAutor("Test NG");
		project.setNombre("Test Project");
		project.setVersion(0);		
		project.setPath( "./test.project" );
		project.saveProyecto();
	}

	@Test
	public void testNew() throws Exception {
		final Model model = project.getModelo();
		project.newProyecto();
		assert ( !project.getModelo().equals( model ) );
	}

	@Test
	public void testSave() throws IOException {
		String filePath = "./";
		project.setPath(filePath);
		project.saveProyecto();
		assert ( new File( filePath).exists() );
	}

	@Test(dependsOnMethods={"testSave"})
	public void testOpen() throws IOException {		
		project.openProyecto( new File("./") );
	}

	@Test
	public void testClose() throws Exception {
		project.closeProyecto();
		project.newProyecto();
	}

	@Test
	public void testExport() throws Exception {
		project.newProyecto();
		project.exportar( new File( "C:/export.xmi") );
	}

	@Test(dependsOnMethods={"testExport"})
	public void testImport() throws Exception {
		project.newProyecto();
		project.importar( new File( "C:/export.xm" ) );		
	}

	@Test
	public void testModified() {
		UmlClass class1 = repository.getUmlPackage().getCore().getUmlClass().createUmlClass();
		class1.setNamespace(project.getModelo());
		State state = project.getCurrentState();

		assert ((state == State.NEW|| state == State.MODIFIED)) : "This must be true";
	}

}
