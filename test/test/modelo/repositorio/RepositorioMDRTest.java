package test.modelo.repositorio;

import modelo.repositorio.Repositorio;
import modelo.repositorio.RepositorioImpl;

import org.omg.uml.foundation.core.UmlClass;
import org.omg.uml.modelmanagement.Model;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

@Test(groups={"model"},enabled=true,suiteName="modelo")
public class RepositorioMDRTest {
	
	Repositorio repository = RepositorioImpl.getInstance();
	
	@BeforeClass
	public void create(){
		repository.start();
	}
	
	@Test
	public void testReset(){		 
		Model first = repository.getUmlPackage().getModelManagement().getModel().createModel();		
		repository.reset();
		Model second = repository.getUmlPackage().getModelManagement().getModel().createModel();		
		
		assert !first.equals( second ) : "Models must be different";
	}
	
	@Test
	public void testContainer(){
		Model model = repository.getUmlPackage().getModelManagement().getModel().createModel();
		UmlClass class1 = repository.getUmlPackage().getCore().getUmlClass().createUmlClass();
		class1.setNamespace( model );
		int sizeBefore = model.getOwnedElement().size();
		
		UmlClass class2 = repository.getUmlPackage().getCore().getUmlClass().createUmlClass();
		class2.setNamespace( model );
		int sizeAfter = model.getOwnedElement().size();		
		
		assert ( sizeAfter > sizeBefore ): "Container is failing.";
		
	}
	
	@Test
	public void testTransaction(){
		String beforeTransform = "before transformation";
		Model model = repository.getUmlPackage().getModelManagement().getModel().createModel();
		model.setName(beforeTransform);
		repository.iniciarTransaccion( true );
		model.setName( "After transformation" );
		repository.finalizarTransaccion(true);
		
		assert (beforeTransform.equals( model.getName() )):"Value must not change";
	}
	
	@AfterClass
	public void destroy(){
		repository.shutdown();
	}

}
