package modelo.repositorio;

import javax.jmi.reflect.RefObject;
import javax.jmi.reflect.RefPackage;

import org.netbeans.api.xmi.XMIInputConfig;
import org.netbeans.lib.jmi.xmi.XmiContext;

/**
 * Clase que resuelve las referencias XMI, los IDs existentes son analizados, y
 * nuevos son generados de ser necesario.
 * 
 * @author timoteo
 * 
 */
public class RefResolver extends XmiContext {

	private final Repositorio repository;

	/**
	 * The Constructor.
	 * 
	 * @param extent
	 *            the extent
	 * @param config
	 *            the config
	 */
	public RefResolver(final RefPackage[] extent, final XMIInputConfig config,
			final Repositorio repository) {
		super(extent, config);
		this.repository = repository;
	}

	/**
	 * Register.
	 * 
	 * @param systemId
	 *            the system id
	 * @param xmiId
	 *            the xmi id
	 * @param object
	 *            the object
	 */
	@Override
	public void register(final String systemId, final String xmiId,
			final RefObject object) {
		readObject(object, xmiId);
		super.register(systemId, xmiId, object);

	}

	/**
	 * This method is called every time an object is read from the XMI file.
	 * This method verifies if this ID was generated by MVCASE. If this is the
	 * case, the ID that was read is stored into the IDs HashMap. Otherwise, a
	 * new one is generated.
	 * 
	 * @param object
	 *            the object that was read
	 * @param xmiId
	 *            the id that was read from the XMI.
	 * 
	 * @see #setNewUid(RefObject)
	 * @see #setUid(RefObject, String)
	 */
	private void readObject(final RefObject object, final String xmiId) {
		if (!xmiId.startsWith(RepositorioImpl.TOOL_STRING)) {
			repository.setNewUid(object);
		} else {
			repository.setUid(object, xmiId);
		}
	}
}
