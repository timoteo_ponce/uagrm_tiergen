/*
 * Repositorio.java
 *
 * Created on 27 de febrero de 2009, 23:15
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package modelo.repositorio;

import java.util.Collection;
import java.util.HashMap;

import javax.jmi.model.ModelPackage;
import javax.jmi.model.MofPackage;
import javax.jmi.reflect.RefObject;
import javax.jmi.reflect.RefPackage;

import org.netbeans.api.mdr.events.MDRPreChangeListener;
import org.omg.uml.UmlPackage;
import org.omg.uml.modelmanagement.Model;
import org.uml.diagrammanagement.Diagram;

/**
 * Base repository for models handling, here
 * the base values are defined, CASE3CAPAS is used
 * because was the project's old name :)
 * 
 * @author Juan Timoteo Ponce Ortiz
 */
public interface Repositorio {

	/**
	 * Define el nombre de la herramienta, esto es usado para crear los IDs
	 * propios de la herramienta.
	 */
	public static final String TOOL_STRING = "TIERGEN";//CASE3CAPAS
	/**
	 * Ubicacion de el fichero UML Metamodel XMI.
	 */
	public static final String metamodelXMIFile = "resources/uml.xmi";
	/**
	 * Direcorio temporal de almacenamiento,
	 * usado por MDR.
	 */
	public static final String storageDirectory = "storage/mdr";
	/**
	 * Temporary extent name, used to create a temporary extent.
	 */
	public static final String TEMP = "TempExtent";
	/**
	 * Name of the MOF instance extent, which is the metamodel, used to instantiate the UML metamodel
	 */
	public static final String MOF_INSTANCE = "MOFInstance";
	/**
	 * Name of the MOF Metamodel extent, which is the metametamodel, used to access the MOF instances.
	 */
	public static final String MOF_MM = "MOF";
	/**
	 * Name of the UML Instance extent, which is the model being edited by the tool.
	 */
	public static final String UML_INSTANCE = "UMLInstance";
	/**
	 * Name of the UML metamodel extent, which is the metamodel, used to access the UML instance.
	 */
	public static final String UML_MM = "UML";

	/**
	 * Cierra el repositorio.
	 */
	void shutdown();    

	/**
	 * Retorna el paquete UML principal.
	 * @return paquete UML principal.
	 */
	UmlPackage getUmlPackage();

	/**
	 * Retorna el paquete MOF.
	 *
	 * El paquete MOF es el que permite acceder y crear los diferentes
	 * elementos del metamodelo de UML.
	 * @return paquete MOF.
	 */
	ModelPackage getMof();

	/**
	 * Inicializa el repositorio.
	 * @param listener Un escuchador de cambios en el repositorio.
	 * Es utilizado para saber si un proyecto ha cambiado.
	 */
	//ESTE NO CREO QUE DEBA IR
	void addListener(MDRPreChangeListener listener);
	/**
	 * Reinicia el repositorio, eliminando todos sus datos.
	 */
	void reset();
	/**
	 * Initializes the repository, clearing all data on it.
	 * @throws Exception
	 */    

	/**
	 * Retorna el metamodelo UML.
	 * @return metamodelo UML.
	 */
	ModelPackage getUmlMetamodel();

	/**
	 * Retorna un paquete temporal. Uno nuevo es creado si no hay alguno.
	 * @return paquete temporal.
	 * @throws java.lang.Exception exc
	 */
	RefPackage getTempPackage() throws Exception ;

	/**
	 * Retorna el paquete UML.
	 * @return paquete UML.
	 * @throws java.lang.Exception exc
	 */
	MofPackage getMofPackage() throws Exception ;    

	/**
	 * Retorna todos los diagramas en el proyecto actual.
	 * @return coleccion de los diagramas existentes en el proyecto.
	 */
	Collection<Diagram> getAllDiagrams();

	/**
	 * Sets a new ID for the supplied object. This is important, since the original
	 * sequential ID is not suitable for version control, since two different elements
	 * in two different files could have the same ID. The generated ID is then stored
	 * into the IDs HashMap
	 * 
	 * @param ro the supplied object
	 * 
	 * @return the newly generated ID.
	 * 
	 * @see #idsMap
	 * @see #setUid(RefObject, String)
	 * @see #getUid(RefObject)
	 */
	String setNewUid(final RefObject ro);

	/**
	 * Stores the newly generated ID into a HashMap, associating it with the old ID.
	 * 
	 * @param ro the object to have its ID changed
	 * @param uid the newly generated ID, to be stored into the HashMap.
	 * 
	 * @see #getUid(RefObject)
	 * @see #idsMap
	 */
	void setUid(final RefObject ro,final String uid);

	/**
	 * Gets an ID from the HashMap, using as a key the ID of a supplied object.
	 * 
	 * @param ro the supplied object.
	 * 
	 * @return the ID that was generated for that object.
	 * 
	 * @see #setUid(RefObject, String)
	 * @see #idsMap
	 */
	String getUid(final RefObject ro);


	/**
	 * Resets the IDs HashMap, erasing all elements.
	 */
	void resetIdsMap();

	/**
	 * Gets the IDs HashMap.
	 * 
	 * @return the IDs HashMap.
	 * 
	 * @see #idsMap
	 */
	HashMap<String, String> getIdsMap();    

	/**
	 * Gets the root of the UML model. This is a start point for the project. Here is where
	 * all packages and elements will be inserted.
	 * 
	 * @return the root of the UML model.
	 */
	Model getModelRoot();    

	/**
	 * Iniciar transaccion.
	 * 
	 * @param rollback the rollback
	 */
	void iniciarTransaccion(final boolean rollback);

	/**
	 * Finalizar transaccion.
	 * 
	 * @param rollback the rollback
	 */
	void finalizarTransaccion(final boolean rollback);

	/**
	 * Start.
	 */
	void start();

	/**
	 * Checks if the repository it's clean.
	 * 
	 * @return true, if is clean
	 */
	boolean isClean();

}
