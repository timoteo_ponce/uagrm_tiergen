package modelo.repositorio;

import javax.jmi.reflect.RefObject;

import org.netbeans.api.xmi.XMIReferenceProvider;

public class RefProvider implements XMIReferenceProvider {

	private final Repositorio repository;

	public RefProvider(final Repositorio repository) {
		this.repository = repository;
	}

	/**
	 * Gets the reference.
	 * 
	 * @param obj
	 *            the obj
	 * 
	 * @return the reference
	 */
	public XMIReferenceProvider.XMIReference getReference(final RefObject obj) {
		String uid = repository.getUid(obj);
		if (uid == null) {
			uid = repository.setNewUid(obj);
		}
		return new XMIReferenceProvider.XMIReference(null, uid);
	}

}
