/*
 * Repositorio.java
 *
 * Created on 16 de septiembre de 2008, 13:59
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package modelo.repositorio;

import java.io.FileNotFoundException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import javax.jmi.model.ModelPackage;
import javax.jmi.model.MofPackage;
import javax.jmi.reflect.RefObject;
import javax.jmi.reflect.RefPackage;

import org.apache.log4j.Logger;
import org.netbeans.api.mdr.MDRManager;
import org.netbeans.api.mdr.MDRepository;
import org.netbeans.api.mdr.events.MDRChangeEvent;
import org.netbeans.api.mdr.events.MDRPreChangeListener;
import org.netbeans.api.mdr.events.VetoChangeException;
import org.netbeans.api.xmi.XMIReader;
import org.netbeans.api.xmi.XMIReaderFactory;
import org.omg.uml.UmlPackage;
import org.omg.uml.modelmanagement.Model;
import org.uml.diagrammanagement.Diagram;

/**
 * Clase utilidad para el acceso al modulo MDR en Netbeans. Provee
 * funcionalidades para iniciar el repositorio, importar y exportar a xmi,
 * ademas de otras operaciones básicas
 * 
 * @author Daniel
 */
public final class RepositorioImpl implements Repositorio {
	private static final Logger LOG = Logger.getLogger(RepositorioImpl.class);
	/**
	 * Repositorio de metadatos.
	 */
	private MDRepository repository;
	/**
	 * Basic UML package
	 */
	private UmlPackage uml;

	/**
	 * Basic MOF package.
	 */
	private ModelPackage mof;

	/**
	 * Object used to manage the Meta Data repository.
	 */
	private MDRManager mdrManager;

	/**
	 * HashMap that holds the new IDs for the objects, associated to the old
	 * ones as the key for the HashMap.
	 * 
	 * @see #setUid(RefObject, String)
	 * @see #getUid(RefObject)
	 */
	private HashMap<String, String> idsMap;

	private boolean clean;

	// TODO remove singleton instances
	private static final Repositorio INSTANCE = new RepositorioImpl();

	public static Repositorio getInstance() {
		return INSTANCE;
	}

	/**
	 * Instantiates a new repositorio impl.
	 */
	public RepositorioImpl() {

	}

	/**
	 * Cierra el repositorio.
	 */
	public void shutdown() {
		if (mdrManager != null) {
			LOG.info("Shutting down MDRepository");
			mdrManager.shutdownAll();
			LOG.info("MDRepository stopped");
		}
	}

	/**
	 * Retorna el repositorio.
	 * 
	 * @return repositorio de datos.
	 */
	public MDRepository getRepository() {
		return repository;
	}

	/**
	 * Retorna el paquete UML principal.
	 * 
	 * @return paquete UML principal.
	 */
	public UmlPackage getUmlPackage() {
		return uml;
	}

	/**
	 * Retorna el paquete MOF.
	 * 
	 * El paquete MOF es el que permite acceder y crear los diferentes elementos
	 * del metamodelo de UML.
	 * 
	 * @return paquete MOF.
	 */
	public ModelPackage getMof() {
		return mof;
	}

	/**
	 * Inicializa el repositorio.
	 * 
	 * @param listener
	 *            Un escuchador de cambios en el repositorio. Es utilizado para
	 *            saber si un proyecto ha cambiado.
	 */
	public void startRepository(final MDRPreChangeListener listener) {
		LOG.info("Starting MDRepository");

		System.setProperty(
				"org.netbeans.mdr.storagemodel.StorageFactoryClassName",
		"org.netbeans.mdr.persistence.memoryimpl.StorageFactoryImpl");
		mdrManager = MDRManager.getDefault();

		repository = mdrManager.getDefaultRepository();

		try {
			init();
		} catch (Exception e) {
			LOG.error("Error inicializando el repositorio", e);
		}

		repository.addListener(listener);
		LOG.info("RepositorioMDR iniciado");
		repository.addListener(new MDRPreChangeListener() {

			@Override
			public void changeCancelled(final MDRChangeEvent arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void plannedChange(final MDRChangeEvent arg0)
			throws VetoChangeException {
				// TODO Auto-generated method stub

			}

			@Override
			public void change(final MDRChangeEvent arg0) {
				if (isClean()) {
					setClean(false);
				}
			}

		});
	}

	/**
	 * Reinicia el repositorio, eliminando todos sus datos.
	 */
	public void reset() {
		try {
			if (!isClean()) {
				resetIdsMap();
				LOG.info("Reseteando repositorio");
				init();
				LOG.info("Repositorio reseteado");
				setClean(true);
			}
		} catch (Exception e) {
			LOG.error("Error reseteando repositorio", e);
		}
	}

	/**
	 * Initializes the repository, clearing all data on it.
	 * 
	 * @throws Exception
	 *             the exception
	 */
	private void init() throws Exception {
		try {
			clearAllExtents();
		} catch (FileNotFoundException fnfe) {
			LOG
			.info("Storage file did not exist. A new one was created!",
					fnfe);
		}
		mof = (ModelPackage) repository.getExtent(MOF_INSTANCE);
		uml = (UmlPackage) repository.getExtent(UML_INSTANCE);
		if (mof == null) {
			mof = (ModelPackage) repository.createExtent(MOF_INSTANCE);
		}
		if (uml == null) {
			uml = (UmlPackage) repository.createExtent(UML_INSTANCE,
					getMofPackage());
		}
	}

	/**
	 * Clears all extents from the repository.
	 * 
	 * @throws Exception
	 *             the exception
	 */
	private void clearAllExtents() throws Exception {
		LOG.info("Limpiando extensiones");
		final String[] extents = repository.getExtentNames();

		for (int i = 0; i < extents.length; i++) {
			final String extent = extents[i];
			if (extent.equals(UML_INSTANCE)) {
				RefPackage pack = repository.getExtent(extent);
				pack.refDelete();
			}
		}
	}

	/**
	 * Retorna el metamodelo UML.
	 * 
	 * @return metamodelo UML.
	 */
	public ModelPackage getUmlMetamodel() {
		try {
			ModelPackage umlMM = (ModelPackage) repository.getExtent(UML_MM);
			if (umlMM == null) {
				umlMM = (ModelPackage) repository.createExtent(UML_MM);
			}
			return umlMM;
		} catch (Exception e) {
			LOG.info("Error obteniendo UmlMetamodel", e);
			return null;
		}
	}

	/**
	 * Retorna un paquete temporal. Uno nuevo es creado si no hay alguno.
	 * 
	 * @return paquete temporal.
	 * 
	 * @throws java.lang.Exception
	 *             exc
	 * @throws Exception
	 *             the exception
	 */
	public RefPackage getTempPackage() throws Exception {
		RefPackage temp = repository.getExtent(TEMP);
		if (temp == null) {
			temp = repository.createExtent(TEMP, getMofPackage());
		}
		return temp;
	}

	/**
	 * Retorna el paquete UML.
	 * 
	 * @return paquete UML.
	 * 
	 * @throws java.lang.Exception
	 *             exc
	 * @throws Exception
	 *             the exception
	 */
	public MofPackage getMofPackage() throws Exception {
		ModelPackage umlMM = (ModelPackage) repository.getExtent(UML_MM);
		if (umlMM == null) {
			umlMM = (ModelPackage) repository.createExtent(UML_MM);
		}
		MofPackage result = getUmlPackage(umlMM);
		if (result == null) {
			XMIReader reader = XMIReaderFactory.getDefault().createXMIReader();
			reader.read(UmlPackage.class.getResource(metamodelXMIFile)
					.toString(), umlMM);
		}
		result = getUmlPackage(umlMM);
		return result;
	}

	/**
	 * Gets the UML package, from the specified metamodel.
	 * 
	 * @param umlMM
	 *            the metamodel to get the UML package.
	 * 
	 * @return the UML package for the supplied metamodel
	 */
	private MofPackage getUmlPackage(final ModelPackage umlMM) {

		for (Iterator<MofPackage> it = umlMM.getMofPackage().refAllOfClass()
				.iterator(); it.hasNext();) {
			final MofPackage pkg = it.next();
			if (pkg.getContainer() == null && "UML".equals(pkg.getName())) {
				return pkg;
			}
		}
		return null;
	}

	/**
	 * Retorna todos los diagramas en el proyecto actual.
	 * 
	 * @return coleccion de los diagramas existentes en el proyecto.
	 */
	@SuppressWarnings("unchecked")
	public Collection<Diagram> getAllDiagrams() {
		final Collection<Diagram> diagrams = uml.getDiagramManagement()
		.getDiagram().refAllOfClass();
		return diagrams;
	}

	/**
	 * Sets a new ID for the supplied object. This is important, since the
	 * original sequential ID is not suitable for version control, since two
	 * different elements in two different files could have the same ID. The
	 * generated ID is then stored into the IDs HashMap
	 * 
	 * @param ro
	 *            the supplied object
	 * 
	 * @return the newly generated ID.
	 * 
	 * @see #idsMap
	 * @see #setUid(RefObject, String)
	 * @see #getUid(RefObject)
	 */
	public String setNewUid(final RefObject ro) {
		String localHost = "localhost";
		try {
			localHost = InetAddress.getLocalHost().getHostAddress();
		} catch (UnknownHostException e) {
			LOG.error(e, e);
		}
		final String now = Long.toHexString(new Date().getTime());
		final String random = Long
		.toHexString((long) (Math.random() * 1000000));
		final String uid = TOOL_STRING + ":" + localHost + ":" + now + random;
		setUid(ro, uid);
		return uid;
	}

	/**
	 * Stores the newly generated ID into a HashMap, associating it with the old
	 * ID.
	 * 
	 * @param ro
	 *            the object to have its ID changed
	 * @param uid
	 *            the newly generated ID, to be stored into the HashMap.
	 * 
	 * @see #getUid(RefObject)
	 * @see #idsMap
	 */
	public void setUid(final RefObject ro, final String uid) {
		getIdsMap().put(ro.refMofId(), uid);
	}

	/**
	 * Gets an ID from the HashMap, using as a key the ID of a supplied object.
	 * 
	 * @param ro
	 *            the supplied object.
	 * 
	 * @return the ID that was generated for that object.
	 * 
	 * @see #setUid(RefObject, String)
	 * @see #idsMap
	 */
	public String getUid(final RefObject ro) {
		return getIdsMap().get(ro.refMofId());
	}

	/**
	 * Resets the IDs HashMap, erasing all elements.
	 */
	public void resetIdsMap() {
		idsMap = null;
	}

	/**
	 * Gets the IDs HashMap.
	 * 
	 * @return the IDs HashMap.
	 * 
	 * @see #idsMap
	 */
	public HashMap<String, String> getIdsMap() {
		if (idsMap == null) {
			idsMap = new HashMap<String, String>();
		}
		return idsMap;
	}

	/**
	 * This method is called every time an object is read from the XMI file.
	 * This method verifies if this ID was generated by MVCASE. If this is the
	 * case, the ID that was read is stored into the IDs HashMap. Otherwise, a
	 * new one is generated.
	 * 
	 * @param object
	 *            the object that was read
	 * @param xmiId
	 *            the id that was read from the XMI.
	 * 
	 * @see #setNewUid(RefObject)
	 * @see #setUid(RefObject, String)
	 */
	private void readObject(final RefObject object, final String xmiId) {
		if (!xmiId.startsWith(TOOL_STRING)) {
			setNewUid(object);
		} else {
			setUid(object, xmiId);
		}
	}

	/**
	 * Gets the root of the UML model. This is a start point for the project.
	 * Here is where all packages and elements will be inserted.
	 * 
	 * @return the root of the UML model.
	 */
	@SuppressWarnings("unchecked")
	public Model getModelRoot() {
		final Collection<Model> c = uml.getModelManagement().getModel()
		.refAllOfClass();
		final Iterator<Model> it = c.iterator();

		if (it.hasNext()) {
			final Model m = it.next();
			return m;
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see modelo.repositorio.Repositorio#iniciarTransaccion(boolean)
	 */
	public void iniciarTransaccion(final boolean rollback) {
		getRepository().beginTrans(rollback);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see modelo.repositorio.Repositorio#finalizarTransaccion(boolean)
	 */
	public void finalizarTransaccion(final boolean rollback) {
		getRepository().endTrans(rollback);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see modelo.repositorio.Repositorio#start()
	 */
	public void start() {
		startRepository(null);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * modelo.repositorio.Repositorio#addListener(org.netbeans.api.mdr.events
	 * .MDRPreChangeListener)
	 */
	public void addListener(final MDRPreChangeListener listener) {
		getRepository().addListener(listener);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see modelo.repositorio.Repositorio#isClean()
	 */
	@Override
	public boolean isClean() {
		return clean;
	}

	/**
	 * Sets the flag value to clean.
	 * 
	 * @param clean
	 *            the new clean
	 */
	public void setClean(final boolean clean) {
		this.clean = clean;
	}

}
