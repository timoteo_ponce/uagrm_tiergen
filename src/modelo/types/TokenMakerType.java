package modelo.types;

import com.sun.jedit.syntax.BatchFileTokenMarker;
import com.sun.jedit.syntax.CCTokenMarker;
import com.sun.jedit.syntax.CTokenMarker;
import com.sun.jedit.syntax.EiffelTokenMarker;
import com.sun.jedit.syntax.HTMLTokenMarker;
import com.sun.jedit.syntax.IDLTokenMarker;
import com.sun.jedit.syntax.JavaScriptTokenMarker;
import com.sun.jedit.syntax.JavaTokenMarker;
import com.sun.jedit.syntax.MakefileTokenMarker;
import com.sun.jedit.syntax.PHPTokenMarker;
import com.sun.jedit.syntax.PatchTokenMarker;
import com.sun.jedit.syntax.PerlTokenMarker;
import com.sun.jedit.syntax.PropsTokenMarker;
import com.sun.jedit.syntax.PythonTokenMarker;
import com.sun.jedit.syntax.SQLTokenMarker;
import com.sun.jedit.syntax.ShellScriptTokenMarker;
import com.sun.jedit.syntax.TSQLTokenMarker;
import com.sun.jedit.syntax.TeXTokenMarker;
import com.sun.jedit.syntax.XMLTokenMarker;

/**
 * All enumerations are used to store metadata about an model object.
 * The Enum TokenMakerType.
 */
public enum TokenMakerType {
	JAVA(JavaTokenMarker.class.getName() , ".java"),
	BATCH(BatchFileTokenMarker.class.getName() , ".bat"),
	CC(CCTokenMarker.class.getName() , ".cc"),
	C(CTokenMarker.class.getName() , ".c,.h"),
	EIFFEL(EiffelTokenMarker.class.getName() , ".efl"),
	HTML(HTMLTokenMarker.class.getName() , ".html"),
	IDL(IDLTokenMarker.class.getName() , ".idl"),
	JAVASCRIPT(JavaScriptTokenMarker.class.getName() , ".js"),
	MAKEFILE(MakefileTokenMarker.class.getName() , ".make"),
	PATCH(PatchTokenMarker.class.getName() , ".patch"),
	PERL(PerlTokenMarker.class.getName() , ".pl"),
	PHP(PHPTokenMarker.class.getName() , ".php" ),
	PROPS(PropsTokenMarker.class.getName() , ".properties"),
	PYTHON(PythonTokenMarker.class.getName() , ".py"),
	SHELL(ShellScriptTokenMarker.class.getName() , ".sh" ),
	SQL(SQLTokenMarker.class.getName() , ".sql"),
	TEX(TeXTokenMarker.class.getName() , ".tex"),
	TSQL(TSQLTokenMarker.class.getName() , ".tsql"),
	XML(XMLTokenMarker.class.getName() , ".xml");
	
	private String key;
	private String extension;
	
	TokenMakerType( String  key , String extension){
		this.key = key;
		this.extension = extension;
	}
	
	@Override
	public String toString() {			
		return key;
	}

	public String getExtension() {
		return extension;
	}

	public void setExtension(String extension) {
		this.extension = extension;
	}
	
	

}
