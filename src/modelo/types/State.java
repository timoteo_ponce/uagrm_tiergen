package modelo.types;

public enum State {
	CLOSED,
	NEW,
	SAVED,
	OPENED,
	MODIFIED;	
}
