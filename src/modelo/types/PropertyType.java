package modelo.types;

/**
 * All enumerations are used to store metadata about an model object.
 * The Enum PropertyType.
 */
public enum PropertyType {
	MODELO_PRINCIPAL("ModeloPrincipal"),
	UML_PACKAGE("UmlPackage"), 
	NOMBRE("Nombre"), 
	AUTOR("Autor"), 
	PATH("Path"), 
	GENPATH("GenPath"),
	TEMPLATE("Template"), 
	DBMS("Dbms"), 
	DB_NAME("DbName"), 
	PROJECT_MODIFIED("ProjectModified"),
	MODEL_MODIFIED("ModelModified"),
	VERSION("Version"),
	GENERATE_BUSSINESS("GenerateBussiness"),
	GENERATE_DATA("GenerateData"),
	GENERATE_PRESENTACION("GeneratePresentacion");

	private String name;

	PropertyType(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return name;
	}

}
