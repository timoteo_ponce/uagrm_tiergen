package modelo.types;

/**
 * All enumerations are used to store metadata about an model object.
 * 
 * The Enum Field, used to set properties for fields of an entity.
 */
public enum Field {
	NAME,TYPE,
    ID,NULLABLE,GENERATED_VALUE,EMBEDDED,COLUMN_NAME,TEMPORAL_TYPE
}
