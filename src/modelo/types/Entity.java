package modelo.types;

/**
 * All enumerations are used to store metadata about an model object.
 * The Enum Entity to define proeprties for an entity.
 */
public enum Entity {
	ENTITY,EMBEDDABLE,TABLE_NAME
}
