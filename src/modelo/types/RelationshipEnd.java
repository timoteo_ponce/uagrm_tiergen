package modelo.types;

/**
 * All enumerations are used to store metadata about an model object.
 * The Enum RelationshipEnd.
 */
public enum RelationshipEnd {
	CHILD,PARENT,END_ONE,END_TWO,AGGREGATION,COMPOSITION,NAVIGABLE,SOURCE,DESTINY
}
