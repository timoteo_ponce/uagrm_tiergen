package modelo.types;

/**
 * All enumerations are used to store metadata about an model object.
 * The Enum Relationships.
 */
public enum Relationships {
	INHERITANCE,
	ASSOCIATION,
	DEPENDENCY,
	ABSTRACTION,
	INHERITANCE_TYPE,
	ONE_TO_ONE,
	ONE_TO_MANY,
	MANY_TO_ONE,
	MANY_TO_MANY,
	CASCADE_TYPE,
	FETCH_TYPE,
	TYPE
}
