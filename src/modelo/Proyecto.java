/*
 * 
 */
package modelo;

import java.io.File;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Properties;

import modelo.repositorio.Repositorio;
import modelo.types.PropertyType;
import modelo.types.State;

import org.apache.log4j.Logger;
import org.netbeans.api.mdr.events.MDRChangeEvent;
import org.netbeans.api.mdr.events.MDRPreChangeListener;
import org.omg.uml.UmlPackage;
import org.omg.uml.foundation.core.DataType;
import org.omg.uml.modelmanagement.Model;
import org.uml.diagrammanagement.Diagram;
import org.uml.diagrammanagement.Property;
import org.uml.diagrammanagement.Uml1SemanticModelBridge;

import util.Configuracion;
import util.Constantes;
import util.uml.UMLUtil;
import controlador.persistencia.MotorPersistencia;

/**
 * Esta clase mantiene todos los datos referentes a un proyecto de disenho,
 * conteniendo toda la informacion necesaria para esto
 * 
 * @author Juan Timoteo Ponce Ortiz
 * @version 0.1
 */
public final class Proyecto extends Modelo {

	private static Logger log = Logger.getLogger(Proyecto.class);
	public static final String NOMBRE_GENERICO = "Proyecto generico";

	private Model modeloPrincipal;
	private UmlPackage umlPackage;
	private final Properties properties = new Properties();
	private final Repositorio repositorio;
	private final MotorPersistencia persistenceManager;
	private State currentState;

	/**
	 * Constructor por defecto
	 */
	public Proyecto(final Repositorio repositorio,
			final MotorPersistencia persistenceManager) {
		if (repositorio == null) {
			throw new NullPointerException("Repository can't be null");
		}
		this.persistenceManager = persistenceManager;
		this.repositorio = repositorio;
		this.repositorio.addListener(new MDRPreChangeListener() {
			public void plannedChange(final MDRChangeEvent mDRChangeEvent) {
				// setModelModified(true);
			}

			public void changeCancelled(final MDRChangeEvent mDRChangeEvent) {
				// setModificado( false );
			}

			public void change(final MDRChangeEvent mDRChangeEvent) {
				// setModelModified(true);
			}
		});
		currentState = State.CLOSED;
	}

	/**
	 * Asigna a todos los atributos un estado inicial vacio.
	 */
	public void init() {
		log.info("Loading values by default");
		setGenerateData(true);
		setGenerateBussiness(true);
		setGeneratePresentation(true);
		setModelModified(false);
		setPath("");
		setAutor("");
		setGenPath("");
		setTemplate("");
		setDbms("");
		setDbName("");
		umlPackage = repositorio.getUmlPackage();
		setNombre(NOMBRE_GENERICO);

		modeloPrincipal = UMLUtil.crearModelo(getNombre());

		org.omg.uml.modelmanagement.UmlPackage paquete = UMLUtil
		.crearPaquete("Disenho de datos");
		paquete.setNamespace(modeloPrincipal);

		final Diagram diagrama = UMLUtil.crearDiagrama("Diagrama de Clases");
		final Uml1SemanticModelBridge puenteDiagrama = UMLUtil
		.crearPuenteSemantico(paquete);
		diagrama.setOwner(puenteDiagrama);
		final Property diagramaProp = UMLUtil.crearPropiedad(
				Constantes.MODEL_DIAGRAM_TYPE, Constantes.MODEL_CLASS_DIAGRAM);
		diagrama.getProperty().add(diagramaProp);

		addDefaultTypes();
	}

	/**
	 * Retorna el modelo principal que contiene todos los elementos del
	 * proyecto.
	 * 
	 * @return Modelo del proyecto
	 */
	public Model getModelo() {
		return modeloPrincipal;
	}

	/**
	 * {@link Repositorio }.
	 * 
	 * @return paquete uml principal
	 */
	public UmlPackage getUmlPackage() {
		return umlPackage;
	}

	/**
	 * Asigna el modelo principal.
	 * 
	 * @param modeloPrincipal
	 *            modelo nuevo
	 * @param source
	 *            the source
	 */
	public void setModeloPrincipal(final Model modeloPrincipal,
			final Object source) {
		setModeloPrincipal(modeloPrincipal);
		firePropertyChange(PropertyType.MODELO_PRINCIPAL.toString(), source,
				modeloPrincipal);
	}

	/**
	 * Sets the modelo principal.
	 * 
	 * @param modeloPrincipal
	 *            the new modelo principal
	 */
	public void setModeloPrincipal(final Model modeloPrincipal) {
		this.modeloPrincipal = modeloPrincipal;
		setCurrentState(State.MODIFIED);
	}

	/**
	 * Sets the uml.
	 * 
	 * @param umlPackage
	 *            the uml package
	 * @param source
	 *            the source
	 */
	public void setUml(final UmlPackage umlPackage, final Object source) {
		setUml(umlPackage);
		firePropertyChange(PropertyType.UML_PACKAGE.toString(), source,
				umlPackage);
	}

	/**
	 * Sets the uml.
	 * 
	 * @param umlPackage
	 *            the new uml
	 */
	public void setUml(final UmlPackage umlPackage) {
		this.umlPackage = umlPackage;
		setCurrentState(State.MODIFIED);
	}

	/**
	 * Gets the autor.
	 * 
	 * @return the autor
	 */
	public String getAutor() {
		return properties.getProperty(PropertyType.AUTOR.toString());
	}

	/**
	 * Sets the autor.
	 * 
	 * @param autor
	 *            the autor
	 * @param source
	 *            the source
	 */
	public void setAutor(final String autor, final Object source) {
		setAutor(autor);
		firePropertyChange(PropertyType.AUTOR.toString(), source, autor);
	}

	/**
	 * Sets the autor.
	 * 
	 * @param autor
	 *            the new autor
	 */
	public void setAutor(final String autor) {
		properties.setProperty(PropertyType.AUTOR.toString(), autor);
		setCurrentState(State.MODIFIED);
	}

	/**
	 * Retorna el nombre del proyecto.
	 * 
	 * @return nombre proyecto
	 */
	public String getNombre() {
		return properties.getProperty(PropertyType.NOMBRE.toString());
	}

	/**
	 * Sets the nombre.
	 * 
	 * @param nombre
	 *            the nombre
	 * @param source
	 *            the source
	 */
	public void setNombre(final String nombre, final Object source) {
		setNombre(nombre);
		firePropertyChange(PropertyType.NOMBRE.toString(), source, nombre);

	}

	/**
	 * Asigna nombre al proyecto, al realizar esta operacion, el contenido del
	 * proyecto seria modificado.
	 * 
	 * @param nombre
	 *            nombre proyecto
	 */
	public void setNombre(final String nombre) {
		properties.setProperty(PropertyType.NOMBRE.toString(), nombre);
		if (modeloPrincipal != null) {
			modeloPrincipal.setName(nombre);
			setCurrentState(State.MODIFIED);
		}
	}

	/**
	 * Carga un proyecto existente a partir de una direccion de archivo.
	 * 
	 * @param projectFile
	 *            the project file
	 * 
	 * @return Estado de la carga
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public void openProyecto(final File projectFile) throws IOException {
		if (projectFile != null) {
			if (projectFile.getPath().equals(getPath())) {
				return;
			}
			setPath(projectFile);
			loadMetadata();
			setPath(projectFile);

			repositorio.reset();
			umlPackage = repositorio.getUmlPackage();

			final Model model = persistenceManager.load(new File(getPath()
					+ "/models.xmi"));

			if (model != null) {
				setModeloPrincipal(model);
			}
			// motor.destroy();
			setCurrentState(State.OPENED);
		} else {
			log.error("Fichero invalido");
			throw new NullPointerException("Invalid file");
		}
	}

	/**
	 * Exportar.
	 * 
	 * @param exportFile
	 *            the export file
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public void exportar(final File exportFile) throws IOException {
		if (exportFile != null) {
			persistenceManager.export(exportFile, umlPackage);
		} else {
			throw new NullPointerException("Invalid export file");
		}
	}

	/**
	 * Importar.
	 * 
	 * @param importFile
	 *            the import file
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public void importar(final File importFile) throws IOException {
		if (importFile != null && importFile.exists()) {
			repositorio.reset();
			umlPackage = repositorio.getUmlPackage();

			UMLUtil.crearModelo(properties.getProperty(PropertyType.NOMBRE
					.toString()));

			final Model model = persistenceManager.importar(importFile);

			if (model != null) {
				setModeloPrincipal(model);
			}
			// motor.destroy();
			setCurrentState(State.OPENED);
		} else {
			throw new NullPointerException("Null import file");
		}
	}

	/**
	 * Almacena el proyecto actual en una direcci�n espec�fica.
	 * 
	 * @param file
	 *            archivo de salida
	 * 
	 * @return Resultado del guardado
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public void saveComo(final File file) throws IOException {
		setPath(file);
		saveProyecto();
	}

	/**
	 * Almacena un proyecto en un fichero XMI, redirecciona las operaciones en
	 * el caso de que el proyecto no se haya guardado nunca.
	 * 
	 * @return estado de almacenado
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public void saveProyecto() throws IOException {
		if (umlPackage != null) {

			if (isModified()) {

				if (!getPath().isEmpty()) {
					// creamos los directorios
					final File genDir = new File(getPath() + "/generated");
					if (!genDir.exists()) {
						if (!genDir.mkdirs()) {
							throw new IOException("Can't create folders");
						}
					}
					persistenceManager.save(
							new File(getPath() + "/models.xmi"), umlPackage);
					// motor.destroy();
					saveMetadata();
					setCurrentState(State.SAVED);
				} else {
					throw new NullPointerException("path is null");
				}
			}
		} else {
			log.error("No hay proyecto");
			throw new NullPointerException("root uml package is null");
		}
	}

	/**
	 * Genera un nuevo proyecto, preguntando si se quiere almacenar el actual.
	 * 
	 * @return estado de la operacion
	 * 
	 * @throws Exception
	 *             the exception
	 */
	public void newProyecto() throws Exception {
		closeProyecto();
		init();
		setCurrentState(State.NEW);
	}

	/**
	 * Close proyecto.
	 */
	public void closeProyecto() {
		setModeloPrincipal(null);
		setPath("");
		setModelModified(false);
		repositorio.reset();
		setCurrentState(State.CLOSED);
	}

	/**
	 * Retorna el fichero de almacenamiento.
	 * 
	 * @return fichero de almacenamiento
	 */
	public String getPath() {
		return properties.getProperty(PropertyType.PATH.toString());
	}

	/**
	 * Asigna el directorio de almacenamiento del proyecto.
	 * 
	 * @param file
	 *            fichero de almacenamiento
	 */
	public void setPath(final File file) {
		if (file != null) {
			if (file.isDirectory()) {
				properties.setProperty(PropertyType.PATH.toString(), file
						.getAbsolutePath());
			} else {
				properties.setProperty(PropertyType.PATH.toString(), file
						.getParent());
			}
			setCurrentState(State.MODIFIED);
		} else {
			throw new NullPointerException("Null folder");
		}
	}

	/**
	 * Sets the path.
	 * 
	 * @param file
	 *            the file
	 * @param source
	 *            the source
	 */
	public void setPath(final File file, final Object source) {
		setPath(file);
		firePropertyChange(PropertyType.PATH.toString(), source, getPath());
	}

	/**
	 * Sets the path.
	 * 
	 * @param filePath
	 *            the new path
	 */
	public void setPath(final String filePath) {
		properties.setProperty(PropertyType.PATH.toString(), filePath);
		setCurrentState(State.MODIFIED);
	}

	/**
	 * Sets the path.
	 * 
	 * @param filePath
	 *            the file path
	 * @param source
	 *            the source
	 */
	public void setPath(final String filePath, final Object source) {
		setPath(filePath);
		firePropertyChange(PropertyType.PATH.toString(), source, getPath());
	}

	/**
	 * Sets the model modified.
	 * 
	 * @param modelModified
	 *            the new model modified
	 */
	public void setModelModified(final Boolean modelModified) {
		setCurrentState(State.MODIFIED);
	}

	/**
	 * Sets the model modified.
	 * 
	 * @param modelModified
	 *            the model modified
	 * @param source
	 *            the source
	 */
	public void setModelModified(final Boolean modelModified,
			final Object source) {
		setModelModified(modelModified);
		firePropertyChange(PropertyType.MODEL_MODIFIED.toString(), source,
				modelModified);
	}

	/**
	 * Gets the version.
	 * 
	 * @return the version
	 */
	public int getVersion() {
		return (Integer.parseInt(properties.getProperty(PropertyType.VERSION
				.toString())));
	}

	/**
	 * Sets the version.
	 * 
	 * @param version
	 *            the new version
	 */
	public void setVersion(final int version) {
		properties.setProperty(PropertyType.VERSION.toString(), version + "");
		setCurrentState(State.MODIFIED);
	}

	/**
	 * Sets the version.
	 * 
	 * @param version
	 *            the version
	 * @param source
	 *            the source
	 */
	public void setVersion(final int version, final Object source) {
		setVersion(version);
		firePropertyChange(PropertyType.VERSION.toString(), source, version);
	}

	/**
	 * Adds the default types.
	 */
	private void addDefaultTypes() {
		log.info("Adding default datatypes");
		final Properties dataTypes = new Properties();
		try {
			dataTypes.load(getClass().getResourceAsStream(
			"/dataTypes.properties"));
		} catch (IOException e) {
			log.error(e.getMessage(), e);
		}
		org.omg.uml.modelmanagement.UmlPackage paquete = UMLUtil
		.crearPaquete("Tipos de dato");
		paquete.setNamespace(modeloPrincipal);

		for (Enumeration en = dataTypes.keys(); en.hasMoreElements();) {
			final String key = en.nextElement().toString();
			final DataType type = UMLUtil.crearTipoDato();
			type.setNamespace(paquete);
			type.setName(dataTypes.getProperty(key));
			log.info("Loading dataType: " + key);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "nombre: " + getNombre() + "\npath: " + getPath();
	}

	/**
	 * Gets the gen path.
	 * 
	 * @return the gen path
	 */
	public String getGenPath() {
		return getPath() + "/generated";
	}

	/**
	 * Sets the gen path.
	 * 
	 * @param genPath
	 *            the new gen path
	 */
	public void setGenPath(final String genPath) {
		properties.setProperty(PropertyType.GENPATH.toString(), genPath);
	}

	/**
	 * Sets the gen path.
	 * 
	 * @param genPath
	 *            the gen path
	 * @param source
	 *            the source
	 */
	public void setGenPath(final String genPath, final Object source) {
		setGenPath(genPath);
		// firePropertyChange( PropertyType.GENPATH.toString(),source
		// ,getGenPath() );
	}

	/**
	 * Gets the db name.
	 * 
	 * @return the db name
	 */
	public String getDbName() {
		return properties.getProperty(PropertyType.DB_NAME.toString());
	}

	/**
	 * Sets the db name.
	 * 
	 * @param dbName
	 *            the new db name
	 */
	public void setDbName(final String dbName) {
		properties.setProperty(PropertyType.DB_NAME.toString(), dbName);
		setCurrentState(State.MODIFIED);
	}

	/**
	 * Sets the db name.
	 * 
	 * @param dbName
	 *            the db name
	 * @param source
	 *            the source
	 */
	public void setDbName(final String dbName, final Object source) {
		setDbName(dbName);
		firePropertyChange(PropertyType.DB_NAME.toString(), source, getDbName());
	}

	/**
	 * Gets the dbms.
	 * 
	 * @return the dbms
	 */
	public String getDbms() {
		return properties.getProperty(PropertyType.DBMS.toString());
	}

	/**
	 * Sets the dbms.
	 * 
	 * @param dbms
	 *            the new dbms
	 */
	public void setDbms(final String dbms) {
		properties.setProperty(PropertyType.DBMS.toString(), dbms);
		setCurrentState(State.MODIFIED);
	}

	/**
	 * Sets the dbms.
	 * 
	 * @param dbms
	 *            the dbms
	 * @param source
	 *            the source
	 */
	public void setDbms(final String dbms, final Object source) {
		setDbms(dbms);
		firePropertyChange(PropertyType.DBMS.toString(), source, getDbms());
	}

	/**
	 * Checks if is generate bussiness.
	 * 
	 * @return true, if is generate bussiness
	 */
	public boolean isGenerateBussiness() {
		return Boolean.valueOf(properties
				.getProperty(PropertyType.GENERATE_BUSSINESS.toString()));
	}

	/**
	 * Sets the generate bussiness.
	 * 
	 * @param generateBussiness
	 *            the new generate bussiness
	 */
	public void setGenerateBussiness(final boolean generateBussiness) {
		properties.setProperty(PropertyType.GENERATE_BUSSINESS.toString(),
				generateBussiness + "");
		setCurrentState(State.MODIFIED);
	}

	/**
	 * Checks if is generate data.
	 * 
	 * @return true, if is generate data
	 */
	public boolean isGenerateData() {
		return Boolean.valueOf(properties
				.getProperty(PropertyType.GENERATE_DATA.toString()));
	}

	/**
	 * Sets the generate data.
	 * 
	 * @param generateData
	 *            the new generate data
	 */
	public void setGenerateData(final boolean generateData) {
		properties.setProperty(PropertyType.GENERATE_DATA.toString(),
				generateData + "");
		setCurrentState(State.MODIFIED);
	}

	/**
	 * Checks if is generate presentation.
	 * 
	 * @return true, if is generate presentation
	 */
	public boolean isGeneratePresentation() {
		return Boolean.valueOf(properties
				.getProperty(PropertyType.GENERATE_PRESENTACION.toString()));
	}

	/**
	 * Sets the generate presentation.
	 * 
	 * @param generatePresentation
	 *            the new generate presentation
	 */
	public void setGeneratePresentation(final boolean generatePresentation) {
		properties.setProperty(PropertyType.GENERATE_PRESENTACION.toString(),
				generatePresentation + "");
		setCurrentState(State.MODIFIED);
	}

	/**
	 * Gets the template.
	 * 
	 * @return the template
	 */
	public String getTemplate() {
		return properties.getProperty(PropertyType.TEMPLATE.toString());
	}

	/**
	 * Sets the template.
	 * 
	 * @param template
	 *            the new template
	 */
	public void setTemplate(final String template) {
		properties.setProperty(PropertyType.TEMPLATE.toString(), template);
		setCurrentState(State.MODIFIED);
	}

	/**
	 * Sets the template.
	 * 
	 * @param template
	 *            the template
	 * @param source
	 *            the source
	 */
	public void setTemplate(final String template, final Object source) {
		setTemplate(template);
		// firePropertyChange(PropertyType.TEMPLATE.toString(), null,
		// getTemplate());
	}

	/**
	 * Sets the modelo.
	 * 
	 * @param sourceClass
	 *            the new modelo
	 */
	public void setModelo(final String sourceClass) {
		// firePropertyChange( PropertyType.MODELO_PRINCIPAL, null , sourceClass
		// );
	}

	/**
	 * Save metadata.
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	private void saveMetadata() throws IOException {
		log.info("Saving metadata : " + getPath());
		Configuracion.saveProperties(properties, getPath() + "/project.conf");
	}

	/**
	 * Load metadata.
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	private void loadMetadata() throws IOException {
		log.info("Loading metadata : " + getPath());
		final Properties temp = Configuracion.loadProperties(getPath()
				+ "/project.conf");
		for (Iterator it = temp.keySet().iterator(); it.hasNext();) {
			final String key = it.next().toString();
			properties.setProperty(key, temp.getProperty(key));
		}
	}

	/**
	 * Gets the properties.
	 * 
	 * @return the properties
	 */
	public Properties getProperties() {
		return properties;
	}

	public State getCurrentState() {
		return currentState;
	}

	public void setCurrentState(final State currentState) {
		this.currentState = currentState;
	}

	public final boolean isModified() {
		return (isActive() && currentState != State.SAVED);
	}

	public final boolean isActive() {
		return (currentState != State.CLOSED);
	}

	public Repositorio getRepositorio() {
		return repositorio;
	}

}
