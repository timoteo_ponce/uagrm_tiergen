/*
 * PropertyTableModel.java
 *
 * Created on 5 de marzo de 2009, 23:26
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package util;

import java.util.Hashtable;

import javax.swing.table.AbstractTableModel;

/**
 * The Class PropertyTableModel.
 * 
 * @author Juan Timoteo Ponce Ortiz
 */
public final class PropertyTableModel extends AbstractTableModel{
    
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** The Constant colNames. */
    public static final String[] colNames = new String[]{
        "Property","Value"
    };
    
    /** The properties. */
    private Hashtable<String,String> properties;
    
    /**
     * Creates a new instance of PropertyTableModel.
     * 
     * @param properties the properties
     */
    public PropertyTableModel(final Hashtable<String,String> properties) {
        setProperties( properties );
    }    
    
    
    /**
     * Instantiates a new property table model.
     */
    public PropertyTableModel() {        
    }

    /* (non-Javadoc)
     * @see javax.swing.table.TableModel#getRowCount()
     */
    public int getRowCount() {
        return properties.size();
    }

    /* (non-Javadoc)
     * @see javax.swing.table.TableModel#getColumnCount()
     */
    public int getColumnCount() {
        return colNames.length;
    }
    
    /* (non-Javadoc)
     * @see javax.swing.table.AbstractTableModel#getColumnName(int)
     */
    public String getColumnName(final int column){
        return colNames[ column ];
    }

    /* (non-Javadoc)
     * @see javax.swing.table.TableModel#getValueAt(int, int)
     */
    public Object getValueAt(final int rowIndex,final  int columnIndex) {
        if( columnIndex < getColumnCount() && rowIndex < getRowCount() ){
            
            int index = 0;            
            for( Object obj : properties.keySet() ){
            	final String key = (String)obj;
                if( index == rowIndex ){
                    if( columnIndex == 0 )
                        return key;
                    return properties.get( key );
                }
                index++;
            }
        }        
        return null;
    }

    /**
     * Gets the properties.
     * 
     * @return the properties
     */
    public Hashtable<String,String> getProperties() {
        return properties;
    }

    /**
     * Sets the properties.
     * 
     * @param properties the new properties
     */
    public void setProperties(Hashtable<String,String> properties) {
        this.properties = properties;
        fireTableDataChanged();
    }    

}
