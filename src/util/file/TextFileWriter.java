package util.file;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.StringTokenizer;

/**
 * Clase que escribe archivos de texto. Facilita la indentacion de el archivo,
 * utilizando las funciones incTab() y decTab().
 * 
 * @author JAVAHISPANO, tu comnunidad
 */
public final class TextFileWriter {

    /** The outfile. */
    private final File outfile;

    /** The outstream. */
    private final FileOutputStream outstream;

    /** The out. */
    private final PrintWriter out;

    /** The tab. */
    private int tab = 0;// cantidad de caracteres para tabular, al inicio 0

    /** The TAB. */
    private static int TAB = 4;

    /** The ref. */
    private int ref = 1;

    /**
     * Constructor por defecto.
     * 
     * @param name
     *            the name
     * @throws IOException
     */
    public TextFileWriter(String fileName) throws IOException {
	this(new File(fileName));
    }

    /**
     * Constructor por defecto.
     * 
     * @param f
     *            the f
     * @throws IOException
     */
    public TextFileWriter(File file) throws IOException {
	ref = 1;
	outfile = file;
	outfile.mkdirs();
	outfile.delete();
	outfile.createNewFile();
	outstream = new FileOutputStream(outfile);
	out = new PrintWriter(outstream);
    }

    /**
     * Gets the out.
     * 
     * @return the out
     */
    public PrintWriter getOut() {
	return out;
    }

    /**
     * Sets the tab.
     * 
     * @param p_tab
     *            the p_tab
     */
    public void setTab(int p_tab) {
	tab = p_tab;
    }

    /**
     * Gets the tab.
     * 
     * @return the tab
     */
    public int getTab() {
	return tab;
    }

    /**
     * Gets the ref.
     * 
     * @return the ref
     */
    public int getRef() {
	return ref;
    }

    /**
     * Inc ref.
     */
    public void incRef() {
	ref++;
    }

    /**
     * Gets the ref inc.
     * 
     * @return the ref inc
     */
    public int getRefInc() {
	return ref++;
    }

    /**
     * Inc tab.
     */
    public void incTab() {
	tab += TAB;
    }

    /**
     * Dec tab.
     */
    public void decTab() {
	tab -= TAB;
	if (tab < 0)
	    tab = 0;
    }

    /**
     * Write.
     * 
     * @param what
     *            the what
     */
    public void write(String what) {
	for (int i = 0; i < tab; i++)
	    out.print(" ");
	out.print(what);
	out.flush();
    }

    /**
     * Writeln.
     * 
     * @param what
     *            the what
     */
    public void writeln(String what) {
	for (int i = 0; i < tab; i++)
	    out.print(" ");
	out.println(what);
	out.flush();
    }

    /**
     * Writeln with tab between lines.
     * 
     * @param what
     *            the what
     */
    public void writelnWithTabBetweenLines(String what) {
	final StringTokenizer st = new StringTokenizer(what, "\n\r\f");
	while (st.hasMoreTokens()) {
	    final String line = st.nextToken();
	    writeln(line);
	}
    }

    /**
     * Writeln no tab.
     * 
     * @param what
     *            the what
     */
    public void writelnNoTab(String what) {
	out.println(what);
	out.flush();
    }

    /**
     * Write no tab.
     * 
     * @param what
     *            the what
     */
    public void writeNoTab(String what) {
	out.print(what);
	out.flush();
    }

    /**
     * Feedln.
     */
    public void feedln() {
	out.println();
	out.flush();
    }

    /**
     * Close.
     */
    public void close() throws IOException {
	outstream.close();
	out.close();
    }
}
