package util.file;

import java.io.File;
import java.io.FileFilter;

// TODO: Auto-generated Javadoc
/**
 * Filtro para aceptar ficheros xmi.
 * 
 * @author Daniel
 */
public final class FiltroFicheros extends javax.swing.filechooser.FileFilter implements FileFilter {
	
	/** The extension. */
	private String extension;

	/**
	 * The Constructor.
	 * 
	 * @param extension the extension
	 */
	public FiltroFicheros(String extension) {
		super();
		this.extension = extension;
	}

	/**
	 * Determina si acepta o no un fichero.
	 * 
	 * @param f Fichero a evaluar
	 * 
	 * @return Aceptado
	 */
	public boolean accept(File f) {
		if (f.isDirectory())
			return true;
		final String fname = f.getName();
		if (fname.toUpperCase().endsWith(extension.toUpperCase()) )
			return true;
		return false;
	}

	/**
	 * Descripcion del filtro.
	 * 
	 * @return the description
	 */
	public String getDescription() {
		return extension.toUpperCase() + " Fichero : *." + extension.toLowerCase();
	}

	/**
	 * Gets the extension.
	 * 
	 * @return the extension
	 */
	public String getExtension() {
		return extension;
	}

	/**
	 * Sets the extension.
	 * 
	 * @param extension the new extension
	 */
	public void setExtension(String extension) {
		this.extension = extension;
	}

}
