package util.file;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.LineNumberReader;

/**
 * Clase que lee archivos de texto(uno a la vez).
 * 
 * @author JAVAHISPANO, tu comnunidad
 */
public final class TextFileReader {

    /** The instream. */
    private final FileInputStream instream;

    /** The inreader. */
    private final InputStreamReader inreader;

    /** The in. */
    private final LineNumberReader lineReader;

    /**
     * Constructor por defecto.
     * 
     * @param f
     *            el archivo de texto a ser leido
     * @throws FileNotFoundException
     */
    public TextFileReader(File file) throws FileNotFoundException {
	instream = new FileInputStream(file);
	inreader = new InputStreamReader(instream);
	lineReader = new LineNumberReader(inreader);
    }

    /**
     * Constructor por defecto(utilizando una direccion).
     * 
     * @param name
     *            nombre del archivo a leer.
     * @throws FileNotFoundException
     */
    public TextFileReader(String fileName) throws FileNotFoundException {
	this(new File(fileName));
    }

    /**
     * Leer una linea de texto del archivo.
     * 
     * @return la linea leida, null si es el fin de archivo.
     */
    public String readln() throws IOException {
	return lineReader.readLine();
    }

    /**
     * Retorna el numero de linea actual.
     * 
     * @return numero de linea actual.
     */
    public int lineNumber() {
	return lineReader.getLineNumber();
    }

    /**
     * Cierra el fichero.
     */
    public void close() throws IOException {
	lineReader.close();
	inreader.close();
	instream.close();
    }

    /**
     * Prints the.
     * 
     * @param str
     *            the str
     */
    void print(String str) {
	System.out.println(str);
    }

    /**
     * Prints the err.
     * 
     * @param str
     *            the str
     */
    void printErr(String str) {
	System.err.println(str);
    }

    /**
     * Gets the line of.
     * 
     * @param str
     *            the str
     * 
     * @return the line of
     */
    public String getLineOf(String str) throws IOException {
	String out = lineReader.readLine();
	while (out != null && !out.toLowerCase().contains(str.toLowerCase())) {
	    out = lineReader.readLine();
	}
	return out;
    }

}
