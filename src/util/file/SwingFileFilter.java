package util.file;

import java.io.File;

/**
 * Filtro para aceptar ficheros xmi.
 * 
 * @author Daniel
 */
public final class SwingFileFilter extends javax.swing.filechooser.FileFilter {

    /** The extension. */
    private final String extension;

    /**
     * The Constructor.
     * 
     * @param extension
     *            the extension
     */
    public SwingFileFilter(String extension) {
	super();
	this.extension = extension;
    }

    /**
     * Determina si acepta o no un fichero.
     * 
     * @param f
     *            Fichero a evaluar
     * 
     * @return Aceptado
     */
    @Override
    public boolean accept(File f) {
	// System.out.println(f.getName());
	if (f.getName().isEmpty() || f.getName().trim().startsWith("."))
	    return false;
	// System.out.println(f.getName() + "????");
	if (f.isDirectory())
	    return true;
	final String fname = f.getName();
	if (extension.isEmpty()
		|| fname.toUpperCase().endsWith(extension.toUpperCase()))
	    return true;
	return false;
    }

    /**
     * Descripcion del filtro.
     * 
     * @return the description
     */
    @Override
    public String getDescription() {
	return extension.toUpperCase() + " Fichero : *."
		+ extension.toLowerCase();
    }

    /**
     * Gets the extension.
     * 
     * @return the extension
     */
    public String getExtension() {
	return extension;
    }

}
