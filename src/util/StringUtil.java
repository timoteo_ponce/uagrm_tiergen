/*
 * StringUtil.java
 *
 * Created on 28 de enero de 2009, 21:18
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package util;

// TODO: Auto-generated Javadoc
/**
 * The Class StringUtil.
 * 
 * @author Juan Timoteo Ponce Ortiz
 */
public final class StringUtil {    
    
    /**
     * First to upper.
     * 
     * @param str the str
     * 
     * @return the string
     */
    public String firstToUpper(final String str){
    	String out = str.charAt( 0 ) + "";
        out = out.toUpperCase() + str.substring( 1 , str.length() );
        return out;
    }
    
    /**
     * First to lower.
     * 
     * @param str the str
     * 
     * @return the string
     */
    public String firstToLower(final String str){
    	String out = str.charAt( 0 ) + "";
        out = out.toLowerCase() + str.substring( 1 , str.length() );
        return out;
    }
    
    /**
     * To upper.
     * 
     * @param str the str
     * 
     * @return the string
     */
    public String toUpper(final String str){        
        return str.toUpperCase();
    }
    
    /**
     * To lower.
     * 
     * @param str the str
     * 
     * @return the string
     */
    public String toLower(final String str){
    	String out = str.charAt( 0 ) + "";
        out = out.toLowerCase() + str.substring( 1 , str.length() );
        return out.toLowerCase();
    }
    
    
}
