/*
 * Constantes.java
 *
 * Created on 29 de enero de 2009, 22:45
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package util;

// TODO: Auto-generated Javadoc
/**
 * The Class Constantes.
 * 
 * @author Juan Timoteo Ponce Ortiz
 */
public final class Constantes {
    //para el metamodelo
    /** The Constant UML_STEREOTYPE. */
    public static final String UML_STEREOTYPE       =   "Stereotype";
    
    /** The Constant UML_RELATIONSHIP. */
    public static final String UML_RELATIONSHIP     =   "Relationship";
    
    /** The Constant UML_CORE. */
    public static final String UML_CORE             =   "Core";
    //para la representacion de elementos de modelo
    /** The Constant MODEL_EDGE_TYPE_ID. */
    public static final int MODEL_EDGE_TYPE_ID      =   0;
    
    /** The Constant MODEL_EDGE_TYPE. */
    public static final String MODEL_EDGE_TYPE      =   "EdgeType";
    
    /** The Constant MODEL_NODE_TYPE_ID. */
    public static final int MODEL_NODE_TYPE_ID      =   1;
    
    /** The Constant MODEL_NODE_TYPE. */
    public static final String MODEL_NODE_TYPE      =   "NodeType";
    
    /** The Constant MODEL_DIAGRAM_TYPE_ID. */
    public static final int MODEL_DIAGRAM_TYPE_ID   =   2;
    
    /** The Constant MODEL_DIAGRAM_TYPE. */
    public static final String MODEL_DIAGRAM_TYPE   =   "DiagramType";    
    
    /** The Constant MODEL_CLASS_DIAGRAM. */
    public static final String MODEL_CLASS_DIAGRAM  =   "ClassDiagram";
    
    /** The Constant MODEL_EDGE_TEXT. */
    public static final String MODEL_EDGE_TEXT      =   "EdgeText";
    //para configuracion
    //public static final String CONFIGURATION_FILE   =   "/configuration.properties";
    /** The Constant LAST_PATH. */
    public static final String LAST_PATH            =   "last_path";
    
    /** The Constant TEMPLATE_NAME. */
    public static final String TEMPLATE_NAME        =   "name";
    
    /** The Constant TEMPLATE_VERSION. */
    public static final String TEMPLATE_VERSION     =   "version";
    
    /** The Constant TEMPLATE_AUTHOR. */
    public static final String TEMPLATE_AUTHOR      =   "author";
    
    /** The Constant TEMPLATE_LANGUAGE. */
    public static final String TEMPLATE_LANGUAGE    =   "language";
    //build
    /** The Constant COMMON_LIB_PROPERTY. */
    public static final String COMMON_LIB_PROPERTY  =   "common.lib.dir";
    //app
    /** The Constant APPLICATION_SHORT_NAME. */
    public static final String APPLICATION_SHORT_NAME		= 	"tiergen.home";
}
