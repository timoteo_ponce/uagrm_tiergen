package util.gui;

import java.awt.BorderLayout;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JToolBar;

import vista.graficador.BarraTareas;
import vista.graficador.GrDiagrama;

/**
 * The Class PanelGrDiagrama.
 * 
 * @author Juan Timoteo Ponce Ortiz
 */
public class PanelGrDiagrama extends JPanel {

    /** The barra. */
    protected final BarraTareas barra;

    /** The diagrama. */
    protected final GrDiagrama diagrama;

    /**
     * Instantiates a new panel gr diagrama.
     * 
     * @param barra
     *            the barra
     * @param diagrama
     *            the diagrama
     */
    private PanelGrDiagrama(BarraTareas barra, GrDiagrama diagrama) {
	super();
	this.barra = barra;
	this.diagrama = diagrama;
	setLayout(new BorderLayout());
	init();
    }

    public static PanelGrDiagrama newInstance(BarraTareas barra,
	    GrDiagrama diagrama) {
	return new PanelGrDiagrama(barra, diagrama);
    }

    /**
     * Inits the.
     */
    void init() {
	barra.setOrientation(JToolBar.HORIZONTAL);
	final JPanel panel = new JPanel();
	panel.setLayout(new BorderLayout());
	panel.add(barra, BorderLayout.NORTH);
	panel.add(diagrama, BorderLayout.CENTER);
	add(new JScrollPane(panel), BorderLayout.CENTER);
    }

    /**
     * Gets the diagrama.
     * 
     * @return the diagrama
     */
    public GrDiagrama getDiagrama() {
	return diagrama;
    }

    public BarraTareas getBarra() {
	return barra;
    }

}
