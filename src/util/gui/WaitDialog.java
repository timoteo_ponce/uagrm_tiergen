/*
 * Created by JFormDesigner on Mon Jul 13 21:03:02 BOT 2009
 */

package util.gui;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Rectangle;
import java.awt.SystemColor;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;

import org.apache.log4j.Logger;

import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;

/**
 * @author Timo
 */
public final class WaitDialog extends JFrame {
	private static final Logger LOG = Logger.getLogger(WaitDialog.class);
	private final JLabel jLabel1 = new JLabel();
	private final JLabel jLabel2 = new JLabel();
	Rectangle bounds;

	// single instance of this class, used through out the scope of the
	// application
	private static WaitDialog dlg = new WaitDialog();

	public WaitDialog() {
		initComponents();
		try {
			init();
		} catch (Exception e) {
			LOG.error(e,e);
		}
	}

	private void init() throws Exception {
		getContentPane().setLayout(null);
		this.setSize(new Dimension(400, 150));
		setResizable(false);

		setTitle("Por favor espere");
		bounds = getGraphicsConfiguration().getBounds();
		URL imgURL = ClassLoader.getSystemResource("iconos/gtk-info.png");
		if (imgURL != null) {
			ImageIcon img = new ImageIcon(imgURL);
			jLabel1.setIcon(img);
		}
		jLabel1.setBounds(new Rectangle(15, 20, 65, 85));
		jLabel2.setText("Espere mientras la operacion termina...");
		jLabel2.setForeground(SystemColor.textText);
		jLabel2.setBounds(new Rectangle(90, 30, 300, 65));
		jLabel2.setFont(new Font("Dialog", 1, 13));
		getContentPane().add(jLabel2, null);

		getContentPane().add(jLabel1, null);
	}

	/**
	 * This static method uses pre-created dialog, positions it in the center
	 * and displays it to the user.
	 */
	public static void showDialog() {
		dlg.setLocation((int) dlg.bounds.getWidth() / 2 - 200, (int) dlg.bounds
				.getHeight() / 2 - 75);
		dlg.show();
		dlg.paint(dlg.getGraphics());
	}

	/**
	 * 
	 * This static method hides the wait dialog.
	 */
	public static void hideDialog() {
		dlg.hide();
	}

	private void initComponents() {
		// JFormDesigner - Component initialization - DO NOT MODIFY
		// //GEN-BEGIN:initComponents

		CellConstraints cc = new CellConstraints();

		// ======== this ========
		setTitle("Espere por favor");
		Container contentPane = getContentPane();
		contentPane.setLayout(new FormLayout(
				"2*(default), 21dlu:grow, $lcgap, default",
				"fill:default, 3*($lgap, default)"));

		// ---- busyLabel ----

		setSize(400, 125);
		setLocationRelativeTo(null);
		// //GEN-END:initComponents
	}

	// JFormDesigner - Variables declaration - DO NOT MODIFY
	// //GEN-BEGIN:variables

	// JFormDesigner - End of variables declaration //GEN-END:variables
}
