/*
 * Created by JFormDesigner on Fri Jun 12 00:38:56 BOT 2009
 */

package util.gui;

import java.awt.Container;
import java.awt.Dialog;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.WindowConstants;

import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;

/**
 * The Class JAbout.
 * 
 * @author Juan Timoteo Ponce Ortiz
 */
public final class JAbout extends JDialog {

    /**
     * Instantiates a new j about.
     * 
     * @param owner
     *            the owner
     */
    public JAbout(Frame owner) {
	super(owner);
	initComponents();
    }

    /**
     * Instantiates a new j about.
     * 
     * @param owner
     *            the owner
     */
    public JAbout(Dialog owner) {
	super(owner);
	initComponents();
    }

    private void button1ActionPerformed(ActionEvent e) {
	dispose();
    }

    /**
     * Inits the components.
     */
    private void initComponents() {
	// JFormDesigner - Component initialization - DO NOT MODIFY
	// //GEN-BEGIN:initComponents
	aboutLbl = new JLabel();
	panelInfo = new JPanel();
	lblTitle = new JLabel();
	lbComments = new JLabel();
	lblVersion = new JLabel();
	lblVersionValue = new JLabel();
	lblOwner = new JLabel();
	lblOwnerValue = new JLabel();
	lblHomePage = new JLabel();
	lblHomePageValue = new JLabel();
	button1 = new JButton();
	CellConstraints cc = new CellConstraints();

	// ======== this ========
	setModal(true);
	setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
	setTitle("Informacion");
	Container contentPane = getContentPane();
	contentPane
		.setLayout(new FormLayout("89dlu, $lcgap, 180dlu", "129dlu"));

	// ---- aboutLbl ----
	aboutLbl.setIcon(new ImageIcon(getClass().getResource("/about.PNG")));
	contentPane.add(aboutLbl, cc.xywh(1, 1, 1, 1, CellConstraints.FILL,
		CellConstraints.FILL));

	// ======== panelInfo ========
	{
	    panelInfo.setLayout(new FormLayout(
		    "48dlu, $lcgap, 77dlu, $lcgap, 48dlu",
		    "25dlu, $lgap, default, $lgap, 19dlu, 4*($lgap, default)"));

	    // ---- lblTitle ----
	    lblTitle.setText("<html><h3>Title<h3><html>");
	    panelInfo.add(lblTitle, cc.xywh(1, 1, 5, 2, CellConstraints.FILL,
		    CellConstraints.FILL));

	    // ---- lbComments ----
	    lbComments.setText("Comments");
	    panelInfo.add(lbComments, cc.xywh(1, 3, 5, 4,
		    CellConstraints.DEFAULT, CellConstraints.FILL));

	    // ---- lblVersion ----
	    lblVersion.setText("<html><b>Version:</b></html>");
	    panelInfo.add(lblVersion, cc.xy(1, 7));

	    // ---- lblVersionValue ----
	    lblVersionValue.setText("text");
	    panelInfo.add(lblVersionValue, cc.xywh(3, 7, 3, 1));

	    // ---- lblOwner ----
	    lblOwner.setText("<html><b>Autor:</b></html>");
	    panelInfo.add(lblOwner, cc.xy(1, 9));

	    // ---- lblOwnerValue ----
	    lblOwnerValue.setText("text");
	    panelInfo.add(lblOwnerValue, cc.xywh(3, 9, 3, 1));

	    // ---- lblHomePage ----
	    lblHomePage.setText("<html><b>Web:</b></html>");
	    panelInfo.add(lblHomePage, cc.xy(1, 11));

	    // ---- lblHomePageValue ----
	    lblHomePageValue.setText("text");
	    panelInfo.add(lblHomePageValue, cc.xywh(3, 11, 3, 1));

	    // ---- button1 ----
	    button1.setText("Cerrar");
	    button1.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
		    button1ActionPerformed(e);
		}
	    });
	    panelInfo.add(button1, cc.xy(5, 13));
	}
	contentPane.add(panelInfo, cc.xy(3, 1));
	setSize(555, 235);
	setLocationRelativeTo(null);
	// JFormDesigner - End of component initialization
	// //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY
    // //GEN-BEGIN:variables
    private JLabel aboutLbl;
    private JPanel panelInfo;
    private JLabel lblTitle;
    private JLabel lbComments;
    private JLabel lblVersion;
    private JLabel lblVersionValue;
    private JLabel lblOwner;
    private JLabel lblOwnerValue;
    private JLabel lblHomePage;
    private JLabel lblHomePageValue;
    private JButton button1;

    // JFormDesigner - End of variables declaration //GEN-END:variables

    @Override
    public void setTitle(final String title) {
	lblTitle.setText("<html><h3>" + title + "</h3></html>");
    }

    public void setComments(final String comments) {
	lbComments.setText(comments);
    }

    public void setVersion(final String version) {
	lblVersionValue.setText(version);
    }

    public void setAuthor(final String author) {
	lblOwnerValue.setText(author);
    }

    public void setWeb(final String url, final String name) {
	lblHomePageValue.setText("<html><a href='" + url + "'>" + name
		+ "</a></html>");
    }

}
