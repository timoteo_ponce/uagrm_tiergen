package util.gui;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;

/**
 * The Class CloseableTabbedPane.
 * 
 * @author Juan Timoteo Ponce Ortiz
 */
public final class CloseableTabbedPane extends JPanel {

    /**
     * Instantiates a new closeable tabbed pane.
     */
    public CloseableTabbedPane() {
	initComponents();
    }

    /**
     * Adds the tab.
     * 
     * @param title
     *            the title
     * @param comp
     *            the comp
     */
    public void addTab(String title, Component comp) {
	tabbedPane.add(title, comp);
	comboTabs.addItem(title);
    }

    /**
     * Removes the tab.
     * 
     * @param index
     *            the index
     */
    public void removeTab(int index) {
	if (index > -1 && index < tabbedPane.getComponentCount()) {
	    tabbedPane.remove(index);
	    comboTabs.remove(index);
	    updateUI();
	}
    }

    /**
     * Change tab panel.
     */
    private void changeTabPanel() {
	int index = comboTabs.getSelectedIndex();
	if (index > -1) {
	    tabbedPane.setSelectedIndex(index);
	}
    }

    /**
     * Gets the selected tab.
     * 
     * @return the selected tab
     */
    public Component getSelectedTab() {
	return tabbedPane.getSelectedComponent();
    }

    /**
     * Sets the selected tab.
     * 
     * @param comp
     *            the new selected tab
     */
    public void setSelectedTab(Component comp) {
	tabbedPane.setSelectedComponent(comp);
    }

    /**
     * Sets the selected index tab.
     * 
     * @param index
     *            the new selected index tab
     */
    public void setSelectedIndexTab(int index) {
	tabbedPane.setSelectedIndex(index);
    }

    /**
     * Gets the selected index tab.
     * 
     * @return the selected index tab
     */
    public int getSelectedIndexTab() {
	return tabbedPane.getSelectedIndex();
    }

    /**
     * Gets the tab component at.
     * 
     * @param index
     *            the index
     * 
     * @return the tab component at
     */
    public Component getTabComponentAt(int index) {
	return tabbedPane.getComponentAt(index);
    }

    /**
     * Clear all tabs.
     */
    public void clearAllTabs() {
	tabbedPane.removeAll();
	comboTabs.removeAllItems();
	updateUI();
    }

    /**
     * Checks if is empty.
     * 
     * @return true, if is empty
     */
    public boolean isEmpty() {
	return (tabbedPane.getComponentCount() == 0);
    }

    /**
     * Close current tab.
     */
    public void closeCurrentTab() {
	int index = tabbedPane.getSelectedIndex();
	if (index != -1) {
	    firePropertyChange("removing_tab", getSelectedTab(), null);
	    tabbedPane.remove(index);
	    comboTabs.removeItemAt(index);
	    updateUI();
	}
    }

    /**
     * Gets the tab component count.
     * 
     * @return the tab component count
     */
    public int getTabComponentCount() {
	return tabbedPane.getComponentCount();
    }

    /**
     * Move left.
     */
    public void moveLeft() {
	if (tabbedPane.getSelectedIndex() > 0) {
	    tabbedPane.setSelectedIndex(tabbedPane.getSelectedIndex() - 1);
	    comboTabs.setSelectedIndex(tabbedPane.getSelectedIndex());
	}
    }

    /**
     * Move right.
     */
    public void moveRight() {
	if (tabbedPane.getComponentCount() > 0
		&& tabbedPane.getSelectedIndex() < tabbedPane
			.getComponentCount() - 1) {
	    tabbedPane.setSelectedIndex(tabbedPane.getSelectedIndex() + 1);
	    comboTabs.setSelectedIndex(tabbedPane.getSelectedIndex());
	}
    }

    /**
     * Btn refresh action performed.
     * 
     * @param e
     *            the e
     */
    private void btnRefreshActionPerformed(ActionEvent e) {
	PanelGrDiagrama panel = (PanelGrDiagrama) getSelectedTab();
	panel.getDiagrama().updateVista();
    }

    /**
     * Inits the components.
     */
    private void initComponents() {
	// JFormDesigner - Component initialization - DO NOT
	// MODIFY//GEN-BEGIN:initComponents
	panelNorth = new JPanel();
	comboTabs = new JComboBox();
	btnLeft = new JButton();
	btnRight = new JButton();
	btnRefresh = new JButton();
	btnClose = new JButton();
	tabbedPane = new JTabbedPane();
	CellConstraints cc = new CellConstraints();

	// ======== this ========
	setLayout(new FormLayout("266dlu:grow", "19dlu, $lgap, 175dlu:grow"));

	// ======== panelNorth ========
	{
	    panelNorth
		    .setLayout(new FormLayout(
			    "161dlu, 3*($lcgap, default), $lcgap, 9dlu:grow, $lcgap, default",
			    "default"));

	    // ---- comboTabs ----
	    comboTabs.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
		    changeTabPanel();
		}
	    });
	    panelNorth.add(comboTabs, cc.xy(1, 1));

	    // ---- btnLeft ----
	    btnLeft.setIcon(new ImageIcon(getClass().getResource(
		    "/iconos/go-previous.png")));
	    btnLeft.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
		    moveLeft();
		}
	    });
	    panelNorth.add(btnLeft, cc.xy(3, 1));

	    // ---- btnRight ----
	    btnRight.setIcon(new ImageIcon(getClass().getResource(
		    "/iconos/go-next.png")));
	    btnRight.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
		    moveRight();
		}
	    });
	    panelNorth.add(btnRight, cc.xy(5, 1));

	    // ---- btnRefresh ----
	    btnRefresh.setIcon(new ImageIcon(getClass().getResource(
		    "/iconos/gnome-session-switch.png")));
	    btnRefresh.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
		    btnRefreshActionPerformed(e);
		}
	    });
	    panelNorth.add(btnRefresh, cc.xywh(7, 1, 1, 1,
		    CellConstraints.DEFAULT, CellConstraints.CENTER));

	    // ---- btnClose ----
	    btnClose.setIcon(new ImageIcon(getClass().getResource(
		    "/iconos/close.png")));
	    btnClose.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
		    closeCurrentTab();
		}
	    });
	    panelNorth.add(btnClose, cc.xy(11, 1));
	}
	add(panelNorth, cc.xy(1, 1));
	add(tabbedPane, cc.xywh(1, 3, 1, 1, CellConstraints.FILL,
		CellConstraints.FILL));
	// JFormDesigner - End of component
	// initialization//GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT
    // MODIFY//GEN-BEGIN:variables
    /** The panel north. */
    private JPanel panelNorth;

    /** The combo tabs. */
    private JComboBox comboTabs;

    /** The btn left. */
    private JButton btnLeft;

    /** The btn right. */
    private JButton btnRight;

    /** The btn refresh. */
    private JButton btnRefresh;

    /** The btn close. */
    private JButton btnClose;

    /** The tabbed pane. */
    private JTabbedPane tabbedPane;
    // JFormDesigner - End of variables declaration//GEN-END:variables
}
