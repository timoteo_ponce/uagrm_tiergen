package util;

import java.awt.Component;
import java.io.File;
import java.io.IOException;

import javax.help.HelpBroker;
import javax.help.HelpSet;
import javax.help.HelpSetException;

import org.apache.log4j.Logger;

/**
 * The Class HelpUtil.
 */
public final class HelpUtil {

    private static Logger log = Logger.getLogger(HelpUtil.class);

    /** The instance. */
    private static final HelpUtil instance = new HelpUtil();

    /** The helpset. */
    private HelpSet helpset;

    /** The broker. */
    private HelpBroker broker;

    /**
     * Instantiates a new help util.
     */
    private HelpUtil() {
	try {
	    String helpPath = Configuracion.getParam("help_set");
	    helpset = new HelpSet(getClass().getClassLoader(), new File(
		    helpPath).toURI().toURL());
	} catch (IOException e) {
	    log.error(e.getMessage(), e);
	} catch (HelpSetException e) {
	    log.error(e.getMessage(), e);
	}
    }

    /**
     * Gets the single instance of HelpUtil.
     * 
     * @return single instance of HelpUtil
     */
    public static HelpUtil getInstance() {
	return instance;
    }

    /**
     * Adds the help on button.
     * 
     * @param comp
     *            the comp
     * @param target
     *            the target
     */
    public void addHelpOnButton(Component comp, String target) {
	HelpBroker broker = helpset.createHelpBroker();
	broker.enableHelpOnButton(comp, target, helpset);
    }

    /**
     * Adds the help on key.
     * 
     * @param comp
     *            the comp
     * @param target
     *            the target
     */
    public void addHelpOnKey(Component comp, String target) {
	HelpBroker broker = helpset.createHelpBroker();
	broker.enableHelpKey(comp, target, helpset);
    }

    /**
     * Adds the help.
     * 
     * @param comp
     *            the comp
     * @param target
     *            the target
     */
    public void addHelp(Component comp, String target) {
	HelpBroker broker = helpset.createHelpBroker();
	broker.enableHelp(comp, target, helpset);
    }

    /**
     * Gets the helpset.
     * 
     * @return the helpset
     */
    public HelpSet getHelpset() {
	return helpset;
    }

    /**
     * Gets the broker.
     * 
     * @return the broker
     */
    public HelpBroker getBroker() {
	if (broker == null)
	    broker = helpset.createHelpBroker();
	return broker;
    }

}
