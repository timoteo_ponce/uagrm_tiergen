/*
 * ElementoVisualUtil.java
 *
 * Created on 6 de noviembre de 2008, 22:45
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
package util.uml;

import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import modelo.repositorio.RepositorioImpl;

import org.apache.log4j.Logger;
import org.omg.uml.foundation.core.Classifier;
import org.omg.uml.foundation.core.ModelElement;
import org.uml.diagrammanagement.Diagram;
import org.uml.diagrammanagement.GraphEdge;
import org.uml.diagrammanagement.GraphElement;
import org.uml.diagrammanagement.GraphNode;
import org.uml.diagrammanagement.Property;
import org.uml.diagrammanagement.SemanticModelBridge;
import org.uml.diagrammanagement.SimpleSemanticModelBridge;
import org.uml.diagrammanagement.Uml1SemanticModelBridge;

import util.AppUtil;
import util.Constantes;
import controlador.ControllerProyecto;

/**
 * The Class ElementoVisualUtil.
 * 
 * @author Juan Timoteo Ponce Ortiz
 */
public final class ElementoVisualUtil {

    private static Logger log = Logger.getLogger(ElementoVisualUtil.class);

    /**
     * Checks if is comun.
     * 
     * @param ge
     *            the ge
     * 
     * @return true, if is comun
     */
    public static boolean isComun(final GraphElement ge) {
	final Properties props = ElementoVisualUtil.getPropiedades(ge);
	String type = "";

	if (ge instanceof GraphEdge)
	    type = (String) props.get(Constantes.MODEL_EDGE_TYPE);
	else if (ge instanceof GraphNode)
	    type = (String) props.get(Constantes.MODEL_NODE_TYPE);

	return type.startsWith("vista.graficador.comun");
    }

    /**
     * Retorna las propiedades de un elemento grafico.
     * 
     * @param ge
     *            elemento grafico
     * 
     * @return tabla con todas las propiedades
     */
    public static Properties getPropiedades(final GraphElement ge) {
	if (ge == null)
	    throw new InvalidParameterException("This can't be null!");
	final Properties props = new Properties();
	final Iterator<Property> it = ge.getProperty().iterator();

	while (it.hasNext()) {
	    final Property prop = it.next();
	    props.put(prop.getKey(), prop.getValue());
	}
	return props;
    }

    /**
     * Gets the elemento logico.
     * 
     * @param ge
     *            the ge
     * 
     * @return the elemento logico
     */
    public static ModelElement getElementoLogico(final GraphElement ge) {
	final SemanticModelBridge smb = ge.getSemanticModel();
	if (smb != null) {
	    if (smb instanceof Uml1SemanticModelBridge) {
		final Uml1SemanticModelBridge bridge = (Uml1SemanticModelBridge) smb;
		return bridge.getElement();
	    }
	}
	return null;
    }

    /**
     * Gets the graph element.
     * 
     * @param element
     *            the element
     * 
     * @return the graph element
     */
    @SuppressWarnings("unchecked")
    public static GraphElement getGraphElement(final ModelElement element) {
	log.info("Searching graph element for: " + element.getName());
	final Collection<Diagram> diagrams = RepositorioImpl.getInstance()
		.getAllDiagrams();

	for (Iterator<Diagram> it = diagrams.iterator(); it.hasNext();) {
	    final Diagram currentDiagrama = it.next();

	    final Collection<GraphElement> tempElements = currentDiagrama
		    .getContained();

	    for (Iterator<GraphElement> it2 = tempElements.iterator(); it2
		    .hasNext();) {
		final GraphElement currentElement = it2.next();
		final SemanticModelBridge smb = currentElement
			.getSemanticModel();
		if (smb instanceof Uml1SemanticModelBridge) {
		    final Uml1SemanticModelBridge bridge = (Uml1SemanticModelBridge) smb;
		    if (bridge.getElement() != null
			    && bridge.getElement().equals(element)) {
			return currentElement;
		    }
		}
	    }
	}
	log.warn("I can't found you: " + element.getName());
	return null;
    }

    /**
     * Gets the graph element.
     * 
     * @param element
     *            the element
     * @param currentDiagrama
     *            the current diagrama
     * 
     * @return the graph element
     */
    @SuppressWarnings("unchecked")
    public static GraphElement getGraphElement(final ModelElement element,
	    final Diagram currentDiagrama) {
	log.info("Searching graph element for: " + element.getName());
	final Collection<GraphElement> tempElements = currentDiagrama
		.getContained();

	for (Iterator<GraphElement> it = tempElements.iterator(); it.hasNext();) {
	    final GraphElement currentElement = it.next();
	    final SemanticModelBridge smb = currentElement.getSemanticModel();

	    if (smb instanceof Uml1SemanticModelBridge) {
		final Uml1SemanticModelBridge bridge = (Uml1SemanticModelBridge) smb;
		if (bridge.getElement() != null
			&& bridge.getElement().equals(element)) {
		    return currentElement;
		}
	    }
	}
	log.warn("I can't found you: " + element.getName());
	return null;
    }

    /**
     * Editar elemento.
     * 
     * @param ge
     *            the ge
     * @param controller
     *            the controller
     * @param source
     *            the source
     */
    public static void editarElemento(final GraphElement ge,
	    final ControllerProyecto controller, final Object source) {
	final SemanticModelBridge smb = ge.getSemanticModel();
	if (smb != null) {
	    if (smb instanceof Uml1SemanticModelBridge) {
		final Uml1SemanticModelBridge bridge = (Uml1SemanticModelBridge) smb;
		ElementoDeModeloUtil.editElemento(null, bridge.getElement(),
			controller, source);
	    } else if (smb instanceof SimpleSemanticModelBridge) {// is a note
		final SimpleSemanticModelBridge bridge = (SimpleSemanticModelBridge) smb;
		final String content = AppUtil.showInput(null, "Nota",
			"Contenido");
		bridge.setTypeInfo(content);
	    }
	}
    }

    /**
     * Removes the elemento.
     * 
     * @param ge
     *            the ge
     */
    public static void removeElemento(final GraphElement ge) {
	try {
	    final GraphElement container = ge.getContainer();
	    if (container != null) {
		container.getContained().remove(ge);
		final List<GraphEdge> edges = getAllEdges(container, ge);

		for (GraphEdge edge : edges) {
		    container.getContained().remove(edge);
		}
	    }
	    ge.refDelete();
	} catch (ClassCastException e) {
	    log.error("Error eliminado el elemento visual " + e, e);
	}
    }

    /**
     * Gets the all edges.
     * 
     * @param container
     *            the container
     * @param element
     *            the element
     * 
     * @return the all edges
     */
    public static List<GraphEdge> getAllEdges(final GraphElement container,
	    final GraphElement element) {
	final List<GraphEdge> ret = new ArrayList<GraphEdge>();
	final Iterator<GraphElement> it = container.getContained().iterator();

	while (it.hasNext()) {
	    final GraphElement ge = it.next();

	    if (ge instanceof GraphEdge) {
		final GraphEdge edge = (GraphEdge) ge;
		if (edge.getEdgeEnd1().equals(element)
			|| edge.getEdgeEnd2().equals(element)) {
		    ret.add(edge);
		    final List<GraphEdge> allEdges = getAllEdges(container,
			    edge);
		    ret.addAll(allEdges);
		}
	    }
	}
	final GraphElement superContainer = container.getContainer();
	if (superContainer != null) {
	    final List<GraphEdge> allEdges = getAllEdges(superContainer,
		    element);
	    ret.addAll(allEdges);
	}
	return ret;
    }

    /**
     * Elimina un diagrama.
     * 
     * @param d
     *            diagrama a eliminar
     */
    public static void removeDiagrama(final Diagram d) {
	d.refDelete();
    }

    /**
     * Sets the property.
     * 
     * @param graphElement
     *            the graph element
     * @param key
     *            the key
     * @param value
     *            the value
     */
    public static void setProperty(final GraphElement graphElement,
	    final String key, final String value) {
	final Iterator<Property> it = graphElement.getProperty().iterator();

	while (it.hasNext()) {
	    final Property prop = it.next();

	    if (prop.getKey().equals(key)) {
		prop.setValue(value);
		return;
	    }
	}
	final Property prop = UMLUtil.crearPropiedad(key, value);
	graphElement.getProperty().add(prop);
    }

    /**
     * Creates the graph element.
     * 
     * @param elemento
     *            the elemento
     * @param type
     *            the type
     * 
     * @return the graph element
     */
    public static GraphElement createGraphElement(final ModelElement elemento,
	    final int type) {
	final GraphNode graphNode = UMLUtil.crearGraphNode();
	String identifier = "null";
	switch (type) {
	case Constantes.MODEL_NODE_TYPE_ID:
	    identifier = Constantes.MODEL_NODE_TYPE;
	    break;
	case Constantes.MODEL_EDGE_TYPE_ID:
	    identifier = Constantes.MODEL_EDGE_TYPE;
	    break;
	case Constantes.MODEL_DIAGRAM_TYPE_ID:
	    identifier = Constantes.MODEL_DIAGRAM_TYPE;
	    break;
	}
	final Property prop = UMLUtil.crearPropiedad(identifier, elemento
		.getName());
	graphNode.getProperty().add(prop);
	final Uml1SemanticModelBridge bridge = UMLUtil
		.crearPuenteSemantico(elemento);
	graphNode.setSemanticModel(bridge);

	return graphNode;
    }

    /**
     * Gets the all classifiers.
     * 
     * @param diagramElements
     *            the diagram elements
     * 
     * @return the all classifiers
     */
    public static List<Classifier> getAllClassifiers(
	    final Collection diagramElements) {
	final List<Classifier> classes = new ArrayList<Classifier>();

	for (final Iterator it = diagramElements.iterator(); it.hasNext();) {
	    final Object object = it.next();
	    if (object instanceof Classifier) {
		classes.add((Classifier) object);
	    }
	}
	return classes;
    }
}
