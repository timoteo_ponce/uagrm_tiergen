/*
 * PaqueteUtil.java
 *
 * Created on November 9, 2008, 11:42 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package util.uml;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import org.omg.uml.foundation.core.ModelElement;
import org.omg.uml.foundation.core.Namespace;

/**
 * The Class PaqueteUtil.
 * 
 * @author Juan Timoteo Ponce Ortiz
 */
public final class PaqueteUtil {

    /**
     * Pertenece a.
     * 
     * @param me
     *            the me
     * @param path
     *            the path
     * 
     * @return true, if successful
     */
    public static boolean belongsTo(final ModelElement me, final String path) {
	final List<String> namespaces = new ArrayList<String>();
	final StringTokenizer st = new StringTokenizer(path, ":");

	while (st.hasMoreTokens()) {
	    final String token = st.nextToken();
	    namespaces.add(token);
	}
	Namespace owner = me.getNamespace();
	for (final String name : namespaces) {
	    if (owner == null)
		return false;
	    if (!owner.getName().equals(name))
		return false;
	    owner = owner.getNamespace();
	}

	return true;
    }

}
