/*
 * ElementoDeModeloUtil.java
 *
 * Created on 6 de noviembre de 2008, 0:29
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
package util.uml;

import java.awt.Frame;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import javax.jmi.reflect.RefClass;
import javax.jmi.reflect.RefObject;

import modelo.repositorio.RepositorioImpl;

import org.apache.log4j.Logger;
import org.omg.uml.UmlPackage;
import org.omg.uml.foundation.core.Attribute;
import org.omg.uml.foundation.core.Classifier;
import org.omg.uml.foundation.core.Feature;
import org.omg.uml.foundation.core.ModelElement;
import org.omg.uml.foundation.core.Namespace;
import org.omg.uml.foundation.core.Relationship;
import org.omg.uml.foundation.core.Stereotype;

import util.AppUtil;
import util.Constantes;
import vista.editores.DialogoEditor;
import vista.editores.EditorListener;
import controlador.ControllerProyecto;

/**
 * The Class ElementoDeModeloUtil.
 * 
 * @author Juan Timoteo Ponce Ortiz
 */
public final class ElementoDeModeloUtil {

	private static Logger log = Logger.getLogger(ElementoDeModeloUtil.class);

	/**
	 * Determina si un elemento de modelo es contenido por otro(recursivo) *.
	 * 
	 * @param contenido
	 *            el elemento contenido
	 * @param contenedor
	 *            elemento contenedor
	 * 
	 * @return verdadero si esta contenido
	 */
	public static boolean isContenidoBy(final ModelElement contenido,
			final ModelElement contenedor) {
		if (contenido instanceof Namespace && contenedor instanceof Namespace) {
			Namespace lContenido = (Namespace) contenido;
			Namespace lContenedor = (Namespace) contenedor;

			for (Object nextOwnedElement : lContenedor.getOwnedElement()) {
				ModelElement temporalModelElement = (ModelElement) nextOwnedElement;

				if (temporalModelElement.equals(lContenido)) {
					return true;
				}
				if (temporalModelElement instanceof Namespace) {
					Namespace temp = (Namespace) temporalModelElement;
					if (isContenidoBy(contenedor, temp)) {
						return true;
					}
				}
			}

		}
		return false;
	}

	/**
	 * Retorna todos los elementos del tipo especificado.
	 * 
	 * @param tipoElemento
	 *            el tipo deseado
	 * 
	 * @return coleccion conteniendo todos los elementos
	 */
	@SuppressWarnings("unchecked")
	public static Collection getElementosByTipo(final String tipoElemento) {
		ArrayList resultElements = new ArrayList();
		UmlPackage uml = RepositorioImpl.getInstance().getUmlPackage();
		RefClass refClass = RefObjectUtil.findClassProxy(uml, tipoElemento);

		if (refClass != null) {
			resultElements.addAll(refClass.refAllOfClass());
		} else {
			log.warn("No se pudo hallar el elemento");
		}
		return resultElements;
	}

	/**
	 * Retorna todos los elementos del tipo y estereotipo especificado.
	 * 
	 * @param tipoElemento
	 *            el tipo deseado
	 * @param estereotipo
	 *            el estereotipo deseado
	 * 
	 * @return coleccion conteniendo todos los elementos.
	 */
	public static List<ModelElement> getElementosByTipoAndEstereotipo(
			final String tipoElemento, final String estereotipo) {
		List<ModelElement> resultElements = new ArrayList<ModelElement>();
		UmlPackage uml = RepositorioImpl.getInstance().getUmlPackage();
		RefClass refClass = RefObjectUtil.findClassProxy(uml, tipoElemento);

		for (Object nextClass : refClass.refAllOfClass()) {
			RefObject temporalRefObject = (RefObject) nextClass;
			ModelElement temporalModelElement = (ModelElement) temporalRefObject;
			if (EstereotipoUtil.hasEstereotype(temporalModelElement,
					estereotipo)) {
				resultElements.add(temporalModelElement);
			}
		}
		return resultElements;
	}

	/**
	 * Retorna el elemento por tipo, nombre y paquete.
	 * 
	 * @param paquete
	 *            el paquete deseado
	 * @param nombreElemento
	 *            Elemento deseado
	 * @param tipoElemento
	 *            tipo del elemento
	 * 
	 * @return elemento encontrado
	 */
	public static ModelElement getElementoByPaqueteNombreTipo(
			final String paquete, final String nombreElemento,
			final String tipoElemento) {
		UmlPackage uml = RepositorioImpl.getInstance().getUmlPackage();
		RefClass rc = RefObjectUtil.findClassProxy(uml, tipoElemento);
		Iterator<RefObject> it = rc.refAllOfClass().iterator();

		while (it.hasNext()) {
			RefObject ro = it.next();
			ModelElement me = (ModelElement) ro;
			if (me.getName().equals(nombreElemento)
					&& PaqueteUtil.belongsTo(me, paquete)) {
				return me;
			}
		}
		return null;
	}

	/**
	 * Retorna un elemento segun el nombre y tipo.
	 * 
	 * @param nombreElemento
	 *            nombre del elemento
	 * @param tipoElemento
	 *            tipo de elemento
	 * 
	 * @return elemento encontrado
	 */
	public static ModelElement getElementoByNombreTipo(
			final String nombreElemento, final String tipoElemento) {
		UmlPackage uml = RepositorioImpl.getInstance().getUmlPackage();
		RefClass rc = RefObjectUtil.findClassProxy(uml, tipoElemento);
		Iterator<RefObject> it = rc.refAllOfClass().iterator();

		while (it.hasNext()) {
			RefObject ro = it.next();
			ModelElement me = (ModelElement) ro;
			if (me.getName().equals(nombreElemento)) {
				return me;
			}
		}
		return null;
	}

	/**
	 * Crea un elemento con el nombre, tipo y estereotipo especificado.
	 * 
	 * @param nombreClase
	 *            tipo de elemento
	 * @param estereotipo
	 *            estereotipo
	 * @param nombre
	 *            nombre
	 * 
	 * @return elemento creado
	 */
	public static ModelElement crearElemento(final String nombreClase,
			final String estereotipo, final String nombre,
			final Namespace namespace) {
		ModelElement me = crearElemento(nombreClase, estereotipo, namespace);
		if (nombre != null) {
			me.setName(nombre);
		}
		return me;
	}

	/**
	 * Crea un elemento con el tipo y estereotipo especificado.
	 * 
	 * @param nombreClase
	 *            tipo de elemento
	 * @param estereotipo
	 *            estereotipo
	 * 
	 * @return elemento creado
	 */
	public static ModelElement crearElemento(final String nombreClase,
			final String estereotipo, final Namespace namespace) {
		ModelElement me = crearElemento(nombreClase);
		if (estereotipo != null && namespace != null) {
			Stereotype temp = (Stereotype) getElementoByNombreTipo(estereotipo,
					Constantes.UML_STEREOTYPE);
			if (temp == null) {
				temp = UMLUtil.crearEstereotipo(estereotipo);
				temp.setNamespace(namespace);
				temp.getBaseClass().add(nombreClase);
			}
			me.getStereotype().add(estereotipo);
		}
		return me;
	}

	/**
	 * Crea un elemento con el tipo especificado.
	 * 
	 * @param nombreClase
	 *            tipo de elemento
	 * 
	 * @return the model element
	 */
	public static ModelElement crearElemento(final String nombreClase) {
		ModelElement me = null;
		UmlPackage uml = RepositorioImpl.getInstance().getUmlPackage();
		RefClass rc = RefObjectUtil.findClassProxy(uml, nombreClase);
		if (rc == null) {
			log
			.error("No hay proxy  RefObjectUtil.findClassProxy() returns null");
		} else {
			me = (ModelElement) rc.refCreateInstance(null);
		}
		return me;
	}

	/**
	 * Llama al editor para un elementodemodelo.
	 * 
	 * @param padre
	 *            the padre
	 * @param me
	 *            the me
	 * @param controller
	 *            the controller
	 * @param source
	 *            the source
	 */
	public static void editElemento(final Frame padre, final ModelElement me,
			final ControllerProyecto controller, final Object source) {
		RepositorioImpl.getInstance().iniciarTransaccion(true);
		DialogoEditor dialog = new DialogoEditor(padre, true, me);

		dialog.addListener(new EditorListener() {

			public void aceptar(final ModelElement elemento) {
				RepositorioImpl.getInstance().finalizarTransaccion(false);
				controller.changeModelo(source);
			}

			public void cancelar() {
				RepositorioImpl.getInstance().finalizarTransaccion(true);
			}
		});
		dialog.setVisible(true);
	}

	/**
	 * Elimina un elemento, mostrando la confirmacion.
	 * 
	 * @param me
	 *            elemento a borrar
	 * @param confirmar
	 *            the confirmar
	 * @param controller
	 *            the controller
	 */
	public static void removeElemento(final ModelElement me,
			final boolean confirmar, final ControllerProyecto controller) {
		boolean eliminar = false;
		if (confirmar) {
			eliminar = AppUtil.showConfirmar(null, "Eliminado",
					"Eliminar el elemento  " + me.getName() + " ?");
		}
		if (confirmar == eliminar) {
			removeElemento(me);
			controller.changeModelo(me);
		}
	}

	/**
	 * Elimina un elemento del proyecto.
	 * 
	 * @param me
	 *            elemento a borrar
	 */
	public static void removeElemento(final ModelElement me) {
		List<Relationship> borrar = new ArrayList<Relationship>();
		Iterator<Relationship> iRelaciones = RelacionUtil.getAllRelaciones()
		.iterator();

		while (iRelaciones.hasNext()) {
			Relationship rel = iRelaciones.next();
			if (RelacionUtil.relacionContiene(rel, me, RelacionUtil.CUALQUIERA)) {
				borrar.add(rel);
			}
		}
		// borramos todo lo que se relacione al elemento
		for (Relationship relationship : borrar) {
			removeElemento(relationship);
		}
		me.refDelete();
	}

	/**
	 * Gets the attribute by name.
	 * 
	 * @param element
	 *            the element
	 * @param name
	 *            the name
	 * 
	 * @return the attribute by name
	 */
	public static Attribute getAttributeByName(final Classifier element,
			final String name) {
		log.info("Searching attribute: " + name);
		Attribute out = null;
		Collection<Feature> features = element.getFeature();

		for (Feature currentFeature : features) {
			if (currentFeature instanceof Attribute) {
				if (name.equals(currentFeature.getName())) {
					log.info("I found you! : " + name);
					return (Attribute) currentFeature;
				}
			}
		}
		return out;
	}

	/**
	 * Checks if is primitive.
	 * 
	 * @param element
	 *            the element
	 * 
	 * @return true, if is primitive
	 */
	public static boolean isPrimitive(final ModelElement element) {
		if (element instanceof Attribute) {
			Attribute feat = (Attribute) element;
			if (feat.getType() != null) {
				return ("java.lang.Integer".equals(feat.getType().getName()) || "java.lang.Long"
						.equals(feat.getType().getName()));
			}
		}
		return false;
	}
}
