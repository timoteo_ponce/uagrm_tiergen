/*
 * RefObjectUtil.java
 *
 * Created on 6 de noviembre de 2008, 0:30
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package util.uml;

import java.util.Iterator;

import javax.jmi.reflect.RefClass;
import javax.jmi.reflect.RefPackage;

/**
 * The Class RefObjectUtil.
 * 
 * @author Juan Timoteo Ponce Ortiz
 */
public final class RefObjectUtil {

    /**
     * Retorna la clase proxy para el paquete especifico y el nombre de clase.
     * 
     * @param refPaquete
     *            paquete
     * @param nombreClase
     *            nombre de clase
     * 
     * @return la clase proxy para el paquete y el nombre de clase
     */
    public static RefClass findClassProxy(final RefPackage refPaquete,
	    final String nombreClase) {
	try {
	    final RefClass rc = refPaquete.refClass(nombreClase);
	    return rc;
	} catch (Exception e) {
	    for (final Iterator<RefPackage> it = refPaquete.refAllPackages()
		    .iterator(); it.hasNext();) {
		final RefPackage rp = it.next();
		final RefClass rc = findClassProxy(rp, nombreClase);
		if (rc != null)
		    return rc;
	    }
	}
	return null;
    }

}
