/*
 * EstereotipoUtil.java
 *
 * Created on 6 de noviembre de 2008, 1:06
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package util.uml;

import java.util.Collection;
import java.util.Iterator;

import org.omg.uml.foundation.core.ModelElement;
import org.omg.uml.foundation.core.Stereotype;

// TODO: Auto-generated Javadoc
/**
 * The Class EstereotipoUtil.
 * 
 * @author Juan Timoteo Ponce Ortiz
 */
public final class EstereotipoUtil {

	/**
	 * A cadena.
	 * 
	 * @param stereotypes the stereotypes
	 * 
	 * @return the string
	 */
	public static String toString(final Collection<Stereotype> stereotypes) {		
		final StringBuilder buffer = new StringBuilder();
		
		for (Iterator<Stereotype> it = stereotypes.iterator(); it.hasNext();) {
			final Stereotype s =  it.next();
			buffer.append(s.getName());
			if (it.hasNext())
				buffer.append(",");			
		}		
		return buffer.toString();
	}

	/**
	 * Tiene estereotipo.
	 * 
	 * @param me the me
	 * @param stereo the stereo
	 * 
	 * @return true, if successful
	 */
	public static boolean hasEstereotype(final ModelElement me,final  String stereo) {
		for (Iterator<Stereotype> it = me.getStereotype().iterator(); it.hasNext();) {			
			final Stereotype s = it.next();
			if (s.getName().equals(stereo))
				return true;
		}		
		return false;
	}

}
