/*
 * RelacionUtil.java
 *
 * Created on 6 de noviembre de 2008, 1:20
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
package util.uml;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.jmi.reflect.RefClass;
import javax.jmi.reflect.RefObject;

import modelo.repositorio.RepositorioImpl;

import org.omg.uml.foundation.core.AssociationEnd;
import org.omg.uml.foundation.core.Classifier;
import org.omg.uml.foundation.core.Dependency;
import org.omg.uml.foundation.core.GeneralizableElement;
import org.omg.uml.foundation.core.Generalization;
import org.omg.uml.foundation.core.ModelElement;
import org.omg.uml.foundation.core.Relationship;
import org.omg.uml.foundation.core.UmlAssociation;
import org.omg.uml.foundation.datatypes.Multiplicity;
import org.omg.uml.foundation.datatypes.MultiplicityRange;

import util.Constantes;
import vista.explorador.NodoExplorador;

/**
 * The Class RelacionUtil.
 * 
 * @author Juan Timoteo Ponce Ortiz
 */
public final class RelacionUtil {

	/** The Constant FIN1. */
	public static final int FIN1 = 0;

	/** The Constant FIN2. */
	public static final int FIN2 = 1;

	/** The Constant CUALQUIERA. */
	public static final int CUALQUIERA = 2;

	/**
	 * Gets the all relaciones.
	 * 
	 * @return the all relaciones
	 */
	public static List<Relationship> getAllRelaciones() {
		final List<Relationship> out = new ArrayList<Relationship>();
		final RefClass rc = RepositorioImpl.getInstance().getUmlPackage()
				.refPackage(Constantes.UML_CORE).refClass(
						Constantes.UML_RELATIONSHIP);

		for (Iterator<RefObject> it = rc.refAllOfType().iterator(); it
				.hasNext();) {
			final RefObject ro = it.next();
			if (ro instanceof Relationship) {
				out.add((Relationship) ro);
			}
		}
		return out;
	}

	/**
	 * Relacion contiene.
	 * 
	 * @param rel
	 *            the rel
	 * @param me
	 *            the me
	 * @param lado
	 *            the lado
	 * 
	 * @return true, if relacion contiene
	 */
	public static boolean relacionContiene(final Relationship rel,
			final ModelElement me, final int lado) {
		if (rel instanceof UmlAssociation) {
			final UmlAssociation umlAssociation = (UmlAssociation) rel;
			final Iterator<AssociationEnd> ends = umlAssociation
					.getConnection().iterator();

			AssociationEnd ae1 = null;
			AssociationEnd ae2 = null;
			if (ends.hasNext()) {
				ae1 = ends.next();
			}
			if (ends.hasNext()) {
				ae2 = ends.next();
			}
			if (ae1 != null && ae2 != null) {
				final Classifier classifier1 = ae1.getParticipant();
				final Classifier classifier2 = ae2.getParticipant();

				if (lado == CUALQUIERA
						&& (classifier1.equals(me) || classifier2.equals(me))) {
					return true;
				} else if (lado == FIN1 && (classifier1.equals(me))) {
					return true;
				} else if (lado == FIN2 && (classifier2.equals(me))) {
					return true;
				}
			}
		} else if (rel instanceof Dependency) {
			final Dependency dep = (Dependency) rel;

			for (Iterator<ModelElement> iClient = dep.getClient().iterator(); iClient
					.hasNext();) {
				final ModelElement client = iClient.next();

				if (lado == CUALQUIERA || lado == FIN1) {
					if (client.equals(me)) {
						return true;
					}
				}

			}
			
			for (Iterator<ModelElement> iSupplier = dep.getSupplier().iterator(); iSupplier.hasNext();) {
				final ModelElement supplier = iSupplier.next();
				if (lado == CUALQUIERA || lado == FIN2) {
					if (supplier.equals(me)) {
						return true;
					}
				}				
			}
		} else if (rel instanceof Generalization) {
			final Generalization generalization = (Generalization) rel;
			
			if (lado == CUALQUIERA
					&& (me.equals(generalization.getChild()) || me
							.equals(generalization.getParent()))) {
				return true;
			} else if (lado == FIN1 && me.equals(generalization.getChild())) {
				return true;
			} else if (lado == FIN2 && me.equals(generalization.getParent())) {
				return true;
			}
		}
		return false;

	}

	/**
	 * Adds the nodos relacion.
	 * 
	 * @param relNode
	 *            the rel node
	 */
	public static void addNodosRelacion(final NodoExplorador relNode) {
		try {
			final ModelElement me = relNode.getElemento();
			
			if (me != null && me instanceof Relationship) {
				final Relationship rel = (Relationship) me;
				
				if (rel instanceof UmlAssociation) {
					final UmlAssociation ass = (UmlAssociation) rel;
					
					for (Iterator<AssociationEnd> ends = ass.getConnection().iterator(); ends.hasNext();) {
						final AssociationEnd ae = ends.next();
						final Classifier classifier = ae.getParticipant();
						final Multiplicity multiplicity = ae.getMultiplicity();
						
						String sRange = "";
						if (multiplicity != null
								&& multiplicity.getRange() != null) {
							
							for (Iterator<MultiplicityRange> ranges = multiplicity.getRange().iterator(); ranges.hasNext();) {
								final MultiplicityRange mr =  ranges.next();
								final String sLower = (mr.getLower() == -1) ? "*"
										: Integer.toString(mr.getLower());
								final String sUpper = (mr.getUpper() == -1) ? "*"
										: Integer.toString(mr.getUpper());
								sRange = sLower + ".." + sUpper;								
							}							
						}
						final NodoExplorador endNode = new NodoExplorador(
								sRange + " " + classifier.getName(),
								classifier, relNode.getPadre(), true);
						relNode.add(endNode);						
					}					
					
				} else if (rel instanceof Generalization) {
					final Generalization gen = (Generalization) rel;
					final GeneralizableElement geChild = gen.getChild();
					final GeneralizableElement geParent = gen.getParent();
					final NodoExplorador childNode = new NodoExplorador(
							"Hijo: " + geChild.getName(), geChild, relNode
									.getPadre(), true);
					final NodoExplorador parentNode = new NodoExplorador(
							"Padre: " + geParent.getName(), geParent, relNode
									.getPadre(), true);
					relNode.add(childNode);
					relNode.add(parentNode);
				} else if (rel instanceof Dependency) {
					final Dependency dep = (Dependency) rel;
					
					for (Iterator<ModelElement> clients = dep.getClient().iterator(); clients.hasNext();) {					
						final ModelElement client = clients.next();
						final NodoExplorador clientNode = new NodoExplorador("Cliente: " + client.getName(), client, relNode
								.getPadre(), true);
						relNode.add(clientNode);
					}
					
					for (Iterator<ModelElement> suppliers = dep.getSupplier().iterator(); suppliers.hasNext();) {
						final ModelElement supplier = suppliers.next();
						final NodoExplorador supplierNode = new NodoExplorador(
								"Proveedor: " + supplier.getName(), supplier,
						relNode.getPadre(), true);
						relNode.add(supplierNode);
					}
				}
			}
		} catch (NullPointerException npe) {
		}
	}

	/**
	 * Checks if is one to one.
	 * 
	 * @param association
	 *            the association
	 * 
	 * @return true, if is one to one
	 */
	public static boolean isOneToOne(final UmlAssociation association) {
		final AssociationEnd source = (AssociationEnd) association
				.getConnection().get(0);
		final AssociationEnd dest = (AssociationEnd) association
				.getConnection().get(1);
		final Multiplicity sourceMult = source.getMultiplicity();
		final Multiplicity destMult = dest.getMultiplicity();
		return (MultiplicidadUtil.isOne(sourceMult) && MultiplicidadUtil
				.isOne(destMult));
	}

	/**
	 * Checks if is one to many.
	 * 
	 * @param association
	 *            the association
	 * 
	 * @return true, if is one to many
	 */
	public static boolean isOneToMany(final UmlAssociation association) {
		final AssociationEnd source = (AssociationEnd) association
				.getConnection().get(0);
		final AssociationEnd dest = (AssociationEnd) association
				.getConnection().get(1);
		final Multiplicity sourceMult = source.getMultiplicity();
		final Multiplicity destMult = dest.getMultiplicity();
		return (MultiplicidadUtil.isOne(sourceMult) && MultiplicidadUtil
				.isMany(destMult));
	}

	/**
	 * Checks if is many to one.
	 * 
	 * @param association
	 *            the association
	 * 
	 * @return true, if is many to one
	 */
	public static boolean isManyToOne(final UmlAssociation association) {
		final AssociationEnd source = (AssociationEnd) association
				.getConnection().get(0);
		final AssociationEnd dest = (AssociationEnd) association
				.getConnection().get(1);
		final Multiplicity sourceMult = source.getMultiplicity();
		final Multiplicity destMult = dest.getMultiplicity();
		return (MultiplicidadUtil.isMany(sourceMult) && MultiplicidadUtil
				.isOne(destMult));
	}

	/**
	 * Checks if is many to many.
	 * 
	 * @param association
	 *            the association
	 * 
	 * @return true, if is many to many
	 */
	public static boolean isManyToMany(final UmlAssociation association) {
		final AssociationEnd source = (AssociationEnd) association
				.getConnection().get(0);
		final AssociationEnd dest = (AssociationEnd) association
				.getConnection().get(1);
		final Multiplicity sourceMult = source.getMultiplicity();
		final Multiplicity destMult = dest.getMultiplicity();
		return (MultiplicidadUtil.isMany(sourceMult) && MultiplicidadUtil
				.isMany(destMult));
	}

	/**
	 * Gets the association participant.
	 * 
	 * @param relation
	 *            the relation
	 * @param index
	 *            the index
	 * 
	 * @return the association participant
	 */
	public static ModelElement getAssociationParticipant(
			final Relationship relation, final int index) {

		if (relation instanceof UmlAssociation) {
			final UmlAssociation association = (UmlAssociation) relation;
			final AssociationEnd endSource = (AssociationEnd) association
					.getConnection().get(index);
			return endSource.getParticipant();
		} else if (relation instanceof Generalization) {
			final Generalization generalization = (Generalization) relation;
			if (index == 0)
				return generalization.getChild();
			else
				return generalization.getParent();
		} else if (relation instanceof Dependency) {
			final Dependency dependency = (Dependency) relation;
			if (index == 0)
				return (ModelElement) dependency.getClient().iterator().next();
			else
				return (ModelElement) dependency.getSupplier().iterator()
						.next();
		}
		return null;
	}

	/**
	 * Gets the association name.
	 * 
	 * @param relation
	 *            the relation
	 * @param index
	 *            the index
	 * 
	 * @return the association name
	 */
	public static String getAssociationName(final Relationship relation,
			final int index) {
		final UmlAssociation association = (UmlAssociation) relation;
		final AssociationEnd endSource = (AssociationEnd) association
				.getConnection().get(index);
		return endSource.getName();
	}

}
