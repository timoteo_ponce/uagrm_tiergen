/*
 * UMLUtil.java
 *
 * Created on 20 de octubre de 2008, 23:46
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package util.uml;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import modelo.repositorio.Repositorio;
import modelo.repositorio.RepositorioImpl;

import org.apache.log4j.Logger;
import org.omg.uml.foundation.core.Abstraction;
import org.omg.uml.foundation.core.AssociationClass;
import org.omg.uml.foundation.core.AssociationEnd;
import org.omg.uml.foundation.core.AssociationEndClass;
import org.omg.uml.foundation.core.Attribute;
import org.omg.uml.foundation.core.AttributeClass;
import org.omg.uml.foundation.core.Classifier;
import org.omg.uml.foundation.core.ClassifierClass;
import org.omg.uml.foundation.core.DataType;
import org.omg.uml.foundation.core.DataTypeClass;
import org.omg.uml.foundation.core.Generalization;
import org.omg.uml.foundation.core.Interface;
import org.omg.uml.foundation.core.InterfaceClass;
import org.omg.uml.foundation.core.Method;
import org.omg.uml.foundation.core.MethodClass;
import org.omg.uml.foundation.core.ModelElement;
import org.omg.uml.foundation.core.Namespace;
import org.omg.uml.foundation.core.Operation;
import org.omg.uml.foundation.core.OperationClass;
import org.omg.uml.foundation.core.Parameter;
import org.omg.uml.foundation.core.ParameterClass;
import org.omg.uml.foundation.core.Relationship;
import org.omg.uml.foundation.core.Stereotype;
import org.omg.uml.foundation.core.StereotypeClass;
import org.omg.uml.foundation.core.UmlAssociation;
import org.omg.uml.foundation.core.UmlAssociationClass;
import org.omg.uml.foundation.core.UmlClass;
import org.omg.uml.foundation.core.UmlClassClass;
import org.omg.uml.foundation.datatypes.CallConcurrencyKindEnum;
import org.omg.uml.foundation.datatypes.ChangeableKindEnum;
import org.omg.uml.foundation.datatypes.Expression;
import org.omg.uml.foundation.datatypes.ExpressionClass;
import org.omg.uml.foundation.datatypes.Multiplicity;
import org.omg.uml.foundation.datatypes.MultiplicityClass;
import org.omg.uml.foundation.datatypes.MultiplicityRange;
import org.omg.uml.foundation.datatypes.MultiplicityRangeClass;
import org.omg.uml.foundation.datatypes.OrderingKindEnum;
import org.omg.uml.foundation.datatypes.ParameterDirectionKindEnum;
import org.omg.uml.foundation.datatypes.ProcedureExpression;
import org.omg.uml.foundation.datatypes.ProcedureExpressionClass;
import org.omg.uml.foundation.datatypes.ScopeKindEnum;
import org.omg.uml.foundation.datatypes.VisibilityKindEnum;
import org.omg.uml.modelmanagement.Model;
import org.omg.uml.modelmanagement.ModelClass;
import org.omg.uml.modelmanagement.UmlPackage;
import org.omg.uml.modelmanagement.UmlPackageClass;
import org.uml.diagrammanagement.Diagram;
import org.uml.diagrammanagement.DiagramClass;
import org.uml.diagrammanagement.GraphEdge;
import org.uml.diagrammanagement.GraphEdgeClass;
import org.uml.diagrammanagement.GraphNode;
import org.uml.diagrammanagement.GraphNodeClass;
import org.uml.diagrammanagement.Property;
import org.uml.diagrammanagement.PropertyClass;
import org.uml.diagrammanagement.Uml1SemanticModelBridge;
import org.uml.diagrammanagement.Uml1SemanticModelBridgeClass;
import org.uml.diagrammanagement.Vertex;
import org.uml.diagrammanagement.VertexClass;

import util.AppUtil;
import util.Constantes;
import vista.graficador.GrDiagrama;
import vista.graficador.GrLineaVisual;
import vista.graficador.GrNodoVisual;
import vista.graficador.uml.GrAbstraccion;
import vista.graficador.uml.GrAsociacion;
import vista.graficador.uml.GrClase;
import vista.graficador.uml.GrClaseAsociacion;
import vista.graficador.uml.GrDependencia;
import vista.graficador.uml.GrGeneralizacion;
import vista.graficador.uml.GrInterfaz;
import vista.graficador.uml.GrPaquete;

/**
 * The Class UMLUtil.
 * 
 * @author Juan Timoteo Ponce Ortiz
 */
public final class UMLUtil {

	private static Logger log = Logger.getLogger(UMLUtil.class);

	/** The Constant repository. */
	private static final Repositorio repository = RepositorioImpl.getInstance();

	/**
	 * Crear modelo.
	 * 
	 * @param nombre
	 *            the nombre
	 * 
	 * @return the model
	 */
	public static Model crearModelo(final String nombre) {
		final ModelClass aux = repository.getUmlPackage().getModelManagement()
		.getModel();
		return aux.createModel(nombre, VisibilityKindEnum.VK_PUBLIC, false,
				false, false, false);
	}

	/**
	 * Crear paquete.
	 * 
	 * @param nombre
	 *            the nombre
	 * 
	 * @return the uml package
	 */
	public static org.omg.uml.modelmanagement.UmlPackage crearPaquete(
			final String nombre) {
		final UmlPackageClass aux = repository.getUmlPackage()
		.getModelManagement().getUmlPackage();
		return aux.createUmlPackage(nombre, VisibilityKindEnum.VK_PUBLIC,
				false, false, false, false);
	}

	/**
	 * Crear diagrama.
	 * 
	 * @param nombre
	 *            the nombre
	 * 
	 * @return the diagram
	 */
	public static Diagram crearDiagrama(final String nombre) {
		final DiagramClass aux = repository.getUmlPackage()
		.getDiagramManagement().getDiagram();
		return aux.createDiagram(nombre);
	}

	/**
	 * Crear puente semantico.
	 * 
	 * @param elemento
	 *            the elemento
	 * 
	 * @return the uml1 semantic model bridge
	 */
	public static Uml1SemanticModelBridge crearPuenteSemantico(
			final ModelElement elemento) {
		final Uml1SemanticModelBridgeClass aux = repository.getUmlPackage()
		.getDiagramManagement().getUml1SemanticModelBridge();
		final Uml1SemanticModelBridge out = aux.createUml1SemanticModelBridge();
		out.setElement(elemento);
		return out;
	}

	/**
	 * Crear propiedad.
	 * 
	 * @param llave
	 *            the llave
	 * @param valor
	 *            the valor
	 * 
	 * @return the property
	 */
	public static Property crearPropiedad(final String llave, final String valor) {
		final PropertyClass aux = repository.getUmlPackage()
		.getDiagramManagement().getProperty();
		final Property out = aux.createProperty(llave, valor);
		return out;
	}

	/**
	 * Crear clase.
	 * 
	 * @param nombre
	 *            the nombre
	 * 
	 * @return the uml class
	 */
	public static UmlClass crearClase(final String nombre) {
		final UmlClassClass aux = repository.getUmlPackage().getCore()
		.getUmlClass();
		return aux.createUmlClass(nombre, VisibilityKindEnum.VK_PUBLIC, false,
				false, false, false, false);
	}

	/**
	 * Crear interfaz.
	 * 
	 * @param nombre
	 *            the nombre
	 * 
	 * @return the interface
	 */
	public static Interface crearInterfaz(final String nombre) {
		final InterfaceClass aux = repository.getUmlPackage().getCore()
		.getInterface();
		return aux.createInterface(nombre, VisibilityKindEnum.VK_PUBLIC, false,
				false, false, false);
	}

	/**
	 * Crear atributo.
	 * 
	 * @param nombre
	 *            the nombre
	 * 
	 * @return the attribute
	 */
	public static Attribute crearAtributo(final String nombre) {
		final AttributeClass aux = repository.getUmlPackage().getCore()
		.getAttribute();
		return aux.createAttribute(nombre, VisibilityKindEnum.VK_PRIVATE,
				false, ScopeKindEnum.SK_INSTANCE, null,
				ChangeableKindEnum.CK_CHANGEABLE, ScopeKindEnum.SK_INSTANCE,
				OrderingKindEnum.OK_UNORDERED, null);
	}

	/**
	 * Crear operacion.
	 * 
	 * @param nombre
	 *            the nombre
	 * 
	 * @return the operation
	 */
	public static Operation crearOperacion(final String nombre) {
		final OperationClass aux = repository.getUmlPackage().getCore()
		.getOperation();
		return aux
		.createOperation(nombre, VisibilityKindEnum.VK_PUBLIC, false,
				ScopeKindEnum.SK_CLASSIFIER, false,
				CallConcurrencyKindEnum.CCK_SEQUENTIAL, false, false,
				false, "");
	}

	/**
	 * A cadena.
	 * 
	 * @param par
	 *            the par
	 * 
	 * @return the string
	 */
	public static String toString(final Parameter par) {
		final Classifier tipo = par.getType();
		String out = null;
		if (tipo != null) {
			out = tipo.getName();
		}
		out = par.getName() + " : " + out;
		if (par.getKind() == ParameterDirectionKindEnum.PDK_IN) {
			out += " -> Entrada";
		} else if (par.getKind() == ParameterDirectionKindEnum.PDK_OUT) {
			out += " -> Salida";
		} else if (par.getKind() == ParameterDirectionKindEnum.PDK_INOUT) {
			out += " -> Entrada/Salida";
		} else if (par.getKind() == ParameterDirectionKindEnum.PDK_RETURN) {
			out += " -> Retorno";
		}
		return out;
	}

	/**
	 * Gets the all clasificadores.
	 * 
	 * @return the all clasificadores
	 */
	@SuppressWarnings("unchecked")
	public static Collection<Classifier> getAllClasificadores() {
		final ClassifierClass aux = repository.getUmlPackage().getCore()
		.getClassifier();
		return aux.refAllOfType();
	}

	/**
	 * Gets the all tipos.
	 * 
	 * @return the all tipos
	 */
	@SuppressWarnings("unchecked")
	public static Collection<DataType> getAllTipos() {
		final DataTypeClass aux = repository.getUmlPackage().getCore()
		.getDataType();
		return aux.refAllOfType();
	}

	@SuppressWarnings("unchecked")
	public static Collection<Stereotype> getAllStereotypes() {
		final StereotypeClass aux = repository.getUmlPackage().getCore()
		.getStereotype();
		return aux.refAllOfType();
	}

	/**
	 * Crear tipo dato.
	 * 
	 * @param nombre
	 *            the nombre
	 * 
	 * @return the data type
	 */
	public static DataType crearTipoDato(final String nombre) {
		final DataTypeClass aux = repository.getUmlPackage().getCore()
		.getDataType();
		return aux.createDataType(nombre, null, false, false, false, false);
	}

	/**
	 * Existe en modelo.
	 * 
	 * @param elemento
	 *            the elemento
	 * @param model
	 *            the model
	 * 
	 * @return true, if existe en modelo
	 */
	public static boolean existeEnModelo(final ModelElement elemento,
			final Model model) {
		for (final Object elem : model.getOwnedElement()) {
			log.info(elem.getClass().toString() + "; " + elemento.getClass());
			if (elemento.getClass().equals(elem.getClass())) {
				return (elemento.getClass().equals(elem.getClass()));
			} else if (elem instanceof org.omg.uml.modelmanagement.UmlPackage) {
				return existeEnPaquete(elemento, (UmlPackage) elem);
			}
		}
		return false;
	}

	/**
	 * Existe en paquete.
	 * 
	 * @param elemento
	 *            the elemento
	 * @param model
	 *            the model
	 * 
	 * @return true, if existe en paquete
	 */
	public static boolean existeEnPaquete(final ModelElement elemento,
			final org.omg.uml.modelmanagement.UmlPackage model) {
		for (final Object elem : model.getOwnedElement()) {
			log.info(elem.getClass().toString() + "; " + elemento.getClass());
			if (elemento.getClass().equals(elem.getClass())) {
				return (elemento.getClass().equals(elem.getClass()));
			} else if (elem instanceof org.omg.uml.modelmanagement.UmlPackage) {
				return existeEnPaquete(elemento, (UmlPackage) elem);
			}
		}
		return false;
	}

	/*
	 * public static void main(String[] args){ String str = new String("");
	 * System.out.println( str.getClass().toString()); }
	 */

	/**
	 * Crear multiplicidad.
	 * 
	 * @return the multiplicity
	 */
	public static Multiplicity crearMultiplicidad() {
		final MultiplicityClass aux = repository.getUmlPackage().getDataTypes()
		.getMultiplicity();
		return aux.createMultiplicity();
	}

	/**
	 * Crear rango multiplicidad.
	 * 
	 * @param par1
	 *            the par1
	 * @param par2
	 *            the par2
	 * 
	 * @return the multiplicity range
	 */
	public static MultiplicityRange crearRangoMultiplicidad(final int par1,
			final int par2) {
		final MultiplicityRangeClass aux = repository.getUmlPackage()
		.getDataTypes().getMultiplicityRange();
		return aux.createMultiplicityRange(par1, par2);
	}

	/**
	 * Crear expresion.
	 * 
	 * @return the expression
	 */
	public static Expression crearExpresion() {
		final ExpressionClass aux = repository.getUmlPackage().getDataTypes()
		.getExpression();
		return aux.createExpression();
	}

	/**
	 * Crear tipo dato.
	 * 
	 * @return the data type
	 */
	public static DataType crearTipoDato() {
		final DataTypeClass aux = repository.getUmlPackage().getCore()
		.getDataType();
		return aux.createDataType();
	}

	/**
	 * Gets the all instancias.
	 * 
	 * @param namespace
	 *            the namespace
	 * @param clase
	 *            the clase
	 * 
	 * @return the all instancias
	 */
	@SuppressWarnings("unchecked")
	public static Collection<ModelElement> getAllInstancias(
			final Namespace namespace, final String clase) {
		final List<ModelElement> instancias = new ArrayList<ModelElement>();
		try {
			final Class<?> c = Class.forName(clase);
			final Iterator<ModelElement> it = namespace.getOwnedElement()
			.iterator();

			while (it.hasNext()) {
				final ModelElement me = it.next();

				if (c.isInstance(me)) {
					instancias.add(me);
				}
				if (me instanceof Namespace) {
					instancias.addAll(getAllInstancias((Namespace) me, clase));
				}
			}

		} catch (ClassNotFoundException e) {
			log.error("Class doesn't exists " + e);
		}
		return instancias;
	}

	/**
	 * Crear metodo.
	 * 
	 * @return the method
	 */
	public static Method crearMetodo() {
		final MethodClass aux = repository.getUmlPackage().getCore()
		.getMethod();
		return aux.createMethod();
	}

	/**
	 * Crear proceso expr.
	 * 
	 * @return the procedure expression
	 */
	public static ProcedureExpression crearProcesoExpr() {
		final ProcedureExpressionClass aux = repository.getUmlPackage()
		.getDataTypes().getProcedureExpression();
		return aux.createProcedureExpression();
	}

	/**
	 * Crear parametro.
	 * 
	 * @param nombre
	 *            the nombre
	 * 
	 * @return the parameter
	 */
	public static Parameter crearParametro(final String nombre) {
		final ParameterClass aux = repository.getUmlPackage().getCore()
		.getParameter();
		return aux.createParameter(nombre, VisibilityKindEnum.VK_PUBLIC, false,
				null, ParameterDirectionKindEnum.PDK_IN);
	}

	/**
	 * Crear asociacion end.
	 * 
	 * @return the association end
	 */
	public static AssociationEnd crearAsociacionEnd() {
		final AssociationEndClass aux = repository.getUmlPackage().getCore()
		.getAssociationEnd();
		return aux.createAssociationEnd();
	}

	/**
	 * Crear graph node.
	 * 
	 * @return the graph node
	 */
	public static GraphNode crearGraphNode() {
		final GraphNodeClass aux = repository.getUmlPackage()
		.getDiagramManagement().getGraphNode();
		return aux.createGraphNode();
	}

	/**
	 * Crear graph edge.
	 * 
	 * @return the graph edge
	 */
	public static GraphEdge crearGraphEdge() {
		final GraphEdgeClass aux = repository.getUmlPackage()
		.getDiagramManagement().getGraphEdge();
		return aux.createGraphEdge();
	}

	/**
	 * Crear vertice.
	 * 
	 * @return the vertex
	 */
	public static Vertex crearVertice() {
		final VertexClass aux = repository.getUmlPackage()
		.getDiagramManagement().getVertex();
		return aux.createVertex();
	}

	/**
	 * Crear estereotipo.
	 * 
	 * @param nombre
	 *            the nombre
	 * 
	 * @return the stereotype
	 */
	public static Stereotype crearEstereotipo(final String nombre) {
		final StereotypeClass aux = repository.getUmlPackage().getCore()
		.getStereotype();
		Stereotype out = aux.createStereotype();
		out.setName(nombre);
		return out;
	}

	/**
	 * Crear asociacion.
	 * 
	 * @return the uml association
	 */
	public static UmlAssociation crearAsociacion() {
		final UmlAssociationClass aux = repository.getUmlPackage().getCore()
		.getUmlAssociation();
		return aux.createUmlAssociation();
	}

	/**
	 * Crear graph node.
	 * 
	 * @param elemento
	 *            the elemento
	 * 
	 * @return the graph node
	 */
	@SuppressWarnings("unchecked")
	public static GraphNode crearGraphNode(final ModelElement elemento) {
		final String claseGrafica = getVisualClass(elemento);
		final GraphNode graphNode = crearGraphNode();

		final Property prop = UMLUtil.crearPropiedad(
				Constantes.MODEL_NODE_TYPE, claseGrafica);
		graphNode.getProperty().add(prop);

		final Uml1SemanticModelBridge bridge = UMLUtil
		.crearPuenteSemantico(elemento);
		graphNode.setSemanticModel(bridge);
		return graphNode;
	}

	/**
	 * Crear graph edge.
	 * 
	 * @param element
	 *            the element
	 * 
	 * @return the graph edge
	 */
	@SuppressWarnings("unchecked")
	public static GraphEdge crearGraphEdge(final Relationship element) {
		final GraphEdge graphEdge = crearGraphEdge();
		final String graphClass = getVisualClass(element);

		final Property prop = UMLUtil.crearPropiedad(
				Constantes.MODEL_EDGE_TYPE, graphClass);
		graphEdge.getProperty().add(prop);
		final Uml1SemanticModelBridge bridge = UMLUtil
		.crearPuenteSemantico(element);
		graphEdge.setSemanticModel(bridge);
		return graphEdge;
	}

	/**
	 * Instance gr linea visual.
	 * 
	 * @param diagramer
	 *            the diagramer
	 * @param edge
	 *            the edge
	 * 
	 * @return the gr linea visual
	 */
	public static GrLineaVisual instanceGrLineaVisual(
			final GrDiagrama diagramer, final GraphEdge edge) {
		final Properties props = ElementoVisualUtil.getPropiedades(edge);
		final String tipoLinea = (String) props.get(Constantes.MODEL_EDGE_TYPE);

		try {
			final GrLineaVisual newLinea = (GrLineaVisual) AppUtil
			.classForName(tipoLinea, new Object[] { diagramer, edge });
			return newLinea;
		} catch (ClassNotFoundException ex) {
			log.error("No se pudo hallar la clase para la linea " + ex);
		} catch (IllegalArgumentException ex) {
			log.error(ex.getMessage() + " " + ex);
		} catch (InvocationTargetException ex) {
			log.error(ex.getMessage() + " " + ex);
		} catch (IllegalAccessException ex) {
			log.error(ex.getMessage() + " " + ex);
		} catch (InstantiationException ex) {
			log.error(ex.getMessage() + " " + ex);
		}
		return null;
	}

	/**
	 * Instance gr nodo visual.
	 * 
	 * @param diagramer
	 *            the diagramer
	 * @param node
	 *            the node
	 * 
	 * @return the gr nodo visual
	 */
	public static GrNodoVisual instanceGrNodoVisual(final GrDiagrama diagramer,
			final GraphNode node) {
		final Properties props = ElementoVisualUtil.getPropiedades(node);
		final String type = (String) props.get(Constantes.MODEL_NODE_TYPE);
		try {
			final GrNodoVisual newNode = (GrNodoVisual) AppUtil.classForName(
					type, new Object[] { diagramer, node });
			return newNode;
		} catch (ClassNotFoundException ex) {
			log.error(ex.getMessage() + " " + ex);
		} catch (IllegalArgumentException ex) {
			log.error(ex.getMessage() + " " + ex);
		} catch (InvocationTargetException ex) {
			log.error(ex.getMessage() + " " + ex);
		} catch (IllegalAccessException ex) {
			log.error(ex.getMessage() + " " + ex);
		} catch (InstantiationException ex) {
			log.error(ex.getMessage() + " " + ex);
		}
		return null;
	}

	/**
	 * Gets the visual class.
	 * 
	 * @param element
	 *            the element
	 * 
	 * @return the visual class
	 */
	public static String getVisualClass(final ModelElement element) {
		if (element instanceof UmlClass) {
			return GrClase.class.getName();
		}
		if (element instanceof UmlAssociation) {
			return GrAsociacion.class.getName();
		}
		if (element instanceof Abstraction) {
			return GrAbstraccion.class.getName();
		}
		if (element instanceof AssociationClass) {
			return GrClaseAsociacion.class.getName();
		}
		if (element instanceof org.omg.uml.foundation.core.Dependency) {
			return GrDependencia.class.getName();
		}
		if (element instanceof Generalization) {
			return GrGeneralizacion.class.getName();
		}
		if (element instanceof Interface) {
			return GrInterfaz.class.getName();
		}
		if (element instanceof UmlPackage) {
			return GrPaquete.class.getName();
		}
		return GrClase.class.getName();
	}

}
