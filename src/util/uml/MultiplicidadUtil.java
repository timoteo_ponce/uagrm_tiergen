/*
 * MultiplicidadUtil.java
 *
 * Created on November 2, 2008, 10:33 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package util.uml;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.StringTokenizer;

import org.apache.log4j.Logger;
import org.omg.uml.foundation.datatypes.Multiplicity;
import org.omg.uml.foundation.datatypes.MultiplicityRange;

/**
 * The Class MultiplicidadUtil.
 * 
 * @author Juan Timoteo Ponce Ortiz
 */
public final class MultiplicidadUtil {

	private static final Logger LOG = Logger.getLogger(MultiplicidadUtil.class);

	/** Convierte una mutiplicidad a cadena. */
	public static final int COMA = 0;

	/** The Constant CORCHETES. */
	public static final int CORCHETES = 1;

	/**
	 * Determina si la multiplicidad es equivalente a uno.
	 * 
	 * @param mult
	 *            multiplicidad a verificar
	 * 
	 * @return verdadero si la multiplicidad es equivalente a uno
	 */
	public static boolean isOne(final Multiplicity mult) {
		if (mult == null || mult.getRange() == null)
			return false;
		for (Object object : mult.getRange()) {
			MultiplicityRange rango = (MultiplicityRange) object;
			int bajo = rango.getLower();
			int alto = rango.getUpper();
			if (bajo != 0 && bajo != 1)
				return false;
			if (alto != 0 && alto != 1)
				return false;
		}

		return true;
	}

	/**
	 * Checks if is many.
	 * 
	 * @param mult
	 *            the mult
	 * 
	 * @return true, if is many
	 */
	public static boolean isMany(final Multiplicity mult) {
		if (mult == null || mult.getRange() == null)
			return false;
		for (Object object : mult.getRange()) {
			MultiplicityRange rango = (MultiplicityRange) object;
			int bajo = rango.getLower();
			int alto = rango.getUpper();
			if (bajo != -1)
				return false;
			if (alto != -1)
				return false;
		}

		return true;
	}

	/**
	 * 1 rango[ 0,1 ] * rango[ -1 , -1 ].
	 * 
	 * @param mult
	 *            the mult
	 * @param tipo
	 *            the tipo
	 * 
	 * @return the string
	 */
	public static String toString(final Multiplicity mult, final int tipo) {
		final StringBuilder out = new StringBuilder();

		if (mult == null || mult.getRange() == null)
			return out.toString();
		for (Iterator<MultiplicityRange> it = mult.getRange().iterator(); it
				.hasNext();) {
			final MultiplicityRange rango = it.next();
			int bajo = rango.getLower();
			int alto = rango.getUpper();
			String temp = "";
			if (bajo == alto)
				temp = (bajo == -1 ? "*" : Integer.toString(bajo));
			else {
				temp = (bajo == -1 ? "*" : Integer.toString(bajo));
				temp += "..";
				temp += (alto == -1 ? "*" : Integer.toString(alto));
			}
			if (tipo == CORCHETES)
				out.append("[" + temp + "]");
			else if (tipo == COMA) {
				out.append(temp);
				if (it.hasNext())
					out.append(",");
			}
		}
		return out.toString();
	}

	/**
	 * Convierte una cadena a multiplicidad .
	 * 
	 * @param str
	 *            the string to be parsed
	 * 
	 * @return a multiplicity object for the supplied String.
	 */
	public static Multiplicity toMultiplicity(final String str) {
		boolean sw = false;
		final ArrayList<String> multiplicidades = new ArrayList<String>();
		final StringTokenizer stoken = new StringTokenizer(str, "\\[\\], ");

		while (stoken.hasMoreTokens()) {
			int bajo = -1;
			int alto = -1;
			String actual = stoken.nextToken();

			final StringTokenizer stokenAux = new StringTokenizer(actual, ".");
			String cadBajo = "";
			String cadAlto = "";

			if (stokenAux.hasMoreTokens())
				cadBajo = stokenAux.nextToken().trim();
			if (stokenAux.hasMoreTokens())
				cadAlto = stokenAux.nextToken().trim();

			try {
				if (!cadBajo.equals("*") && !cadBajo.equalsIgnoreCase("n"))
					bajo = Integer.parseInt(cadBajo);
				if (cadAlto.equals(""))
					alto = bajo;
				else if (!cadAlto.equals("*") && !cadAlto.equalsIgnoreCase("n"))
					alto = Integer.parseInt(cadAlto);
				sw = true;

				multiplicidades.add(bajo + "," + alto);

			} catch (NumberFormatException e) {
				LOG.error(e, e);
			}
		}

		if (sw) {
			final Multiplicity out = UMLUtil.crearMultiplicidad();
			out.getRange().clear();

			for (final String elem : multiplicidades) {
				final int bajo = Integer.parseInt(elem.substring(0, elem
						.indexOf(",")));
				final int alto = Integer.parseInt(elem.substring(elem
						.indexOf(",") + 1, elem.length()));
				final MultiplicityRange rango = UMLUtil
						.crearRangoMultiplicidad(bajo, alto);
				out.getRange().add(rango);
			}
			return out;
		}
		return null;
	}

}
