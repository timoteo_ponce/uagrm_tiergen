/*
 * AtributoUtil.java
 *
 * Created on 6 de noviembre de 2008, 0:29
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package util.uml;

import org.omg.uml.foundation.core.Attribute;
import org.omg.uml.foundation.datatypes.Multiplicity;

/**
 * The Class AtributoUtil.
 * 
 * @author Juan Timoteo Ponce Ortiz
 */
public final class AtributoUtil {
    
    /**
     * A cadena.
     * 
     * @param atrib the atrib
     * 
     * @return the string
     */
    public static String toString(final  Attribute atrib ){
        String out = VisibilidadUtil.toString( atrib.getVisibility() ) +  atrib.getName();        
        if( atrib.getType() != null )
            out = out + ":" + atrib.getType().getName();
        final Multiplicity mult = atrib.getMultiplicity();
        if( mult != null )
        	out += MultiplicidadUtil.toString( mult , MultiplicidadUtil.CORCHETES );
        return out;
    }
    
    
}
