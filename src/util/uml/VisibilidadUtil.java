/*
 * VisibilidadUtil.java
 *
 * Created on 14 de noviembre de 2008, 12:23
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
package util.uml;

import org.omg.uml.foundation.datatypes.VisibilityKind;
import org.omg.uml.foundation.datatypes.VisibilityKindEnum;

/**
 * The Class VisibilidadUtil.
 * 
 * @author Juan Timoteo Ponce Ortiz
 */
public final class VisibilidadUtil {

    /**
     * A cadena.
     * 
     * @param vis
     *            the vis
     * 
     * @return the string
     */
    public static String toString(final VisibilityKind vis) {
	if (vis == VisibilityKindEnum.VK_PUBLIC) {
	    return "+";
	}
	if (vis == VisibilityKindEnum.VK_PRIVATE) {
	    return "-";
	}
	if (vis == VisibilityKindEnum.VK_PROTECTED) {
	    return "#";
	}
	if (vis == VisibilityKindEnum.VK_PACKAGE) {
	    return "~";
	}
	return "";
    }

    /**
     * A cadena codigo.
     * 
     * @param vis
     *            the vis
     * 
     * @return the string
     */
    public static String toCodeString(final VisibilityKind vis) {
	if (vis == VisibilityKindEnum.VK_PUBLIC) {
	    return "public";
	}
	if (vis == VisibilityKindEnum.VK_PRIVATE) {
	    return "private";
	}
	if (vis == VisibilityKindEnum.VK_PROTECTED) {
	    return "protected";
	}
	if (vis == VisibilityKindEnum.VK_PACKAGE) {
	    return "package";
	}
	return "";
    }
}
