/*
 * OperacionUtil.java
 *
 * Created on 6 de noviembre de 2008, 0:29
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package util.uml;

import java.util.HashSet;
import java.util.Iterator;

import org.omg.uml.foundation.core.Classifier;
import org.omg.uml.foundation.core.Operation;
import org.omg.uml.foundation.core.Parameter;
import org.omg.uml.foundation.datatypes.ParameterDirectionKindEnum;

/**
 * The Class OperacionUtil.
 * 
 * @author Juan Timoteo Ponce Ortiz
 */
public final class OperacionUtil {

    /**
     * A cadena.
     * 
     * @param oper
     *            the oper
     * 
     * @return the string
     */
    public static String toString(final Operation oper) {
	final StringBuilder out = new StringBuilder();
	out.append(VisibilidadUtil.toString(oper.getVisibility())
		+ oper.getName());
	HashSet<String> returns = new HashSet<String>();
	HashSet<String> pars = new HashSet<String>();

	for (final Iterator<Parameter> it = oper.getParameter().iterator(); it
		.hasNext();) {
	    final Parameter par = it.next();
	    final Classifier classifier = par.getType();
	    String tempTipo = null;
	    if (classifier != null)
		tempTipo = classifier.getName();
	    if (par.getKind() == ParameterDirectionKindEnum.PDK_RETURN)
		returns.add(tempTipo);
	    else
		pars.add(tempTipo);
	}

	out.append("(");
	for (final Iterator<String> it = pars.iterator(); it.hasNext();) {
	    final String tempTipo = it.next();
	    out.append(tempTipo);
	    if (it.hasNext())
		out.append(", ");
	}
	out.append(")");

	if (returns.iterator().hasNext())
	    out.append(": ");
	else
	    out.append(": null");

	for (final Iterator<String> it = returns.iterator(); it.hasNext();) {
	    final String tempTipo = it.next();
	    out.append(tempTipo);
	    if (it.hasNext())
		out.append(", ");
	}
	return out.toString();
    }
}
