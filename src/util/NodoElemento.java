/*
 * NodoElemento.java
 *
 * Created on October 26, 2008, 9:18 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package util;

import org.omg.uml.foundation.core.Attribute;
import org.omg.uml.foundation.core.ModelElement;
import org.omg.uml.foundation.core.Operation;
import org.omg.uml.foundation.core.Parameter;
import org.omg.uml.foundation.core.UmlClass;
import util.uml.AtributoUtil;
import util.uml.OperacionUtil;
import util.uml.UMLUtil;

/**
 * Clase utilizada para ser manipulada por los componentes.
 * 
 * @author Juan Timoteo Ponce Ortiz
 */
public final class NodoElemento {
    private final ModelElement elemento;
    /** Creates a new instance of NodoElemento */
    public NodoElemento(ModelElement elemento) {
        this.elemento = elemento;
    }
    
    /**
     * 
     * @return 
     */
    public String toString() {
        if( elemento instanceof Attribute) {
        	final Attribute atrib = (Attribute)elemento;
            return AtributoUtil.toString( atrib );
        }         
        if( elemento instanceof Operation) {
        	final Operation oper = (Operation)elemento;
            return OperacionUtil.toString( oper );
        } 
        if( elemento instanceof Parameter ) {
        	final Parameter par = (Parameter)elemento;
            return UMLUtil.toString( par );
        }
        if( elemento instanceof UmlClass) {
        	final UmlClass clase = (UmlClass) elemento;
            return clase.getName();
        } 
        if( elemento.getName() != null) {
            return elemento.getName();
        }
        return "Sin nombre";
    }

    /**
     * 
     * @return 
     */
    public ModelElement getElemento() {
        return elemento;
    }

    /**
     * 
     * @param obj 
     * @return 
     */
    public boolean equals(Object obj) {
        if( obj == null || elemento == null )
            return false;
        return this.toString().equals( obj.toString() );
    }    
    
}
