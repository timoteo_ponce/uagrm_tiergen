package util;

import java.awt.Image;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;

/**
 * The Class ImageSelection.
 */
public final class ImageSelection implements Transferable {

    /** The image. */
    private final Image image;

    /**
     * Se guarda la imagen que se le pasa.
     * 
     * @param image
     *            the image
     */
    public ImageSelection(Image image) {
	this.image = image;
    }

    /**
     * Devuelve los DataFlavor soportados, es decir, DataFlavor.imageFlavor
     * 
     * @return the transfer data flavors
     */
    public DataFlavor[] getTransferDataFlavors() {
	return new DataFlavor[] { DataFlavor.imageFlavor };
    }

    /**
     * Devuelve true si el flavor que se le pasa es DataFlavor.imageFlavor
     * 
     * @param flavor
     *            the flavor
     * 
     * @return true, if checks if is data flavor supported
     */
    public boolean isDataFlavorSupported(DataFlavor flavor) {
	return DataFlavor.imageFlavor.equals(flavor);
    }

    /**
     * Devuelve la imagen si el DataFlavor que se le pasa es
     * DataFlavor.imageFlavor
     * 
     * @param flavor
     *            the flavor
     * 
     * @return the transfer data
     * 
     * @throws UnsupportedFlavorException
     *             the unsupported flavor exception
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public Object getTransferData(DataFlavor flavor)
	    throws UnsupportedFlavorException, IOException {
	if (!DataFlavor.imageFlavor.equals(flavor)) {
	    throw new UnsupportedFlavorException(flavor);
	}
	return image;
    }
}