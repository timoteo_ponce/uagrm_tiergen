/*
 * ModeloLista.java
 *
 * Created on 19 de mayo de 2008, 10:24 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package util;

import java.util.ArrayList;
import java.util.List;

import javax.swing.AbstractListModel;

/**
 * Defined list model for UI implementations.
 * 
 * @author Juan TImoteo Ponce Ortiz
 */
public final class ModeloLista<T> extends AbstractListModel {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/** The data. */
	private final List<T> data;

	/**
	 * Instantiates a new modelo lista.
	 */
	public ModeloLista() {
		data = new ArrayList<T>();
	}

	/* (non-Javadoc)
	 * @see javax.swing.ListModel#getSize()
	 */
	public int getSize() {
		return data.size();
	}

	/**
	 * Gets the element at.
	 * 
	 * @param index the index
	 * 
	 * @return the element at
	 */
	public Object getElementAt(int index) {
		return data.get(index);
	}

	/**
	 * Adds the param object to the listmodel.
	 * 
	 * @param obj the obj
	 */
	public void add(T obj) {
		data.add(obj);
		fireContentsChanged(this, 0, data.size() - 1);
	}

	/**
	 * Removes the.
	 * 
	 * @param index the index
	 */
	public void remove(int index) {
		data.remove(index);
		fireContentsChanged(this, 0, data.size() - 1);
	}

	/**
	 * Clear.
	 */
	public void clear() {
		data.clear();
	}

	/**
	 * Sets the.
	 * 
	 * @param index the index
	 * @param object the object
	 */
	public void set(int index, T object) {
		data.set(index, object);
		fireContentsChanged(this, 0, data.size() - 1);
	}

	/**
	 * Adds the all.
	 * 
	 * @param list the list
	 */
	public void addAll(List<T> list) {
		data.addAll(list);
		fireContentsChanged(this, 0, data.size() - 1);
	}

}