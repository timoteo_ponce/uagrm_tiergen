/*
 * ComboNodoElemento.java
 *
 * Created on November 2, 2008, 11:32 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package util;

import java.util.ArrayList;
import javax.swing.JComboBox;

/**
 * The Class ComboGenerico.
 * 
 * @author Juan Timoteo Ponce Ortiz
 */
public final class ComboGenerico<T> extends JComboBox {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/** The elementos. */
	private final ArrayList<T> elementos;

	/**
	 * Creates a new instance of ComboNodoElemento.
	 */
	public ComboGenerico() {
		elementos = new ArrayList<T>();
	}

	/**
	 * Sets the selected.
	 * 
	 * @param ele the ele
	 */
	public void setSelected(T ele) {
		final int index = elementos.lastIndexOf(ele);
		// System.out.println( "setSel " + index );
		if (index != -1)
			setSelectedIndex(index);
	}

	/**
	 * Gets the selected.
	 * 
	 * @return the selected
	 */
	public T getSelected() {
		final int index = getSelectedIndex();
		// System.out.println( "getSel " + index );
		if (index != -1)
			return elementos.get(index);
		return null;
	}

	/**
	 * Gets the item at.
	 * 
	 * @param index the index
	 * 
	 * @return the item at
	 */
	public Object getItemAt(int index) {
		return index < elementos.size() && index > -1 ? elementos.get(index) : null;
	}

}
