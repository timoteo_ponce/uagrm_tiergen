package util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;

/**
 * The Class Configuracion.
 * 
 * @author Juan Timoteo Ponce Ortiz
 */
public final class Configuracion {

	private static final Logger LOG = Logger.getLogger(Configuracion.class);

	/** The app properties. */
	private static Properties appProperties;

	/** The Constant SETTINGS_FILE. */
	private static final String SETTINGS_FILE = "/configuration.properties";

	/**
	 * Gets the param.
	 * 
	 * @param param
	 *            the param
	 * 
	 * @return the param
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static String getParam(final String param) {
		if (appProperties == null) {
			try {
				loadAppPropertyFile();
			} catch (IOException e) {
				LOG.error(e.getMessage(), e);
			}
		}
		String str = null;
		str = appProperties.getProperty(param);
		return str;
	}

	/**
	 * Load app property file.
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	private static void loadAppPropertyFile() throws IOException {
		appProperties = new Properties();
		final InputStream fis = Configuracion.class
		.getResourceAsStream(SETTINGS_FILE);
		appProperties.load(fis);
		fis.close();
	}

	/**
	 * Sets the param.
	 * 
	 * @param propertyName
	 *            the property name
	 * @param propertyValue
	 *            the property value
	 * 
	 * @throws FileNotFoundException
	 *             the file not found exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static void setParam(final String propertyName,
			final String propertyValue) {
		if (appProperties == null) {
			try {
				loadAppPropertyFile();
			} catch (IOException e) {
				LOG.error(e.getMessage(), e);
			}
		}
		appProperties.setProperty(propertyName, propertyValue);
		writeChangesToAppFile();
	}

	/**
	 * Write changes to app file.
	 */
	private static void writeChangesToAppFile() {
		FileOutputStream fos = null;
		try {
			fos = new FileOutputStream(Configuracion.class.getResource(
					SETTINGS_FILE).toURI().getPath());
			appProperties.store(fos, null);
		} catch (Exception e) {
			LOG.error(e, e);
		} finally {
			try {
				if (fos != null) {
					fos.close();
				}
			} catch (IOException e) {
				LOG.error(e.getMessage(), e);
			}
		}
	}

	/**
	 * Load properties.
	 * 
	 * @param path
	 *            the path
	 * 
	 * @return the properties
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static Properties loadProperties(final String path)
	throws IOException {
		final File file = new File(path);
		final BufferedReader reader = new BufferedReader(new FileReader(file));
		final Properties out = new Properties();
		out.load(reader);
		reader.close();

		return out;
	}

	/**
	 * Save properties.
	 * 
	 * @param props
	 *            the props
	 * @param path
	 *            the path
	 */
	public static void saveProperties(final Properties props, final String path) {
		FileOutputStream fos = null;
		try {
			fos = new FileOutputStream(path);
			props.store(fos, null);
		} catch (Exception e) {
			LOG.error(e, e);
		} finally {
			try {
				if (fos != null) {
					fos.close();
				}
			} catch (IOException e) {
				LOG.error(e, e);
			}
		}
	}
}
