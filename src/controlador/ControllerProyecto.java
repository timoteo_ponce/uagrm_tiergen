/*
 * ControllerProyecto.java
 *
 * Created on 23 de febrero de 2009, 23:38
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package controlador;

import modelo.types.PropertyType;


/**
 * Controller class for Proyecto properties changes.
 * 
 * @author Juan Timoteo Ponce Ortiz
 */
public final class ControllerProyecto extends Controller{	
	
	private ControllerProyecto() {
	}
	
	public static ControllerProyecto newInstance(){
		return new ControllerProyecto();
	}
    
    
    /**
     * Change the model.
     * 
     * @param source event source class
     */
    public  void changeModelo(Object source){                
        setModelProperty( PropertyType.MODEL_MODIFIED.toString() , true , source );
    }    
    
    /**
     * Change uml package.
     * 
     * @param newPackage the new package
     * @param source the source
     */
    public  void changeUmlPackage(Object newPackage,Object source){
        setModelProperty( PropertyType.UML_PACKAGE.toString() , newPackage , source );
    }    
    
    /**
     * Change nombre.
     * 
     * @param newNombre the new nombre
     * @param source the source
     */
    public  void changeNombre(String newNombre,Object source){
        setModelProperty( PropertyType.NOMBRE.toString() , newNombre , source );
    }    
    
    /**
     * Change autor.
     * 
     * @param newAutor the new autor
     * @param source the source
     */
    public  void changeAutor(String newAutor,Object source){
        setModelProperty( PropertyType.AUTOR.toString(), newAutor , source );
    }    
    
    /**
     * Change path.
     * 
     * @param newPath the new path
     * @param source the source
     */
    public  void changePath(String newPath,Object source){
        setModelProperty( PropertyType.PATH.toString() , newPath , source );
    }    
    
    /**
     * Change gen path.
     * 
     * @param newGenPath the new gen path
     * @param source the source
     */
    public  void changeGenPath(String newGenPath,Object source){
        //setModelProperty( GENPATH_PROPERTY , newGenPath);
    }    
    
    /**
     * Change template.
     * 
     * @param newTemplate the new template
     * @param source the source
     */
    public  void changeTemplate(String newTemplate,Object source){
        setModelProperty( PropertyType.TEMPLATE.toString() , newTemplate , source );
    }                
    
    /**
     * Change dbms.
     * 
     * @param newDbms the new dbms
     * @param source the source
     */
    public  void changeDbms(String newDbms,Object source){
        setModelProperty( PropertyType.DBMS.toString() , newDbms , source );
    }                
    
    /**
     * Change db name.
     * 
     * @param newDbName the new db name
     * @param source the source
     */
    public  void changeDbName(String newDbName,Object source){
        setModelProperty( PropertyType.DB_NAME.toString() , newDbName , source );
    }                
    
    /**
     * Change db name.
     * 
     * @param version the version
     * @param source the source
     */
    public  void changeDbName(int version,Object source){
        setModelProperty( PropertyType.VERSION.toString() , version , source );
    }                
    
    /**
     * Change modificado.
     * 
     * @param newValue the new value
     * @param source the source
     */
    public  void changeModificado(boolean newValue,Object source){
        setModelProperty( PropertyType.PROJECT_MODIFIED.toString(), newValue , source );
    }    
}
