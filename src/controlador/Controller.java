/*
 * AbstractController.java
 *
 * Created on 23 de febrero de 2009, 23:38
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package controlador;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import modelo.Modelo;
import vista.Vista;

/**
 * Base controller class, control all events using PropertyChangeSupport.
 * 
 * @author Juan Timoteo Ponce Ortiz
 */
public abstract class Controller implements PropertyChangeListener {

	/** The log. */
	private static Logger log = Logger.getLogger(Controller.class);

	// Vectors that hold a list of the registered models and views for this
	// controller.

	/** The registered views. */
	private final List<Vista> registeredViews;
	
	/** The registered models. */
	private final List<Modelo> registeredModels;

	/**
	 * Creates a new instance of Controller.
	 */
	public Controller() {
		registeredViews = new ArrayList<Vista>();
		registeredModels = new ArrayList<Modelo>();
	}

	/**
	 * Binds a model to this controller. Once added, the controller will listen
	 * for all model property changes and propogate them on to registered views.
	 * In addition, it is also responsible for resetting the model properties
	 * when a view changes state.
	 * 
	 * @param model The model to be added
	 */
	public void addModel(Modelo model) {
		registeredModels.add(model);
		model.addPropertyChangeListener(this);
	}

	/**
	 * Unbinds a model from this controller.
	 * 
	 * @param model The model to be removed
	 */
	public void removeModel(Modelo model) {
		registeredModels.remove(model);
		model.removePropertyChangeListener(this);
	}

	/**
	 * Binds a view to this controller. The controller will propogate all model
	 * property changes to each view for consideration.
	 * 
	 * @param view The view to be added
	 */
	public void addView(Vista view) {
		registeredViews.add(view);
	}

	/**
	 * Unbinds a view from this controller.
	 * 
	 * @param view The view to be removed
	 */
	public void removeView(Vista view) {
		registeredViews.remove(view);
	}

	// Used to observe property changes from registered models and propogate
	// them on to all the views.

	/**
	 * This method is used to implement the PropertyChangeListener interface.
	 * Any model changes will be sent to this controller through the use of this
	 * method.
	 * 
	 * @param evt An object that describes the model's property change.
	 */
	public void propertyChange(PropertyChangeEvent evt) {

		for (Vista view : registeredViews) {
			view.modelPropertyChange(evt);
		}
	}
	
	/**
	 * Clear.
	 */
	public void clear(){
		registeredViews.clear();
		registeredModels.clear();
	}

	/**
	 * Convienence method that subclasses can call upon to fire off property
	 * changes back to the models. This method used reflection to inspect each
	 * of the model classes to determine if it is the owner of the property in
	 * question. If it isn't, a NoSuchMethodException is throws (which the
	 * method ignores).
	 * 
	 * @param propertyName The name of the property
	 * @param newValue An object that represents the new value of the property.
	 * @param source the source
	 */
	protected void setModelProperty(String propertyName, Object newValue,
			Object source) {
//		System.out.println(this.getClass().getName() + " trying property set"
//				+ propertyName);
		for (Modelo model : registeredModels) {

			try {
				Method method = model.getClass().getMethod(
						"set" + propertyName,
						new Class[] { newValue.getClass(),
								java.lang.Object.class });
				method.invoke(model, newValue, source);
			} catch (SecurityException e) {
				log.error(e.getMessage(), e);
			} catch (NoSuchMethodException e) {
				log.error(e.getMessage(), e);
			} catch (IllegalArgumentException e) {
				log.error(e.getMessage(), e);
			} catch (IllegalAccessException e) {
				log.error(e.getMessage(), e);
			} catch (InvocationTargetException e) {
				log.error(e.getMessage(), e);
			}

		}
	}

	/**
	 * Gets the model property.
	 * 
	 * @param propertyName the property name
	 * 
	 * @return the model property
	 */
	public Object getModelProperty(String propertyName) {
		for (Modelo model : registeredModels) {

			try {
				Method method = model.getClass().getMethod(
						"get" + propertyName);
				Object obj = method.invoke(model);
				return obj;
			} catch (SecurityException e) {
				log.error(e.getMessage(), e);
			} catch (NoSuchMethodException e) {
				log.error(e.getMessage(), e);
			} catch (IllegalArgumentException e) {
				log.error(e.getMessage(), e);
			} catch (IllegalAccessException e) {
				log.error(e.getMessage(), e);
			} catch (InvocationTargetException e) {
				log.error(e.getMessage(), e);
			}
		}
		return null;
	}

}
