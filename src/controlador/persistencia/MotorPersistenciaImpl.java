/*
 * PersistenciaXMI.java
 *
 * Created on 20 de octubre de 2008, 12:30
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package controlador.persistencia;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InvalidObjectException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.jmi.reflect.RefObject;
import javax.jmi.reflect.RefPackage;
import javax.jmi.xmi.MalformedXMIException;

import modelo.repositorio.RefProvider;
import modelo.repositorio.RefResolver;
import modelo.repositorio.Repositorio;

import org.apache.log4j.Logger;
import org.netbeans.api.xmi.XMIReader;
import org.netbeans.api.xmi.XMIReaderFactory;
import org.netbeans.api.xmi.XMIWriter;
import org.netbeans.api.xmi.XMIWriterFactory;
import org.omg.uml.UmlPackage;
import org.omg.uml.foundation.core.ModelElement;
import org.omg.uml.modelmanagement.Model;

import util.file.TextFileReader;
import util.file.TextFileWriter;

/**
 * TODO falta desacoplar MDR.
 * 
 * @author Juan Timoteo Ponce Ortiz
 */
public final class MotorPersistenciaImpl implements MotorPersistencia {

	/** The log. */
	private static final Logger LOG = Logger
	.getLogger(MotorPersistenciaImpl.class);

	/** The Constant repository. */
	private final Repositorio repository;

	/**
	 * Creates a new instance of PersistenciaXMI.
	 */
	public MotorPersistenciaImpl(final Repositorio repository) {
		this.repository = repository;
	}

	/**
	 * Save.
	 * 
	 * @param file
	 *            the file
	 * @param uml
	 *            the uml
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public void save(File file, final UmlPackage uml) throws IOException {
		LOG.info("Saving model to : " + file.getPath());
		if (!file.getPath().endsWith(".xmi")) {
			file = new File(file.getPath() + ".xmi");
		}

		final File temp = File.createTempFile("_tmp", null);
		final FileOutputStream out = new FileOutputStream(temp);
		final XMIWriter writer = XMIWriterFactory.getDefault()
		.createXMIWriter();
		final RefProvider rp = new RefProvider(repository);

		writer.getConfiguration().setReferenceProvider(rp);
		writer.write(out, uml, null);
		resetSystemDateFromFile(temp, file);
		temp.delete();
	}

	/**
	 * Load.
	 * 
	 * @param file
	 *            the file
	 * 
	 * @return the model
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public synchronized Model load(final File file) throws IOException {
		try {
			LOG.info("Loading model from : " + file.getPath());
			final String uri = file.toURI().toString();
			final XMIReader reader = XMIReaderFactory.getDefault()
			.createXMIReader();
			final RefResolver rr = new RefResolver(
					new RefPackage[] { repository.getUmlPackage() }, reader
					.getConfiguration(), repository);

			reader.getConfiguration().setReferenceResolver(rr);
			reader.read(uri, repository.getUmlPackage());

			return repository.getModelRoot();

		} catch (MalformedXMIException ex) {
			LOG.error(ex);
		}
		return null;
	}

	/**
	 * Resets the System date with an empty String, so that every generated XMI
	 * has the same time stamp. This is necessary because if a file is opened
	 * and saved, even without modifying anything, the contents of the files
	 * will be different, difficulting version control
	 * 
	 * @param temp
	 *            the temporary file where to save the XMI without the
	 *            timestamp.
	 * @param file
	 *            the original file, with the timestamp.
	 */
	private static void resetSystemDateFromFile(final File temp, final File file) {
		try {
			final TextFileReader fr = new TextFileReader(temp);
			final TextFileWriter fw = new TextFileWriter(file);
			String ln = fr.readln();
			while (ln != null) {
				final String replace = ln
				.replaceFirst("timestamp\\s*=\\s*[\"'][^\"']*[\"']",
				"timestamp = ''");
				fw.writeln(replace);
				ln = fr.readln();
			}
		} catch (Exception e) {
			LOG.error("Failed resetting file date", e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see controlador.persistencia.MotorPersistencia#getExtension()
	 */
	public String getExtension() {
		return "xmi";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see controlador.persistencia.MotorPersistencia#export(java.io.File,
	 * org.omg.uml.UmlPackage)
	 */
	public void export(File file, final UmlPackage uml) throws IOException {
		LOG.info("Exporting model to file: " + file.getPath());
		if (!file.getPath().endsWith(".xmi")) {
			file = new File(file.getPath() + ".xmi");
		}

		final File temp = File.createTempFile("_tmp", null);
		final FileOutputStream out = new FileOutputStream(temp);
		final List<Object> arr = new ArrayList<Object>();
		arr.add(repository.getModelRoot());
		final XMIWriter writer = XMIWriterFactory.getDefault()
		.createXMIWriter();
		final RefProvider rp = new RefProvider(repository);

		writer.getConfiguration().setReferenceProvider(rp);
		writer.write(out, arr, null);
		resetSystemDateFromFile(temp, file);
		temp.delete();

	}

	// arreglar este codigo
	/*
	 * (non-Javadoc)
	 * 
	 * @see controlador.persistencia.MotorPersistencia#importar(java.io.File)
	 */
	@SuppressWarnings("unchecked")
	public Model importar(final File file) throws IOException {
		LOG.info("Exporting model from file: " + file.getPath());
		final Model modelRoot = repository.getModelRoot();
		try {
			final List<Object> toDelete = new ArrayList<Object>();
			final List<Object> elements = new ArrayList<Object>();

			final XMIReader reader = XMIReaderFactory.getDefault()
			.createXMIReader();
			final RefResolver rr = new RefResolver(
					new RefPackage[] { repository.getUmlPackage() }, reader
					.getConfiguration(), repository);
			reader.getConfiguration().setReferenceResolver(rr);
			final Collection<RefObject> imported = reader.read(file.toURI()
					.toString(), repository.getUmlPackage());

			for (RefObject refObject : imported) {

				if (refObject instanceof Model) {
					final Model mod = (Model) refObject;

					for (Object nextOwned : mod.getOwnedElement()) {
						final ModelElement temporalModelElement = (ModelElement) nextOwned;
						elements.add(temporalModelElement);
					}
					toDelete.add(mod);
				}
			}

			for (Object nextElement : elements) {
				final ModelElement temporalModelElement = (ModelElement) nextElement;
				temporalModelElement.setNamespace(modelRoot);
				modelRoot.getOwnedElement().add(temporalModelElement);
			}

			for (Object nextToDelete : toDelete) {
				final RefObject temporalRefObject = (RefObject) nextToDelete;
				temporalRefObject.refDelete();
			}
		} catch (MalformedXMIException e) {
			LOG.error(e.getMessage(), e);
			throw new InvalidObjectException("XMI format not supported");
		}
		return modelRoot;
	}

}
