/*
 * MotorPersistencia.java
 *
 * Created on 20 de octubre de 2008, 12:29
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package controlador.persistencia;

import java.io.File;
import java.io.IOException;
import org.omg.uml.UmlPackage;
import org.omg.uml.modelmanagement.Model;

/**
 * Interfaz que debe cumplir toda clase que quiera
 * persistir un proyecto de esta aplicacion
 * @author Juan Timoteo Ponce Ortiz
 */
public interface MotorPersistencia {        
    /**
     * Almacena el contenido de el repositorio, es decir, del paquete
     * UML principal en un archivo.
     * @param file el archivo donde guardara los datos
     * @param uml el paquete principal a almacenar
     * @return estado
     */
    void save( File file,final UmlPackage uml )throws IOException;
    
    /**
     * Cargar el contenido UML a partir de un fichero.
     * 
     * @param file el archivo de donde leer el modelo
     * 
     * @return modelo
     * 
     * @throws IOException Signals that an I/O exception has occurred.
     */
    
    Model load(final File file )throws IOException;
    
    /**
     * Export.
     * 
     * @param file the file
     * @param uml the uml
     * 
     * @throws IOException Signals that an I/O exception has occurred.
     */
    void export(File file,final UmlPackage uml )throws IOException;
    
    /**
     * Importar.
     * 
     * @param file the file
     * 
     * @return the model
     * 
     * @throws IOException Signals that an I/O exception has occurred.
     */
    Model importar(final File file )throws IOException;
    
    /**
     * Retorna la extension que manipula el motor.
     * 
     * @return extension
     */
    String getExtension();
    
    /**
     * Destruir el motor de guardado.
     */
//    public void destroy();
}
