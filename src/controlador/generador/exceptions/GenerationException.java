/**
 * 
 */
package controlador.generador.exceptions;

/**
 * @author Timo
 * 
 */
public class GenerationException extends Exception {
    /**
     * 
     */
    public GenerationException(String message) {
	super(message);
    }
}
