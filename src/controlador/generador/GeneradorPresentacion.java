/*
 * GeneradorPresentacion.java
 *
 * Created on 8 de diciembre de 2008, 23:15
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package controlador.generador;

import java.util.Collection;

import org.omg.uml.foundation.core.Classifier;

import controlador.generador.exceptions.GenerationException;

/**
 * Genera las interfaces para las clases persistentes
 * 
 * @author Juan Timoteo Ponce Ortiz
 */
public interface GeneradorPresentacion extends GeneradorCodigo {

    void generateEntityUI() throws GenerationException;

    /**
     * @param classes
     */
    void generateMainUI(Collection<Classifier> classes)
	    throws GenerationException;
}
