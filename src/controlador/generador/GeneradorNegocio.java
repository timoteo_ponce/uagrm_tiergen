/*
 * GeneradorNegocio.java
 *
 * Created on 8 de diciembre de 2008, 23:15
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package controlador.generador;

import controlador.generador.exceptions.GenerationException;

/**
 * Genera el codigo de las clases de negocio de un diagrama.
 * 
 * @author Juan Timoteo Ponce Ortiz
 */
public interface GeneradorNegocio extends GeneradorCodigo {

    /**
     * Generates the bussiness class
     * 
     * @throws Exception
     *             the exception
     */
    void generarNegocio() throws GenerationException;
}
