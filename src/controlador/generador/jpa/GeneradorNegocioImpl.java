/*
 * GeneradorNegocioImpl.java
 *
 * Created on 8 de diciembre de 2008, 23:19
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package controlador.generador.jpa;

import org.apache.log4j.Logger;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.exception.ParseErrorException;
import org.apache.velocity.exception.ResourceNotFoundException;
import org.omg.uml.foundation.core.Classifier;

import util.Configuracion;
import util.StringUtil;
import util.file.FileOperator;
import controlador.generador.GeneradorNegocio;
import controlador.generador.exceptions.GenerationException;

/**
 * Genera codigo fuente de las clases de negocio para el lenguaje de
 * programacion Java
 * 
 * @author Juan Timoteo Ponce Ortiz
 */
public final class GeneradorNegocioImpl extends GeneradorBasico implements
	GeneradorNegocio {

    private static Logger log = Logger.getLogger(GeneradorNegocioImpl.class);

    /**
     * Creates a new instance of GeneradorNegocioImpl
     * 
     * @param elemento
     * @param path
     */
    public GeneradorNegocioImpl(final Classifier elemento, final String path) {
	super(elemento, path);
    }

    /**
     * Instantiates a new generador negocio impl.
     */
    public GeneradorNegocioImpl() {
	super(null, null);
    }

    /*
     * (non-Javadoc)
     * 
     * @see controlador.generador.GeneradorNegocio#generarNegocio()
     */
    public void generarNegocio() throws GenerationException {
	log.info("Generating bussines class: " + getClase().getName());
	final VelocityContext context = new VelocityContext();

	context.put("current_package", Configuracion
		.getParam("generation.bussiness.pkg"));
	context.put("data_package", Configuracion
		.getParam("generation.data.pkg"));
	context.put("class", getClase());
	context.put("util", new StringUtil());
	try {
	    final Template template = Velocity
		    .getTemplate("templates/jpa/bussiness.vm");
	    FileOperator.mergeTemplate(template, context, getPath() + "/"
		    + getClase().getName() + "Bussiness.java");
	} catch (ResourceNotFoundException e) {
	    log.error(e.getMessage(), e);
	    throw new GenerationException(e.getMessage());
	} catch (ParseErrorException e) {
	    log.error(e.getMessage(), e);
	    throw new GenerationException(e.getMessage());
	} catch (Exception e) {
	    log.error(e.getMessage(), e);
	    throw new GenerationException(e.getMessage());
	}
    }
}
