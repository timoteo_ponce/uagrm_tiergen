/*
 * GeneradorPresentacionImpl.java
 *
 * Created on 8 de diciembre de 2008, 23:19
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package controlador.generador.jpa;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import modelo.types.Field;

import org.apache.log4j.Logger;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.exception.ParseErrorException;
import org.apache.velocity.exception.ResourceNotFoundException;
import org.omg.uml.foundation.core.Attribute;
import org.omg.uml.foundation.core.Classifier;

import util.Configuracion;
import util.StringUtil;
import util.file.FileOperator;
import controlador.generador.GeneradorPresentacion;
import controlador.generador.exceptions.GenerationException;

/**
 * 
 * @author Juan Timoteo Ponce Ortiz
 */
public final class GeneradorPresentacionImpl extends GeneradorBasico implements
	GeneradorPresentacion {

    private static Logger log = Logger
	    .getLogger(GeneradorPresentacionImpl.class);

    /**
     * Creates a new instance of GeneradorPresentacionImpl
     * 
     * @param elemento
     * @param path
     */
    public GeneradorPresentacionImpl(final Classifier elemento,
	    final String path) {
	super(elemento, path);
    }

    /**
     * Instantiates a new generador presentacion impl.
     */
    public GeneradorPresentacionImpl() {
	super(null, null);
    }

    /*
     * (non-Javadoc)
     * 
     * @see controlador.generador.GeneradorPresentacion#generateEntityUI()
     */
    public void generateEntityUI() throws GenerationException {
	final VelocityContext context = new VelocityContext();
	context.put("current_package", Configuracion
		.getParam("generation.view.pkg"));
	context.put("data_package", Configuracion
		.getParam("generation.data.pkg"));
	context.put("class", getClase());
	context.put("column_names", getColumnNames());
	context.put("util", new StringUtil());
	// now, id properties
	List<Properties> fieldsMap = null;

	final Properties idField = super.getIdMap();
	final String isGenerated = idField
		.get(Field.GENERATED_VALUE.toString())
		+ "";

	if (isGenerated != null && isGenerated.isEmpty())
	    fieldsMap = getFieldsMap(null);// si hay un campo generated, hay que
	// obviarlo
	else
	    fieldsMap = getFieldsMap(idField);

	fieldsMap.addAll(super.getInheritedFields());
	context.put("field_list", fieldsMap);
	try {
	    final Template template = Velocity
		    .getTemplate("templates/jpa/entity_panel.vm");
	    FileOperator.mergeTemplate(template, context, getPath() + "/Panel"
		    + getClase().getName() + ".java");
	} catch (ResourceNotFoundException e) {
	    log.error(e.getMessage(), e);
	    throw new GenerationException(e.getMessage());
	} catch (ParseErrorException e) {
	    log.error(e.getMessage(), e);
	    throw new GenerationException(e.getMessage());
	} catch (Exception e) {
	    log.error(e.getMessage(), e);
	    throw new GenerationException(e.getMessage());
	}
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * controlador.generador.GeneradorPresentacion#generateMainUI(java.util.
     * List)
     */
    public void generateMainUI(Collection<Classifier> classes)
	    throws GenerationException {
	VelocityContext context = new VelocityContext();
	context.put("current_package", "presentacion");
	context.put("classes", classes);

	try {
	    Template template = Velocity
		    .getTemplate("templates/jpa/MainFrame.vm");
	    FileOperator.mergeTemplate(template, context, getPath()
		    + "/MainFrame.java");
	} catch (ResourceNotFoundException e) {
	    log.error(e.getMessage(), e);
	    throw new GenerationException(e.getMessage());
	} catch (ParseErrorException e) {
	    log.error(e.getMessage(), e);
	    throw new GenerationException(e.getMessage());
	} catch (Exception e) {
	    log.error(e.getMessage(), e);
	    throw new GenerationException(e.getMessage());
	}
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * controlador.generador.jpa.GeneradorBasico#generarFactory(java.util.Collection
     * )
     */
    @Override
    public void generarFactory(final Collection<Classifier> diagramElements)
	    throws GenerationException {
	generateMainUI(diagramElements);
    }

    /**
     * Gets the column names.
     * 
     * @return the column names
     */
    private String getColumnNames() {
	final StringBuilder columns = new StringBuilder();

	for (final Iterator<Object> it = getClase().getFeature().iterator(); it
		.hasNext();) {
	    final Object feature = it.next();

	    if (feature instanceof Attribute) {
		columns.append('\"' + ((Attribute) feature).getName() + '\"');
		if (it.hasNext())
		    columns.append(",");
	    }
	}
	return columns.toString();
    }

}
