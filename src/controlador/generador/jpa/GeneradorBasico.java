/*
 * GeneradorBasico.java
 *
 * Created on 20 de enero de 2009, 20:33
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
package controlador.generador.jpa;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import modelo.types.Field;
import modelo.types.RelationshipEnd;
import modelo.types.Relationships;

import org.apache.log4j.Logger;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.exception.ParseErrorException;
import org.apache.velocity.exception.ResourceNotFoundException;
import org.omg.uml.foundation.core.Attribute;
import org.omg.uml.foundation.core.Classifier;
import org.omg.uml.foundation.core.Feature;
import org.omg.uml.foundation.core.Generalization;
import org.omg.uml.foundation.core.Relationship;
import org.omg.uml.foundation.core.UmlAssociation;
import org.uml.diagrammanagement.GraphEdge;
import org.uml.diagrammanagement.GraphElement;

import util.Configuracion;
import util.file.FileOperator;
import util.uml.ElementoDeModeloUtil;
import util.uml.ElementoVisualUtil;
import util.uml.RelacionUtil;
import controlador.generador.GeneradorCodigo;
import controlador.generador.exceptions.GenerationException;

/**
 * The Class GeneradorBasico.
 * 
 * @author Juan Timoteo Ponce Ortiz
 */
public class GeneradorBasico implements GeneradorCodigo {

    private static Logger log = Logger.getLogger(GeneradorBasico.class);

    private String path;
    private Classifier elemento;

    /** relationships of the diagram. */
    private final List<Relationship> relaciones;
    protected Properties properties;

    /**
     * Creates a new instance of GeneradorBasico
     */
    public GeneradorBasico(final Classifier elemento, final String path) {
	this.elemento = elemento;
	this.path = path;
	relaciones = new ArrayList<Relationship>();
    }

    /*
     * (non-Javadoc)
     * 
     * @see controlador.generador.GeneradorCodigo#setPath(java.lang.String)
     */
    public void setPath(String path) {
	this.path = path;
    }

    /**
     * Gets the path.
     * 
     * @return the path
     */
    public String getPath() {
	return path;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * controlador.generador.GeneradorCodigo#setClase(org.omg.uml.foundation
     * .core.Classifier)
     */
    public void setClase(final Classifier clase) {
	elemento = clase;
	properties = ElementoVisualUtil.getPropiedades(ElementoVisualUtil
		.getGraphElement(getClase()));
    }

    /**
     * Gets the clase.
     * 
     * @return the clase
     */
    public Classifier getClase() {
	return elemento;
    }

    /**
     * Gets the relaciones.
     * 
     * @return the relaciones
     */
    public List<Relationship> getRelaciones() {
	return relaciones;
    }

    /*
     * (non-Javadoc)
     * 
     * @see controlador.generador.GeneradorCodigo#setRelaciones(java.util.List)
     */
    @Override
    public void setRelaciones(final List<Relationship> relations) {
	relaciones.clear();
	relaciones.addAll(relations);
    }

    /*
     * (non-Javadoc)
     * 
     * @see controlador.generador.GeneradorCodigo#prepare(java.lang.String)
     */
    public void prepare(final String templateName) throws GenerationException {
	try {
	    log.info("Running 'prepare' targe from template : " + templateName);
	    final File destiny = prepareFolders(path + "/build.xml");

	    final VelocityContext context = new VelocityContext();
	    context.put("tiergen_home", FileOperator.getCurrentPath());
	    context.put("project_src_data", Configuracion
		    .getParam("generation.data.pkg"));
	    context.put("project_src_bussiness", Configuracion
		    .getParam("generation.bussiness.pkg"));
	    context.put("project_src_presentation", Configuracion
		    .getParam("generation.view.pkg"));
	    final Template template = Velocity.getTemplate("templates"
		    + File.separator + templateName + File.separator
		    + "build.xml");
	    FileOperator.mergeTemplate(template, context, destiny);

	    // falta ejecutar el prepare dentro del buildfile
	    final Properties properties = new Properties();

	    final String line = Configuracion.getParam("custom_properties");
	    if (line != null && line.trim().length() > 0) {
		final String[] array = line.split(",");
		for (int i = 0; i < array.length; i += 2) {
		    properties.setProperty(array[i], array[i + 1]);
		}
	    }
	    FileOperator.runBuildFile(destiny, "prepare", properties);
	} catch (IOException e) {
	    log.error(e.getMessage(), e);
	    throw new GenerationException(e.getMessage());
	} catch (ParseErrorException e) {
	    log.error(e.getMessage(), e);
	    throw new GenerationException(e.getMessage());
	} catch (ResourceNotFoundException e) {
	    log.error(e.getMessage(), e);
	    throw new GenerationException(e.getMessage());
	} catch (Exception e) {
	    log.error(e.getMessage(), e);
	    throw new GenerationException(e.getMessage());
	}

    }

    private File prepareFolders(String string) throws IOException {
	final File genDir = new File(path);
	if (!genDir.exists()) {
	    if (!genDir.mkdirs()) {
		throw new IOException("Can't create folders");
	    }
	}
	return FileOperator.createOrLoadFile(path + "/build.xml");
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * controlador.generador.GeneradorCodigo#generarFactory(java.util.Collection
     * )
     */
    @SuppressWarnings("unchecked")
    public void generarFactory(Collection<Classifier> diagramElements)
	    throws GenerationException {
    }

    /**
     * Gets the atribute map.
     * 
     * @param attr
     *            the attr
     * 
     * @return the atribute map
     */
    protected Properties getAtributeMap(final Attribute attr) {
	log.info("Preparing map for attribute: " + attr.getName());
	final Properties local_props = ElementoVisualUtil
		.getPropiedades(ElementoVisualUtil.getGraphElement(attr));
	String prop;
	final Properties map = new Properties();

	map.setProperty(Field.NAME.toString(), attr.getName());
	map.setProperty(Field.TYPE.toString(), attr.getType().getName());

	prop = local_props.getProperty(Field.EMBEDDED.toString());
	if (prop != null) {
	    map.setProperty(Field.EMBEDDED.toString(), prop);
	}

	prop = local_props.getProperty(Field.NULLABLE.toString());
	map
		.setProperty(Field.NULLABLE.toString(), ((prop == null || prop
			.isEmpty()) ? Boolean.toString(false) : Boolean
			.toString(true)));

	prop = local_props.getProperty(Field.COLUMN_NAME.toString());
	if (prop != null) {
	    map.setProperty(Field.COLUMN_NAME.toString(), prop);
	}

	prop = local_props.getProperty(Field.TEMPORAL_TYPE.toString());
	if (prop != null) {
	    map.setProperty(Field.TEMPORAL_TYPE.toString(), prop);
	}
	return map;
    }

    /**
     * Gets the id map.
     * 
     * @return the id map
     */
    protected Properties getIdMap() {
	log.info("Preparing map for Id");
	final Properties map = new Properties();
	Object prop = properties.get(Field.ID.toString());

	if (prop != null && !prop.toString().isEmpty()) {
	    log.info("Id: " + prop);
	    final Attribute attr = ElementoDeModeloUtil.getAttributeByName(
		    getClase(), prop.toString());

	    map.setProperty(Field.NAME.toString(), attr.getName());
	    map.setProperty(Field.TYPE.toString(), attr.getType().getName());

	    prop = properties.get(Field.GENERATED_VALUE.toString());
	    if (prop != null) {
		map.setProperty(Field.GENERATED_VALUE.toString(), prop
			.toString());
	    } else {
		map.setProperty(Field.GENERATED_VALUE.toString(), "AUTO");
	    }

	    final Properties local_props = ElementoVisualUtil
		    .getPropiedades(ElementoVisualUtil.getGraphElement(attr));
	    prop = local_props.get(Field.NULLABLE.toString());
	    map.setProperty(Field.NULLABLE.toString(), ((prop == null || prop
		    .toString().isEmpty()) ? Boolean.toString(false) : Boolean
		    .toString(true)));

	    prop = local_props.get(Field.COLUMN_NAME.toString());
	    if (prop != null) {
		map.setProperty(Field.COLUMN_NAME.toString(), prop.toString());
	    }
	} else {
	    log.info("Id is null, creating an empty one");
	    map.setProperty(Field.NAME.toString(), "");
	    map.setProperty(Field.TYPE.toString(), "");
	}
	return map;
    }

    /**
     * Return all fields mapped, less the id field.
     * 
     * @param idMap
     *            the id map
     * 
     * @return the fields map
     */
    protected List<Properties> getFieldsMap(final Map idMap) {
	log.info("Creating list of attributes as maps " + idMap);
	final List<Properties> fieldsMap = new ArrayList<Properties>();
	final Collection<Feature> features = getClase().getFeature();
	String idField = null;

	if (idMap != null) {
	    idField = idMap.get(Field.NAME.toString()) + "";
	}
	for (Object feature : features) {
	    if (feature instanceof Attribute) {
		if (idField == null
			|| !((Attribute) feature).getName().equals(idField)) {
		    fieldsMap.add(getAtributeMap((Attribute) feature));
		}
	    }
	}
	return fieldsMap;
    }

    /**
     * Gets the inherited fields.
     * 
     * @return the inherited fields
     */
    protected List<Properties> getInheritedFields() {
	log.info("Getting inherited fields from relationships ");
	List<Properties> out = new ArrayList<Properties>();

	for (Relationship relationship : relaciones) {
	    if (relationship instanceof Generalization) {
		final Generalization gen = (Generalization) relationship;

		if (gen.getChild() == getClase()) {
		    log.info(gen.getName() + " Child: " + getClase().getName());
		    final Classifier parent = (Classifier) gen.getParent();
		    setClase(parent);
		    final Properties idMap = getIdMap();
		    out = getFieldsMap(idMap);
		    setClase((Classifier) gen.getChild());
		    return out;
		}
	    }
	}
	return out;
    }

    /**
     * Gets the relations map.
     * 
     * @return the relations map
     */
    protected List<Properties> getRelationsMap() {
	log.info("Getting relatioships map");
	final List<Properties> out = new ArrayList<Properties>();

	for (Relationship relation : relaciones) {
	    final GraphEdge edge = (GraphEdge) ElementoVisualUtil
		    .getGraphElement(relation);
	    if (edge != null) {
		if (relation instanceof Generalization) {
		    final Generalization generalization = (Generalization) relation;

		    if (generalization.getChild().equals(elemento)
			    || generalization.getParent().equals(elemento)) {

			final Properties props = ElementoVisualUtil
				.getPropiedades(edge);
			final String temp = props
				.getProperty(Relationships.INHERITANCE_TYPE
					.toString());
			if (temp == null) {
			    props.setProperty(Relationships.INHERITANCE_TYPE
				    .toString(), "");
			}
			props.setProperty(Field.TYPE.toString(),
				Relationships.INHERITANCE.toString());
			props.setProperty(RelationshipEnd.CHILD.toString(),
				generalization.getChild().getName());
			props.setProperty(RelationshipEnd.PARENT.toString(),
				generalization.getParent().getName());
			out.add(props);
		    }
		}
	    }
	}
	return out;
    }

    /**
     * Gets the associations map.
     * 
     * @return the associations map
     */
    protected List<Properties> getAssociationsMap() {
	log.info("Getting associations maps");
	final List<Properties> out = new ArrayList<Properties>();

	for (Relationship relation : relaciones) {
	    if (relation instanceof UmlAssociation) {
		final Classifier end1 = (Classifier) RelacionUtil
			.getAssociationParticipant(relation, 0);
		final Classifier end2 = (Classifier) RelacionUtil
			.getAssociationParticipant(relation, 1);
		String name1 = RelacionUtil.getAssociationName(relation, 0);
		String name2 = RelacionUtil.getAssociationName(relation, 1);

		final boolean isSource = end1.equals(elemento);
		final boolean isDest = end2.equals(elemento);

		final UmlAssociation association = (UmlAssociation) relation;
		if (RelacionUtil.isOneToOne(association)) {
		    if (isSource) {
			log.info(getClase().getName() + " is source");
			final Properties custom = new Properties();// not
			// information
			// needed
			custom.setProperty(Field.TYPE.toString(),
				Relationships.ONE_TO_ONE.toString());
			if (name2 == null || name2.isEmpty())
			    name2 = end2.getName().toLowerCase();
			custom.setProperty(Field.NAME.toString(), name2);
			custom.setProperty(RelationshipEnd.END_TWO.toString(),
				end2.getName());

			addRelationMetadata(custom, association);

			out.add(custom);
		    } else if (isDest) {
			log.info(getClase().getName() + " is destiny");
			final Properties custom = new Properties();// not
			// information
			// needed
			custom.setProperty(Field.TYPE.toString(),
				Relationships.ONE_TO_ONE.toString());
			if (name1 == null || name1.isEmpty())
			    name1 = end1.getName().toLowerCase();
			custom.setProperty(Field.NAME.toString(), name1);
			custom.setProperty(RelationshipEnd.END_ONE.toString(),
				end1.getName());

			addRelationMetadata(custom, association);

			out.add(custom);
		    }
		} else if (RelacionUtil.isOneToMany(association)) {
		    log.info(" relationship one to many");
		    if (isSource) {// is source
			final Properties custom = new Properties();// not
			// information
			// needed
			custom.setProperty(Field.TYPE.toString(),
				Relationships.ONE_TO_MANY.toString());
			if (name2 == null || name2.isEmpty())
			    name2 = end2.getName().toLowerCase();
			custom.setProperty(Field.NAME.toString(), name2);
			custom.setProperty(RelationshipEnd.END_TWO.toString(),
				end2.getName());
			if (name1 == null || name1.isEmpty())
			    name1 = end1.getName().toLowerCase();
			custom.setProperty(RelationshipEnd.END_ONE.toString(),
				name1);

			addRelationMetadata(custom, association);

			out.add(custom);
		    } else if (isDest) {
			final Properties custom = new Properties();// not
			// information
			// needed
			custom.setProperty(Field.TYPE.toString(),
				Relationships.MANY_TO_ONE.toString());
			if (name1 == null || name1.isEmpty())
			    name1 = end1.getName().toLowerCase();
			custom.setProperty(Field.NAME.toString(), name1);
			custom.setProperty(RelationshipEnd.END_TWO.toString(),
				end1.getName());

			addRelationMetadata(custom, association);

			out.add(custom);
		    }
		} else if (RelacionUtil.isManyToMany(association)) {
		    log.info("Relationship is many to many");
		    if (isSource) {
			final Properties custom = new Properties();// not
			// information
			// needed
			custom.setProperty(Field.TYPE.toString(),
				Relationships.MANY_TO_MANY.toString());
			if (name2 == null || name2.isEmpty())
			    name2 = end2.getName().toLowerCase();
			custom.setProperty(Field.NAME.toString(), name2);
			custom.setProperty(RelationshipEnd.END_TWO.toString(),
				end2.getName());

			addRelationMetadata(custom, association);

			out.add(custom);
		    } else if (isDest) {
			final Properties custom = new Properties();// not
			// information
			// needed
			custom.setProperty(Field.TYPE.toString(),
				Relationships.MANY_TO_MANY.toString());
			if (name1 == null || name1.isEmpty())
			    name1 = end1.getName().toLowerCase();
			custom.setProperty(Field.NAME.toString(), name1);
			custom.setProperty(RelationshipEnd.END_TWO.toString(),
				end1.getName());

			addRelationMetadata(custom, association);

			out.add(custom);
		    }
		}
	    }
	}
	return out;
    }

    /**
     * Adds the relation metadata.
     * 
     * @param custom
     *            the custom
     * @param association
     *            the association
     */
    private void addRelationMetadata(Properties custom,
	    UmlAssociation association) {
	final GraphElement graphElement = ElementoVisualUtil
		.getGraphElement(association);
	if (graphElement != null) {
	    final Properties properties = ElementoVisualUtil
		    .getPropiedades(graphElement);
	    String property = (String) properties.get(Relationships.FETCH_TYPE
		    .toString());

	    if (property != null && !property.isEmpty())
		custom.setProperty(Relationships.FETCH_TYPE.toString(),
			property);
	    property = (String) properties.get(Relationships.CASCADE_TYPE
		    .toString());
	    if (property != null && !property.isEmpty())
		custom.setProperty(Relationships.CASCADE_TYPE.toString(),
			property);
	}
    }

}
