/*
 * GeneradorJava.java
 *
 * Created on 8 de diciembre de 2008, 23:18
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
package controlador.generador.jpa;

import java.io.File;
import java.util.Collection;
import java.util.List;
import java.util.Properties;

import modelo.types.Entity;

import org.apache.log4j.Logger;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.exception.ParseErrorException;
import org.apache.velocity.exception.ResourceNotFoundException;
import org.omg.uml.foundation.core.Classifier;

import util.Configuracion;
import util.StringUtil;
import util.file.FileOperator;
import util.uml.VisibilidadUtil;
import controlador.generador.GeneradorDatos;
import controlador.generador.exceptions.GenerationException;

/**
 * Genera el codigo fuente para la capa de datos en el lenguaje de programacion
 * Java.
 * 
 * @author Juan Timoteo Ponce Ortiz
 */
public final class GeneradorDatosImpl extends GeneradorBasico implements
	GeneradorDatos {

    private static Logger log = Logger.getLogger(GeneradorDatosImpl.class);

    private String dbms;
    private String dbName;

    /**
     * Creates a new instance of GeneradorJava
     * 
     * @param elemento
     * @param path
     */
    public GeneradorDatosImpl(final Classifier elemento, final String path) {
	super(elemento, path);
    }

    /**
     * Instantiates a new generador datos impl.
     */
    public GeneradorDatosImpl() {
	super(null, null);
    }

    /*
     * (non-Javadoc)
     * 
     * @see controlador.generador.GeneradorDatos#generarEntidad()
     */
    @Override
    public void generarEntidad() throws GenerationException {
	log.info("Generating entity: " + getClase().getName());

	final VelocityContext context = new VelocityContext();
	// entity properties
	Object prop = properties.get(Entity.ENTITY.toString());
	context.put("entity", (prop != null && !prop.toString().isEmpty()));

	prop = properties.get(Entity.EMBEDDABLE.toString());
	context.put("embeddable", (prop != null && !prop.toString().isEmpty()));

	prop = properties.get(Entity.TABLE_NAME.toString());
	if (prop != null)
	    context.put("table_name", prop.toString());

	context.put("current_package", Configuracion
		.getParam("generation.data.pkg"));
	context.put("class", getClase());
	context.put("util", new StringUtil());
	context.put("visibility", VisibilidadUtil.toCodeString(getClase()
		.getVisibility()));
	// now, id properties
	final Properties idField = super.getIdMap();
	final List<Properties> fieldsMap = super.getFieldsMap(idField);// super.getInheritedFields();

	context.put("id_field", idField);
	context.put("field_list", fieldsMap);
	// relations
	context.put("relations", super.getRelationsMap());
	// associations
	context.put("associations", super.getAssociationsMap());

	try {		
	    Template template = Velocity.getTemplate("templates"
		    + File.separator + "jpa" + File.separator + "entity.vm");
	    FileOperator.mergeTemplate(template, context, getPath()
		    + File.separator + getClase().getName() + ".java");
	} catch (ResourceNotFoundException e) {
	    log.error(e.getMessage(), e);
	    throw new GenerationException(e.getMessage());
	} catch (ParseErrorException e) {
	    log.error(e.getMessage(), e);
	    throw new GenerationException(e.getMessage());
	} catch (Exception e) {
	    log.error(e.getMessage(), e);
	    throw new GenerationException(e.getMessage());
	}
    }

    /*
     * (non-Javadoc)
     * 
     * @see controlador.generador.GeneradorDatos#generarEntityManager()
     */
    @Override
    public void generarEntityManager() throws GenerationException {
	log.info("Generating entity manager");

	final VelocityContext context = new VelocityContext();

	context.put("current_package", Configuracion
		.getParam("generation.data.pkg"));
	context.put("persistence_unit", "defaultPU");
	context.put("util", new StringUtil());
	// context.put("dependencias", new String[]{"uno", "dos", "tres"});
	context.put("visibilidad", VisibilidadUtil.toCodeString(getClase()
		.getVisibility()));
	try {
	    final Template template = Velocity.getTemplate("templates"
		    + File.separator + "jpa" + File.separator
		    + "persistence_manager.vm");
	    FileOperator.mergeTemplate(template, context, getPath()
		    + File.separator + "PersistenceManager.java");
	} catch (ResourceNotFoundException e) {
	    log.error(e.getMessage(), e);
	    throw new GenerationException(e.getMessage());
	} catch (ParseErrorException e) {
	    log.error(e.getMessage(), e);
	    throw new GenerationException(e.getMessage());
	} catch (Exception e) {
	    log.error(e.getMessage(), e);
	    throw new GenerationException(e.getMessage());
	}
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * controlador.generador.jpa.GeneradorBasico#generarFactory(java.util.Collection
     * )
     */
    @Override
    public void generarFactory(final Collection diagramElements)
	    throws GenerationException {
	log.info("Generating factory");
	final VelocityContext context = new VelocityContext();
	context.put(dbms, true);
	context.put("dbName", dbName);
	context.put("classes", diagramElements);
	context.put("data_package", Configuracion
		.getParam("generation.data.pkg"));
	context.put("persistence_unit_name", "defaultPU");

	try {
	    final Template template = Velocity.getTemplate("templates"
		    + File.separator + "jpa" + File.separator
		    + "persistence_unit.vm");
	    FileOperator.mergeTemplate(template, context, getPath()
		    + File.separator + "conf" + File.separator + "META-INF"
		    + File.separator + "persistence.xml");
	} catch (ResourceNotFoundException e) {
	    log.error(e.getMessage(), e);
	    throw new GenerationException(e.getMessage());
	} catch (ParseErrorException e) {
	    log.error(e.getMessage(), e);
	    throw new GenerationException(e.getMessage());
	} catch (Exception e) {
	    log.error(e.getMessage(), e);
	    throw new GenerationException(e.getMessage());
	}
    }

    /*
     * (non-Javadoc)
     * 
     * @see controlador.generador.GeneradorDatos#setDbms(java.lang.String)
     */
    @Override
    public void setDbms(final String dbms) {
	this.dbms = dbms;
    }

    /*
     * (non-Javadoc)
     * 
     * @see controlador.generador.GeneradorDatos#setDbName(java.lang.String)
     */
    @Override
    public void setDbName(final String dbName) {
	this.dbName = dbName;
    }
}
