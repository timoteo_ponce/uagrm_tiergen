/*
 * GeneradorDatos.java
 *
 * Created on 8 de diciembre de 2008, 23:15
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
package controlador.generador;

import org.apache.velocity.exception.ParseErrorException;
import org.apache.velocity.exception.ResourceNotFoundException;

import controlador.generador.exceptions.GenerationException;

/**
 * Encargado de generar las clases de la capa de datos, segun el patron DAO.
 * 
 * @author Juan Timoteo Ponce Ortiz
 */
public interface GeneradorDatos extends GeneradorCodigo {

    /**
     * Generates an entity from design model, using the class field.
     * 
     * @throws ResourceNotFoundException
     *             the resource not found exception
     * @throws ParseErrorException
     *             the parse error exception
     * @throws Exception
     *             the exception
     */
    void generarEntidad() throws GenerationException;

    /**
     * Generates entity manager to manipulate all entities.
     * 
     * @throws ResourceNotFoundException
     *             the resource not found exception
     * @throws ParseErrorException
     *             the parse error exception
     * @throws Exception
     *             the exception
     */
    void generarEntityManager() throws GenerationException;

    /**
     * Sets the database management system to the persistence unit.
     * 
     * @param dbms
     *            database management system
     * 
     * @throws ResourceNotFoundException
     *             the resource not found exception
     * @throws ParseErrorException
     *             the parse error exception
     * @throws Exception
     *             the exception
     */
    void setDbms(String dbms);

    /**
     * Sets the database name
     * 
     * @param dbName
     *            the new database name
     * 
     * @throws ResourceNotFoundException
     *             the resource not found exception
     * @throws ParseErrorException
     *             the parse error exception
     * @throws Exception
     *             the exception
     */
    void setDbName(String dbName);
}
