/*
 * GeneradorCodigo.java
 *
 * Created on 8 de diciembre de 2008, 23:20
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package controlador.generador;

import java.util.Collection;
import java.util.List;

import org.apache.velocity.exception.ParseErrorException;
import org.apache.velocity.exception.ResourceNotFoundException;
import org.omg.uml.foundation.core.Classifier;
import org.omg.uml.foundation.core.Relationship;

import controlador.generador.exceptions.GenerationException;

/**
 * Clase base para las interfaces de generacion, define los metodos genericos
 * para todo generador de codigo
 * 
 * @author Juan Timoteo Ponce Ortiz
 */
public interface GeneradorCodigo {
    /**
     * Asigna la direccion destino
     * 
     * @param path
     */
    void setPath(String path);

    /**
     * Asigna el elemento a procesar
     * 
     * @param clase
     */
    void setClase(Classifier clase);

    /**
     * Sets the relationships of the generator.
     * 
     * @param relations
     *            the new relaciones
     */
    void setRelaciones(List<Relationship> relations);

    /**
     * Genera la fabrica de clases segun el nivel
     * 
     * @param diagrama
     */
    void generarFactory(Collection<Classifier> diagramElements)
	    throws GenerationException;

    /**
     * Prepare the application to execute compiled sources, using Apache Ant
     * build files.
     * 
     * @param template
     *            generation template
     * 
     * @throws ResourceNotFoundException
     *             the resource not found exception
     * @throws ParseErrorException
     *             the parse error exception
     * @throws Exception
     *             the exception
     */
    void prepare(String template) throws GenerationException;
}
