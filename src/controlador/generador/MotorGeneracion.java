/*
 * MotorGeneracion.java
 *
 * Created on 8 de diciembre de 2008, 23:15
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
package controlador.generador;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.exception.VelocityException;
import org.omg.uml.foundation.core.Classifier;
import org.omg.uml.foundation.core.Generalization;
import org.omg.uml.foundation.core.ModelElement;
import org.omg.uml.foundation.core.Relationship;
import org.omg.uml.foundation.core.UmlAssociation;
import org.uml.diagrammanagement.Diagram;
import org.uml.diagrammanagement.GraphEdge;
import org.uml.diagrammanagement.GraphNode;
import org.uml.diagrammanagement.Uml1SemanticModelBridge;

import util.Configuracion;
import controlador.generador.exceptions.GenerationException;

/**
 * Clase encargada de generar el codigo de un diagrama
 * 
 * @author Juan Timoteo Ponce Ortiz
 */
public final class MotorGeneracion {

    private static Logger log = Logger.getLogger(MotorGeneracion.class);

    public static final String TEMPLATE_ENGINE_CONFIG = "config/velocity.properties";

    private Diagram diagrama;
    private boolean generarDatos;
    private boolean generarNegocio;
    private boolean generarPresentacion;
    private String path;

    /**
     * Creates a new instance of MotorGeneracion
     * 
     * @param path
     * @param diagrama
     * @param genDatos
     * @param genNegocio
     * @param genPresentacion
     */
    public MotorGeneracion(final String path, final Diagram diagrama,
	    final boolean genDatos, final boolean genNegocio,
	    final boolean genPresentacion) {
	try {
	    log.info("Loading velocity configuration from : "
		    + TEMPLATE_ENGINE_CONFIG);
	    Velocity.init(TEMPLATE_ENGINE_CONFIG);
	} catch (Exception e) {
	    log.error("Invalid configuration file : "+TEMPLATE_ENGINE_CONFIG + " > "+ e.getCause());
	}
	this.diagrama = diagrama;
	generarDatos = genDatos;
	generarNegocio = genNegocio;
	generarPresentacion = genPresentacion;
	this.path = path + "/"
	+ diagrama.getName().toLowerCase().replace(" ", "_");
    }

    public Diagram getDiagrama() {
	return diagrama;
    }

    public void setDiagrama(Diagram diagrama) {
	this.diagrama = diagrama;
    }

    public boolean isGenerarDatos() {
	return generarDatos;
    }

    public void setGenerarDatos(boolean generarDatos) {
	this.generarDatos = generarDatos;
    }

    public boolean isGenerarNegocio() {
	return generarNegocio;
    }

    public void setGenerarNegocio(boolean generarNegocio) {
	this.generarNegocio = generarNegocio;
    }

    public boolean isGenerarPresentacion() {
	return generarPresentacion;
    }

    public void setGenerarPresentacion(boolean generarPresentacion) {
	this.generarPresentacion = generarPresentacion;
    }

    public String getPath() {
	return path;
    }

    public void setPath(String path) {
	this.path = path;
    }

    /**
     * Genera el codigo del diagrama, segun las capas seleccionadas
     * 
     * @param lenguaje
     * @return
     * @throws InvocationTargetException
     * @throws VelocityException
     *             ,InterruptedException
     */
    public void generarCodigo(final String template, final String dbms,
	    final String dbName) throws GenerationException {
	log.info("Starting code generation: " + template);

	// final Runnable runner = new Runnable() {
	// @Override
	// public void run() {
	try {
	    if (diagrama != null) {
		final List<Classifier> classes = new ArrayList<Classifier>();
		final List<Relationship> relations = getDiagramRelationships();
		GeneradorDatos genDatos = null;
		GeneradorNegocio genNegocio = null;
		GeneradorPresentacion genPresentacion = null;

		log.info("Creating generator instances from template: "
			+ template);
		if (generarDatos) {
		    genDatos = (GeneradorDatos) Class.forName(
			    "controlador.generador." + template
			    + ".GeneradorDatosImpl").newInstance();
		}
		if (generarNegocio) {
		    genNegocio = (GeneradorNegocio) Class.forName(
			    "controlador.generador." + template
			    + ".GeneradorNegocioImpl").newInstance();
		}
		if (generarPresentacion) {
		    genPresentacion = (GeneradorPresentacion) Class.forName(
			    "controlador.generador." + template
			    + ".GeneradorPresentacionImpl")
			    .newInstance();
		}

		if (genDatos != null) {
		    genDatos.setPath(path);// solo para el prepare
		    genDatos.prepare(template);// "jpa"

		    genDatos.setRelaciones(relations);
		    genDatos.setPath(path + "/src/"
			    + Configuracion.getParam("generation.data.pkg"));// must
		    // be
		    // a
		    // folder
		    for (Object elem : diagrama.getContained()) {
			if (elem instanceof GraphNode) {
			    final GraphNode aux = (GraphNode) elem;
			    if (aux.getSemanticModel() instanceof Uml1SemanticModelBridge) {// uml
				final Uml1SemanticModelBridge puente = (Uml1SemanticModelBridge) aux
				.getSemanticModel();
				if (puente.getElement() instanceof Classifier) {
				    classes.add((Classifier) puente
					    .getElement());
				    genDatos.setClase((Classifier) puente
					    .getElement());
				    genDatos.generarEntidad();
				}
			    }
			}
		    }
		    log.info("Generating data layer");
		    genDatos.setDbms(dbms);
		    genDatos.setDbName(dbName);
		    genDatos.generarEntityManager();
		    genDatos.setPath(path);
		    genDatos.generarFactory(classes);
		}
		if (genNegocio != null) {
		    // relations not needed
		    genNegocio.setPath(path
			    + "/src/"
			    + Configuracion
			    .getParam("generation.bussiness.pkg"));// must
		    // be
		    // a
		    // folder
		    for (Classifier currentClass : classes) {
			genNegocio.setClase(currentClass);
			genNegocio.generarNegocio();
		    }
		    log.info("Generating bussiness layer");
		    genNegocio.generarFactory(classes);
		}
		if (genPresentacion != null) {
		    genPresentacion.setPath(path + "/src/"
			    + Configuracion.getParam("generation.view.pkg"));// must
		    // be
		    // a
		    // folder
		    genPresentacion.setRelaciones(relations);
		    for (Classifier currentClass : classes) {
			genPresentacion.setClase(currentClass);
			genPresentacion.generateEntityUI();
		    }
		    log.info("Generating presentation layer");
		    genPresentacion.generateMainUI(classes);
		}
	    }
	} catch (GenerationException e) {
	    log.error("Error on generation process", e);
	    throw e;
	} catch (Exception e) {
	    final String message = "Error reflecting generation classes ";
	    log.error(message, e);
	    throw new GenerationException(message + " :" + e.toString());
	}
	// }
	// };
	// SwingUtilities.invokeAndWait(runner);
    }

    /**
     * Gets the diagram relationships.
     * 
     * @return the diagram relationships
     */
    private List<Relationship> getDiagramRelationships() {
	final ArrayList<Relationship> out = new ArrayList<Relationship>();
	for (Object elem : diagrama.getContained()) {
	    if (elem instanceof GraphEdge) {
		final GraphEdge edge = (GraphEdge) elem;
		final Uml1SemanticModelBridge bridge = (Uml1SemanticModelBridge) edge
		.getSemanticModel();
		if (bridge == null) {
		    continue;
		}
		final ModelElement element = bridge.getElement();

		if (element instanceof Generalization) {
		    out.add((Relationship) element);
		} else if (element instanceof UmlAssociation) {
		    out.add((Relationship) element);
		}
		/*
		 * else if( element instanceof Dependency ) out.add(
		 * (Relationship)element ); else if( element instanceof
		 * Abstraction ) out.add( (Relationship)element );
		 */
	    }
	}
	return out;
    }
}
