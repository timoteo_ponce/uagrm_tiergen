package vista;

import java.awt.Container;
import java.awt.Dialog;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.security.InvalidParameterException;
import java.util.Collection;

import javax.help.HelpBroker;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JTextField;
import javax.swing.SwingWorker;

import modelo.Proyecto;
import modelo.types.TokenMakerType;

import org.apache.log4j.Logger;
import org.apache.velocity.exception.ParseErrorException;
import org.apache.velocity.exception.ResourceNotFoundException;
import org.apache.velocity.exception.VelocityException;
import org.uml.diagrammanagement.Diagram;

import util.AppUtil;
import util.Configuracion;
import util.HelpUtil;

import com.jgoodies.forms.factories.DefaultComponentFactory;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.uif_lite.panel.SimpleInternalFrame;

import controlador.generador.MotorGeneracion;
import controlador.generador.exceptions.GenerationException;

/**
 * The Class UIGenerador.
 * 
 * @author Juan Timoteo Ponce Ortiz
 */
public class UIGenerador extends JDialog {

	/** The logger. */
	private static final Logger LOG = Logger.getLogger(UIGenerador.class);

	/** The project. */
	private final Proyecto project;

	/** The diagrama. */
	private Diagram diagrama;

	/** The all diagrams. */
	private Collection<Diagram> allDiagrams;

	/**
	 * Instantiates a new uI generador.
	 * 
	 * @param owner
	 *            the owner
	 */
	public UIGenerador(final Frame owner, final Proyecto project) {
		super(owner);
		this.project = project;
		initComponents();
	}

	/**
	 * Instantiates a new uI generador.
	 * 
	 * @param owner
	 *            the owner
	 */
	public UIGenerador(final Dialog owner, final Proyecto project) {
		super(owner);
		this.project = project;
		initComponents();
	}

	/**
	 * Creates new form DialogGen.
	 * 
	 * @param onlyConfigure
	 *            the only configure
	 * @param parent
	 *            the parent
	 * @param modal
	 *            the modal
	 * @param diagrama
	 *            the diagrama
	 */
	public UIGenerador(final boolean onlyConfigure,
			final java.awt.Frame parent, final boolean modal,
			final Diagram diagrama, final Proyecto project) {
		super(parent, modal);
		this.project = project;
		this.diagrama = diagrama;
		initComponents();
		setLocationRelativeTo(parent);
		init();
		btnGenerate.setEnabled(!onlyConfigure);
	}

	/**
	 * Inits the.
	 */
	private void init() {
		initEventos();
		if (diagrama != null) {
			setTitle("Generar codigo: " + diagrama.getName());
			txtPath.setText(project.getPath() + "/generated");
		} else {
			throw new InvalidParameterException("Null diagram ");
		}
		// cargamos los templates

		String[] templates = Configuracion.getParam("templates").split(",");
		for (String temp : templates) {
			comboTemplate.addItem(temp);
		}
		// cargamos los DBMS
		templates = Configuracion.getParam("dbms").split(",");
		for (String temp : templates) {
			comboDBMS.addItem(temp);
		}

		allDiagrams = project.getRepositorio().getAllDiagrams();
		comboDiagram.removeAllItems();
		for (Diagram diagram : allDiagrams) {
			comboDiagram.addItem(diagram.getName());
		}
		comboDiagram.setSelectedItem(diagrama);
		if (diagrama != null) {
			// comboDiagram.setSelectedItem(diagrama.getName());
		}
		loadValues();
	}

	/**
	 * Inits the eventos.
	 */
	void initEventos() {
		DialogGenListener listener = new DialogGenListener();
		btnGenerate.addActionListener(listener);
		btnAccept.addActionListener(listener);
		btnCancel.addActionListener(listener);
		btnPath.addActionListener(listener);
		btnExampleDatos.addActionListener(listener);
		btnExampleNegocio.addActionListener(listener);
		btnExamplePresentacion.addActionListener(listener);

		/*
		 * chkCapaDatos.addActionListener( listener );
		 * chkCapaNegocio.addActionListener( listener );
		 * chkCapaPresentacion.addActionListener( listener );
		 * chkFabrica.addActionListener( listener );
		 * chkGenDoc.addActionListener( listener );
		 * chkIntroCod.addActionListener( listener ); chkPOJO.addActionListener(
		 * listener ); chkUseEJB.addActionListener( listener );
		 */
	}

	/**
	 * The listener interface for receiving dialogGen events. The class that is
	 * interested in processing a dialogGen event implements this interface, and
	 * the object created with that class is registered with a component using
	 * the component's <code>addDialogGenListener<code> method. When
	 * the dialogGen event occurs, that object's appropriate
	 * method is invoked.
	 * 
	 * @see DialogGenEvent
	 */
	class DialogGenListener implements ActionListener {

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent
		 * )
		 */
		@Override
		public void actionPerformed(final ActionEvent e) {
			if (e.getSource().equals(btnGenerate)) {
				generar();
				return;
			}

			if (e.getSource().equals(btnAccept)) {
				aceptar();
				return;
			}

			if (e.getSource().equals(btnCancel)) {
				cancelar();
				return;
			}

			if (e.getSource().equals(btnPath)) {
				setPath();
				return;
			}

			if (e.getSource().equals(btnExampleDatos)) {
				describirDatos();
				return;
			}

			if (e.getSource().equals(btnExampleNegocio)) {
				describirNegocio();
				return;
			}

			if (e.getSource().equals(btnExamplePresentacion)) {
				describirPresentacion();
				return;
			}
		}
	}

	/**
	 * Validar.
	 * 
	 * @return true, if successful
	 */
	private boolean validar() {
		if (txtPath.getText().isEmpty() && txtDB.getText().isEmpty()) {
			return false;
		}
		return true;
	}

	/**
	 * Generar.
	 */
	private void generar() {
		getProgressBar().setStringPainted(true);
		getProgressBar().setString("Generando codigo");
		GeneratorThread temp = new GeneratorThread(this);
		temp.addPropertyChangeListener(new PropertyChangeListener() {

			@Override
			public void propertyChange(final PropertyChangeEvent evt) {
				if ("progress".equals(evt.getPropertyName())) {
					int progress = (Integer) evt.getNewValue();
					progressBar.setValue(progress);
				}

			}
		});
		temp.execute();
	}

	/**
	 * Aceptar.
	 */
	private void aceptar() {
		updateValues();
		dispose();
	}

	/**
	 * Cancelar.
	 */
	private void cancelar() {
		dispose();
	}

	/**
	 * Sets the path.
	 */
	private void setPath() {
		File f = AppUtil.showGuardarCarpeta(this, "Generar codigo", "");
		if (f != null) {
			try {
				txtPath.setText(f.getAbsolutePath());
			} catch (Exception ex) {
				LOG.error(ex);
			}
		}
	}

	/**
	 * Describir datos.
	 */
	private void describirDatos() {
		setModal(false);
		HelpBroker broker = HelpUtil.getInstance().getBroker();
		broker.setCurrentID("capa_datos");
		broker.setDisplayed(true);
	}

	/**
	 * Describir negocio.
	 */
	private void describirNegocio() {
		HelpBroker broker = HelpUtil.getInstance().getBroker();
		broker.setCurrentID("capa_negocio");
		broker.setDisplayed(true);
	}

	/**
	 * Describir presentacion.
	 */
	private void describirPresentacion() {
		HelpBroker broker = HelpUtil.getInstance().getBroker();
		broker.setCurrentID("capa_presentacion");
		broker.setDisplayed(true);
	}

	/**
	 * Load values.
	 */
	private void loadValues() {
		if (project.getGenPath() != null) {
			txtPath.setText(project.getGenPath());
		}
		if (project.getTemplate() != null) {
			comboTemplate.setSelectedItem(project.getTemplate());
		}
		if (project.getDbms() != null) {
			comboDBMS.setSelectedItem(project.getDbms());
		}
		if (project.getDbName() != null) {
			txtDB.setText(project.getDbName());
		}
		chkCapaDatos.setSelected(project.isGenerateData());
		chkCapaNegocio.setSelected(project.isGenerateBussiness());
		chkCapaPresentacion.setSelected(project.isGeneratePresentation());
	}

	/**
	 * Update values.
	 */
	private void updateValues() {
		// project.setGenPath(txtPath.getText());
		project.setTemplate(comboTemplate.getSelectedItem().toString());
		project.setDbms(comboDBMS.getSelectedItem().toString());
		project.setDbName(txtDB.getText());
		project.setGenerateData(chkCapaDatos.isSelected());
		project.setGenerateBussiness(chkCapaNegocio.isSelected());
		project.setGeneratePresentation(chkCapaPresentacion.isSelected());
	}

	/**
	 * Inits the components.
	 */
	private void initComponents() {
		// JFormDesigner - Component initialization - DO NOT
		// MODIFY//GEN-BEGIN:initComponents
		DefaultComponentFactory compFactory = DefaultComponentFactory
		.getInstance();
		panelGeneral = new SimpleInternalFrame();
		lblDiagram = compFactory.createLabel("Diagrama:");
		comboDiagram = new JComboBox();
		lblPath = compFactory.createLabel("Direccion destino:");
		txtPath = new JTextField();
		btnPath = new JButton();
		lblTemplate = compFactory.createLabel("Plantilla:");
		comboTemplate = new JComboBox();
		lblDbms = compFactory.createLabel("Gestor de datos:");
		comboDBMS = new JComboBox();
		lblDbName = compFactory.createLabel("Base de datos");
		txtDB = new JTextField();
		panelGeneration = new SimpleInternalFrame();
		chkCapaDatos = new JCheckBox();
		lblDatos = new JLabel();
		btnExampleDatos = new JButton();
		chkCapaNegocio = new JCheckBox();
		lblNegocio = new JLabel();
		btnExampleNegocio = new JButton();
		chkCapaPresentacion = new JCheckBox();
		lblPresentacion = new JLabel();
		btnExamplePresentacion = new JButton();
		panelProgress = new JPanel();
		progressBar = new JProgressBar();
		panelButtons = new JPanel();
		btnGenerate = new JButton();
		btnAccept = new JButton();
		btnCancel = new JButton();
		CellConstraints cc = new CellConstraints();

		// ======== this ========
		setTitle("Generar c\u00f3digo");
		Container contentPane = getContentPane();
		contentPane.setLayout(new FormLayout("pref, 215dlu:grow, $lcgap, pref",
		"107dlu, $lgap, 112dlu:grow, $lgap, default"));

		// ======== panelGeneral ========
		{
			panelGeneral.setTitle("Datos generales");
			Container panelGeneralContentPane = panelGeneral.getContentPane();
			panelGeneralContentPane
			.setLayout(new FormLayout(
					"right:63dlu, $lcgap, 81dlu:grow, $lcgap, 95dlu:grow, $lcgap, 47dlu",
			"4*(default, $lgap), default"));

			// ---- lblDiagram ----
			lblDiagram.setLabelFor(comboDiagram);
			panelGeneralContentPane.add(lblDiagram, cc.xy(1, 1));
			panelGeneralContentPane.add(comboDiagram, cc.xywh(3, 1, 3, 1));
			panelGeneralContentPane.add(lblPath, cc.xy(1, 3));
			panelGeneralContentPane.add(txtPath, cc.xywh(3, 3, 3, 1));

			// ---- btnPath ----
			btnPath.setIcon(new ImageIcon(getClass().getResource(
			"/iconos/search-small.png")));
			panelGeneralContentPane.add(btnPath, cc.xy(7, 3));
			panelGeneralContentPane.add(lblTemplate, cc.xy(1, 5));
			panelGeneralContentPane.add(comboTemplate, cc.xywh(3, 5, 3, 1));
			panelGeneralContentPane.add(lblDbms, cc.xy(1, 7));
			panelGeneralContentPane.add(comboDBMS, cc.xywh(3, 7, 3, 1));
			panelGeneralContentPane.add(lblDbName, cc.xy(1, 9));
			panelGeneralContentPane.add(txtDB, cc.xywh(3, 9, 3, 1));
		}
		contentPane.add(panelGeneral, cc.xywh(2, 1, 1, 1,
				CellConstraints.DEFAULT, CellConstraints.FILL));

		// ======== panelGeneration ========
		{
			panelGeneration.setTitle("Generaci\u00f3n");
			Container panelGenerationContentPane = panelGeneration
			.getContentPane();
			panelGenerationContentPane
			.setLayout(new FormLayout(
					"default, $lcgap, left:123dlu, $lcgap, 36dlu, $lcgap, 77dlu, $lcgap, default:grow",
			"3*(default, $lgap), default"));

			// ---- chkCapaDatos ----
			chkCapaDatos.setText("Generar capa de datos");
			panelGenerationContentPane.add(chkCapaDatos, cc.xy(3, 3));
			panelGenerationContentPane.add(lblDatos, cc.xy(5, 3));

			// ---- btnExampleDatos ----
			btnExampleDatos.setText("Ver ejemplo");
			btnExampleDatos.setIcon(new ImageIcon(getClass().getResource(
			"/iconos/gtk-info.png")));
			panelGenerationContentPane.add(btnExampleDatos, cc.xy(7, 3));

			// ---- chkCapaNegocio ----
			chkCapaNegocio.setText("Generar capa de negocio");
			panelGenerationContentPane.add(chkCapaNegocio, cc.xy(3, 5));
			panelGenerationContentPane.add(lblNegocio, cc.xy(5, 5));

			// ---- btnExampleNegocio ----
			btnExampleNegocio.setText("Ver ejemplo");
			btnExampleNegocio.setIcon(new ImageIcon(getClass().getResource(
			"/iconos/gtk-info.png")));
			panelGenerationContentPane.add(btnExampleNegocio, cc.xy(7, 5));

			// ---- chkCapaPresentacion ----
			chkCapaPresentacion.setText("Generar capa de presentaci\u00f3n");
			panelGenerationContentPane.add(chkCapaPresentacion, cc.xy(3, 7));
			panelGenerationContentPane.add(lblPresentacion, cc.xy(5, 7));

			// ---- btnExamplePresentacion ----
			btnExamplePresentacion.setText("Ver ejemplo");
			btnExamplePresentacion.setIcon(new ImageIcon(getClass()
					.getResource("/iconos/gtk-info.png")));
			panelGenerationContentPane.add(btnExamplePresentacion, cc.xy(7, 7));
		}
		contentPane.add(panelGeneration, cc.xywh(2, 3, 1, 1,
				CellConstraints.DEFAULT, CellConstraints.FILL));

		// ======== panelProgress ========
		{
			panelProgress.setLayout(new FormLayout("61dlu:grow",
			"default, $lgap, default"));
			panelProgress.add(progressBar, cc.xy(1, 1));

			// ======== panelButtons ========
			{
				panelButtons
				.setLayout(new FormLayout(
						"default:grow, $lcgap, 81dlu, $lcgap, 75dlu, $lcgap, 83dlu, $lcgap, default:grow",
				"default"));
				((FormLayout) panelButtons.getLayout())
				.setColumnGroups(new int[][] { { 3, 5, 7 } });

				// ---- btnGenerate ----
				btnGenerate.setText("Generar c\u00f3digo");
				btnGenerate.setIcon(new ImageIcon(getClass().getResource(
				"/iconos/gnome-session-switch.png")));
				panelButtons.add(btnGenerate, cc.xy(3, 1));

				// ---- btnAccept ----
				btnAccept.setText("Guardar");
				btnAccept.setIcon(new ImageIcon(getClass().getResource(
				"/iconos/dialog-apply.png")));
				panelButtons.add(btnAccept, cc.xy(5, 1));

				// ---- btnCancel ----
				btnCancel.setText("Cancelar");
				btnCancel.setIcon(new ImageIcon(getClass().getResource(
				"/iconos/window-close.png")));
				panelButtons.add(btnCancel, cc.xy(7, 1));
			}
			panelProgress.add(panelButtons, cc.xy(1, 3));
		}
		contentPane.add(panelProgress, cc.xy(2, 5));
		setSize(565, 445);
		setLocationRelativeTo(null);
		// initialization//GEN-END:initComponents
	}

	// JFormDesigner - Variables declaration - DO NOT
	// MODIFY//GEN-BEGIN:variables
	private SimpleInternalFrame panelGeneral;
	private JLabel lblDiagram;
	private JComboBox comboDiagram;
	private JLabel lblPath;
	private JTextField txtPath;
	private JButton btnPath;
	private JLabel lblTemplate;
	private JComboBox comboTemplate;
	private JLabel lblDbms;
	private JComboBox comboDBMS;
	private JLabel lblDbName;
	private JTextField txtDB;
	private SimpleInternalFrame panelGeneration;
	private JCheckBox chkCapaDatos;
	private JLabel lblDatos;
	private JButton btnExampleDatos;
	private JCheckBox chkCapaNegocio;
	private JLabel lblNegocio;
	private JButton btnExampleNegocio;
	private JCheckBox chkCapaPresentacion;
	private JLabel lblPresentacion;
	private JButton btnExamplePresentacion;
	private JPanel panelProgress;
	private JProgressBar progressBar;
	private JPanel panelButtons;
	private JButton btnGenerate;
	private JButton btnAccept;
	private JButton btnCancel;

	// JFormDesigner - End of variables declaration//GEN-END:variables
	/**
	 * Gets the chk capa datos.
	 * 
	 * @return the chk capa datos
	 */
	public JCheckBox getChkCapaDatos() {
		return chkCapaDatos;
	}

	/**
	 * Gets the chk capa negocio.
	 * 
	 * @return the chk capa negocio
	 */
	public JCheckBox getChkCapaNegocio() {
		return chkCapaNegocio;
	}

	/**
	 * Gets the chk capa presentacion.
	 * 
	 * @return the chk capa presentacion
	 */
	public JCheckBox getChkCapaPresentacion() {
		return chkCapaPresentacion;
	}

	/**
	 * Gets the txt db.
	 * 
	 * @return the txt db
	 */
	public JTextField getTxtDB() {
		return txtDB;
	}

	/**
	 * Gets the txt path.
	 * 
	 * @return the txt path
	 */
	public JTextField getTxtPath() {
		return txtPath;
	}

	/**
	 * Gets the diagrama.
	 * 
	 * @return the diagrama
	 */
	public Diagram getDiagrama() {
		return diagrama;
	}

	/**
	 * Gets the project.
	 * 
	 * @return the project
	 */
	public Proyecto getProject() {
		return project;
	}

	/**
	 * Gets the progress bar.
	 * 
	 * @return the progress bar
	 */
	public JProgressBar getProgressBar() {
		return progressBar;
	}

	/**
	 * Show generated code.
	 */
	public void showGeneratedCode() {
		UICodeViewer codeViewer = UICodeViewer.newInstance();
		codeViewer.setType(TokenMakerType.JAVA);
		codeViewer.setRootFolder(project.getGenPath());
		codeViewer.loadFiles();
		codeViewer.setVisible(true);
	}

	/**
	 * The Class GeneratorThread.
	 */
	static class GeneratorThread extends SwingWorker<Void, Void> {

		/** The ui generador. */
		UIGenerador uiGenerador;

		/**
		 * Instantiates a new generator thread.
		 * 
		 * @param uiGenerador
		 *            the ui generador
		 */
		public GeneratorThread(final UIGenerador uiGenerador) {
			this.uiGenerador = uiGenerador;
		}

		/**
		 * Sleep.
		 * 
		 * @param millis
		 *            the millis
		 */
		private static void sleep(final int millis) {
			try {
				Thread.currentThread().sleep(millis);
			} catch (InterruptedException e) {
				LOG.error(e, e);
			}
		}

		/**
		 * Generate code.
		 * 
		 * @throws ResourceNotFoundException
		 *             the resource not found exception
		 * @throws ParseErrorException
		 *             the parse error exception
		 * @throws VelocityException
		 *             the velocity exception
		 * @throws InterruptedException
		 *             the interrupted exception
		 * @throws InvocationTargetException
		 *             the invocation target exception
		 */
		private void generateCode() throws GenerationException {
			// diagrama = allDiagrams[comboDiagram.getSelectedIndex()];
			uiGenerador.updateValues();
			setProgress(20);
			GeneratorThread.sleep(500);

			final MotorGeneracion motor = new MotorGeneracion(uiGenerador
					.getTxtPath().getText(), uiGenerador.getDiagrama(),
					uiGenerador.getChkCapaDatos().isSelected(), uiGenerador
					.getChkCapaNegocio().isSelected(), uiGenerador
					.getChkCapaPresentacion().isSelected());

			final String template = uiGenerador.comboTemplate.getSelectedItem()
			.toString();
			final String dbms = uiGenerador.comboDBMS.getSelectedItem()
			.toString();
			final String dbName = uiGenerador.txtDB.getText();

			setProgress(40);
			sleep(500);

			motor.generarCodigo(template, dbms, dbName);

			setProgress(60);
			sleep(500);

		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see javax.swing.SwingWorker#doInBackground()
		 */
		@Override
		protected Void doInBackground() {
			setProgress(5);
			sleep(500);
			if (!uiGenerador.validar()) {
				AppUtil.showAdvertencia(uiGenerador,
				"Debe rellenar los campos ");
				return null;
			}
			try {
				setProgress(10);
				sleep(500);
				generateCode();
				sleep(500);
				setProgress(90);

				boolean confirm = AppUtil.showConfirmar(uiGenerador, "Estado",
						"El diagrama fue generado correctamente,"
						+ "\n desea ver el codigo generado?");
				if (confirm) {
					uiGenerador.showGeneratedCode();
				}
			} catch (GenerationException e) {
				LOG.error(e.getMessage(), e);
				AppUtil.showError(uiGenerador, e.getMessage());
			}
			return null;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see javax.swing.SwingWorker#done()
		 */
		@Override
		protected void done() {
			super.done();
			uiGenerador.getProgressBar().getModel().setValue(100);
		}

	}

}
