/*
 * Created by JFormDesigner on Sun May 17 19:09:25 BOT 2009
 */

package vista.editores;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.Map;

import javax.persistence.TemporalType;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JTextField;

import modelo.types.Field;

import org.omg.uml.foundation.core.Attribute;
import org.omg.uml.foundation.core.Classifier;
import org.omg.uml.foundation.core.DataType;
import org.omg.uml.foundation.datatypes.Expression;
import org.omg.uml.foundation.datatypes.Multiplicity;
import org.uml.diagrammanagement.GraphElement;
import org.uml.diagrammanagement.GraphNode;

import util.ComboGenerico;
import util.Constantes;
import util.NodoElemento;
import util.uml.ElementoVisualUtil;
import util.uml.MultiplicidadUtil;
import util.uml.UMLUtil;
import vista.Main;

import com.jgoodies.forms.factories.DefaultComponentFactory;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;

// TODO: Auto-generated Javadoc
/**
 * The Class EditorAtributo.
 * 
 * @author Timo
 */
public final class EditorAtributo extends EditorBase {

    /** The current type. */
    int currentType = -1;

    /**
     * Instantiates a new editor atributo.
     */
    public EditorAtributo() {
	initComponents();
    }

    /**
     * Instantiates a new editor atributo.
     * 
     * @param element
     *            the element
     */
    public EditorAtributo(Attribute element) {
	super(element);
	initComponents();
	loadTemporalTypes();
	super.loadMultiplicityToComboBox(comboMultiplicidad);
	super.loadTypesToComboBox(comboTipos);
	loadProperties();
    }

    /**
     * Load temporal types.
     */
    private void loadTemporalTypes() {
	comboTemporalType.removeAllItems();
	for (TemporalType item : TemporalType.values())
	    comboTemporalType.addItem(item);
    }

    /*
     * (non-Javadoc)
     * 
     * @see vista.editores.EditorBase#loadProperties()
     */
    @Override
    public void loadProperties() {
	if (getGraphElement() == null) {
	    setGraphElement(ElementoVisualUtil.createGraphElement(getElement(),
		    Constantes.MODEL_NODE_TYPE_ID));
	    Main.getUIPrincipal().getCurrentDiagram().getContained().add(
		    ((GraphNode) getGraphElement()));
	}
	Map properties = ElementoVisualUtil.getPropiedades(getGraphElement());
	// <editor-fold defaultstate="collapsed" desc="EJB3 ">
	Object prop = properties.get(Field.NULLABLE.toString());
	if (prop != null && !prop.toString().isEmpty())
	    chkNullable.setSelected(true);

	prop = properties.get(Field.EMBEDDED.toString());
	if (prop != null && !prop.toString().isEmpty())
	    chkEmbedded.setSelected(true);

	prop = properties.get(Field.COLUMN_NAME.toString());
	if (prop != null && !prop.toString().isEmpty()) {
	    chkCoilumnName.setSelected(true);
	    txtColumnName.setText(prop.toString());
	    txtColumnName.setEditable(true);
	}
	prop = properties.get(Field.TEMPORAL_TYPE.toString());
	if (prop != null && !prop.toString().isEmpty()) {
	    comboTemporalType.setEnabled(true);
	    comboTemporalType.setSelectedItem(TemporalType.valueOf(prop
		    .toString()));
	}
    }

    /*
     * (non-Javadoc)
     * 
     * @see vista.editores.EditorBase#saveProperties()
     */
    @Override
    public void saveProperties() {
	GraphElement graphElement = getGraphElement();
	if (chkNullable.isSelected())
	    ElementoVisualUtil.setProperty(graphElement, Field.NULLABLE
		    .toString(), "0");
	else
	    ElementoVisualUtil.setProperty(graphElement, Field.NULLABLE
		    .toString(), "");

	if (chkEmbedded.isSelected())
	    ElementoVisualUtil.setProperty(graphElement, Field.EMBEDDED
		    .toString(), "0");
	else
	    ElementoVisualUtil.setProperty(graphElement, Field.EMBEDDED
		    .toString(), "");
	if (chkCoilumnName.isSelected())
	    ElementoVisualUtil.setProperty(graphElement, Field.COLUMN_NAME
		    .toString(), txtColumnName.getText());
	else
	    ElementoVisualUtil.setProperty(graphElement, Field.COLUMN_NAME
		    .toString(), "");
	if (comboTemporalType.isEnabled())
	    ElementoVisualUtil
		    .setProperty(graphElement, Field.TEMPORAL_TYPE.toString(),
			    comboTemporalType.getSelectedItem().toString());
	else
	    ElementoVisualUtil.setProperty(graphElement, Field.TEMPORAL_TYPE
		    .toString(), "");
    }

    /**
     * Chk coilumn name action performed.
     * 
     * @param e
     *            the e
     */
    private void chkCoilumnNameActionPerformed() {
	txtColumnName.setEditable(chkCoilumnName.isSelected());
    }

    /**
     * Combo tipos item state changed.
     * 
     * @param e
     *            the e
     */
    private void comboTiposItemStateChanged() {
	int index = comboTipos.getSelectedIndex();
	if (index > -1 && currentType != index) {
	    currentType = index;
	    String currentName = comboTipos.getSelectedItem().toString();
	    boolean visible = currentName.contains("Date")
		    || currentName.contains("Time");
	    comboTemporalType.setEnabled(visible);
	}
    }

    /**
     * Inits the components.
     */
    private void initComponents() {
	// JFormDesigner - Component initialization - DO NOT MODIFY
	// //GEN-BEGIN:initComponents
	DefaultComponentFactory compFactory = DefaultComponentFactory
		.getInstance();
	lblTipo = compFactory.createLabel("Tipo");
	comboTipos = new ComboGenerico<NodoElemento>();
	lblMultilpicity = compFactory.createLabel("Multiplicidad");
	comboMultiplicidad = new JComboBox();
	lvlValue = compFactory.createLabel("Valor inicial");
	txtValor = new JTextField();
	lblTemporal = compFactory.createLabel("Temporal");
	comboTemporalType = new JComboBox();
	chkEmbedded = new JCheckBox();
	chkNullable = new JCheckBox();
	chkCoilumnName = new JCheckBox();
	txtColumnName = new JTextField();
	CellConstraints cc = new CellConstraints();

	// ======== this ========
	setLayout(new FormLayout(
		"11dlu, $lcgap, right:46dlu, $lcgap, 92dlu, $lcgap, default:grow",
		"8*(default, $lgap), 8dlu:grow"));

	// ---- lblTipo ----
	lblTipo.setLabelFor(comboTipos);
	add(lblTipo, cc.xy(3, 3));

	// ---- comboTipos ----
	comboTipos.addItemListener(new ItemListener() {
	    public void itemStateChanged(ItemEvent e) {
		comboTiposItemStateChanged();
	    }
	});
	add(comboTipos, cc.xy(5, 3));

	// ---- lblMultilpicity ----
	lblMultilpicity.setLabelFor(comboMultiplicidad);
	add(lblMultilpicity, cc.xy(3, 5));
	add(comboMultiplicidad, cc.xy(5, 5));

	// ---- lvlValue ----
	lvlValue.setLabelFor(txtValor);
	add(lvlValue, cc.xy(3, 7));
	add(txtValor, cc.xy(5, 7));

	// ---- lblTemporal ----
	lblTemporal.setLabelFor(comboTemporalType);
	add(lblTemporal, cc.xy(3, 9));

	// ---- comboTemporalType ----
	comboTemporalType.setEditable(true);
	add(comboTemporalType, cc.xy(5, 9));

	// ---- chkEmbedded ----
	chkEmbedded.setText("Embedded");
	add(chkEmbedded, cc.xy(5, 11));

	// ---- chkNullable ----
	chkNullable.setText("Nullable");
	add(chkNullable, cc.xy(5, 13));

	// ---- chkCoilumnName ----
	chkCoilumnName.setText("Column");
	chkCoilumnName.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent e) {
		chkCoilumnNameActionPerformed();
	    }
	});
	add(chkCoilumnName, cc.xy(3, 15));

	// ---- txtColumnName ----
	txtColumnName.setEditable(false);
	add(txtColumnName, cc.xy(5, 15));
	// //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY
    // //GEN-BEGIN:variables
    private JLabel lblTipo;
    private JComboBox comboTipos;
    private JLabel lblMultilpicity;
    private JComboBox comboMultiplicidad;
    private JLabel lvlValue;
    private JTextField txtValor;
    private JLabel lblTemporal;
    private JComboBox comboTemporalType;
    private JCheckBox chkEmbedded;
    private JCheckBox chkNullable;
    private JCheckBox chkCoilumnName;
    private JTextField txtColumnName;

    // JFormDesigner - End of variables declaration //GEN-END:variables

    /*
     * (non-Javadoc)
     * 
     * @see vista.editores.EditorBase#getElement()
     */
    public Attribute getElement() {
	return (Attribute) super.getElement();
    }

    /*
     * (non-Javadoc)
     * 
     * @see vista.editores.EditorBase#updateModelo()
     */
    @Override
    public void updateModelo() {
	// tipo de dato
	Object temp = comboTipos.getSelectedItem();

	if (temp != null && !temp.toString().isEmpty()) {
	    if (temp instanceof NodoElemento) {
		NodoElemento tipo = (NodoElemento) temp;
		if (tipo.getElemento() instanceof Classifier) {
		    getElement().setType((Classifier) tipo.getElemento());
		} else if (tipo.getElemento() instanceof DataType) {
		    getElement().setType((DataType) tipo.getElemento());
		} else {
		    DataType dt = UMLUtil.crearTipoDato(temp.toString());
		    getElement().setType(dt);
		}
	    } else {
		DataType td = UMLUtil.crearTipoDato();
		td.setNamespace(getElement().getNamespace());
		td.setName(temp.toString());
		getElement().setType(td);
	    }
	} else {
	    getElement().setType(null);
	}
	// valor
	Expression exp = getElement().getInitialValue();
	if (txtValor.getText().isEmpty() && exp != null) {
	    exp.refDelete();
	    getElement().setInitialValue(null);
	} else {
	    if (exp == null) {
		exp = UMLUtil.crearExpresion();
		exp.setLanguage("java");
		getElement().setInitialValue(exp);
	    }
	    exp.setBody(txtValor.getText());
	}
	// multiplicidad
	Multiplicity mult = getElement().getMultiplicity();
	if (mult != null) {
	    mult.refDelete();
	    getElement().setMultiplicity(null);
	}
	Multiplicity nueva = MultiplicidadUtil
		.toMultiplicity((String) comboMultiplicidad.getSelectedItem());
	if (nueva != null)
	    getElement().setMultiplicity(nueva);
	saveProperties();
    }

    /*
     * (non-Javadoc)
     * 
     * @see vista.editores.EditorBase#updateVista()
     */
    @Override
    public void updateVista() {
	if (getElement().getInitialValue() != null) {
	    txtValor.setText(getElement().getInitialValue().getBody());
	}

	Multiplicity mul = getElement().getMultiplicity();
	comboMultiplicidad.setSelectedItem(MultiplicidadUtil.toString(mul,
		MultiplicidadUtil.COMA));

	comboTemporalType.removeAllItems();
	TemporalType[] types = TemporalType.values();
	for (TemporalType item : types)
	    comboTemporalType.addItem(item);
	loadProperties();
    }

    /*
     * (non-Javadoc)
     * 
     * @see vista.editores.EditorBase#getTitulo()
     */
    @Override
    public String getTitulo() {
	return this.getClass().getSimpleName();
    }
}
