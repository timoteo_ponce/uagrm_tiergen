/*
 * Created by JFormDesigner on Sun May 17 09:46:57 BOT 2009
 */

package vista.editores;

import javax.swing.*;
import org.omg.uml.foundation.core.GeneralizableElement;

import com.jgoodies.forms.layout.*;

// TODO: Auto-generated Javadoc
/**
 * The Class EditorGeneralizable.
 * 
 * @author Timo
 */
public final class EditorGeneralizable extends EditorBase {
	
	/**
	 * Instantiates a new editor generalizable.
	 */
	public EditorGeneralizable() {
		initComponents();
	}
	
	/**
	 * Instantiates a new editor generalizable.
	 * 
	 * @param element the element
	 */
	public EditorGeneralizable(GeneralizableElement element) {
		super( element );
		initComponents();
	}

	/**
	 * Inits the components.
	 */
	private void initComponents() {
		// JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
		chkAbstracto = new JCheckBox();
		CellConstraints cc = new CellConstraints();

		//======== this ========
		setLayout(new FormLayout(
			"2*(default, $lcgap), default:grow",
			"2*(default, $lgap), default"));

		//---- chkAbstracto ----
		chkAbstracto.setText("Abstracto");
		add(chkAbstracto, cc.xy(3, 3));
		// JFormDesigner - End of component initialization  //GEN-END:initComponents
	}

	// JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
	/** The chk abstracto. */
	private JCheckBox chkAbstracto;
	// JFormDesigner - End of variables declaration  //GEN-END:variables
	
	/* (non-Javadoc)
	 * @see vista.editores.EditorBase#getElement()
	 */
	public GeneralizableElement getElement(){
		return (GeneralizableElement) super.getElement();
	}
	
	/* (non-Javadoc)
	 * @see vista.editores.EditorBase#updateVista()
	 */
	@Override
	public void updateVista() {	
		chkAbstracto.setSelected( getElement().isAbstract() );
	}
	
	/* (non-Javadoc)
	 * @see vista.editores.EditorBase#updateModelo()
	 */
	@Override
	public void updateModelo() {
		getElement().setAbstract( chkAbstracto.isSelected() );		
	}
	
	/* (non-Javadoc)
	 * @see vista.editores.EditorBase#getTitulo()
	 */
	@Override
	public String getTitulo() {
		return this.getClass().getSimpleName();
	}
}
