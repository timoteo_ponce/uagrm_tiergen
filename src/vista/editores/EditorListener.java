/*
 * EditorListener.java
 *
 * Created on 21 de octubre de 2008, 1:06
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package vista.editores;

import org.omg.uml.foundation.core.ModelElement;


// TODO: Auto-generated Javadoc
/**
 * Escuchador de una vista de edicion.
 * 
 * @author Juan Timoteo Ponce Ortiz
 */
public interface EditorListener {
    
    /**
     * Proceso ejecutado cuando se aceptan las modificaciones al
     * elemento del editor.
     * 
     * @param elemento elemento editable
     */
    public void aceptar( ModelElement elemento);
    
    /**
     * Proceso ejecutado cuando se cancela una edicion.
     */
    public void cancelar();
}
