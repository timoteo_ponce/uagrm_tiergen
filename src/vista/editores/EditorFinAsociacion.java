/*
 * EditorAsociacionEnd.java
 *
 * Created on 11 de noviembre de 2008, 0:08
 */

package vista.editores;

import java.beans.PropertyChangeEvent;

import org.omg.uml.foundation.core.AssociationEnd;
import org.omg.uml.foundation.datatypes.AggregationKindEnum;
import org.omg.uml.foundation.datatypes.Multiplicity;
import org.omg.uml.foundation.datatypes.VisibilityKindEnum;

import util.uml.MultiplicidadUtil;

/**
 * The Class EditorFinAsociacion.
 * 
 * @author Juan Timoteo Ponce Ortiz
 */

// CAMBIAR EL NOMBRE A FINASOCIACION
public final class EditorFinAsociacion extends javax.swing.JPanel implements
	VistaEdicion {

    /** The elemento. */
    private AssociationEnd elemento;

    /**
     * Creates new form EditorAsociacionEnd.
     * 
     * @param elemento
     *            the elemento
     */
    public EditorFinAsociacion(AssociationEnd elemento) {
	this.elemento = elemento;
	initComponents();

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed"
    // desc=" Codigo Generado  ">//GEN-BEGIN:initComponents
    private void initComponents() {
	panelPrincipal = new javax.swing.JPanel();
	jLabel1 = new javax.swing.JLabel();
	comboVisibilidad = new javax.swing.JComboBox();
	jPanel1 = new javax.swing.JPanel();
	comboMultiplicidad = new javax.swing.JComboBox();
	jLabel2 = new javax.swing.JLabel();
	jLabel3 = new javax.swing.JLabel();
	comboAgregacion = new javax.swing.JComboBox();
	chkNavegable = new javax.swing.JCheckBox();

	panelPrincipal.setBorder(javax.swing.BorderFactory
		.createTitledBorder(""));
	jLabel1.setText("Visibilidad:");

	comboVisibilidad.setModel(new javax.swing.DefaultComboBoxModel(
		new String[] { "Ninguna", "Publica", "Privada", "Protegida",
			"Paquete" }));

	javax.swing.GroupLayout panelPrincipalLayout = new javax.swing.GroupLayout(
		panelPrincipal);
	panelPrincipal.setLayout(panelPrincipalLayout);
	panelPrincipalLayout
		.setHorizontalGroup(panelPrincipalLayout
			.createParallelGroup(
				javax.swing.GroupLayout.Alignment.LEADING)
			.addGroup(
				javax.swing.GroupLayout.Alignment.TRAILING,
				panelPrincipalLayout
					.createSequentialGroup()
					.addGap(27, 27, 27)
					.addComponent(jLabel1)
					.addPreferredGap(
						javax.swing.LayoutStyle.ComponentPlacement.RELATED,
						125, Short.MAX_VALUE)
					.addComponent(
						comboVisibilidad,
						javax.swing.GroupLayout.PREFERRED_SIZE,
						javax.swing.GroupLayout.DEFAULT_SIZE,
						javax.swing.GroupLayout.PREFERRED_SIZE)
					.addGap(41, 41, 41)));
	panelPrincipalLayout.setVerticalGroup(panelPrincipalLayout
		.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
		.addGroup(
			panelPrincipalLayout.createParallelGroup(
				javax.swing.GroupLayout.Alignment.BASELINE)
				.addComponent(comboVisibilidad,
					javax.swing.GroupLayout.PREFERRED_SIZE,
					javax.swing.GroupLayout.DEFAULT_SIZE,
					javax.swing.GroupLayout.PREFERRED_SIZE)
				.addComponent(jLabel1)));

	jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
	comboMultiplicidad.setModel(new javax.swing.DefaultComboBoxModel(
		new String[] { "", "1", "0..1", "*", "1..*" }));

	jLabel2.setText("Multiplicidad:");

	jLabel3.setText("Agregacion:");

	comboAgregacion.setModel(new javax.swing.DefaultComboBoxModel(
		new String[] { "Ninguna", "Agregacion", "Composicion" }));

	chkNavegable.setText("Navegable");
	chkNavegable.setBorder(javax.swing.BorderFactory.createEmptyBorder(0,
		0, 0, 0));
	chkNavegable.setMargin(new java.awt.Insets(0, 0, 0, 0));

	javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(
		jPanel1);
	jPanel1.setLayout(jPanel1Layout);
	jPanel1Layout
		.setHorizontalGroup(jPanel1Layout
			.createParallelGroup(
				javax.swing.GroupLayout.Alignment.LEADING)
			.addGroup(
				jPanel1Layout
					.createSequentialGroup()
					.addContainerGap()
					.addGroup(
						jPanel1Layout
							.createParallelGroup(
								javax.swing.GroupLayout.Alignment.TRAILING)
							.addComponent(jLabel2)
							.addComponent(jLabel3))
					.addGap(29, 29, 29)
					.addGroup(
						jPanel1Layout
							.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
							.addComponent(
								chkNavegable)
							.addGroup(
								jPanel1Layout
									.createParallelGroup(
										javax.swing.GroupLayout.Alignment.LEADING,
										false)
									.addComponent(
										comboAgregacion,
										0,
										javax.swing.GroupLayout.DEFAULT_SIZE,
										Short.MAX_VALUE)
									.addComponent(
										comboMultiplicidad,
										0,
										167,
										Short.MAX_VALUE)))
					.addContainerGap(46, Short.MAX_VALUE)));
	jPanel1Layout
		.setVerticalGroup(jPanel1Layout
			.createParallelGroup(
				javax.swing.GroupLayout.Alignment.LEADING)
			.addGroup(
				jPanel1Layout
					.createSequentialGroup()
					.addContainerGap()
					.addGroup(
						jPanel1Layout
							.createParallelGroup(
								javax.swing.GroupLayout.Alignment.BASELINE)
							.addComponent(
								comboMultiplicidad,
								javax.swing.GroupLayout.PREFERRED_SIZE,
								javax.swing.GroupLayout.DEFAULT_SIZE,
								javax.swing.GroupLayout.PREFERRED_SIZE)
							.addComponent(jLabel2))
					.addGap(16, 16, 16)
					.addGroup(
						jPanel1Layout
							.createParallelGroup(
								javax.swing.GroupLayout.Alignment.BASELINE)
							.addComponent(
								comboAgregacion,
								javax.swing.GroupLayout.PREFERRED_SIZE,
								javax.swing.GroupLayout.DEFAULT_SIZE,
								javax.swing.GroupLayout.PREFERRED_SIZE)
							.addComponent(jLabel3))
					.addPreferredGap(
						javax.swing.LayoutStyle.ComponentPlacement.RELATED,
						17, Short.MAX_VALUE)
					.addComponent(chkNavegable)));

	javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
	setLayout(layout);
	layout
		.setHorizontalGroup(layout
			.createParallelGroup(
				javax.swing.GroupLayout.Alignment.LEADING)
			.addGroup(
				layout
					.createSequentialGroup()
					.addContainerGap()
					.addGroup(
						layout
							.createParallelGroup(
								javax.swing.GroupLayout.Alignment.TRAILING,
								false)
							.addComponent(
								jPanel1,
								javax.swing.GroupLayout.Alignment.LEADING,
								javax.swing.GroupLayout.DEFAULT_SIZE,
								javax.swing.GroupLayout.DEFAULT_SIZE,
								Short.MAX_VALUE)
							.addComponent(
								panelPrincipal,
								javax.swing.GroupLayout.Alignment.LEADING,
								javax.swing.GroupLayout.DEFAULT_SIZE,
								javax.swing.GroupLayout.DEFAULT_SIZE,
								Short.MAX_VALUE))
					.addContainerGap(
						javax.swing.GroupLayout.DEFAULT_SIZE,
						Short.MAX_VALUE)));
	layout
		.setVerticalGroup(layout
			.createParallelGroup(
				javax.swing.GroupLayout.Alignment.LEADING)
			.addGroup(
				layout
					.createSequentialGroup()
					.addGap(23, 23, 23)
					.addComponent(
						panelPrincipal,
						javax.swing.GroupLayout.PREFERRED_SIZE,
						javax.swing.GroupLayout.DEFAULT_SIZE,
						javax.swing.GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(
						javax.swing.LayoutStyle.ComponentPlacement.RELATED)
					.addComponent(
						jPanel1,
						javax.swing.GroupLayout.PREFERRED_SIZE,
						javax.swing.GroupLayout.DEFAULT_SIZE,
						javax.swing.GroupLayout.PREFERRED_SIZE)
					.addContainerGap(
						javax.swing.GroupLayout.DEFAULT_SIZE,
						Short.MAX_VALUE)));
    }// </editor-fold>//GEN-END:initComponents

    // Declaracion de varibales -no modificar//GEN-BEGIN:variables
    /** The chk navegable. */
    private javax.swing.JCheckBox chkNavegable;

    /** The combo agregacion. */
    private javax.swing.JComboBox comboAgregacion;

    /** The combo multiplicidad. */
    private javax.swing.JComboBox comboMultiplicidad;

    /** The combo visibilidad. */
    private javax.swing.JComboBox comboVisibilidad;

    /** The j label1. */
    private javax.swing.JLabel jLabel1;

    /** The j label2. */
    private javax.swing.JLabel jLabel2;

    /** The j label3. */
    private javax.swing.JLabel jLabel3;

    /** The j panel1. */
    private javax.swing.JPanel jPanel1;

    /** The panel principal. */
    private javax.swing.JPanel panelPrincipal;

    // Fin de declaracion de variables//GEN-END:variables

    /*
     * (non-Javadoc)
     * 
     * @see vista.editores.VistaEdicion#updateModelo()
     */
    public void updateModelo() {
	int index = comboVisibilidad.getSelectedIndex();
	switch (index) {
	case 0:
	    elemento.setVisibility(null);
	    break;
	case 1:
	    elemento.setVisibility(VisibilityKindEnum.VK_PUBLIC);
	    break;
	case 2:
	    elemento.setVisibility(VisibilityKindEnum.VK_PRIVATE);
	    break;
	case 3:
	    elemento.setVisibility(VisibilityKindEnum.VK_PROTECTED);
	    break;
	case 4:
	    elemento.setVisibility(VisibilityKindEnum.VK_PACKAGE);
	    break;
	}

	// multiplicidad
	Multiplicity mult = elemento.getMultiplicity();
	if (mult != null) {
	    mult.refDelete();
	    elemento.setMultiplicity(null);
	}
	Multiplicity nueva = MultiplicidadUtil
		.toMultiplicity((String) comboMultiplicidad.getSelectedItem());
	if (nueva != null) {
	    elemento.setMultiplicity(nueva);
	}
	//
	index = comboAgregacion.getSelectedIndex();
	switch (index) {
	case 0:
	    elemento.setAggregation(AggregationKindEnum.AK_NONE);
	    break;
	case 1:
	    elemento.setAggregation(AggregationKindEnum.AK_AGGREGATE);
	    break;
	case 2:
	    elemento.setAggregation(AggregationKindEnum.AK_COMPOSITE);
	    break;
	}
	elemento.setNavigable(chkNavegable.isSelected());
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * vista.editores.VistaEdicion#addListener(vista.editores.EditorListener)
     */
    public void addListener(EditorListener listener) {
    }

    /*
     * (non-Javadoc)
     * 
     * @see vista.Vista#setModelo(java.lang.Object)
     */
    public void setModelo(Object modelo) {
	elemento = (AssociationEnd) modelo;
    }

    /*
     * (non-Javadoc)
     * 
     * @see vista.Vista#updateVista()
     */
    public void updateVista() {
	if (elemento.getVisibility() == null)
	    comboVisibilidad.setSelectedIndex(0);
	else if (elemento.getVisibility() == VisibilityKindEnum.VK_PUBLIC)
	    comboVisibilidad.setSelectedIndex(1);
	else if (elemento.getVisibility() == VisibilityKindEnum.VK_PRIVATE)
	    comboVisibilidad.setSelectedIndex(2);
	else if (elemento.getVisibility() == VisibilityKindEnum.VK_PROTECTED)
	    comboVisibilidad.setSelectedIndex(3);
	else if (elemento.getVisibility() == VisibilityKindEnum.VK_PACKAGE)
	    comboVisibilidad.setSelectedIndex(4);

	if (elemento.getAggregation() == AggregationKindEnum.AK_NONE)
	    comboAgregacion.setSelectedIndex(0);
	else if (elemento.getAggregation() == AggregationKindEnum.AK_AGGREGATE)
	    comboAgregacion.setSelectedIndex(1);
	else if (elemento.getAggregation() == AggregationKindEnum.AK_COMPOSITE)
	    comboAgregacion.setSelectedIndex(2);

	Multiplicity mult = elemento.getMultiplicity();
	comboMultiplicidad.setSelectedItem(MultiplicidadUtil.toString(mult,
		MultiplicidadUtil.COMA));

	chkNavegable.setSelected(elemento.isNavigable());
    }

    /*
     * (non-Javadoc)
     * 
     * @see vista.Vista#getTitulo()
     */
    public String getTitulo() {
	return "Editor FinAsociacion";
    }

    /*
     * (non-Javadoc)
     * 
     * @see vista.Vista#modelPropertyChange(java.beans.PropertyChangeEvent)
     */
    public void modelPropertyChange(PropertyChangeEvent evt) {
    }

}
