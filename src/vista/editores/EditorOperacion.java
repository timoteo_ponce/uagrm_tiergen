/*
 * Created by JFormDesigner on Sun May 17 20:06:39 BOT 2009
 */

package vista.editores;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.TitledBorder;

import org.omg.uml.foundation.core.ModelElement;
import org.omg.uml.foundation.core.Operation;
import org.omg.uml.foundation.core.Parameter;

import util.ModeloLista;
import util.NodoElemento;
import util.uml.UMLUtil;

import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;

// TODO: Auto-generated Javadoc
/**
 * The Class EditorOperacion.
 * 
 * @author Timo
 */
public final class EditorOperacion extends EditorBase {

    /** The modelo parametros. */
    private ModeloLista<NodoElemento> modeloParametros;

    /**
     * Instantiates a new editor operacion.
     */
    public EditorOperacion() {
	initComponents();
    }

    /**
     * Instantiates a new editor operacion.
     * 
     * @param operation
     *            the operation
     */
    public EditorOperacion(Operation operation) {
	super(operation);
	initComponents();
	modeloParametros = new ModeloLista<NodoElemento>();
	listaParametros.setModel(modeloParametros);
    }

    /**
     * Btn add par action performed.
     * 
     * @param e
     *            the e
     */
    private void btnAddParActionPerformed() {
	addParametro();
    }

    /**
     * Btn edit par action performed.
     * 
     * @param e
     *            the e
     */
    private void btnEditParActionPerformed() {
	editParametro();
    }

    /**
     * Btn del par action performed.
     * 
     * @param e
     *            the e
     */
    private void btnDelParActionPerformed() {
	removeParametro();
    }

    private void listaParametrosMouseClicked(MouseEvent e) {
	if (e.getClickCount() == 2)
	    editParametro();
    }

    /**
     * Inits the components.
     */
    private void initComponents() {
	// JFormDesigner - Component initialization - DO NOT MODIFY
	// //GEN-BEGIN:initComponents
	scrollPane = new JScrollPane();
	listaParametros = new JList();
	panelButtons = new JPanel();
	btnAddPar = new JButton();
	btnEditPar = new JButton();
	btnDelPar = new JButton();
	chkAbstracto = new JCheckBox();
	CellConstraints cc = new CellConstraints();

	// ======== this ========
	setLayout(new FormLayout(
		"default, $lcgap, 111dlu, $lcgap, 76dlu, $lcgap, default:grow",
		"default, $lgap, 117dlu, $lgap, 13dlu:grow"));

	// ======== scrollPane ========
	{
	    scrollPane.setViewportBorder(new TitledBorder("Parametros"));

	    // ---- listaParametros ----
	    listaParametros.addMouseListener(new MouseAdapter() {
		@Override
		public void mouseClicked(MouseEvent e) {
		    listaParametrosMouseClicked(e);
		}
	    });
	    scrollPane.setViewportView(listaParametros);
	}
	add(scrollPane, cc.xywh(3, 3, 1, 1, CellConstraints.FILL,
		CellConstraints.FILL));

	// ======== panelButtons ========
	{
	    panelButtons.setLayout(new FormLayout(
		    "default, $lcgap, 49dlu, $lcgap, default",
		    "3*(default, $lgap), default"));

	    // ---- btnAddPar ----
	    btnAddPar.setText("Agregar");
	    btnAddPar.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
		    btnAddParActionPerformed();
		}
	    });
	    panelButtons.add(btnAddPar, cc.xy(3, 1));

	    // ---- btnEditPar ----
	    btnEditPar.setText("Modificar");
	    btnEditPar.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
		    btnEditParActionPerformed();
		}
	    });
	    panelButtons.add(btnEditPar, cc.xy(3, 3));

	    // ---- btnDelPar ----
	    btnDelPar.setText("Eliminar");
	    btnDelPar.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
		    btnDelParActionPerformed();
		}
	    });
	    panelButtons.add(btnDelPar, cc.xy(3, 5));

	    // ---- chkAbstracto ----
	    chkAbstracto.setText("Abstracto");
	    panelButtons.add(chkAbstracto, cc.xy(3, 7));
	}
	add(panelButtons, cc.xy(5, 3));
	// //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY
    // //GEN-BEGIN:variables
    private JScrollPane scrollPane;
    private JList listaParametros;
    private JPanel panelButtons;
    private JButton btnAddPar;
    private JButton btnEditPar;
    private JButton btnDelPar;
    private JCheckBox chkAbstracto;

    // JFormDesigner - End of variables declaration //GEN-END:variables

    /*
     * (non-Javadoc)
     * 
     * @see vista.editores.EditorBase#getElement()
     */
    public Operation getElement() {
	return (Operation) super.getElement();
    }

    /*
     * (non-Javadoc)
     * 
     * @see vista.editores.EditorBase#updateVista()
     */
    @Override
    public void updateVista() {
	chkAbstracto.setSelected(getElement().isAbstract());
	modeloParametros.clear();

	for (Object elem : getElement().getParameter()) {
	    modeloParametros.add(new NodoElemento((ModelElement) elem));
	}
    }

    /*
     * (non-Javadoc)
     * 
     * @see vista.editores.EditorBase#updateModelo()
     */
    @Override
    public void updateModelo() {
	getElement().setAbstract(chkAbstracto.isSelected());
    }

    /*
     * (non-Javadoc)
     * 
     * @see vista.editores.EditorBase#getTitulo()
     */
    @Override
    public String getTitulo() {
	return this.getClass().getSimpleName();
    }

    /**
     * Adds the parametro.
     */
    private void addParametro() {
	Parameter par = UMLUtil.crearParametro("Parametro"
		+ (getElement().getParameter().size() + 1));
	getElement().getParameter().add(par);
	updateVista();
    }

    /**
     * Edits the parametro.
     */
    private void editParametro() {
	final int index = listaParametros.getSelectedIndex();
	if (index != -1) {
	    NodoElemento nodo = (NodoElemento) listaParametros
		    .getSelectedValue();
	    DialogoEditor dialog = new DialogoEditor(null, true, nodo
		    .getElemento());
	    dialog.addListener(new EditorListener() {
		public void aceptar(ModelElement elemento) {
		    updateVista();
		}

		public void cancelar() {
		    updateVista();
		}
	    });
	    dialog.setVisible(true);
	}
    }

    /**
     * Removes the parametro.
     */
    private void removeParametro() {
	final int index = listaParametros.getSelectedIndex();
	if (index != -1) {
	    NodoElemento nodo = (NodoElemento) listaParametros
		    .getSelectedValue();
	    nodo.getElemento().refDelete();
	    modeloParametros.remove(index);
	}
    }
}
