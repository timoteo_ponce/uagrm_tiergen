/*
 * Created by JFormDesigner on Sun May 17 20:19:12 BOT 2009
 */

package vista.editores;

import java.util.Collection;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JTextField;

import org.omg.uml.foundation.core.Attribute;
import org.omg.uml.foundation.core.Classifier;
import org.omg.uml.foundation.core.DataType;
import org.omg.uml.foundation.core.ModelElement;
import org.omg.uml.foundation.core.Parameter;
import org.omg.uml.foundation.core.UmlClass;
import org.omg.uml.foundation.datatypes.Expression;
import org.omg.uml.foundation.datatypes.ParameterDirectionKind;
import org.omg.uml.foundation.datatypes.ParameterDirectionKindEnum;

import util.NodoElemento;
import util.uml.UMLUtil;

import com.jgoodies.forms.factories.DefaultComponentFactory;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;

// TODO: Auto-generated Javadoc
/**
 * The Class EditorParametro.
 * 
 * @author Timo
 */
public final class EditorParametro extends EditorBase {

    /**
     * Instantiates a new editor parametro.
     */
    public EditorParametro() {
	initComponents();
    }

    /**
     * Instantiates a new editor parametro.
     * 
     * @param element
     *            the element
     */
    public EditorParametro(Parameter element) {
	super(element);
	initComponents();
	loadTypesToComboBox(comboTipos);
	loadDirectionKinds();
    }

    /**
     * Load direction kinds.
     */
    private void loadDirectionKinds() {
	comboDireccion.removeAllItems();
	comboDireccion.addItem(ParameterDirectionKindEnum.PDK_IN);
	comboDireccion.addItem(ParameterDirectionKindEnum.PDK_OUT);
	comboDireccion.addItem(ParameterDirectionKindEnum.PDK_INOUT);
	comboDireccion.addItem(ParameterDirectionKindEnum.PDK_RETURN);
    }

    /*
     * (non-Javadoc)
     * 
     * @see vista.editores.EditorBase#loadTypesToComboBox(javax.swing.JComboBox)
     */
    @Override
    public void loadTypesToComboBox(JComboBox comboTipos) {
	// cargamos todos los elementos de tipo
	comboTipos.removeAllItems();
	if (getElement().getType() != null) {
	    comboTipos.addItem(new NodoElemento(((Attribute) getElement())
		    .getType()));
	    comboTipos.setSelectedIndex(1);
	}
	final Collection<Classifier> classes = UMLUtil.getAllClasificadores();
	for (Classifier elem : classes) {
	    if (elem instanceof UmlClass) {
		comboTipos.addItem(new NodoElemento((ModelElement) elem));
	    }
	}
	final Collection<DataType> types = UMLUtil.getAllTipos();
	for (DataType elem : types) {
	    comboTipos.addItem(new NodoElemento((ModelElement) elem));
	}
    }

    /**
     * Inits the components.
     */
    private void initComponents() {
	// JFormDesigner - Component initialization - DO NOT MODIFY
	// //GEN-BEGIN:initComponents
	DefaultComponentFactory compFactory = DefaultComponentFactory
		.getInstance();
	lblType = compFactory.createLabel("Tipo:");
	comboTipos = new JComboBox();
	lblValue = compFactory.createLabel("Valor:");
	txtValor = new JTextField();
	lblDirection = compFactory.createLabel("Direccion:");
	comboDireccion = new JComboBox();
	CellConstraints cc = new CellConstraints();

	// ======== this ========
	setLayout(new FormLayout(
		"right:55dlu, $lcgap, 103dlu, $lcgap, 38dlu:grow",
		"4*(default, $lgap), default:grow"));

	// ---- lblType ----
	lblType.setLabelFor(comboTipos);
	add(lblType, cc.xy(1, 3));
	add(comboTipos, cc.xy(3, 3));

	// ---- lblValue ----
	lblValue.setLabelFor(txtValor);
	add(lblValue, cc.xy(1, 5));
	add(txtValor, cc.xy(3, 5));

	// ---- lblDirection ----
	lblDirection.setLabelFor(comboDireccion);
	add(lblDirection, cc.xy(1, 7));
	add(comboDireccion, cc.xy(3, 7));
	// JFormDesigner - End of component initialization
	// //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY
    // //GEN-BEGIN:variables
    /** The lbl type. */
    private JLabel lblType;

    /** The combo tipos. */
    private JComboBox comboTipos;

    /** The lbl value. */
    private JLabel lblValue;

    /** The txt valor. */
    private JTextField txtValor;

    /** The lbl direction. */
    private JLabel lblDirection;

    /** The combo direccion. */
    private JComboBox comboDireccion;

    // JFormDesigner - End of variables declaration //GEN-END:variables

    /*
     * (non-Javadoc)
     * 
     * @see vista.editores.EditorBase#getElement()
     */
    public Parameter getElement() {
	return (Parameter) super.getElement();
    }

    /*
     * (non-Javadoc)
     * 
     * @see vista.editores.EditorBase#updateModelo()
     */
    @Override
    public void updateModelo() {
	// tipo de dato
	Object temp = comboTipos.getSelectedItem();

	if (temp != null && !temp.toString().isEmpty()) {
	    NodoElemento tipo = (NodoElemento) temp;
	    if (tipo.getElemento() instanceof Classifier) {
		getElement().setType((Classifier) tipo.getElemento());
	    } else if (tipo.getElemento() instanceof DataType) {
		getElement().setType((DataType) tipo.getElemento());
	    } else {
		DataType dt = UMLUtil.crearTipoDato(temp.toString());
		getElement().setType(dt);
	    }
	} else
	    getElement().setType(null);
	// valor
	Expression exp = getElement().getDefaultValue();
	if (txtValor.getText().isEmpty() && exp != null) {
	    exp.refDelete();
	    getElement().setDefaultValue(null);
	} else {
	    if (exp == null) {
		exp = UMLUtil.crearExpresion();
		exp.setLanguage("java");
		getElement().setDefaultValue(exp);
	    }
	    exp.setBody(txtValor.getText());
	}
	// direccion
	getElement().setKind(
		(ParameterDirectionKind) comboDireccion.getSelectedItem());
    }

    /*
     * (non-Javadoc)
     * 
     * @see vista.editores.EditorBase#updateVista()
     */
    @Override
    public void updateVista() {
	if (getElement().getDefaultValue() != null)
	    txtValor.setText(getElement().getDefaultValue().getBody());
	comboDireccion.setSelectedItem(getElement().getKind());
    }

    /*
     * (non-Javadoc)
     * 
     * @see vista.editores.EditorBase#getTitulo()
     */
    @Override
    public String getTitulo() {
	return this.getClass().getSimpleName();
    }
}
