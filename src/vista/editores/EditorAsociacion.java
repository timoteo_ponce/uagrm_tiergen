/*
 * Created by JFormDesigner on Sun May 17 11:17:51 BOT 2009
 */

package vista.editores;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Iterator;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.FetchType;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.TitledBorder;

import modelo.types.Relationships;

import org.omg.uml.foundation.core.AssociationEnd;
import org.omg.uml.foundation.core.ModelElement;
import org.omg.uml.foundation.core.UmlAssociation;
import org.uml.diagrammanagement.GraphNode;

import util.Constantes;
import util.ModeloLista;
import util.NodoElemento;
import util.uml.ElementoVisualUtil;
import util.uml.RelacionUtil;
import util.uml.UMLUtil;
import vista.Main;

import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;

/**
 * The Class EditorAsociacion.
 * 
 * @author Juan Timoteo Ponce Ortiz
 */
public final class EditorAsociacion extends EditorBase {

    /** The modelo asoc ends. */
    private ModeloLista<NodoElemento> modeloAsocEnds;

    /** The type. */
    private Relationships type;

    /** The properties. */
    private Map properties;

    /**
     * Instantiates a new editor asociacion.
     */
    public EditorAsociacion() {
	initComponents();
    }

    /**
     * Instantiates a new editor asociacion.
     * 
     * @param elemento
     *            the elemento
     */
    public EditorAsociacion(UmlAssociation elemento) {
	super(elemento);
	modeloAsocEnds = new ModeloLista<NodoElemento>();
	initComponents();
	init();
    }

    /**
     * Inits the.
     */
    private void init() {
	listaAsociacionEnd.setModel(modeloAsocEnds);

	comboCascade.removeAllItems();
	comboCascade.addItem("NONE");
	CascadeType[] cascadeTypes = CascadeType.values();
	for (CascadeType cascadeType : cascadeTypes)
	    comboCascade.addItem(cascadeType);

	comboFetch.removeAllItems();
	comboFetch.addItem("NONE");
	FetchType[] fetchTypes = FetchType.values();
	for (FetchType fetchType : fetchTypes)
	    comboFetch.addItem(fetchType);
    }

    /**
     * Btn add action performed.
     * 
     * @param e
     *            the e
     */
    private void btnAddActionPerformed() {
	addFinAsociacion();
    }

    /**
     * Btn edit action performed.
     * 
     * @param e
     *            the e
     */
    private void btnEditActionPerformed() {
	editFinAsociacion();
    }

    /**
     * Btn del action performed.
     * 
     * @param e
     *            the e
     */
    private void btnDelActionPerformed() {
	delFinAsociacion();
    }

    private void listaAsociacionEndMouseClicked(MouseEvent e) {
	if (e.getClickCount() == 2)
	    editFinAsociacion();
    }

    /**
     * Inits the components.
     */
    private void initComponents() {
	// JFormDesigner - Component initialization - DO NOT MODIFY
	// //GEN-BEGIN:initComponents
	scrollPane = new JScrollPane();
	listaAsociacionEnd = new JList();
	panelButtons = new JPanel();
	btnAdd = new JButton();
	btnEdit = new JButton();
	btnDel = new JButton();
	panel1 = new JPanel();
	lblType = new JLabel();
	lblValueType = new JLabel();
	lblCascade = new JLabel();
	comboCascade = new JComboBox();
	lblFetch = new JLabel();
	comboFetch = new JComboBox();
	CellConstraints cc = new CellConstraints();

	// ======== this ========
	setLayout(new FormLayout("default, $lcgap, 137dlu, $lcgap, 93dlu",
		"default, $lgap, 80dlu:grow, 2*($lgap, default)"));

	// ======== scrollPane ========
	{
	    scrollPane.setViewportBorder(new TitledBorder("Elementos"));

	    // ---- listaAsociacionEnd ----
	    listaAsociacionEnd.addMouseListener(new MouseAdapter() {
		@Override
		public void mouseClicked(MouseEvent e) {
		    listaAsociacionEndMouseClicked(e);
		}
	    });
	    scrollPane.setViewportView(listaAsociacionEnd);
	}
	add(scrollPane, cc.xywh(3, 3, 1, 1, CellConstraints.FILL,
		CellConstraints.FILL));

	// ======== panelButtons ========
	{
	    panelButtons.setLayout(new FormLayout(
		    "9dlu, $lcgap, 61dlu, $lcgap, default:grow",
		    "2*(default, $lgap), default"));

	    // ---- btnAdd ----
	    btnAdd.setText("Agregar");
	    btnAdd.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
		    btnAddActionPerformed();
		}
	    });
	    panelButtons.add(btnAdd, cc.xy(3, 1));

	    // ---- btnEdit ----
	    btnEdit.setText("Modificar");
	    btnEdit.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
		    btnEditActionPerformed();
		}
	    });
	    panelButtons.add(btnEdit, cc.xy(3, 3));

	    // ---- btnDel ----
	    btnDel.setText("Eliminar");
	    btnDel.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
		    btnDelActionPerformed();
		}
	    });
	    panelButtons.add(btnDel, cc.xy(3, 5));
	}
	add(panelButtons, cc.xy(5, 3));

	// ======== panel1 ========
	{
	    panel1.setLayout(new FormLayout("50dlu, $lcgap, 79dlu:grow",
		    "2*(default, $lgap), default"));

	    // ---- lblType ----
	    lblType.setText("Tipo:");
	    panel1.add(lblType, cc.xy(1, 1));

	    // ---- lblValueType ----
	    lblValueType.setText("text");
	    panel1.add(lblValueType, cc.xy(3, 1));

	    // ---- lblCascade ----
	    lblCascade.setText("Propagacion:");
	    panel1.add(lblCascade, cc.xy(1, 3));

	    // ---- comboCascade ----
	    comboCascade.setEnabled(false);
	    panel1.add(comboCascade, cc.xy(3, 3));

	    // ---- lblFetch ----
	    lblFetch.setText("Extraccion:");
	    panel1.add(lblFetch, cc.xy(1, 5));

	    // ---- comboFetch ----
	    comboFetch.setEnabled(false);
	    panel1.add(comboFetch, cc.xy(3, 5));
	}
	add(panel1, cc.xy(3, 5));
	// //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY
    // //GEN-BEGIN:variables
    private JScrollPane scrollPane;
    private JList listaAsociacionEnd;
    private JPanel panelButtons;
    private JButton btnAdd;
    private JButton btnEdit;
    private JButton btnDel;
    private JPanel panel1;
    private JLabel lblType;
    private JLabel lblValueType;
    private JLabel lblCascade;
    private JComboBox comboCascade;
    private JLabel lblFetch;
    private JComboBox comboFetch;

    // JFormDesigner - End of variables declaration //GEN-END:variables

    /**
     * Adds the fin asociacion.
     */
    private void addFinAsociacion() {
	if (util.AppUtil.showConfirmar(null, "Advertencia",
		"Seguro que quiere agregar un fin de asociacion?")) {
	    AssociationEnd ase = UMLUtil.crearAsociacionEnd();
	    ase.setAssociation(getElement());
	    getElement().getConnection().add(ase);
	    modeloAsocEnds.add(new NodoElemento(ase));
	}
    }

    /**
     * Edits the fin asociacion.
     */
    private void editFinAsociacion() {
	int index = listaAsociacionEnd.getSelectedIndex();
	if (index != -1) {
	    NodoElemento nodo = (NodoElemento) listaAsociacionEnd
		    .getSelectedValue();
	    DialogoEditor dialog = new DialogoEditor(null, true, nodo
		    .getElemento());
	    dialog.addListener(new EditorListener() {
		@Override
		public void aceptar(ModelElement elemento) {
		    updateVista();
		}

		@Override
		public void cancelar() {
		    updateVista();
		}
	    });
	    dialog.setVisible(true);
	}
    }

    /**
     * Del fin asociacion.
     */
    private void delFinAsociacion() {
	if (util.AppUtil.showConfirmar(null, "Advertencia",
		"Seguro que quiere eliminar un fin de asociacion?")) {
	    int index = listaAsociacionEnd.getSelectedIndex();
	    if (index != -1) {
		NodoElemento nodo = (NodoElemento) listaAsociacionEnd
			.getSelectedValue();
		nodo.getElemento().refDelete();
		modeloAsocEnds.remove(index);
	    }
	}
    }

    /*
     * (non-Javadoc)
     * 
     * @see vista.editores.EditorBase#getElement()
     */
    @Override
    public UmlAssociation getElement() {
	return (UmlAssociation) super.getElement();
    }

    /*
     * (non-Javadoc)
     * 
     * @see vista.editores.EditorBase#loadProperties()
     */
    @Override
    public void loadProperties() {
	if (getGraphElement() == null) {
	    setGraphElement(ElementoVisualUtil.createGraphElement(getElement(),
		    Constantes.MODEL_EDGE_TYPE_ID));
	    Main.getUIPrincipal().getCurrentDiagram().getContained().add(
		    ((GraphNode) getGraphElement()));
	}
	properties = ElementoVisualUtil.getPropiedades(getGraphElement());
    }

    /*
     * (non-Javadoc)
     * 
     * @see vista.editores.EditorBase#updateVista()
     */
    @Override
    public void updateVista() {
	modeloAsocEnds.clear();
	final Iterator<AssociationEnd> it = getElement().getConnection()
		.iterator();
	while (it.hasNext()) {
	    AssociationEnd ase = it.next();
	    modeloAsocEnds.add(new NodoElemento(ase));
	}
	if (RelacionUtil.isOneToOne(getElement()))
	    type = Relationships.ONE_TO_ONE;
	else if (RelacionUtil.isOneToMany(getElement()))
	    type = Relationships.ONE_TO_MANY;
	else if (RelacionUtil.isManyToOne(getElement()))
	    type = Relationships.MANY_TO_ONE;
	else if (RelacionUtil.isManyToMany(getElement()))
	    type = Relationships.MANY_TO_MANY;
	else
	    type = null;
	lblValueType.setText((type == null ? "NORMAL" : type.toString()));

	loadProperties();
	if (type != null) {
	    comboCascade.setEnabled(true);
	    comboFetch.setEnabled(true);

	    String property = (String) properties
		    .get(Relationships.CASCADE_TYPE.toString());
	    if (property != null && !property.isEmpty())
		comboCascade.setSelectedItem(CascadeType.valueOf(property));
	    else
		comboCascade.setSelectedIndex(0);

	    property = (String) properties.get(Relationships.FETCH_TYPE
		    .toString());
	    if (property != null && !property.isEmpty())
		comboFetch.setSelectedItem(FetchType.valueOf(property));
	    else
		comboFetch.setSelectedIndex(0);
	}
    }

    /*
     * (non-Javadoc)
     * 
     * @see vista.editores.EditorBase#updateModelo()
     */
    @Override
    public void updateModelo() {
	if (comboCascade.isEnabled()) {
	    if (comboCascade.getSelectedIndex() > 0)
		ElementoVisualUtil.setProperty(getGraphElement(),
			Relationships.CASCADE_TYPE.toString(), comboCascade
				.getSelectedItem().toString());
	    else
		ElementoVisualUtil.setProperty(getGraphElement(),
			Relationships.CASCADE_TYPE.toString(), "");
	    if (comboFetch.getSelectedIndex() > 0)
		ElementoVisualUtil.setProperty(getGraphElement(),
			Relationships.FETCH_TYPE.toString(), comboFetch
				.getSelectedItem().toString());
	    else
		ElementoVisualUtil.setProperty(getGraphElement(),
			Relationships.FETCH_TYPE.toString(), "");
	}
	/*
	 * getElement().getConnection().clear(); for (int i = 0; i <
	 * modeloAsocEnds.getSize() ; i++) { AssociationEnd ae =
	 * (AssociationEnd) ( (NodoElemento) modeloAsocEnds.getElementAt( i )
	 * ).getElemento(); getElement().getConnection().add( ae ); }
	 */
    }

    /*
     * (non-Javadoc)
     * 
     * @see vista.editores.EditorBase#getTitulo()
     */
    @Override
    public String getTitulo() {
	return this.getClass().getSimpleName();
    }
}
