/*
 * Created by JFormDesigner on Sun May 17 18:34:57 BOT 2009
 */

package vista.editores;

import java.util.Map;

import javax.persistence.InheritanceType;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JTextField;

import modelo.types.Relationships;

import org.omg.uml.foundation.core.Generalization;
import org.uml.diagrammanagement.GraphNode;

import util.Constantes;
import util.uml.ElementoVisualUtil;
import vista.Main;

import com.jgoodies.forms.factories.DefaultComponentFactory;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;

/**
 * The Class EditorGeneralizacion.
 * 
 * @author Timo
 */
public final class EditorGeneralizacion extends EditorBase {

    /**
     * Instantiates a new editor generalizacion.
     */
    public EditorGeneralizacion() {
	initComponents();
    }

    /**
     * Instantiates a new editor generalizacion.
     * 
     * @param element
     *            the element
     */
    public EditorGeneralizacion(Generalization element) {
	super(element);
	initComponents();
	loadMetadata();
	loadInheritanceStrategies();
    }

    /**
     * Load inheritance strategies.
     */
    private void loadInheritanceStrategies() {
	comboInheritanceStratg.setEnabled(true);
	comboInheritanceStratg.removeAllItems();
	comboInheritanceStratg.addItem("NONE");
	InheritanceType[] types = InheritanceType.values();
	for (InheritanceType item : types)
	    comboInheritanceStratg.addItem(item);
    }

    /**
     * Load metadata.
     */
    private void loadMetadata() {
	if (getGraphElement() == null) {
	    setGraphElement(ElementoVisualUtil.createGraphElement(getElement(),
		    Constantes.MODEL_EDGE_TYPE_ID));
	    Main.getUIPrincipal().getCurrentDiagram().getContained().add(
		    ((GraphNode) getGraphElement()));
	}
    }

    /**
     * Inits the components.
     */
    private void initComponents() {
	// JFormDesigner - Component initialization - DO NOT MODIFY
	// //GEN-BEGIN:initComponents
	DefaultComponentFactory compFactory = DefaultComponentFactory
		.getInstance();
	lblHijo = compFactory.createLabel("Hijo:");
	txtHijo = new JTextField();
	lblPadre = compFactory.createLabel("Padre:");
	txtPadre = new JTextField();
	lblInheritance = compFactory.createLabel("Herencia");
	comboInheritanceStratg = new JComboBox();
	CellConstraints cc = new CellConstraints();

	// ======== this ========
	setLayout(new FormLayout(
		"default, $lcgap, right:37dlu, $lcgap, 103dlu, $lcgap, default:grow",
		"default, $lgap, 13dlu, 2*($lgap, default), $lgap, default:grow"));

	// ---- lblHijo ----
	lblHijo.setLabelFor(txtHijo);
	add(lblHijo, cc.xy(3, 3));

	// ---- txtHijo ----
	txtHijo.setEditable(false);
	add(txtHijo, cc.xy(5, 3));

	// ---- lblPadre ----
	lblPadre.setLabelFor(txtPadre);
	add(lblPadre, cc.xy(3, 5));

	// ---- txtPadre ----
	txtPadre.setEditable(false);
	add(txtPadre, cc.xy(5, 5));

	// ---- lblInheritance ----
	lblInheritance.setLabelFor(comboInheritanceStratg);
	add(lblInheritance, cc.xy(3, 7));
	add(comboInheritanceStratg, cc.xy(5, 7));
	// //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY
    // //GEN-BEGIN:variables
    private JLabel lblHijo;
    private JTextField txtHijo;
    private JLabel lblPadre;
    private JTextField txtPadre;
    private JLabel lblInheritance;
    private JComboBox comboInheritanceStratg;

    // JFormDesigner - End of variables declaration //GEN-END:variables

    /*
     * (non-Javadoc)
     * 
     * @see vista.editores.EditorBase#getElement()
     */
    public Generalization getElement() {
	return (Generalization) super.getElement();
    }

    /*
     * (non-Javadoc)
     * 
     * @see vista.editores.EditorBase#getTitulo()
     */
    @Override
    public String getTitulo() {
	return this.getClass().getSimpleName();
    }

    /*
     * (non-Javadoc)
     * 
     * @see vista.editores.EditorBase#updateVista()
     */
    @Override
    public void updateVista() {
	txtHijo.setText(getElement().getChild().getName());
	txtPadre.setText(getElement().getParent().getName());
	Map properties = ElementoVisualUtil.getPropiedades(getGraphElement());

	Object prop = properties.get(Relationships.INHERITANCE_TYPE.toString());
	if (prop != null && !prop.toString().isEmpty())
	    comboInheritanceStratg.setSelectedItem(InheritanceType.valueOf(prop
		    .toString()));
	else
	    comboInheritanceStratg.setSelectedIndex(0);
    }

    /*
     * (non-Javadoc)
     * 
     * @see vista.editores.EditorBase#updateModelo()
     */
    @Override
    public void updateModelo() {
	if (comboInheritanceStratg.getSelectedIndex() > 0) {
	    ElementoVisualUtil.setProperty(getGraphElement(),
		    Relationships.INHERITANCE_TYPE.toString(),
		    comboInheritanceStratg.getSelectedItem().toString());
	} else
	    ElementoVisualUtil.setProperty(getGraphElement(),
		    Relationships.INHERITANCE_TYPE.toString(), "");
    }
}
