/*
 * 
 */
package vista.editores;

import java.awt.Container;
import java.beans.PropertyChangeEvent;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import org.apache.log4j.Logger;
import org.omg.uml.foundation.core.AssociationEnd;
import org.omg.uml.foundation.core.Attribute;
import org.omg.uml.foundation.core.Classifier;
import org.omg.uml.foundation.core.Feature;
import org.omg.uml.foundation.core.GeneralizableElement;
import org.omg.uml.foundation.core.Generalization;
import org.omg.uml.foundation.core.ModelElement;
import org.omg.uml.foundation.core.Namespace;
import org.omg.uml.foundation.core.Operation;
import org.omg.uml.foundation.core.Parameter;
import org.omg.uml.foundation.core.Stereotype;
import org.omg.uml.foundation.core.UmlAssociation;

import util.NodoElemento;
import util.uml.UMLUtil;

import com.jgoodies.forms.factories.DefaultComponentFactory;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.uif_lite.panel.SimpleInternalFrame;

/**
 * The Class PanelEditorGenerico.
 * 
 * @author Timo
 */
public class PanelEditorGenerico extends JPanel implements VistaEdicion {

	/** The log. */
	private static Logger log = Logger.getLogger(PanelEditorGenerico.class);

	/** The editores. */
	private final List<VistaEdicion> editores;

	/** The elemento. */
	private ModelElement elemento;

	/** The dialog. */
	private final DialogoEditor dialog;

	private final Namespace namespace;

	/**
	 * Instantiates a new panel editor generico.
	 * 
	 * @param dialog
	 *            the dialog
	 */
	public PanelEditorGenerico(final DialogoEditor dialog,
			final Namespace namespace) {
		this.dialog = dialog;
		editores = new ArrayList<VistaEdicion>();
		this.namespace = namespace;
		initComponents();
	}

	/**
	 * Creates new form EditorElementoModelo.
	 * 
	 * @param elemento
	 *            the elemento
	 * @param dialog
	 *            the dialog
	 */
	public PanelEditorGenerico(final ModelElement elemento,
			final DialogoEditor dialog) {
		this(dialog, elemento.getNamespace());
		this.elemento = elemento;
		addEditoresNecesarios();
		updateVista();
	}


	/**
	 * Adds the editores necesarios.
	 */
	private void addEditoresNecesarios() {
		log.info("Adding necessary editors");
		if (elemento instanceof GeneralizableElement) {
			addEditor(new EditorGeneralizable((GeneralizableElement) elemento));
		}
		if (elemento instanceof Classifier) {
			addEditor(new EditorClasificador((Classifier) elemento));
			addEditor(new EditorAtributoClasificador((Classifier) elemento));
			addEditor(new EditorOperacionClasificador((Classifier) elemento));
		}
		if (elemento instanceof Feature) {
			addEditor(new EditorCaracteristica((Feature) elemento));
		}
		if (elemento instanceof Attribute) {
			addEditor(new EditorAtributo((Attribute) elemento));
		}
		if (elemento instanceof Operation) {
			addEditor(new EditorOperacion((Operation) elemento));
		}
		if (elemento instanceof Parameter) {
			addEditor(new EditorParametro((Parameter) elemento));
		}
		if (elemento instanceof UmlAssociation) {
			addEditor(new EditorAsociacion((UmlAssociation) elemento));
		}
		if (elemento instanceof AssociationEnd) {
			addEditor(new EditorFinAsociacion((AssociationEnd) elemento));
		}
		if (elemento instanceof Generalization) {
			addEditor(new EditorGeneralizacion((Generalization) elemento));
		}
	}

	/**
	 * Sets the modelo.
	 * 
	 * @param modelo
	 *            the modelo
	 */
	public void setModelo(final Object modelo) {
		this.elemento = (ModelElement) modelo;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see vista.Vista#updateVista()
	 */
	public void updateVista() {
		// <editor-fold defaultstate="collapsed" desc="UML values">
		txtNombre.setText(elemento.getName());
		if (elemento.getNamespace() != null) {
			txtNamespace.setText(elemento.getNamespace().getName());
		} else {
			txtNamespace.setText("Nada");
		}
		// estereotipos
		comboEstereotipo.removeAllItems();
		// comboEstereotipo.addItem("");
		final Collection<Stereotype> estereotypes = UMLUtil.getAllStereotypes();
		for (Stereotype elem : estereotypes) {
			comboEstereotipo.addItem(new NodoElemento(elem));
		}

		if (elemento.getStereotype().size() > 0) {
			comboEstereotipo.setSelectedItem(new NodoElemento(
					(ModelElement) elemento.getStereotype().toArray()[0]));
		}
		// </editor-fold>
		// <editor-fold defaultstate="collapsed" desc="Update editors">
		for (VistaEdicion elem : editores) {
			elem.updateVista();
		}
		for (VistaEdicion elem : editores) {
			elem.updateVista();
		}
		// </editor-fold>
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see vista.editores.VistaEdicion#updateModelo()
	 */
	public void updateModelo() {
		// <editor-fold defaultstate="collapsed" desc="UML values">
		// nombre
		if (!txtNombre.getText().isEmpty()) {
			elemento.setName(txtNombre.getText());
		}
		// estereotipo
		elemento.getStereotype().clear();
		final Object selected = comboEstereotipo.getSelectedItem();

		if (selected != null && !selected.toString().isEmpty()) {
			if (selected instanceof NodoElemento) {
				final NodoElemento nodo = (NodoElemento) selected;
				elemento.getStereotype().add(nodo.getElemento());
			} else {
				final Stereotype estereo = UMLUtil
				.crearEstereotipo(comboEstereotipo.getSelectedItem()
						.toString());
				estereo.setNamespace(namespace);
				estereo.getBaseClass().add(elemento.getClass().getName());
				elemento.getStereotype().add(estereo);
			}
		}
		// </editor-fold>
		for (VistaEdicion elem : editores) {
			elem.updateModelo();
		}
	}

	/**
	 * Adds the listener.
	 * 
	 * @param listener
	 *            the listener
	 */
	public void addListener(final EditorListener listener) {
	}

	/**
	 * Adds the editor.
	 * 
	 * @param editor
	 *            the editor
	 */
	public void addEditor(final VistaEdicion editor) {
		log.info("Adding " + editor.getTitulo());
		if (!editores.contains(editor)) {
			editores.add(editor);
			panelEditores.add(editor.getTitulo(), (JPanel) editor);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see vista.Vista#getTitulo()
	 */
	public String getTitulo() {
		return "PanelEditorGenerico";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see vista.Vista#modelPropertyChange(java.beans.PropertyChangeEvent)
	 */
	public void modelPropertyChange(final PropertyChangeEvent evt) {
	}

	/**
	 * Inits the components.
	 */
	private void initComponents() {
		// JFormDesigner - Component initialization - DO NOT
		// MODIFY//GEN-BEGIN:initComponents
		DefaultComponentFactory compFactory = DefaultComponentFactory
		.getInstance();
		commonPanel = new SimpleInternalFrame();
		lblName = compFactory.createLabel("Nombre:");
		txtNombre = new JTextField();
		lblNamespace = compFactory.createLabel("AreaNombre:");
		txtNamespace = new JTextField();
		lblStereotype = compFactory.createLabel("Estereotipo:");
		comboEstereotipo = new JComboBox();
		panelEditores = new JTabbedPane();
		CellConstraints cc = new CellConstraints();

		// ======== this ========
		setLayout(new FormLayout(
				"default:grow, 203dlu:grow, $lcgap, default:grow, $lcgap, default",
		"88dlu, $lgap, 65dlu:grow"));

		// ======== commonPanel ========
		{
			commonPanel.setTitle("Comun");
			Container commonPanelContentPane = commonPanel.getContentPane();
			commonPanelContentPane
			.setLayout(new FormLayout(
					"default:grow, $lcgap, left:49dlu, $lcgap, 125dlu, $lcgap, default:grow",
			"3*(default, $lgap), default"));

			// ---- lblName ----
			lblName.setHorizontalAlignment(SwingConstants.RIGHT);
			commonPanelContentPane.add(lblName, cc.xywh(3, 1, 1, 1,
					CellConstraints.RIGHT, CellConstraints.DEFAULT));
			commonPanelContentPane.add(txtNombre, cc.xy(5, 1));
			commonPanelContentPane.add(lblNamespace, cc.xywh(3, 3, 1, 1,
					CellConstraints.RIGHT, CellConstraints.DEFAULT));

			// ---- txtNamespace ----
			txtNamespace.setEditable(false);
			commonPanelContentPane.add(txtNamespace, cc.xy(5, 3));
			commonPanelContentPane.add(lblStereotype, cc.xywh(3, 5, 1, 1,
					CellConstraints.RIGHT, CellConstraints.DEFAULT));

			// ---- comboEstereotipo ----
			comboEstereotipo.setEditable(true);
			commonPanelContentPane.add(comboEstereotipo, cc.xy(5, 5));
		}
		add(commonPanel, cc.xy(2, 1));

		// ======== panelEditores ========
		{
			panelEditores.setTabPlacement(SwingConstants.LEFT);
		}
		add(panelEditores, cc.xywh(1, 3, 4, 1, CellConstraints.FILL,
				CellConstraints.FILL));
		// initialization//GEN-END:initComponents
	}

	// JFormDesigner - Variables declaration - DO NOT
	// MODIFY//GEN-BEGIN:variables
	private SimpleInternalFrame commonPanel;
	private JLabel lblName;
	private JTextField txtNombre;
	private JLabel lblNamespace;
	private JTextField txtNamespace;
	private JLabel lblStereotype;
	private JComboBox comboEstereotipo;
	private JTabbedPane panelEditores;
	// JFormDesigner - End of variables declaration//GEN-END:variables
}
