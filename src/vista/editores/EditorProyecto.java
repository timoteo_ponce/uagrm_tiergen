/*
 * Created by JFormDesigner on Tue Jun 16 13:52:31 BOT 2009
 */

package vista.editores;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JTextField;

import modelo.types.PropertyType;
import util.AppUtil;
import util.Configuracion;

import com.jgoodies.forms.factories.DefaultComponentFactory;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;

import controlador.ControllerProyecto;

/**
 * The Class EditorProyecto.
 * 
 * @author Juan Timoteo Ponce Ortiz
 */
public class EditorProyecto extends JDialog {

    /** The controller. */
    private final ControllerProyecto controller;

    /**
     * Instantiates a new editor proyecto.
     * 
     * @param controller
     *            the controller
     * @param parent
     *            the parent
     * @param modal
     *            the modal
     */
    public EditorProyecto(ControllerProyecto controller, java.awt.Frame parent,
	    boolean modal) {
	super(parent, modal);
	this.controller = controller;
	initComponents();
	init();
    }

    /**
     * Inits the.
     */
    private void init() {
	// cargamos los templates y dbms
	// cargamos los templates
	setTitle("Proyecto : "
		+ controller.getModelProperty(PropertyType.NOMBRE.toString()));
	comboDbms.removeAllItems();
	comboTemplate.removeAllItems();

	String[] templates = Configuracion.getParam("templates").split(",");
	for (String temp : templates) {
	    comboTemplate.addItem(temp);
	}
	// cargamos los DBMS
	templates = Configuracion.getParam("dbms").split(",");
	for (String temp : templates) {
	    comboDbms.addItem(temp);
	}

	loadFields();
    }

    /**
     * Load fields.
     */
    private void loadFields() {
	txtName.setText(controller.getModelProperty(PropertyType.NOMBRE
		.toString())
		+ "");
	txtAuthor.setText(controller.getModelProperty(PropertyType.AUTOR
		.toString())
		+ "");
	txtPath.setText(controller.getModelProperty(PropertyType.PATH
		.toString())
		+ "");
	comboTemplate.setSelectedItem(controller
		.getModelProperty(PropertyType.TEMPLATE.toString())
		+ "");
	comboDbms.setSelectedItem(controller.getModelProperty(PropertyType.DBMS
		.toString())
		+ "");
	txtDbName.setText(controller.getModelProperty(PropertyType.DB_NAME
		.toString())
		+ "");
	/*
	 * chkGenData.setSelected( controller.getModelProperty(
	 * ControllerProyecto.NOMBRE_PROPERTY ) ); chkGenBussiness.setSelected(
	 * controller.getModelProperty( ControllerProyecto.NOMBRE_PROPERTY ) );
	 * chkGenPresentation.setSelected( controller.getModelProperty(
	 * ControllerProyecto.NOMBRE_PROPERTY ) );
	 */
    }

    /**
     * Update fields.
     */
    private void updateFields() {
	controller.changeNombre(txtName.getText(), this);
	controller.changeAutor(txtAuthor.getText(), this);
	controller.changePath(txtPath.getText(), this);
	controller.changeTemplate(comboTemplate.getSelectedItem().toString(),
		this);
	controller.changeDbms(comboDbms.getSelectedItem().toString(), this);
	controller.changeDbName(txtDbName.getText(), this);
	/*
	 * proyecto.setGenerateData( chkGenData.isSelected() );
	 * proyecto.setGenerateBussiness( chkGenBussiness.isSelected() );
	 * proyecto.setGeneratePresentation( chkGenPresentation.isSelected());
	 */
    }

    /**
     * Btn ok action performed.
     * 
     * @param e
     *            the e
     */
    private void btnOkActionPerformed() {
	updateFields();
	dispose();
    }

    /**
     * Btn path action performed.
     * 
     * @param e
     *            the e
     */
    private void btnPathActionPerformed() {
	File path = AppUtil.showGuardarCarpeta(this, "Direccion del proyecto",
		"");
	if (path != null)
	    txtPath.setText(path.getPath());
    }

    /**
     * Inits the components.
     */
    private void initComponents() {
	// JFormDesigner - Component initialization - DO NOT MODIFY
	// //GEN-BEGIN:initComponents
	DefaultComponentFactory compFactory = DefaultComponentFactory
		.getInstance();
	separator1 = compFactory.createSeparator("Proyecto");
	label1 = compFactory.createLabel("&Nombre:");
	txtName = new JTextField();
	label2 = compFactory.createLabel("&Autor:");
	txtAuthor = new JTextField();
	label3 = compFactory.createLabel("&Ubicacion:");
	txtPath = new JTextField();
	btnPath = new JButton();
	separator2 = compFactory.createSeparator("Generacion de codigo");
	label4 = compFactory.createLabel("Plan&tilla:");
	comboTemplate = new JComboBox();
	label5 = compFactory.createLabel("&Sistema de base de datos:");
	comboDbms = new JComboBox();
	label6 = compFactory.createLabel("&Base de datos:");
	txtDbName = new JTextField();
	btnOk = new JButton();
	CellConstraints cc = new CellConstraints();

	// ======== this ========
	Container contentPane = getContentPane();
	contentPane
		.setLayout(new FormLayout(
			"default, $lcgap, 91dlu, 2*($lcgap, default:grow), 2*($lcgap, default)",
			"11*(default, $lgap), default"));
	contentPane.add(separator1, cc.xywh(3, 3, 5, 1));

	// ---- label1 ----
	label1.setLabelFor(txtName);
	contentPane.add(label1, cc.xywh(3, 5, 1, 1, CellConstraints.RIGHT,
		CellConstraints.DEFAULT));
	contentPane.add(txtName, cc.xywh(5, 5, 3, 1));

	// ---- label2 ----
	label2.setLabelFor(txtAuthor);
	contentPane.add(label2, cc.xywh(3, 7, 1, 1, CellConstraints.RIGHT,
		CellConstraints.DEFAULT));
	contentPane.add(txtAuthor, cc.xywh(5, 7, 3, 1));

	// ---- label3 ----
	label3.setLabelFor(txtPath);
	contentPane.add(label3, cc.xywh(3, 9, 1, 1, CellConstraints.RIGHT,
		CellConstraints.DEFAULT));

	// ---- txtPath ----
	txtPath.setEditable(false);
	contentPane.add(txtPath, cc.xywh(5, 9, 3, 1));

	// ---- btnPath ----
	btnPath.setIcon(new ImageIcon(getClass().getResource(
		"/iconos/search-small.png")));
	btnPath.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent e) {
		btnPathActionPerformed();
	    }
	});
	contentPane.add(btnPath, cc.xy(9, 9));
	contentPane.add(separator2, cc.xywh(3, 13, 5, 1));

	// ---- label4 ----
	label4.setLabelFor(comboTemplate);
	contentPane.add(label4, cc.xywh(3, 15, 1, 1, CellConstraints.RIGHT,
		CellConstraints.DEFAULT));

	// ---- comboTemplate ----
	comboTemplate.setEditable(true);
	contentPane.add(comboTemplate, cc.xywh(5, 15, 3, 1));

	// ---- label5 ----
	label5.setLabelFor(comboDbms);
	contentPane.add(label5, cc.xywh(3, 17, 1, 1, CellConstraints.RIGHT,
		CellConstraints.DEFAULT));

	// ---- comboDbms ----
	comboDbms.setEditable(true);
	contentPane.add(comboDbms, cc.xywh(5, 17, 3, 1));

	// ---- label6 ----
	label6.setLabelFor(txtDbName);
	contentPane.add(label6, cc.xywh(3, 19, 1, 1, CellConstraints.RIGHT,
		CellConstraints.DEFAULT));
	contentPane.add(txtDbName, cc.xywh(5, 19, 3, 1));

	// ---- btnOk ----
	btnOk.setText("Ok");
	btnOk.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent e) {
		btnOkActionPerformed();
	    }
	});
	contentPane.add(btnOk, cc.xy(5, 23));
	setSize(450, 325);
	setLocationRelativeTo(getOwner());
	// JFormDesigner - End of component initialization
	// //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY
    // //GEN-BEGIN:variables
    private JComponent separator1;
    private JLabel label1;
    private JTextField txtName;
    private JLabel label2;
    private JTextField txtAuthor;
    private JLabel label3;
    private JTextField txtPath;
    private JButton btnPath;
    private JComponent separator2;
    private JLabel label4;
    private JComboBox comboTemplate;
    private JLabel label5;
    private JComboBox comboDbms;
    private JLabel label6;
    private JTextField txtDbName;
    private JButton btnOk;
    // JFormDesigner - End of variables declaration //GEN-END:variables
}
