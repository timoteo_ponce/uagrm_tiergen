/*
 * 
 */
package vista.editores;

import java.beans.PropertyChangeEvent;
import java.util.Collection;

import javax.swing.JComboBox;
import javax.swing.JPanel;

import org.omg.uml.foundation.core.Attribute;
import org.omg.uml.foundation.core.Classifier;
import org.omg.uml.foundation.core.DataType;
import org.omg.uml.foundation.core.ModelElement;
import org.omg.uml.foundation.core.UmlClass;
import org.omg.uml.foundation.datatypes.VisibilityKindEnum;
import org.uml.diagrammanagement.GraphElement;

import util.NodoElemento;
import util.uml.ElementoVisualUtil;
import util.uml.UMLUtil;

// TODO: Auto-generated Javadoc
/**
 * The Class EditorBase.
 */
public class EditorBase extends JPanel implements VistaEdicion {

    /** The element. */
    private ModelElement element;

    /** The graph element. */
    private GraphElement graphElement;

    /**
     * Instantiates a new editor base.
     */
    public EditorBase() {
    }

    /**
     * Instantiates a new editor base.
     * 
     * @param element
     *            the element
     */
    public EditorBase(ModelElement element) {
	this.element = element;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * vista.editores.VistaEdicion#addListener(vista.editores.EditorListener)
     */
    @Override
    public void addListener(EditorListener listener) {
	// TODO Auto-generated method stub

    }

    /*
     * (non-Javadoc)
     * 
     * @see vista.editores.VistaEdicion#updateModelo()
     */
    @Override
    public void updateModelo() {
	// TODO Auto-generated method stub

    }

    /*
     * (non-Javadoc)
     * 
     * @see vista.Vista#getTitulo()
     */
    @Override
    public String getTitulo() {
	// TODO Auto-generated method stub
	return null;
    }

    /*
     * (non-Javadoc)
     * 
     * @see vista.Vista#modelPropertyChange(java.beans.PropertyChangeEvent)
     */
    @Override
    public void modelPropertyChange(PropertyChangeEvent evt) {
	// TODO Auto-generated method stub

    }

    /*
     * (non-Javadoc)
     * 
     * @see vista.Vista#setModelo(java.lang.Object)
     */
    @Override
    public void setModelo(Object modelo) {
	// TODO Auto-generated method stub

    }

    /*
     * (non-Javadoc)
     * 
     * @see vista.Vista#updateVista()
     */
    @Override
    public void updateVista() {
	// TODO Auto-generated method stub

    }

    /**
     * Gets the element.
     * 
     * @return the element
     */
    public ModelElement getElement() {
	return element;
    }

    /**
     * Sets the element.
     * 
     * @param element
     *            the new element
     */
    public void setElement(ModelElement element) {
	this.element = element;
    }

    /**
     * Gets the graph element.
     * 
     * @return the graph element
     */
    public GraphElement getGraphElement() {
	if (graphElement == null)
	    graphElement = ElementoVisualUtil.getGraphElement(element);
	return graphElement;
    }

    /**
     * Sets the graph element.
     * 
     * @param graphElement
     *            the new graph element
     */
    public void setGraphElement(GraphElement graphElement) {
	this.graphElement = graphElement;
    }

    /**
     * Load visibility to combo box.
     * 
     * @param combo
     *            the combo
     */
    public void loadVisibilityToComboBox(JComboBox combo) {
	combo.removeAllItems();
	combo.addItem(VisibilityKindEnum.VK_PRIVATE);
	combo.addItem(VisibilityKindEnum.VK_PROTECTED);
	combo.addItem(VisibilityKindEnum.VK_PUBLIC);
	combo.addItem(VisibilityKindEnum.VK_PACKAGE);
    }

    /**
     * Load properties.
     */
    public void loadProperties() {
    }

    /**
     * Save properties.
     */
    public void saveProperties() {
    }

    /**
     * Load multiplicity to combo box.
     * 
     * @param comboMultiplicidad
     *            the combo multiplicidad
     */
    public void loadMultiplicityToComboBox(JComboBox comboMultiplicidad) {
	comboMultiplicidad.setModel(new javax.swing.DefaultComboBoxModel(
		new String[] { "", "1", "0..1", "*", "1..*" }));
    }

    /**
     * Load types to combo box.
     * 
     * @param comboTipos
     *            the combo tipos
     */
    public void loadTypesToComboBox(JComboBox comboTipos) {
	// cargamos todos los elementos de tipo
	comboTipos.removeAllItems();
	if (((Attribute) getElement()).getType() != null) {
	    comboTipos.addItem(new NodoElemento(((Attribute) getElement())
		    .getType()));
	    comboTipos.setSelectedIndex(0);
	}

	final Collection<Classifier> classes = UMLUtil.getAllClasificadores();
	for (Classifier elem : classes) {
	    if (elem instanceof UmlClass) {
		comboTipos.addItem(new NodoElemento((ModelElement) elem));
	    }
	}

	final Collection<DataType> types = UMLUtil.getAllTipos();
	for (DataType elem : types) {
	    comboTipos.addItem(new NodoElemento((ModelElement) elem));
	}

    }

}
