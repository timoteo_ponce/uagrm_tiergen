/*
 * Created by JFormDesigner on Sun May 17 10:26:58 BOT 2009
 */

package vista.editores;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Map;

import javax.persistence.InheritanceType;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import modelo.types.Entity;

import org.omg.uml.foundation.core.Classifier;
import org.omg.uml.foundation.datatypes.VisibilityKind;
import org.uml.diagrammanagement.GraphElement;

import util.uml.ElementoVisualUtil;

import com.jgoodies.forms.factories.DefaultComponentFactory;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;

/**
 * The Class EditorClasificador.
 * 
 * @author Timo
 */
public final class EditorClasificador extends EditorBase {

    /**
     * Instantiates a new editor clasificador.
     * 
     * @param element
     *            the element
     */
    public EditorClasificador(Classifier element) {
	super(element);
	initComponents();
	initCombos();
    }

    /**
     * Instantiates a new editor clasificador.
     */
    public EditorClasificador() {
	initComponents();
    }

    /**
     * Inits the combos.
     */
    private void initCombos() {
	super.loadVisibilityToComboBox(comboVisibilidad);
	comboInheritanceStratg.removeAllItems();

	for (InheritanceType item : InheritanceType.values()) {
	    comboInheritanceStratg.addItem(item.toString());
	}
    }

    /*
     * (non-Javadoc)
     * 
     * @see vista.editores.EditorBase#loadProperties()
     */
    @Override
    public void loadProperties() {
	Map properties = ElementoVisualUtil.getPropiedades(getGraphElement());
	// <editor-fold defaultstate="collapsed" desc="EJB3 ">
	Object prop = properties.get(Entity.ENTITY.toString());
	if (prop != null && !prop.toString().isEmpty())
	    chkEntity.setSelected(true);

	prop = properties.get(Entity.EMBEDDABLE.toString());
	if (prop != null && !prop.toString().isEmpty())
	    chkEmbeddable.setSelected(true);

	prop = properties.get(Entity.TABLE_NAME.toString());
	if (prop != null && !prop.toString().isEmpty()) {
	    chkTableName.setSelected(true);
	    txtTableName.setText(prop.toString());
	}
	/*
	 * //TODO inheritance, this part must be verified only for generalizated
	 * elements comboInheritanceStratg.removeAllItems();
	 * comboInheritanceStratg.addItem( "NONE" ); InheritanceType[] types =
	 * InheritanceType.values(); for (InheritanceType item: types)
	 * comboInheritanceStratg.addItem( item );
	 * 
	 * prop = properties.get( EJBAnnotations.Entity.INHERITANCE.toString()
	 * ); if( prop != null && !prop.toString().isEmpty() )
	 * comboInheritanceStratg.setSelectedItem( InheritanceType.valueOf(
	 * prop.toString() ) );
	 */
	// </editor-fold>
    }

    /*
     * (non-Javadoc)
     * 
     * @see vista.editores.EditorBase#saveProperties()
     */
    @Override
    public void saveProperties() {
	// <editor-fold defaultstate="collapsed" desc="EJB3 ">
	GraphElement graphElement = getGraphElement();
	if (chkEntity.isSelected())
	    ElementoVisualUtil.setProperty(graphElement, Entity.ENTITY
		    .toString(), "0");
	else
	    ElementoVisualUtil.setProperty(graphElement, Entity.ENTITY
		    .toString(), "");

	if (chkEmbeddable.isSelected())
	    ElementoVisualUtil.setProperty(graphElement, Entity.EMBEDDABLE
		    .toString(), "0");
	else
	    ElementoVisualUtil.setProperty(graphElement, Entity.EMBEDDABLE
		    .toString(), "");
	if (chkTableName.isSelected())
	    ElementoVisualUtil.setProperty(graphElement, Entity.TABLE_NAME
		    .toString(), txtTableName.getText());
	else
	    ElementoVisualUtil.setProperty(graphElement, Entity.TABLE_NAME
		    .toString(), "");
	// </editor-fold>
    }

    /**
     * Chk table name action performed.
     * 
     * @param e
     *            the e
     */
    private void chkTableNameActionPerformed() {
	if (chkTableName.isSelected()) {
	    try {
		String name = JOptionPane
			.showInputDialog("Ingrese el nombre de la tabla");
		txtTableName.setText(name);
	    } catch (Exception e2) {
	    }
	}
    }

    /**
     * Inits the components.
     */
    private void initComponents() {
	// JFormDesigner - Component initialization - DO NOT MODIFY
	// //GEN-BEGIN:initComponents
	DefaultComponentFactory compFactory = DefaultComponentFactory
		.getInstance();
	lblVisibilidad = compFactory.createLabel("Visibilidad");
	comboVisibilidad = new JComboBox();
	lblInheritance = compFactory.createLabel("Herencia");
	comboInheritanceStratg = new JComboBox();
	chkTableName = new JCheckBox();
	txtTableName = new JTextField();
	chkEmbeddable = new JCheckBox();
	chkEntity = new JCheckBox();
	CellConstraints cc = new CellConstraints();

	// ======== this ========
	setLayout(new FormLayout(
		"25dlu, $lcgap, 49dlu, $lcgap, 107dlu, $lcgap, 29dlu:grow",
		"14dlu, $lgap, 15dlu, $lgap, default, $lgap, 15dlu, $lgap, default"));

	// ---- lblVisibilidad ----
	lblVisibilidad.setLabelFor(comboVisibilidad);
	lblVisibilidad.setHorizontalAlignment(SwingConstants.RIGHT);
	add(lblVisibilidad, cc.xy(3, 3));
	add(comboVisibilidad, cc.xy(5, 3));

	// ---- lblInheritance ----
	lblInheritance.setHorizontalAlignment(SwingConstants.RIGHT);
	lblInheritance.setEnabled(false);
	lblInheritance.setToolTipText("Editable desde la relacion");
	add(lblInheritance, cc.xy(3, 5));

	// ---- comboInheritanceStratg ----
	comboInheritanceStratg.setEnabled(false);
	comboInheritanceStratg.setToolTipText("Editable desde la relacion");
	add(comboInheritanceStratg, cc.xy(5, 5));

	// ---- chkTableName ----
	chkTableName.setText("Table name");
	chkTableName.setHorizontalAlignment(SwingConstants.RIGHT);
	chkTableName.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent e) {
		chkTableNameActionPerformed();
	    }
	});
	add(chkTableName, cc.xy(3, 7));

	// ---- txtTableName ----
	txtTableName.setEditable(false);
	add(txtTableName, cc.xy(5, 7));

	// ---- chkEmbeddable ----
	chkEmbeddable.setText("Embeddabe");
	chkEmbeddable.setHorizontalAlignment(SwingConstants.RIGHT);
	add(chkEmbeddable, cc.xy(3, 9));

	// ---- chkEntity ----
	chkEntity.setText("Entity");
	chkEntity.setHorizontalAlignment(SwingConstants.CENTER);
	add(chkEntity, cc.xy(5, 9));
	// //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY
    // //GEN-BEGIN:variables
    private JLabel lblVisibilidad;
    private JComboBox comboVisibilidad;
    private JLabel lblInheritance;
    private JComboBox comboInheritanceStratg;
    private JCheckBox chkTableName;
    private JTextField txtTableName;
    private JCheckBox chkEmbeddable;
    private JCheckBox chkEntity;

    // JFormDesigner - End of variables declaration //GEN-END:variables

    /*
     * (non-Javadoc)
     * 
     * @see vista.editores.EditorBase#getElement()
     */
    @Override
    public Classifier getElement() {
	return (Classifier) super.getElement();
    }

    /*
     * (non-Javadoc)
     * 
     * @see vista.editores.EditorBase#updateVista()
     */
    @Override
    public void updateVista() {
	if (getElement().getVisibility() != null)
	    comboVisibilidad.setSelectedItem(getElement().getVisibility());
	loadProperties();
    }

    /*
     * (non-Javadoc)
     * 
     * @see vista.editores.EditorBase#updateModelo()
     */
    @Override
    public void updateModelo() {
	getElement().setVisibility(
		(VisibilityKind) comboVisibilidad.getSelectedItem());
	saveProperties();
    }

    /*
     * (non-Javadoc)
     * 
     * @see vista.editores.EditorBase#getTitulo()
     */
    @Override
    public String getTitulo() {
	return this.getClass().getSimpleName();
    }
}
