/*
 * Created by JFormDesigner on Sun May 17 18:51:52 BOT 2009
 */

package vista.editores;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Iterator;

import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.TitledBorder;

import org.omg.uml.foundation.core.Classifier;
import org.omg.uml.foundation.core.Feature;
import org.omg.uml.foundation.core.ModelElement;
import org.omg.uml.foundation.core.Operation;

import util.ModeloLista;
import util.NodoElemento;
import util.uml.UMLUtil;

import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;

/**
 * The Class EditorOperacionClasificador.
 * 
 * @author Timo
 */
public final class EditorOperacionClasificador extends EditorBase {

    /** The modelo lista. */
    private ModeloLista<NodoElemento> modeloLista;

    /**
     * Instantiates a new editor operacion clasificador.
     */
    public EditorOperacionClasificador() {
	initComponents();
    }

    /**
     * Instantiates a new editor operacion clasificador.
     * 
     * @param element
     *            the element
     */
    public EditorOperacionClasificador(Classifier element) {
	super(element);
	initComponents();
	modeloLista = new ModeloLista<NodoElemento>();
	listaCaracteristicas.setModel(modeloLista);
    }

    /**
     * Btn add action performed.
     * 
     * @param e
     *            the e
     */
    private void btnAddActionPerformed() {
	addOperacion();
    }

    /**
     * Btn edit action performed.
     * 
     * @param e
     *            the e
     */
    private void btnEditActionPerformed() {
	editOperacion();
    }

    /**
     * Btn del action performed.
     * 
     * @param e
     *            the e
     */
    private void btnDelActionPerformed() {
	removeOperacion();
    }

    private void listaCaracteristicasMouseClicked(MouseEvent e) {
	if (e.getClickCount() == 2)
	    editOperacion();
    }

    /**
     * Inits the components.
     */
    private void initComponents() {
	// JFormDesigner - Component initialization - DO NOT MODIFY
	// //GEN-BEGIN:initComponents
	scrollPane = new JScrollPane();
	listaCaracteristicas = new JList();
	panelButtons = new JPanel();
	btnAdd = new JButton();
	btnEdit = new JButton();
	btnDel = new JButton();
	CellConstraints cc = new CellConstraints();

	// ======== this ========
	setLayout(new FormLayout(
		"6dlu, $lcgap, 98dlu, $lcgap, 63dlu, $lcgap, default:grow",
		"default, $lgap, 101dlu, $lgap, 10dlu:grow"));

	// ======== scrollPane ========
	{
	    scrollPane.setViewportBorder(new TitledBorder("Elementos"));

	    // ---- listaCaracteristicas ----
	    listaCaracteristicas.addMouseListener(new MouseAdapter() {
		@Override
		public void mouseClicked(MouseEvent e) {
		    listaCaracteristicasMouseClicked(e);
		}
	    });
	    scrollPane.setViewportView(listaCaracteristicas);
	}
	add(scrollPane, cc.xywh(3, 3, 1, 1, CellConstraints.FILL,
		CellConstraints.FILL));

	// ======== panelButtons ========
	{
	    panelButtons.setLayout(new FormLayout("61dlu:grow",
		    "2*(default, $lgap), default"));

	    // ---- btnAdd ----
	    btnAdd.setText("Agregar");
	    btnAdd.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
		    btnAddActionPerformed();
		}
	    });
	    panelButtons.add(btnAdd, cc.xy(1, 1));

	    // ---- btnEdit ----
	    btnEdit.setText("Modificar");
	    btnEdit.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
		    btnEditActionPerformed();
		}
	    });
	    panelButtons.add(btnEdit, cc.xy(1, 3));

	    // ---- btnDel ----
	    btnDel.setText("Eliminar");
	    btnDel.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
		    btnDelActionPerformed();
		}
	    });
	    panelButtons.add(btnDel, cc.xy(1, 5));
	}
	add(panelButtons, cc.xy(5, 3));
	// //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY
    // //GEN-BEGIN:variables
    private JScrollPane scrollPane;
    private JList listaCaracteristicas;
    private JPanel panelButtons;
    private JButton btnAdd;
    private JButton btnEdit;
    private JButton btnDel;

    // JFormDesigner - End of variables declaration //GEN-END:variables

    /*
     * (non-Javadoc)
     * 
     * @see vista.editores.EditorBase#getElement()
     */
    @Override
    public Classifier getElement() {
	return (Classifier) super.getElement();
    }

    /*
     * (non-Javadoc)
     * 
     * @see vista.editores.EditorBase#updateVista()
     */
    @Override
    public void updateVista() {
	modeloLista.clear();
	for (Iterator<Feature> it = getElement().getFeature().iterator(); it
		.hasNext();) {
	    Feature feat = it.next();
	    if (feat instanceof Operation) {
		modeloLista.add(new NodoElemento(feat));
	    }
	}
    }

    /*
     * (non-Javadoc)
     * 
     * @see vista.editores.EditorBase#getTitulo()
     */
    @Override
    public String getTitulo() {
	return this.getClass().getSimpleName();
    }

    /**
     * Adds the operacion.
     */
    private void addOperacion() {
	Operation oper = UMLUtil.crearOperacion("Nueva operacion"
		+ (getElement().getFeature().size() + 1));
	getElement().getFeature().add(oper);
	modeloLista.add(new NodoElemento(oper));
    }

    /**
     * Edits the operacion.
     */
    private void editOperacion() {
	final int index = listaCaracteristicas.getSelectedIndex();
	if (index != -1) {
	    NodoElemento nodo = (NodoElemento) listaCaracteristicas
		    .getSelectedValue();
	    DialogoEditor dialog = new DialogoEditor(null, true, nodo
		    .getElemento());
	    dialog.addListener(new EditorListener() {
		@Override
		public void aceptar(ModelElement elemento) {
		    updateVista();
		}

		@Override
		public void cancelar() {
		    updateVista();
		}
	    });
	    dialog.setVisible(true);
	}
    }

    /**
     * Removes the operacion.
     */
    private void removeOperacion() {
	final int index = listaCaracteristicas.getSelectedIndex();
	if (index != -1) {
	    NodoElemento nodo = (NodoElemento) listaCaracteristicas
		    .getSelectedValue();
	    nodo.getElemento().refDelete();
	    modeloLista.remove(index);
	}
    }
}
