/*
 * 
 */
package vista.editores;

import java.awt.*;
import java.awt.event.*;
import java.awt.event.ActionEvent;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeEvent;
import javax.swing.*;
import com.jgoodies.forms.layout.*;
import java.awt.event.ActionListener;
import java.awt.event.WindowListener;
import java.util.ArrayList;
import org.omg.uml.foundation.core.ModelElement;
import util.AppUtil;

// TODO: Auto-generated Javadoc
/**
 * The Class DialogoEditor.
 * 
 * @author Juan Timoteo Ponce Ortiz
 */
public final class DialogoEditor extends JDialog implements VistaEdicion {	
	
	/** The elemento. */
	public ModelElement elemento;
	
	/** The listeners. */
	private ArrayList<EditorListener> listeners;
	
	/** The editor basico. */
	private PanelEditorGenerico editorBasico;

	/**
	 * Instantiates a new dialogo editor.
	 * 
	 * @param owner the owner
	 */
	public DialogoEditor(Frame owner) {
		super(owner);
		initComponents();
	}

	/**
	 * Instantiates a new dialogo editor.
	 * 
	 * @param owner the owner
	 */
	public DialogoEditor(Dialog owner) {
		super(owner);
		initComponents();
	}

	/**
	 * Creates new form EditorDeElemento.
	 * 
	 * @param parent the parent
	 * @param modal the modal
	 * @param elemento the elemento
	 */
	public DialogoEditor(java.awt.Frame parent, boolean modal,
			ModelElement elemento) {
		super(parent, modal);
		this.elemento = elemento;
		initComponents();
		init();
		setLocationRelativeTo(parent);
	}

	/**
	 * Inits the.
	 */
	public void init() {
		listeners = new ArrayList<EditorListener>();
		editorBasico = new PanelEditorGenerico(elemento,this);
		// parte grafica
		setTitle("Edicion " + elemento.getName());
		panelS.add(editorBasico, BorderLayout.CENTER);
		// eventos
		DialogoEditorListener listener = new DialogoEditorListener();
		addWindowListener(listener);
		btnAccept.addActionListener(listener);
		btnCancel.addActionListener(listener);
	}

	/**
	 * Inits the components.
	 */
	private void initComponents() {
		// JFormDesigner - Component initialization - DO NOT
		// MODIFY//GEN-BEGIN:initComponents
		panelS = new JPanel();
		panelOperation = new JPanel();
		btnAccept = new JButton();
		btnCancel = new JButton();
		CellConstraints cc = new CellConstraints();

		//======== this ========
		setTitle("Editor");
		Container contentPane = getContentPane();
		contentPane.setLayout(new FormLayout(
			"101dlu:grow, $lcgap, default, $lcgap, 101dlu:grow",
			"157dlu:grow, $lgap, default"));

		//======== panelS ========
		{
			panelS.setLayout(new BorderLayout());
		}
		contentPane.add(panelS, cc.xywh(1, 1, 5, 1, CellConstraints.FILL, CellConstraints.FILL));

		//======== panelOperation ========
		{
			panelOperation.setLayout(new FormLayout(
				"default:grow, $lcgap, 101dlu, $lcgap, default, $lcgap, 100dlu, $lcgap, default:grow",
				"16dlu"));

			//---- btnAccept ----
			btnAccept.setText("Aceptar");
			btnAccept.setIcon(new ImageIcon(getClass().getResource("/iconos/dialog-apply.png")));
			panelOperation.add(btnAccept, cc.xy(3, 1));

			//---- btnCancel ----
			btnCancel.setText("Cancelar");
			btnCancel.setIcon(new ImageIcon(getClass().getResource("/iconos/window-close.png")));
			panelOperation.add(btnCancel, cc.xy(7, 1));
		}
		contentPane.add(panelOperation, cc.xywh(1, 3, 5, 1, CellConstraints.FILL, CellConstraints.DEFAULT));
		setSize(530, 390);
		setLocationRelativeTo(null);
		// initialization//GEN-END:initComponents
	}

	// JFormDesigner - Variables declaration - DO NOT
	// MODIFY//GEN-BEGIN:variables
	/** The panel s. */
	private JPanel panelS;
	
	/** The panel operation. */
	private JPanel panelOperation;
	
	/** The btn accept. */
	private JButton btnAccept;
	
	/** The btn cancel. */
	private JButton btnCancel;
	// JFormDesigner - End of variables declaration//GEN-END:variables

	/* (non-Javadoc)
	 * @see vista.editores.VistaEdicion#updateModelo()
	 */
	@Override
	public void updateModelo() {
	}

	/**
	 * Sets the modelo.
	 * 
	 * @param modelo the modelo
	 */
	public void setModelo(Object modelo) {
		this.elemento = (ModelElement) modelo;
	}

	/* (non-Javadoc)
	 * @see vista.Vista#updateVista()
	 */
	public void updateVista() {
	}

	/**
	 * Adds the listener.
	 * 
	 * @param listener the listener
	 */
	public void addListener(EditorListener listener) {
		if (!listeners.contains(listener))
			listeners.add(listener);
	}

	/**
	 * Aceptar.
	 */
	protected void aceptar() {
		editorBasico.updateModelo();
		difundirAceptar();
		dispose();
	}

	/**
	 * Cancelar.
	 */
	protected void cancelar() {
		difundirCancelar();
		dispose();
	}

	/**
	 * Difundir aceptar.
	 */
	private void difundirAceptar() {
		for (EditorListener elem : listeners) {
			elem.aceptar(this.elemento);
		}
	}

	/**
	 * Difundir cancelar.
	 */
	private void difundirCancelar() {
		for (EditorListener elem : listeners) {
			elem.cancelar();
		}
	}

	/* (non-Javadoc)
	 * @see vista.Vista#getTitulo()
	 */
	public String getTitulo() {
		return "Dialogo editor";
	}

	/* (non-Javadoc)
	 * @see vista.Vista#modelPropertyChange(java.beans.PropertyChangeEvent)
	 */
	public void modelPropertyChange(PropertyChangeEvent evt) {
	}
	
	/**
	 * Close dialog.
	 */
	public void closeDialog(){
		boolean result = AppUtil.showConfirmar(null, "Advertencia","Desea cerrar el editor, no habran cambios?");
		if (result)
			cancelar();
	}

	/**
	 * The listener interface for receiving dialogoEditor events.
	 * The class that is interested in processing a dialogoEditor
	 * event implements this interface, and the object created
	 * with that class is registered with a component using the
	 * component's <code>addDialogoEditorListener<code> method. When
	 * the dialogoEditor event occurs, that object's appropriate
	 * method is invoked.
	 * 
	 * @see DialogoEditorEvent
	 */
	class DialogoEditorListener implements ActionListener, WindowListener {
		
		/**
		 * Action performed.
		 * 
		 * @param e the e
		 */
		public void actionPerformed(ActionEvent e) {
			if (e.getSource().equals(btnAccept)) {
				aceptar();
				return;
			}

			if (e.getSource().equals(btnCancel)) {
				cancelar();
				return;
			}
		}

		/**
		 * Window opened.
		 * 
		 * @param e the e
		 */
		public void windowOpened(WindowEvent e) {
		}

		/**
		 * Window closing.
		 * 
		 * @param e the e
		 */
		public void windowClosing(WindowEvent e) {
			closeDialog();
		}

		/**
		 * Window closed.
		 * 
		 * @param e the e
		 */
		public void windowClosed(WindowEvent e) {
		}

		/**
		 * Window iconified.
		 * 
		 * @param e the e
		 */
		public void windowIconified(WindowEvent e) {
		}

		/**
		 * Window deiconified.
		 * 
		 * @param e the e
		 */
		public void windowDeiconified(WindowEvent e) {
		}

		/**
		 * Window activated.
		 * 
		 * @param e the e
		 */
		public void windowActivated(WindowEvent e) {
		}

		/**
		 * Window deactivated.
		 * 
		 * @param e the e
		 */
		public void windowDeactivated(WindowEvent e) {
		}

	}
}
