/*
 * Created by JFormDesigner on Sun May 17 11:47:11 BOT 2009
 */

package vista.editores;

import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

import org.omg.uml.foundation.core.Feature;
import org.omg.uml.foundation.datatypes.ScopeKindEnum;
import org.omg.uml.foundation.datatypes.VisibilityKind;

import com.jgoodies.forms.factories.DefaultComponentFactory;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;

/**
 * The Class EditorCaracteristica.
 * 
 * @author Timo
 */
public final class EditorCaracteristica extends EditorBase {

    /**
     * Instantiates a new editor caracteristica.
     */
    public EditorCaracteristica() {
	initComponents();
    }

    /**
     * Instantiates a new editor caracteristica.
     * 
     * @param feat
     *            the feat
     */
    public EditorCaracteristica(Feature feat) {
	super(feat);
	initComponents();
	super.loadVisibilityToComboBox(comboVisibilidad);
    }

    /**
     * Inits the components.
     */
    private void initComponents() {
	// JFormDesigner - Component initialization - DO NOT MODIFY
	// //GEN-BEGIN:initComponents
	DefaultComponentFactory compFactory = DefaultComponentFactory
		.getInstance();
	lblVisibilidad = compFactory.createLabel("Visibilidad");
	comboVisibilidad = new JComboBox();
	chkEstatico = new JCheckBox();
	CellConstraints cc = new CellConstraints();

	// ======== this ========
	setLayout(new FormLayout(
		"default, $lcgap, 40dlu, $lcgap, 84dlu, $lcgap, 20dlu:grow",
		"3*(default, $lgap), default"));

	// ---- lblVisibilidad ----
	lblVisibilidad.setLabelFor(comboVisibilidad);
	lblVisibilidad.setHorizontalAlignment(SwingConstants.RIGHT);
	add(lblVisibilidad, cc.xy(3, 3));
	add(comboVisibilidad, cc.xy(5, 3));

	// ---- chkEstatico ----
	chkEstatico.setText("Estatico");
	add(chkEstatico, cc.xy(5, 5));
	// JFormDesigner - End of component initialization
	// //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY
    // //GEN-BEGIN:variables
    /** The lbl visibilidad. */
    private JLabel lblVisibilidad;

    /** The combo visibilidad. */
    private JComboBox comboVisibilidad;

    /** The chk estatico. */
    private JCheckBox chkEstatico;

    // JFormDesigner - End of variables declaration //GEN-END:variables

    /*
     * (non-Javadoc)
     * 
     * @see vista.editores.EditorBase#getElement()
     */
    @Override
    public Feature getElement() {
	return (Feature) super.getElement();
    }

    /*
     * (non-Javadoc)
     * 
     * @see vista.editores.EditorBase#updateVista()
     */
    @Override
    public void updateVista() {
	if (getElement().getVisibility() != null)
	    comboVisibilidad.setSelectedItem(getElement().getVisibility());

	if (getElement().getOwnerScope() == ScopeKindEnum.SK_CLASSIFIER)
	    chkEstatico.setSelected(true);
	else
	    chkEstatico.setSelected(false);
    }

    /*
     * (non-Javadoc)
     * 
     * @see vista.editores.EditorBase#updateModelo()
     */
    @Override
    public void updateModelo() {
	getElement().setVisibility(
		(VisibilityKind) comboVisibilidad.getSelectedItem());
	if (chkEstatico.isSelected())
	    getElement().setOwnerScope(ScopeKindEnum.SK_CLASSIFIER);
	else
	    getElement().setOwnerScope(ScopeKindEnum.SK_INSTANCE);
    }

    /*
     * (non-Javadoc)
     * 
     * @see vista.editores.EditorBase#getTitulo()
     */
    @Override
    public String getTitulo() {
	return this.getClass().getSimpleName();
    }
}
