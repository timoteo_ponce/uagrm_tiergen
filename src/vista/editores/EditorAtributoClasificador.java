/*
 * Created by JFormDesigner on Sun May 17 17:50:58 BOT 2009
 */

package vista.editores;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Iterator;
import java.util.Map;

import javax.persistence.GenerationType;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.TitledBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import modelo.types.Field;

import org.omg.uml.foundation.core.Attribute;
import org.omg.uml.foundation.core.Classifier;
import org.omg.uml.foundation.core.Feature;
import org.omg.uml.foundation.core.ModelElement;

import util.AppUtil;
import util.ModeloLista;
import util.NodoElemento;
import util.uml.ElementoDeModeloUtil;
import util.uml.ElementoVisualUtil;
import util.uml.UMLUtil;

import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;

/**
 * The Class EditorAtributoClasificador.
 * 
 * @author Timo
 */
public final class EditorAtributoClasificador extends EditorBase {

    /** The modelo lista. */
    private ModeloLista<NodoElemento> modeloLista;

    /** The properties. */
    private Map properties;

    /**
     * Instantiates a new editor atributo clasificador.
     */
    public EditorAtributoClasificador() {
	initComponents();
    }

    /**
     * Instantiates a new editor atributo clasificador.
     * 
     * @param element
     *            the element
     */
    public EditorAtributoClasificador(Classifier element) {
	super(element);
	initComponents();
	modeloLista = new ModeloLista<NodoElemento>();
	listaCaracteristicas.setModel(modeloLista);
	loadProperties();
    }

    /*
     * (non-Javadoc)
     * 
     * @see vista.editores.EditorBase#loadProperties()
     */
    @Override
    public void loadProperties() {
	properties = ElementoVisualUtil.getPropiedades(getGraphElement());
    }

    /**
     * Update id fields.
     */
    private void updateIdFields() {
	int index = listaCaracteristicas.getSelectedIndex();
	chkId.setSelected(false);
	if (index > -1) {
	    NodoElemento node = (NodoElemento) listaCaracteristicas
		    .getSelectedValue();
	    ModelElement feature = node.getElemento();
	    Object prop = properties.get(Field.ID.toString());
	    if (prop != null && !prop.toString().isEmpty()
		    && prop.equals(feature.getName())) {
		chkId.setSelected(true);
		prop = properties.get(Field.GENERATED_VALUE.toString());
		if (prop != null) {
		    String generation = prop.toString();
		    lblTipoGeneracion.setText("Generacion : "
			    + (generation.isEmpty() ? "NONE" : generation));
		}
	    }
	}
    }

    /**
     * Btn add action performed.
     * 
     * @param e
     *            the e
     */
    private void btnAddActionPerformed() {
	addAtributo();
    }

    /**
     * Btn edit action performed.
     * 
     * @param e
     *            the e
     */
    private void btnEditActionPerformed() {
	editAtributo();
    }

    /**
     * Btn del action performed.
     * 
     * @param e
     *            the e
     */
    private void btnDelActionPerformed() {
	removeAtributo();
    }

    /**
     * Chk id action performed.
     * 
     * @param e
     *            the e
     */
    private void chkIdActionPerformed() {
	selectId();
    }

    /**
     * Select id.
     */
    private void selectId() {
	if (!chkId.isSelected()) {
	    ElementoVisualUtil.setProperty(getGraphElement(), Field.ID
		    .toString(), "");
	    ElementoVisualUtil.setProperty(getGraphElement(),
		    Field.GENERATED_VALUE.toString(), "");
	} else {
	    NodoElemento node = (NodoElemento) listaCaracteristicas
		    .getSelectedValue();
	    if (node == null)
		return;
	    ModelElement feature = node.getElemento();
	    ElementoVisualUtil.setProperty(getGraphElement(), Field.ID
		    .toString(), feature.getName());
	}
	loadProperties();
	updateIdFields();
    }

    /**
     * Select generation.
     */
    private void selectGeneration() {
	JComboBox genCombo = new JComboBox();
	genCombo.addItem("NONE");
	for (int i = 0; i < GenerationType.values().length; i++)
	    genCombo.addItem(GenerationType.values()[i]);

	Object[] messages = new Object[2];
	messages[0] = "Seleccione el tipo de generacion de identificador";
	messages[1] = genCombo;

	int result = AppUtil.showOptionPane(this, "Generacion automatica",
		messages);

	if (result == JOptionPane.OK_OPTION) {
	    NodoElemento node = (NodoElemento) listaCaracteristicas
		    .getSelectedValue();
	    ModelElement feature = node.getElemento();
	    // only primitive types: int,long. can be generated
	    if (ElementoDeModeloUtil.isPrimitive(feature)) {
		String item = genCombo.getSelectedItem().toString();
		ElementoVisualUtil.setProperty(getGraphElement(),
			Field.GENERATED_VALUE.toString(),
			(item.equals("NONE") ? "" : item));
		loadProperties();
		updateIdFields();
	    } else
		AppUtil.showAdvertencia(this,
			"El elemento no tiene tipo de dato o no es primitivo");
	}
    }

    /**
     * Lista caracteristicas value changed.
     * 
     * @param e
     *            the e
     */
    private void listaCaracteristicasValueChanged() {
	updateIdFields();
    }

    /**
     * Btn generation type action performed.
     * 
     * @param e
     *            the e
     */
    private void btnGenerationTypeActionPerformed() {
	selectGeneration();
    }

    private void listaCaracteristicasMouseClicked(MouseEvent e) {
	if (e.getClickCount() == 2)
	    editAtributo();
    }

    /**
     * Inits the components.
     */
    private void initComponents() {
	// JFormDesigner - Component initialization - DO NOT MODIFY
	// //GEN-BEGIN:initComponents
	scrollPane = new JScrollPane();
	listaCaracteristicas = new JList();
	panelOperations = new JPanel();
	panelButtons = new JPanel();
	btnAdd = new JButton();
	btnEdit = new JButton();
	btnDel = new JButton();
	panelMetadata = new JPanel();
	chkId = new JCheckBox();
	btnGenerationType = new JButton();
	lblTipoGeneracion = new JLabel();
	CellConstraints cc = new CellConstraints();

	// ======== this ========
	setLayout(new FormLayout("10dlu, $lcgap, 97dlu, $lcgap, 127dlu",
		"9dlu, $lgap, top:107dlu, $lgap, default"));

	// ======== scrollPane ========
	{
	    scrollPane.setViewportBorder(new TitledBorder("Elementos"));

	    // ---- listaCaracteristicas ----
	    listaCaracteristicas
		    .addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent e) {
			    listaCaracteristicasValueChanged();
			}
		    });
	    listaCaracteristicas.addMouseListener(new MouseAdapter() {
		@Override
		public void mouseClicked(MouseEvent e) {
		    listaCaracteristicasMouseClicked(e);
		}
	    });
	    scrollPane.setViewportView(listaCaracteristicas);
	}
	add(scrollPane, cc.xywh(3, 3, 1, 1, CellConstraints.FILL,
		CellConstraints.FILL));

	// ======== panelOperations ========
	{
	    panelOperations.setLayout(new FormLayout(
		    "5dlu, $lcgap, 65dlu, $lcgap, default:grow",
		    "default, $lgap, 51dlu"));

	    // ======== panelButtons ========
	    {
		panelButtons.setLayout(new FormLayout("53dlu",
			"2*(default, $lgap), default"));

		// ---- btnAdd ----
		btnAdd.setText("Agregar");
		btnAdd.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent e) {
			btnAddActionPerformed();
		    }
		});
		panelButtons.add(btnAdd, cc.xy(1, 1));

		// ---- btnEdit ----
		btnEdit.setText("Modificar");
		btnEdit.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent e) {
			btnEditActionPerformed();
		    }
		});
		panelButtons.add(btnEdit, cc.xy(1, 3));

		// ---- btnDel ----
		btnDel.setText("Eliminar");
		btnDel.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent e) {
			btnDelActionPerformed();
		    }
		});
		panelButtons.add(btnDel, cc.xy(1, 5));
	    }
	    panelOperations.add(panelButtons, cc.xy(3, 1));

	    // ======== panelMetadata ========
	    {
		panelMetadata.setLayout(new FormLayout(
			"64dlu, $lcgap, 49dlu, $lcgap, 8dlu:grow",
			"2*(default, $lgap), default"));

		// ---- chkId ----
		chkId.setText("Identificador");
		chkId.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent e) {
			chkIdActionPerformed();
		    }
		});
		panelMetadata.add(chkId, cc.xywh(1, 1, 3, 1));

		// ---- btnGenerationType ----
		btnGenerationType.setText("Tipo de generacion");
		btnGenerationType.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent e) {
			btnGenerationTypeActionPerformed();
		    }
		});
		panelMetadata.add(btnGenerationType, cc.xywh(1, 3, 3, 1));

		// ---- lblTipoGeneracion ----
		lblTipoGeneracion.setText("Generacion:");
		panelMetadata.add(lblTipoGeneracion, cc.xywh(1, 5, 3, 1));
	    }
	    panelOperations.add(panelMetadata, cc.xywh(1, 3, 5, 1));
	}
	add(panelOperations, cc.xy(5, 3));
	// //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY
    // //GEN-BEGIN:variables
    private JScrollPane scrollPane;
    private JList listaCaracteristicas;
    private JPanel panelOperations;
    private JPanel panelButtons;
    private JButton btnAdd;
    private JButton btnEdit;
    private JButton btnDel;
    private JPanel panelMetadata;
    private JCheckBox chkId;
    private JButton btnGenerationType;
    private JLabel lblTipoGeneracion;

    // JFormDesigner - End of variables declaration //GEN-END:variables

    /*
     * (non-Javadoc)
     * 
     * @see vista.editores.EditorBase#getElement()
     */
    public Classifier getElement() {
	return (Classifier) super.getElement();
    }

    /*
     * (non-Javadoc)
     * 
     * @see vista.editores.EditorBase#updateVista()
     */
    @Override
    public void updateVista() {
	modeloLista.clear();
	for (Iterator it = getElement().getFeature().iterator(); it.hasNext();) {
	    Feature feat = (Feature) it.next();
	    if (feat instanceof Attribute) {
		modeloLista.add(new NodoElemento(feat));
	    }
	}
    }

    /*
     * (non-Javadoc)
     * 
     * @see vista.editores.EditorBase#getTitulo()
     */
    @Override
    public String getTitulo() {
	return this.getClass().getSimpleName();
    }

    /**
     * Adds the atributo.
     */
    private void addAtributo() {
	Attribute atrib = UMLUtil.crearAtributo("Nuevo atributo"
		+ getElement().getFeature().size() + 1);
	getElement().getFeature().add(atrib);
	modeloLista.add(new NodoElemento(atrib));
    }

    /**
     * Edits the atributo.
     */
    private void editAtributo() {
	int index = listaCaracteristicas.getSelectedIndex();
	if (index != -1) {
	    NodoElemento nodo = (NodoElemento) listaCaracteristicas
		    .getSelectedValue();
	    DialogoEditor dialog = new DialogoEditor(null, true, nodo
		    .getElemento());
	    dialog.addListener(new EditorListener() {
		@Override
		public void aceptar(ModelElement elemento) {
		    updateVista();
		}

		@Override
		public void cancelar() {
		    updateVista();
		}
	    });
	    dialog.setVisible(true);
	}
    }

    /**
     * Removes the atributo.
     */
    private void removeAtributo() {
	int index = listaCaracteristicas.getSelectedIndex();
	if (index != -1) {
	    NodoElemento nodo = (NodoElemento) listaCaracteristicas
		    .getSelectedValue();
	    nodo.getElemento().refDelete();
	    modeloLista.remove(index);
	}
    }

}
