/*
 * VistaEdicion.java
 *
 * Created on 21 de octubre de 2008, 1:02
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package vista;

import vista.editores.EditorListener;

/**
 * Vista especializada que no solamente vigila, sino
 * tiene la facultad de modificar el elemento que observa.
 * @author Juan Timoteo Ponce Ortiz
 */
public interface VistaEdicion extends Vista{
    /**
     * Actualiza las propiedades del elemento.
     */
    public void updateModelo();
    /**
     * Una vista de edicion genera eventos, por lo puede tener multiples
     * esuchadores.
     * @param listener escuchador a agregar
     */
    public void addListener( EditorListener listener );        
    
}
