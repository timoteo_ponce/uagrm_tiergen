/*
 * UIPropertyViewer.java
 *
 * Created on 9 de marzo de 2009, 22:09
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package vista;

import java.beans.PropertyChangeEvent;
import java.util.Hashtable;
import java.util.Properties;

import javax.swing.JTable;

import modelo.types.Entity;

import org.omg.uml.foundation.core.ModelElement;
import org.omg.uml.foundation.core.UmlClass;
import org.uml.diagrammanagement.GraphElement;

import util.PropertyTableModel;
import util.uml.ElementoVisualUtil;
import util.uml.EstereotipoUtil;
import vista.graficador.ElementoVisual;
import vista.graficador.GrNodoVisual;

/**
 * The Class UIPropertyViewer.
 * 
 * @author Juan Timoteo Ponce Ortiz
 */
public class UIPropertyViewer extends JTable implements Vista {

    /** The table model. */
    private final PropertyTableModel tableModel;

    /** The element. */
    private ElementoVisual element;

    private Properties graphProperties;

    /**
     * Creates a new instance of UIPropertyViewer.
     * 
     * @param props
     *            the props
     */
    public UIPropertyViewer(Hashtable<String, String> props) {
	tableModel = new PropertyTableModel(props);
	setModel(tableModel);
    }

    /*
     * (non-Javadoc)
     * 
     * @see vista.Vista#modelPropertyChange(java.beans.PropertyChangeEvent)
     */
    public void modelPropertyChange(PropertyChangeEvent evt) {
    }

    /*
     * (non-Javadoc)
     * 
     * @see vista.Vista#setModelo(java.lang.Object)
     */
    public void setModelo(Object modelo) {
	element = (ElementoVisual) modelo;
	updateVista();
    }

    /*
     * (non-Javadoc)
     * 
     * @see vista.Vista#updateVista()
     */
    public void updateVista() {
	if (element instanceof GrNodoVisual) {
	    GrNodoVisual nodo = (GrNodoVisual) element;
	    Hashtable<String, String> properties = tableModel.getProperties();

	    if (properties == null)
		properties = new Hashtable<String, String>();
	    properties.clear();

	    ModelElement element = ElementoVisualUtil.getElementoLogico(nodo
		    .getNodoLogico());
	    GraphElement graphElement = ElementoVisualUtil
		    .getGraphElement(element);
	    graphProperties = ElementoVisualUtil.getPropiedades(graphElement);

	    addModelElementProps(properties, element);

	    tableModel.setProperties(properties);
	}
    }

    /**
     * Adds the model element props.
     * 
     * @param props
     *            the props
     * @param element
     *            the element
     */
    private void addModelElementProps(Hashtable<String, String> props,
	    ModelElement element) {
	props.put("Nombre",
		(element.getName() != null ? element.getName() : ""));
	props.put("AreaNombre", (element.getNamespace() != null ? element
		.getNamespace().toString() : ""));
	props.put("Visibilidad", (element.getVisibility() != null ? element
		.getVisibility().toString() : ""));
	props.put("Estereotipo", EstereotipoUtil.toString(element
		.getStereotype()));

	if (element instanceof UmlClass)
	    addClassifierProperties(props, (UmlClass) element);
    }

    /**
     * @param props
     */
    private void addClassifierProperties(Hashtable<String, String> props,
	    UmlClass element) {
	props.put("class", element.getClass().toString());

	// Collection<Feature> features = element.getFeature();
	// for (Feature feature : features) {
	// if (feature instanceof Attribute) {
	// Attribute attr = (Attribute) feature;
	// props.put(VisibilidadUtil.toString(attr.getVisibility()) + " "
	// + attr.getName(), (attr.getType() != null ? attr
	// .getType().getName() : "null"));
	// }
	// }
	String prop = graphProperties.getProperty(Entity.ENTITY.toString());
	props.put("@Entity", (prop != null && !prop.isEmpty() ? "true"
		: "false"));

	prop = graphProperties.getProperty(Entity.EMBEDDABLE.toString());
	props.put("@Embeddable", (prop != null && !prop.isEmpty() ? "true"
		: "false"));

	prop = graphProperties.getProperty(Entity.TABLE_NAME.toString());
	props.put("@Table", (prop != null && !prop.isEmpty() ? prop : "null"));
    }

    /*
     * (non-Javadoc)
     * 
     * @see vista.Vista#getTitulo()
     */
    public String getTitulo() {
	return "";
    }

}
