/*
 * Main.java
 *
 * Created on 19 de enero de 2009, 21:54
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package vista;

import java.awt.Dimension;

import javax.swing.UIManager;

import org.apache.log4j.Logger;

import com.jgoodies.looks.LookUtils;
import com.jgoodies.looks.Options;

/**
 * The Class Main.
 * 
 * @author Juan Timoteo Ponce Ortiz
 */
public final class Main {

	private static final Logger LOG = Logger.getLogger(Main.class);

	/** The uiprincipal. */
	private static UIPrincipal uiprincipal;

	/**
	 * The main method.
	 * 
	 * @param args the command line arguments
	 */
	public static void main(final String args[]) {
		try {
			UIManager.put(Options.USE_SYSTEM_FONTS_APP_KEY, Boolean.TRUE);
			Options.setDefaultIconSize(new Dimension(18, 18));
			String lafName = LookUtils.IS_OS_WINDOWS_XP ? Options
					.getCrossPlatformLookAndFeelClassName() : Options
					.getSystemLookAndFeelClassName();
					UIManager.setLookAndFeel(lafName);

					// UIManager.setLookAndFeel(
					// UIManager.getSystemLookAndFeelClassName() );
					uiprincipal = new UIPrincipal();

					/*
					 * JFrame frame = new JFrame();
					 * frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); JButton btn
					 * = new JButton("Help"); frame.getContentPane().add(btn);
					 * 
					 * // Carga el fichero de ayuda File fichero = new
					 * File("help/help_set.hs"); URL hsURL = fichero.toURI().toURL();
					 * 
					 * // Crea el HelpSet y el HelpBroker HelpSet helpset = new
					 * HelpSet(Main.class.getClassLoader(), hsURL); HelpBroker hb =
					 * helpset.createHelpBroker();
					 * 
					 * // Pone ayuda a item de menu al pulsarlo y a F1 en ventana //
					 * principal y secundaria. hb.enableHelpOnButton(btn, "aplicacion",
					 * helpset); frame.setVisible( true );
					 */
		} catch (Exception e) {
			LOG.error(e, e);
			System.exit(1);
		}
		/*
		 * if( args.length > 0 && args[ 0 ].equals( "splash" ) ){
		 * java.awt.EventQueue.invokeLater(new Runnable() { public void run() {
		 * SplashFrm spl = new SplashFrm(); spl.run(); } }); }else{
		 * java.awt.EventQueue.invokeLater(new Runnable() { public void run() {
		 * new UIPrincipal().setVisible(true); } }); }
		 */
	}

	/**
	 * Gets the uI principal.
	 * 
	 * @return the uI principal
	 */
	public static UIPrincipal getUIPrincipal() {
		return uiprincipal;
	}

}
