/*
 * Vista.java
 *
 * Created on 23 de febrero de 2009, 23:40
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package vista;

import java.beans.PropertyChangeEvent;

/**
 * The Interface Vista.
 * 
 * @author Juan Timoteo Ponce Ortiz
 */
public interface Vista {
    
    /**
     * Called by the controller when it needs to pass along a property change
     * from a model.
     * 
     * @param evt The property change event from the model
     */
    void modelPropertyChange(PropertyChangeEvent evt);
    
    /**
     * Asigna el modelo a la vista.
     * 
     * @param modelo elemento a vigilar
     */
    <T> void setModelo( T modelo );
    
    /**
     * Actualiza la vista, recargando los atributos del modelo, no modifica.
     */
    void updateVista();
    
    /**
     * Gets the titulo.
     * 
     * @return the titulo
     */
    String getTitulo();
}
