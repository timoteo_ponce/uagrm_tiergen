/*
 * GrNodoVisual.java
 *
 * Created on 5 de noviembre de 2008, 10:50
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package vista.graficador;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ContainerEvent;
import java.awt.event.ContainerListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;

import org.omg.uml.foundation.core.ModelElement;
import org.uml.diagrammanagement.GraphNode;

import util.uml.ElementoDeModeloUtil;
import util.uml.ElementoVisualUtil;

/**
 * The Class GrNodoVisual.
 * 
 * @author Juan Timoteo Ponce Ortiz
 */
public abstract class GrNodoVisual extends JPanel implements ElementoVisual {

    /** The graficador. */
    protected final GrDiagrama graficador;

    /** The modelo. */
    protected final GraphNode modelo;

    /** The componentes. */
    private final List<ComponentListeners> componentes;

    /** The component listeners. */
    protected final ComponentListeners componentListeners;

    /** The old y. */
    protected int oldX, oldY;

    /** The drag. */
    protected boolean drag = true;

    /** The design time. */
    private boolean designTime;

    /** The select color. */
    protected Color foreColor, backColor, selectColor;

    /** The is selected. */
    private boolean isSelected;

    /** The menu. */
    JPopupMenu menu;

    /** The mi delete from model. */
    JMenuItem miEdit, miDelete, miDeleteFromModel;

    /** Minimum size a node can have. */
    public final static int MINIMUM_SIZE = 20;

    /**
     * Creates a new instance of GrNodoVisual.
     * 
     * @param graficador
     *            the graficador
     * @param modelo
     *            the modelo
     */
    public GrNodoVisual(GrDiagrama graficador, GraphNode modelo) {
	super();
	this.graficador = graficador;
	this.modelo = modelo;
	foreColor = Color.black;
	backColor = Color.white;
	selectColor = Color.blue;
	componentes = new ArrayList<ComponentListeners>();

	addContainerListener(new ContainerListener() {
	    public void componentAdded(ContainerEvent e) {
		ComponentListeners cls = new ComponentListeners(e.getChild());
		componentes.add(cls);
		if (isDesignTime())
		    cls.removePreviousListeners();
	    }

	    public void componentRemoved(ContainerEvent e) {
		ComponentListeners cls = new ComponentListeners(e.getChild());
		componentes.remove(cls);
	    }
	});
	componentListeners = new ComponentListeners(this);
	setDesigntime(true);
	setBackground(Color.WHITE);
	initMenu();
    }

    /**
     * Gets the minimum width.
     * 
     * @return the minimum width
     */
    protected abstract int getMinimumWidth();

    /**
     * Update nodo visual.
     */
    protected abstract void updateNodoVisual();

    /**
     * Update nodo logico.
     */
    protected abstract void updateNodoLogico();

    /**
     * Inicializa el menu asociado a este nodo.
     */
    private void initMenu() {
	menu = new JPopupMenu();

	miEdit = new JMenuItem("Editar");
	miDelete = new JMenuItem("Eliminar de diagrama");
	miDeleteFromModel = new JMenuItem(
		"<html><b>Eliminar de modelo</b><html>");

	menu.add(miEdit);
	menu.add(miDelete);
	menu.addSeparator();
	menu.add(miDeleteFromModel);

	miEdit.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent ae) {
		edit();
	    }
	});

	miDelete.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent ae) {
		remove();
	    }
	});

	miDeleteFromModel.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent ae) {
		removeFromModel();
	    }
	});
    }

    /**
     * Gets the nodo logico.
     * 
     * @return the nodo logico
     */
    public GraphNode getNodoLogico() {
	return modelo;
    }

    /**
     * Inicia el editor para est nodo.
     */
    @Override
    public void edit() {
	ElementoVisualUtil.editarElemento(modelo, graficador.getController(),
		graficador);
	graficador.reloadGraficador();
    }

    /**
     * Elimina el nodo del diagrama, pero no del modelo.
     */
    @Override
    public void remove() {
	ElementoVisualUtil.removeElemento(modelo);
	graficador.reloadGraficador();
    }

    /**
     * Elimina el nodo del diagrama y del proyecto.
     */
    @Override
    public void removeFromModel() {
	ModelElement me = ElementoVisualUtil.getElementoLogico(modelo);
	remove();
	if (me != null) {
	    ElementoDeModeloUtil.removeElemento(me);
	    graficador.getController().changeModelo(graficador);
	}
    }

    /**
     * Show menu.
     * 
     * @param x
     *            the x
     * @param y
     *            the y
     */
    public void showMenu(int x, int y) {
	menu.show(this, x, y);
    }

    /**
     * Responde al evento de presion del raton.
     * 
     * @param evt
     *            evento de raton
     */
    protected void pressed(MouseEvent evt) {
	// Util.print( this , "Pressed" );
	if (!evt.isConsumed()) {
	    if (isDesignTime()) {
		if ((evt.getModifiers() == MouseEvent.BUTTON1_MASK)
			|| (evt.getModifiers() == MouseEvent.BUTTON1_MASK
				+ MouseEvent.SHIFT_MASK)
			|| (evt.getModifiers() == MouseEvent.BUTTON1_MASK
				+ MouseEvent.CTRL_MASK)) {
		    oldX = graficador.applySnap(this.getLocation().x
			    + evt.getX());
		    oldY = graficador.applySnap(this.getLocation().y
			    + evt.getY());
		} else {
		}
	    }
	}
    }

    /**
     * REsponde al evento de soldato del raton.
     * 
     * @param evt
     *            evento de raton
     */
    protected void released(MouseEvent evt) {
	// Util.print( this , "Released" );
	if (!evt.isConsumed()) {
	    updateLogical();
	}
    }

    /**
     * Entered.
     * 
     * @param evt
     *            the evt
     */
    protected void entered(MouseEvent evt) {
	if (!evt.isConsumed()) {

	}
    }

    /**
     * Exited.
     * 
     * @param evt
     *            the evt
     */
    protected void exited(MouseEvent evt) {
	if (!evt.isConsumed()) {

	}
    }

    /**
     * Responde cuando el mouse es clickeado. Dos veces, se edita el nodo. Click
     * derecho, se muestra el menu de opciones
     * 
     * @param evt
     *            evento de raton
     */
    protected void clicked(MouseEvent evt) {
	// Util.print( this , "Clicked" );
	if (!evt.isConsumed()) {
	    if (isDesignTime()) {
		if ((evt.getModifiers() == MouseEvent.BUTTON1_MASK)
			&& (evt.getClickCount() == 2))
		    edit();
		else if (evt.getModifiers() == MouseEvent.BUTTON3_MASK)
		    showMenu(evt.getX(), evt.getY());
	    }
	}
    }

    /**
     * Responde al evento de arrastre del raton, actualizando la posicion del
     * nodo.
     * 
     * @param evt
     *            evento de raton
     */
    protected void dragged(MouseEvent evt) {
	// Util.print( this , "dragged" );
	if (!evt.isConsumed()) {
	    if (drag) {
		if (isDesignTime()) {
		    if ((evt.getModifiers() == MouseEvent.BUTTON1_MASK)
			    || (evt.getModifiers() == MouseEvent.BUTTON1_MASK
				    + MouseEvent.SHIFT_MASK)
			    || (evt.getModifiers() == MouseEvent.BUTTON1_MASK
				    + MouseEvent.CTRL_MASK)) {
			if (drag) {
			    int curx = this.getLocation().x;
			    int cury = this.getLocation().y;
			    int evtx = evt.getX() + curx;
			    int evty = evt.getY() + cury;
			    int x = curx + (evtx - oldX);
			    int y = cury + (evty - oldY);
			    // if( evt instanceof SentMouseEvent) {
			    if ((evtx > 0 && evtx < getParentSize().width)
				    && (evty > 0 && evty < getParentSize().height)) {
				if (x < 0)
				    x = 0;
				if (y < 0)
				    y = 0;
				this.setLocation(graficador.applySnap(x),
					graficador.applySnap(y));
				oldX = graficador.applySnap(evtx);
				oldY = graficador.applySnap(evty);
			    }
			}
		    }
		}
	    }
	}
    }

    /**
     * Asigna el tamanho al nodo.
     * 
     * @param nwidth
     *            ancho
     * @param nheight
     *            alto
     */
    @Override
    public void setSize(int nwidth, int nheight) {
	if (nwidth < getMinimumWidth()) {
	    final int snapSize = graficador.getSnapSize();
	    setSizeWithSnap(getMinimumWidth() + snapSize, nheight + snapSize);
	} else
	    setSizeWithSnap(nwidth, nheight);
    }

    /**
     * Asigna el nuevo tamanho, usando el snap.
     * 
     * @param nwidth
     *            ancho
     * @param nheight
     *            alto
     */
    private void setSizeWithSnap(int nwidth, int nheight) {
	super.setSize(graficador.applySnapSize(nwidth), graficador
		.applySnapSize(nheight));
	setLocation(graficador.applySnap(getLocation().x), graficador
		.applySnap(getLocation().y));
	updateUI();
    }

    /**
     * Sets the size without snap.
     * 
     * @param nwidth
     *            the nwidth
     * @param nheight
     *            the nheight
     */
    protected void setSizeWithoutSnap(int nwidth, int nheight) {
	super.setSize(nwidth, nheight);
	updateUI();
    }

    /**
     * Gets the parent size.
     * 
     * @return the parent size
     */
    protected Dimension getParentSize() {
	Dimension d = new Dimension(GrDiagrama.ANCHO_MAX, GrDiagrama.ALTO_MAX);
	return d;
    }

    /**
     * Responde al evento de movido del raton, dentro de este nodo.
     * 
     * @param evt
     *            evento del mouse
     */
    protected void moved(MouseEvent evt) {
    }

    /**
     * Checks if is design time.
     * 
     * @return true, if is design time
     */
    public boolean isDesignTime() {
	return designTime;
    }

    /**
     * Sets the designtime.
     * 
     * @param designTime
     *            the new designtime
     */
    public void setDesigntime(boolean designTime) {
	this.designTime = designTime;
	if (designTime) {
	    setCursor(new Cursor(Cursor.HAND_CURSOR));
	    componentListeners.addSpecificListeners();

	    for (ComponentListeners elem : componentes) {
		elem.removePreviousListeners();
	    }
	} else {
	    setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
	    componentListeners.removeSpecificListeners();

	    for (ComponentListeners elem : componentes) {
		elem.addPreviousListeners();
	    }
	}
    }

    /**
     * REtorna el centro del nodo.
     * 
     * @return centro del nodo
     */
    public Point getCenter() {
	return new Point((getLocation().x + (getWidth() / 2)),
		(getLocation().y + (getHeight() / 2)));
    }

    /**
     * Sets the drag.
     * 
     * @param drag
     *            the new drag
     */
    public void setDrag(boolean drag) {
	this.drag = drag;
    }

    /**
     * Checks if is drag.
     * 
     * @return true, if is drag
     */
    public boolean isDrag() {
	return drag;
    }

    /**
     * Sets the fore color.
     * 
     * @param newColor
     *            the new fore color
     */
    public void setForeColor(Color newColor) {
	foreColor = newColor;
    }

    /**
     * Sets the back color.
     * 
     * @param newColor
     *            the new back color
     */
    public void setBackColor(Color newColor) {
	backColor = newColor;
    }

    /**
     * Sets the select color.
     * 
     * @param newColor
     *            the new select color
     */
    public void setSelectColor(Color newColor) {
	selectColor = newColor;
    }

    /**
     * Gets the fore color.
     * 
     * @return the fore color
     */
    public Color getForeColor() {
	return foreColor;
    }

    /**
     * Gets the back color.
     * 
     * @return the back color
     */
    public Color getBackColor() {
	return backColor;
    }

    /**
     * Gets the select color.
     * 
     * @return the select color
     */
    public Color getSelectColor() {
	return selectColor;
    }

    /**
     * Sets the selected.
     * 
     * @param selected
     *            the new selected
     */
    public void setSelected(boolean selected) {
	// setDrag( selected );
	isSelected = selected;
	updateUI();
    }

    /**
     * Checks if is selected.
     * 
     * @return true, if is selected
     */
    public boolean isSelected() {
	return isSelected;
    }

    /**
     * The Class MouseMotionHandler.
     */
    class MouseMotionHandler implements MouseMotionListener {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * java.awt.event.MouseMotionListener#mouseDragged(java.awt.event.MouseEvent
	 * )
	 */
	public void mouseDragged(MouseEvent evt) {
	    dragged(evt);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * java.awt.event.MouseMotionListener#mouseMoved(java.awt.event.MouseEvent
	 * )
	 */
	public void mouseMoved(MouseEvent evt) {
	    moved(evt);
	}
    }

    /**
     * The Class MouseHandler.
     */
    class MouseHandler implements MouseListener {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * java.awt.event.MouseListener#mousePressed(java.awt.event.MouseEvent)
	 */
	public void mousePressed(MouseEvent evt) {
	    pressed(evt);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * java.awt.event.MouseListener#mouseReleased(java.awt.event.MouseEvent)
	 */
	public void mouseReleased(MouseEvent evt) {
	    released(evt);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * java.awt.event.MouseListener#mouseEntered(java.awt.event.MouseEvent)
	 */
	public void mouseEntered(MouseEvent evt) {
	    entered(evt);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * java.awt.event.MouseListener#mouseExited(java.awt.event.MouseEvent)
	 */
	public void mouseExited(MouseEvent evt) {
	    exited(evt);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * java.awt.event.MouseListener#mouseClicked(java.awt.event.MouseEvent)
	 */
	public void mouseClicked(MouseEvent evt) {
	    clicked(evt);
	}
    }

    /**
     * Actualiza la apariencia visual del nodo de acuerdo al modelo EQUIVALENTE
     * actualizar->Vista.
     */
    public void updateVisual() {
	setLocation(modelo.getX(), modelo.getY());
	setSize(modelo.getWidth(), modelo.getHeight());
	updateNodoVisual();
    }

    /**
     * Actualiza el nodo de acuerdo a la apriencia visual EQUIVALENTE->
     * actualizarModelo.
     */
    public void updateLogical() {
	modelo.setX(getLocation().x);
	modelo.setY(getLocation().y);
	modelo.setWidth(getSize().width);
	modelo.setHeight(getSize().height);
	updateNodoLogico();
    }

    /**
     * The Class ComponentListeners.
     */
    class ComponentListeners {

	/** The c. */
	Component c;

	/** The previous mouse listeners. */
	ArrayList<MouseListener> previousMouseListeners;

	/** The previous mouse motion listeners. */
	ArrayList<MouseMotionListener> previousMouseMotionListeners;

	/** The specific mouse listeners. */
	ArrayList<MouseListener> specificMouseListeners;

	/** The specific mouse motion listeners. */
	ArrayList<MouseMotionListener> specificMouseMotionListeners;

	/** The sub components. */
	ArrayList<ComponentListeners> subComponents;

	/**
	 * Instantiates a new component listeners.
	 * 
	 * @param c
	 *            the c
	 */
	public ComponentListeners(Component c) {
	    previousMouseListeners = new ArrayList<MouseListener>();
	    previousMouseMotionListeners = new ArrayList<MouseMotionListener>();
	    specificMouseListeners = new ArrayList<MouseListener>();
	    specificMouseMotionListeners = new ArrayList<MouseMotionListener>();

	    this.c = c;
	    MouseListener[] mls = (c.getListeners(MouseListener.class));
	    for (int i = 0; i < mls.length; i++)
		previousMouseListeners.add(mls[i]);

	    MouseMotionListener[] mmls = (c
		    .getListeners(MouseMotionListener.class));
	    for (int i = 0; i < mmls.length; i++)
		previousMouseMotionListeners.add(mmls[i]);

	    specificMouseListeners.add(new MouseHandler());
	    specificMouseMotionListeners.add(new MouseMotionHandler());

	    if (c instanceof Container) {
		Container cont = (Container) c;
		Component[] comps = cont.getComponents();
		subComponents = new ArrayList<ComponentListeners>();

		for (int j = 0; j < comps.length; j++) {
		    ComponentListeners cl = new ComponentListeners(comps[j]);
		    subComponents.add(cl);
		}
	    }
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object o) {
	    if (o instanceof ComponentListeners) {
		ComponentListeners cl = (ComponentListeners) o;
		if (cl.c == c)
		    return true;
	    }
	    return false;
	}

	/**
	 * Adds the previous listeners.
	 */
	public void addPreviousListeners() {
	    for (MouseListener nextMouseListener : previousMouseListeners) {
		c.addMouseListener(nextMouseListener);
	    }

	    for (MouseMotionListener nextMouseListener : previousMouseMotionListeners) {
		c.addMouseMotionListener(nextMouseListener);
	    }

	    if (subComponents != null && subComponents.size() > 0) {
		for (ComponentListeners nextComponent : subComponents) {
		    nextComponent.addPreviousListeners();
		}
	    }
	}

	/**
	 * Removes the previous listeners.
	 */
	public void removePreviousListeners() {
	    for (MouseListener nextMouseListener : previousMouseListeners) {
		c.removeMouseListener(nextMouseListener);
	    }

	    for (MouseMotionListener nextMouseListener : previousMouseMotionListeners) {
		c.removeMouseMotionListener(nextMouseListener);
	    }

	    if (subComponents != null && subComponents.size() > 0) {
		for (ComponentListeners nextComponent : subComponents) {
		    nextComponent.removePreviousListeners();
		}
	    }
	}

	/**
	 * Adds the specific listeners.
	 */
	public void addSpecificListeners() {
	    for (MouseListener nextMouseListener : specificMouseListeners) {
		c.addMouseListener(nextMouseListener);
	    }

	    for (MouseMotionListener nextMouseListener : specificMouseMotionListeners) {
		c.addMouseMotionListener(nextMouseListener);
	    }

	    if (subComponents != null && subComponents.size() > 0) {
		for (ComponentListeners nextComponent : subComponents) {
		    nextComponent.addSpecificListeners();
		}
	    }
	}

	/**
	 * Removes the specific listeners.
	 */
	public void removeSpecificListeners() {
	    for (MouseListener nextMouseListener : specificMouseListeners) {
		c.removeMouseListener(nextMouseListener);
	    }

	    for (MouseMotionListener nextMouseListener : specificMouseMotionListeners) {
		c.removeMouseMotionListener(nextMouseListener);
	    }

	    if (subComponents != null && subComponents.size() > 0) {
		for (ComponentListeners nextComponent : subComponents) {
		    nextComponent.removeSpecificListeners();
		}
	    }
	}

    }

}
