/*
 * BarraTareas.java
 *
 * Created on November 9, 2008, 9:53 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package vista.graficador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JToolBar;

import org.omg.uml.foundation.core.ModelElement;
import org.omg.uml.foundation.core.Namespace;
import org.omg.uml.foundation.core.Relationship;
import org.omg.uml.foundation.datatypes.VisibilityKindEnum;
import org.uml.diagrammanagement.DiagramManagementPackage;
import org.uml.diagrammanagement.GraphEdge;
import org.uml.diagrammanagement.GraphNode;
import org.uml.diagrammanagement.Property;
import org.uml.diagrammanagement.SimpleSemanticModelBridge;
import org.uml.diagrammanagement.SimpleSemanticModelBridgeClass;
import org.uml.diagrammanagement.Uml1SemanticModelBridge;

import util.Constantes;
import util.uml.ElementoDeModeloUtil;
import util.uml.UMLUtil;
import vista.graficador.comun.Link;
import vista.graficador.comun.Note;
import vista.graficador.uml.GrPaquete;

/**
 * The Class BarraTareas.
 * 
 * @author Juan Timoteo Ponce Ortiz
 */
public class BarraTareas extends JToolBar {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The graficador. */
	protected GrDiagrama graficador;

	private final Namespace rootNamespace;

	private final DiagramManagementPackage diagramManagement;

	/** The btn paquete. */
	private JButton btnPaquete;

	/** The btn note. */
	private final JButton btnNote;

	/** The btn link. */
	private final JButton btnLink;

	/**
	 * Creates a new instance of BarraTareas.
	 * 
	 * @param graficador
	 *            the graficador
	 */
	public BarraTareas(final Namespace rootNamespace,
			final GrDiagrama graficador,
			final DiagramManagementPackage diagramManagement) {
		this.rootNamespace = rootNamespace;
		this.graficador = graficador;
		this.diagramManagement = diagramManagement;

		final ModelElement me = graficador.getDiagrama().getOwner()
		.getElement();
		if (me instanceof Namespace) {
			btnPaquete = new JButton("Paquete");
			btnPaquete.setIcon(new ImageIcon(getClass().getResource(
			"/iconos/Paquete.png")));
			btnPaquete.addActionListener(new BarraTareasClaseListener(
					this.rootNamespace, "Package", GrPaquete.class.getName(),
					null, null, graficador, (Namespace) me,
					Constantes.MODEL_NODE_TYPE_ID));
			add(btnPaquete);
		}

		final CommonListener listener = new CommonListener(graficador);

		btnNote = new JButton("Nota");
		btnNote.setIcon(new ImageIcon(getClass().getResource(
		"/iconos/document-new.png")));
		btnNote.addActionListener(listener);

		btnLink = new JButton("Link");
		btnLink.setIcon(new ImageIcon(getClass().getResource(
		"/iconos/Enlace.png")));
		btnLink.addActionListener(listener);

		add(btnNote);
		add(btnLink);
	}

	public Namespace getRootNamespace() {
		return rootNamespace;
	}

	/**
	 * The listener interface for receiving barraTareasClase events. The class
	 * that is interested in processing a barraTareasClase event implements this
	 * interface, and the object created with that class is registered with a
	 * component using the component's
	 * <code>addBarraTareasClaseListener<code> method. When
	 * the barraTareasClase event occurs, that object's appropriate
	 * method is invoked.
	 * 
	 * @see BarraTareasClaseEvent
	 */
	protected static class BarraTareasClaseListener implements ActionListener {

		/** The nombre clase. */
		protected String nombreClase;

		/** The clase grafica. */
		protected String claseGrafica;

		/** The nombre. */
		protected String nombre;

		/** The estereotipo. */
		protected String estereotipo;

		/** The namespace. */
		protected Namespace namespace;

		/** The tipo elemento. */
		protected int tipoElemento;

		/** The diagrama. */
		protected GrDiagrama diagrama;

		private final Namespace rootNamespace;

		/**
		 * Creates a new instance of BarraTareasClaseListener.
		 * 
		 * @param nombreClase
		 *            the nombre clase
		 * @param claseGrafica
		 *            the clase grafica
		 * @param estereotipo
		 *            the estereotipo
		 * @param nombre
		 *            the nombre
		 * @param diagrama
		 *            the diagrama
		 * @param namespace
		 *            the namespace
		 * @param tipo
		 *            the tipo
		 */
		public BarraTareasClaseListener(final Namespace rootNamespace,
				final String nombreClase, final String claseGrafica,
				final String estereotipo, final String nombre,
				final GrDiagrama diagrama, final Namespace namespace,
				final int tipo) {
			this.rootNamespace = rootNamespace;
			this.nombreClase = nombreClase;
			this.claseGrafica = claseGrafica;
			this.estereotipo = estereotipo;
			this.nombre = nombre;
			this.diagrama = diagrama;
			this.namespace = namespace;
			tipoElemento = tipo;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent
		 * )
		 */
		public void actionPerformed(final ActionEvent e) {

			diagrama.cancelCreacionLineaVisual();
			diagrama.cancelCreacionNodoVisual();
			final ModelElement me = ElementoDeModeloUtil.crearElemento(
					nombreClase, estereotipo, nombre, rootNamespace);

			if (!(me instanceof Relationship)) {
				me.setName("Nuevo " + nombreClase + "");
			}

			if (nombre != null) {
				me.setName(nombre);
			}
			me.setVisibility(VisibilityKindEnum.VK_PUBLIC);
			me.setNamespace(namespace);
			namespace.getOwnedElement().add(me);

			if (tipoElemento == Constantes.MODEL_NODE_TYPE_ID) {

				final GraphNode graphNode = UMLUtil.crearGraphNode();
				final Property prop = UMLUtil.crearPropiedad(
						Constantes.MODEL_NODE_TYPE, claseGrafica);
				graphNode.getProperty().add(prop);
				final Uml1SemanticModelBridge bridge = UMLUtil
				.crearPuenteSemantico(me);
				graphNode.setSemanticModel(bridge);

				diagrama.setCrearNodoVisual(graphNode);
			} else if (tipoElemento == Constantes.MODEL_EDGE_TYPE_ID) {

				final GraphEdge graphEdge = UMLUtil.crearGraphEdge();
				final Property prop = UMLUtil.crearPropiedad(
						Constantes.MODEL_EDGE_TYPE, claseGrafica);
				graphEdge.getProperty().add(prop);
				final Uml1SemanticModelBridge bridge = UMLUtil
				.crearPuenteSemantico(me);
				graphEdge.setSemanticModel(bridge);

				diagrama.setCrearLineaVisual(graphEdge);
			}
		}
	}

	/**
	 * The listener interface for receiving common events. The class that is
	 * interested in processing a common event implements this interface, and
	 * the object created with that class is registered with a component using
	 * the component's <code>addCommonListener<code> method. When
	 * the common event occurs, that object's appropriate
	 * method is invoked.
	 * 
	 * @see CommonEvent
	 */
	class CommonListener implements ActionListener {

		/** The diagrama. */
		private final GrDiagrama diagrama;

		/**
		 * Instantiates a new common listener.
		 * 
		 * @param diagrama
		 *            the diagrama
		 */
		public CommonListener(final GrDiagrama diagrama) {
			this.diagrama = diagrama;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent
		 * )
		 */
		@Override
		public void actionPerformed(final ActionEvent e) {
			final SimpleSemanticModelBridgeClass simpleBridge = diagramManagement
			.getSimpleSemanticModelBridge();

			if (e.getSource().equals(btnNote)) {
				final GraphNode graphNode = UMLUtil.crearGraphNode();

				final Property prop = UMLUtil.crearPropiedad(
						Constantes.MODEL_NODE_TYPE, Note.class.getName());
				graphNode.getProperty().add(prop);
				final SimpleSemanticModelBridge nodeBridge = simpleBridge
				.createSimpleSemanticModelBridge();
				nodeBridge.setTypeInfo("New text");
				graphNode.setSemanticModel(nodeBridge);
				diagrama.setCrearNodo(new Note(diagrama, graphNode));

			} else if (e.getSource().equals(btnLink)) {
				final GraphEdge graphEdge = UMLUtil.crearGraphEdge();

				final Property prop = UMLUtil.crearPropiedad(
						Constantes.MODEL_EDGE_TYPE, Link.class.getName());
				graphEdge.getProperty().add(prop);

				diagrama.setCrearLinea(new Link(diagrama, graphEdge));
			}

		}

	}

}
