/*
 * 
 */

package vista.graficador.comun;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.event.MouseEvent;
import java.util.Iterator;
import java.util.Properties;

import org.uml.diagrammanagement.GraphNode;
import org.uml.diagrammanagement.Property;

import util.Constantes;
import util.uml.ElementoVisualUtil;
import vista.graficador.GrDiagrama;
import vista.graficador.GrLineaVisual;
import vista.graficador.SentMouseEvent;

/**
 * Represents a text that appears attached to an edge, such as label,
 * multiplicity, etc.
 */
public class EdgeText extends Text {
    /**
     * The edge to which this EdgeText is attached.
     */
    private final GrLineaVisual edge;

    /**
     * The difference between the natural x coordinate, where this edge text
     * would normally appear, and the x coordinate of the actual place where it
     * appears. This is used because the user may drag the edge text to a
     * different place than it originally appeared.
     */
    private int dx;
    /**
     * The difference between the natural y coordinate, where this edge text
     * would normally appear, and the y coordinate of the actual place where it
     * appears. This is used because the user may drag the edge text to a
     * different place than it originally appeared.
     */
    private int dy;
    private boolean canmove = true;

    /**
     * @see #edge
     */
    public GrLineaVisual getEdge() {
	return edge;
    }

    /**
     * Basic constructor.
     * 
     * @param graph
     *            the visual graph (diagram) where this text will be inserted.
     * @param graphNode
     *            the logical graph node
     */
    public EdgeText(GrDiagrama graph, GraphNode graphNode) {
	super(graph, graphNode);
	setBackground(new Color(0, 0, 0, 0));

	setCursor(new Cursor(Cursor.HAND_CURSOR));

	edge = null;
	dx = 0;
	dy = 0;

	updateVisual();
    }

    /**
     * Updates the visual appearance of this text, according to the logical
     * graph node.
     */
    @Override
    protected void updateNodoVisual() {
	Properties props = ElementoVisualUtil.getPropiedades(modelo);

	String nodeType = (String) props.get(Constantes.MODEL_NODE_TYPE);
	String strDx = props.getProperty("EdgeText:dx");
	String strDy = props.getProperty("EdgeText:dy");
	if (nodeType != null && nodeType.equals("EdgeText")) {
	    dx = Integer.parseInt(strDx);
	    dy = Integer.parseInt(strDy);
	} else {
	    System.err
		    .println("Found different node types: EdgeText (Visual) and "
			    + nodeType + " (Logical).");
	}
    }

    /**
     * Updates the logical graph node, according to the appearance of this
     * visual edge text.
     */
    protected void updateLogicalGraphNode() {
	Iterator<Property> it = modelo.getProperty().iterator();
	while (it.hasNext()) {
	    Property property = it.next();
	    if (property.getKey().equals("EdgeText:dx")) {
		property.setValue("" + dx);
	    } else if (property.getKey().equals("EdgeText:dy")) {
		property.setValue("" + dy);
	    }
	}
    }

    /**
     * Responds to the mouse click event.
     */
    @Override
    protected void clicked(MouseEvent evt) {
	if (canmove) {
	    super.clicked(evt);
	}
    }

    /**
     * Responds to the mouse drag event. Moves this edge text to follow the
     * mouse movement.
     */
    @Override
    protected void dragged(MouseEvent evt) {
	if (drag && canmove) {
	    if (isDesignTime()) {
		evt.consume();
		if ((evt.getModifiers() == MouseEvent.BUTTON1_MASK)
			|| (evt.getModifiers() == MouseEvent.BUTTON1_MASK
				+ MouseEvent.SHIFT_MASK)
			|| (evt.getModifiers() == MouseEvent.BUTTON1_MASK
				+ MouseEvent.CTRL_MASK)) {
		    if (drag) {
			int curx = this.getLocation().x;
			int cury = this.getLocation().y;
			int evtx = evt.getX() + curx;
			int evty = evt.getY() + cury;
			int x = curx + (evtx - oldX);
			int y = cury + (evty - oldY);
			if (evt instanceof SentMouseEvent) {
			    if ((evtx > 0 && evtx < getParentSize().width)
				    && (evty > 0 && evty < getParentSize().height)) {
				dx += GrDiagrama.applyDefaultSnap(evtx) - oldX;
				dy += GrDiagrama.applyDefaultSnap(evty) - oldY;
				if (x < 0)
				    x = 0;
				if (y < 0)
				    y = 0;
				this.setLocation(
					GrDiagrama.applyDefaultSnap(x),
					GrDiagrama.applyDefaultSnap(y));
				oldX = GrDiagrama.applyDefaultSnap(evtx);
				oldY = GrDiagrama.applyDefaultSnap(evty);
			    }
			}
		    }
		}
	    }
	}
    }

    /**
     * #see #dx
     */
    public int getDx() {
	return dx;
    }

    /**
     * #see #dy
     */
    public int getDy() {
	return dy;
    }

    /**
     * #see #dx
     */
    public void setDx(int p_dx) {
	dx = p_dx;
    }

    /**
     * #see #dy
     */
    public void setDy(int p_dy) {
	dy = p_dy;
    }

    /**
     * Sets the location of this edge text, considering the dx and dy.
     * 
     * @param x
     *            the new x
     * @param y
     *            the new y
     * @see #dx
     * @see #dy
     */
    public void setLocationWithDiff(int x, int y) {
	setLocation(x + dx, y + dy);
	updateLogical();
    }

    public void setCanMove(boolean p_move) {
	canmove = p_move;
	if (canmove)
	    setCursor(new Cursor(Cursor.HAND_CURSOR));
	else
	    setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
    }

    @Override
    public void setForeColor(Color newColor) {
	super.setForeColor(newColor);
	setTextColor(newColor);
    }

}
