/*
 * 
 */

package vista.graficador.comun;

import org.uml.diagrammanagement.GraphEdge;

import vista.graficador.GrDiagrama;
import vista.graficador.GrLineaVisual;


/**
 * Represents an anchor between a{@link mvcase.graph.common.Note} or {@link mvcase.graph.common.Text} 
 * to some element from the diagram.
 * @author Daniel
 */
public final class Link extends GrLineaVisual {
    /**
     * Basic constructor.
     * @param graph the visual graph (diagram) where this anchor will be inserted
     * @param logicalEdge the logical graph edge
     */
    public Link(GrDiagrama graph, GraphEdge logicalEdge)
    {
       super(graph,logicalEdge);
       setTracedline(true);
       setGraphical(true);
    }
    
	@Override
	protected void finalizeCreacionLinea() throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void updateLineaLogica() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void updateLineaVisual() {
		// TODO Auto-generated method stub
		
	}
}
