/*
 * 
 */

package vista.graficador.comun;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.util.StringTokenizer;

import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * Label with multiple lines.
 */
public final class MultiLineLabel extends JPanel{
    private String text;
    private GridLayout layout;
    private int alignment;
    private int nlines;
    
    /**
     * Constant that indicates left alignment.
     */
    public final static int LEFT = JLabel.LEFT;
    /**
     * Constant that indicates center alignment.
     */
    public final static int CENTER = JLabel.CENTER;
    /**
     * Constant that indicates right alignment.
     */
    public final static int RIGHT = JLabel.RIGHT;
    
    /**
     * Basic constructor.
     * @param p_text the text of the label, each line should be separated by a '\n' character.
     * @param p_alignment the alignment
     */
    public MultiLineLabel(String p_text,int p_alignment) {
        text = p_text;
        alignment = p_alignment;
        layout = new GridLayout(1,1);
        setLayout(layout);
        setLabels();
    }
    
    /**
     * Paints this component.
     */
    public void paint(Graphics g) {
        Container parent = getParent();
        if(parent!=null) {
//         setForeground(parent.getForeground());
            setBackground(parent.getBackground());
        }
        super.paint(g);
    }
    
    public void repaint() {
    }
    
    /**
     * Sets the labels, according to the text. Divides the text into tokens, separating by
     * the '\n' character.
     */
    private void setLabels() {
        removeAll();
        StringTokenizer st =  new StringTokenizer(text,"\n");
        nlines = st.countTokens();
        layout.setRows(nlines);
        while(st.hasMoreTokens()) {
            String line = st.nextToken();
            JLabel label = new JLabel(line,alignment);
            add(label);
        }
    }
    
    /**
     * Sets the text of this label.
     * @param p_text the new text of this label.
     */
    public void setText(String p_text) {
        text = p_text;
        setLabels();
    }
    
    /**
     * Gets the text of this label.
     * @return the text of this label.
     */
    public String getText() { return text; }
    
    /**
     * Gets the number of lines of this label.
     * @return the number of lines of this label
     */
    public int getNLines() { return nlines; }
    
    /**
     * Gets a string representation of this label, for testing purposes only.
     * @return a String, each line starting with '|', and ending with "\r\n".
     */
    public String getLines() {
        StringBuilder buffer = new StringBuilder();
        Component[] comps = getComponents();
        for(int i=0;i<nlines;i++) {
            JLabel lcomp = (JLabel)comps[i];
            buffer.append( "|"+lcomp.getText()+"\r\n" );
        }
        return buffer.toString();
    }
    
    /**
     * Sets the foreground color for this label.
     */
    public void setForeground(Color newColor) {
        Component[] comps = getComponents();
        for(int i=0;i<nlines;i++) {
            JLabel lcomp = (JLabel)comps[i];
            lcomp.setForeground(newColor);
        }
    }
    
}
