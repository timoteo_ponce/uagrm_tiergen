//REVISAR EL PRESSED, CREO QUE HAY UN PROBLEMA AHI
//TODO: hay un error que no pillo, no grafican las lineas, este es el viejo
/*
 * GrDiagrama.java
 *
 * Created on 5 de noviembre de 2008, 10:47
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package vista.graficador;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.beans.PropertyChangeEvent;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.swing.JPanel;
import javax.swing.event.MouseInputAdapter;

import modelo.types.PropertyType;

import org.apache.log4j.Logger;
import org.omg.uml.foundation.core.ModelElement;
import org.omg.uml.foundation.core.Namespace;
import org.uml.diagrammanagement.Diagram;
import org.uml.diagrammanagement.DiagramManagementPackage;
import org.uml.diagrammanagement.GraphEdge;
import org.uml.diagrammanagement.GraphElement;
import org.uml.diagrammanagement.GraphNode;
import org.uml.diagrammanagement.SemanticModelBridge;
import org.uml.diagrammanagement.Uml1SemanticModelBridge;

import util.AppUtil;
import util.Constantes;
import util.uml.ElementoVisualUtil;
import util.uml.UMLUtil;
import vista.Vista;
import vista.editores.EditorListener;
import vista.graficador.comun.Note;
import vista.graficador.uml.GrClaseAsociacionLnk;
import controlador.ControllerProyecto;

/**
 * The Class GrDiagrama.
 * 
 * @author Juan Timoteo Ponce Ortiz
 */
public class GrDiagrama extends JPanel implements Vista {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The log. */
	private static final Logger log = Logger.getLogger(GrDiagrama.class);
	// private static Logger logger = Logger.getLogger( GrDiagrama.class );

	/** The diagrama. */
	private Diagram diagrama;

	private final DiagramManagementPackage diagramManagement;

	/** The nodos. */
	private final List<GrNodoVisual> nodos;

	/** The lineas. */
	private final List<GrLineaVisual> lineas;

	/** The nuevo nodo visual. */
	private GrNodoVisual nuevoNodoVisual;

	/** The nueva linea visual. */
	private GrLineaVisual nuevaLineaVisual;

	/** The send events. */
	boolean pointClicked, hasMoreThan2Points, sendEvents = true;

	/** The origen. */
	private GrNodoVisual origen;

	/** The property viewer. */
	private Vista propertyViewer;

	/** The controller. */
	private ControllerProyecto controller;

	/** The Constant ANCHO_MAX. */
	public static final int ANCHO_MAX = 1000;

	/** The Constant ALTO_MAX. */
	public static final int ALTO_MAX = 1000;

	/**
	 * Los elementos graficos son posicionados y redimensionados de acuerdo a
	 * este valor, con coordenadas X,Y multiplo de numero, asi tambien como los
	 * tamanhos.
	 */

	public static final int DEFAULT_SNAP_SIZE = 5;

	private int snapSize = DEFAULT_SNAP_SIZE;

	/** The update model. */
	private boolean updateModel = true;

	/**
	 * Creates a new instance of GrDiagrama.
	 * 
	 * @param controller
	 *            the controller
	 * @param diagrama
	 *            the diagrama
	 */
	public GrDiagrama(final ControllerProyecto controller,
			final Diagram diagrama,
			final DiagramManagementPackage diagramManagement) {
		this.diagrama = diagrama;
		this.controller = controller;
		this.diagramManagement = diagramManagement;
		nuevoNodoVisual = null;
		nuevaLineaVisual = null;
		nodos = new ArrayList<GrNodoVisual>();
		lineas = new ArrayList<GrLineaVisual>();
		init();
		initEvents();
		updateGraficador();
	}

	/**
	 * Sets the property viewer.
	 * 
	 * @param propertyViewer
	 *            the new property viewer
	 */
	public void setPropertyViewer(final Vista propertyViewer) {
		this.propertyViewer = propertyViewer;
	}

	/**
	 * Gets the sNA p_ size.
	 * 
	 * @return the sNA p_ size
	 */
	public int getSnapSize() {
		return snapSize;
	}

	/**
	 * Sets the sNA p_ size.
	 * 
	 * @param snap_size
	 *            the new sNA p_ size
	 */
	public void setSnapSize(final int snapSize) {
		this.snapSize = snapSize;
	}

	/**
	 * Inicializa los elementos de la clase.
	 */
	public void init() {
		nodos.clear();
		lineas.clear();
		setLayout(null);
		final Dimension dim = new Dimension(ANCHO_MAX, ALTO_MAX);
		setPreferredSize(dim);
		setMaximumSize(dim);
		setMinimumSize(dim);
		setBackground(Color.WHITE);
	}

	/**
	 * Convierte valores de tamanho(ancho o alto) a su tamanho basado en el
	 * SNAP_SIZE.
	 * 
	 * @param value
	 *            valor a cambiar
	 * 
	 * @return valor SNAP calculado
	 */

	public int applySnapSize(final int value) {
		return ((value + snapSize) / (2 * snapSize)) * (2 * snapSize);
	}

	/**
	 * Convierte un valor de coordenada a su correspondiente tamanho SNAP.
	 * 
	 * @param value
	 *            coordenada a convertir
	 * 
	 * @return valor SNAP calculado
	 */
	public int applySnap(final int value) {
		return (value / snapSize) * snapSize;
	}

	public static int applyDefaultSnap(final int value) {
		return (value / DEFAULT_SNAP_SIZE) * DEFAULT_SNAP_SIZE;
	}

	/**
	 * Convierte las coordenadas a su valor SNAP.
	 * 
	 * @param p
	 *            punto a convertir
	 * 
	 * @return valor SNAP calculado
	 */
	public Point applySnap(final Point p) {
		return new Point((p.x / snapSize) * snapSize, (p.y / snapSize)
				* snapSize);
	}

	/**
	 * Registra los listeners de eventos.
	 */
	private void initEvents() {
		addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(final MouseEvent evt) {
				mouseHasClicked(evt);
			}

			@Override
			public void mousePressed(final MouseEvent evt) {
				mouseHasPressed(evt);
			}

			@Override
			public void mouseReleased(final MouseEvent evt) {
				mouseHasReleased(evt);
			}
		});

		addMouseListener(new MouseInputAdapter() {
			@Override
			public void mouseEntered(final MouseEvent evt) {
				mouseHasEntered(evt);
			}

			@Override
			public void mouseExited(final MouseEvent evt) {
				mouseHasExited(evt);
			}
		});
		addMouseMotionListener(new MouseMotionAdapter() {
			@Override
			public void mouseDragged(final MouseEvent evt) {
				mouseHasDragged(evt);
			}
		});
	}

	/**
	 * Recarga el diagrama, borrandose y repintado el diagrama.
	 */
	public void reloadGraficador() {
		nodos.clear();
		lineas.clear();
		removeAll();
		updateGraficador();
		repaint();
		updateUI();
	}

	/**
	 * Indica si es posible o no crear un elemento visual.
	 * 
	 * @param ge
	 *            elemento a ser creado
	 * 
	 * @return true si se puede crear
	 */
	private boolean canCrearVisualElemento(final GraphElement ge) {
		if (ge instanceof GrClaseAsociacionLnk) {
			return true;
		} else if (!(ElementoVisualUtil.isComun(ge) && (!(ge.getContainer() instanceof Diagram)))) {
			return true;
		}
		return false;
	}

	/**
	 * Actualiza el graficador, agregando elementos y subelementos al diagrama.
	 * 
	 * @param elemento
	 *            elemeto a agregar
	 */
	private void updateGraficador(final GraphElement elemento) {
		for (Object nextContainedElement : elemento.getContained()) {
			final GraphElement elementToAdd = (GraphElement) nextContainedElement;

			if (canCrearVisualElemento(elementToAdd)) {
				if (elementToAdd instanceof GraphNode) {
					crearNodoVisual((GraphNode) elementToAdd);
				} else if (elementToAdd instanceof GraphEdge) {
					crearLineaVisual((GraphEdge) elementToAdd);
				}
				updateGraficador(elementToAdd);
			}
		}
	}

	/**
	 * Actualiza el graficado, actualizando de forma recursiva las relaciones de
	 * elementos y subelementos.
	 * 
	 * @param elemento
	 *            elemento a actualizar
	 */
	private void updateReferencias(final GraphElement elemento) {
		for (Object nextContainedElement : elemento.getContained()) {
			final GraphElement edgeToUpdate = (GraphElement) nextContainedElement;

			if (edgeToUpdate instanceof GraphEdge) {
				updateReferenciasLineales((GraphEdge) edgeToUpdate);
			}
			updateReferencias(edgeToUpdate);
		}
	}

	/**
	 * Actualiza lineas, tanto su inicio y fin.
	 * 
	 * @param edge
	 *            linea a ser actualizada
	 */
	public void updateReferenciasLineales(final GraphEdge edge) {
		final GraphElement temp1 = edge.getEdgeEnd1();
		final GraphElement temp2 = edge.getEdgeEnd2();
		final ElementoVisual fin1 = getElementoVisual(temp1);
		final ElementoVisual fin2 = getElementoVisual(temp2);
		if (fin1 == null || fin2 == null) {
			log.warn("Error en linea, referencias no cumplidas");
		}
		// else{
		final GrLineaVisual lineaVisual = (GrLineaVisual) getElementoVisual(edge);
		lineaVisual.setFin1(fin1);
		lineaVisual.setFin2(fin2);
		lineaVisual.updateVisual();
		// }
	}

	/**
	 * Crea un nuevo nodo visual, para ser insertado al diagrama cuando se
	 * presione sobre el raton sobre el diagrama.
	 * 
	 * @param node
	 *            nuevo nodo para representar visualmente
	 * 
	 * @return nuevo nodo visual a ser insertado al diagrama
	 */
	public GrNodoVisual setCrearNodoVisual(final GraphNode node) {
		final GrNodoVisual newNodo = UMLUtil.instanceGrNodoVisual(this, node);
		if (newNodo != null) {
			setCrearNodo(newNodo);
		}
		return newNodo;

	}

	/**
	 * Crea un nuevo nodo visual y lo agrega al diagrama.
	 * 
	 * @param node
	 *            nodo a representar de forma visual
	 */
	@SuppressWarnings("unchecked")
	public void crearNodoVisual(final GraphNode node) {
		final Map props = ElementoVisualUtil.getPropiedades(node);
		final String tipoNodo = (String) props.get(Constantes.MODEL_NODE_TYPE);
		if (!isGraphicalElement(tipoNodo)) {
			return;
		}

		final GrNodoVisual newNodo = UMLUtil.instanceGrNodoVisual(this, node);
		if (newNodo != null) {
			newNodo.updateVisual();
			addNodoVisual(newNodo);
		}
	}

	/**
	 * Crea una linea visual a ser insertada en el diagrama cuando se presione
	 * el raton sobre el diagrama.
	 * 
	 * @param edge
	 *            Nueva linea a ser representada
	 */
	public void setCrearLineaVisual(final GraphEdge edge) {
		final GrLineaVisual newLinea = UMLUtil
		.instanceGrLineaVisual(this, edge);
		if (newLinea != null) {
			setCrearLinea(newLinea);
		}
	}

	/**
	 * Crea una linea visual a una linea logica, y la inserta al diagrama.
	 * 
	 * @param edge
	 *            elemento para crear representacion
	 */
	public void crearLineaVisual(final GraphEdge edge) {
		final GrLineaVisual newLinea = UMLUtil
		.instanceGrLineaVisual(this, edge);
		if (newLinea != null) {
			newLinea.updateVisual();
			addLineaVisual(newLinea);
		}

	}

	/**
	 * Asigna el nodo a el atributo temporal, para cuando se presione de nuevo
	 * el raton, se inserte el nodo en el diagrama.
	 * 
	 * @param nodo
	 *            nodo a insertar
	 */
	public void setCrearNodo(final GrNodoVisual nodo) {
		nuevoNodoVisual = nodo;
		cancelCreacionLineaVisual();
	}

	/**
	 * Asigna la linea a el atributo temporal, para cuando se presione de nuevo
	 * el raton, se inserte la linea en el diagrama.
	 * 
	 * @param edge
	 *            linea a ser insertada
	 */
	public void setCrearLinea(final GrLineaVisual edge) {
		nuevaLineaVisual = edge;
		cancelCreacionNodoVisual();
		origen = null;
	}

	/**
	 * Cancela la seleccion de los elementos del diagramaDe-selects all elements
	 * in this diagram.
	 */
	public void deselectAll() {
		for (GrNodoVisual elem : nodos) {
			elem.setSelected(false);
		}

		for (GrLineaVisual elem : lineas) {
			elem.setSelected(false);
		}
	}

	/**
	 * Responde al evento del click del ratom sobre el diagrama Si hay una
	 * linea, al clickear 2 veces, sera seleccionada y editada.
	 * 
	 * @param evt
	 *            evento raton
	 */
	private void mouseHasClicked(final MouseEvent evt) {
		if (evt.getModifiers() == MouseEvent.BUTTON1_MASK
				&& evt.getClickCount() == 2) {
			final GrLineaVisual linea = getLineaByPunto(evt.getPoint());
			if (linea != null) {
				deselectAll();
				linea.setSelected(true);
				linea.edit();
			}
		}
	}

	/**
	 * Responde al evento del ration sobre el diagrama, las acciones posibles
	 * con las siguientes. 1. El usuario a previamente ha presionado el boton de
	 * creacion de un nodo, el nodo se inserta en la posicion del raton. 2. El
	 * usuario previamente ha presionado el boton de creacion de relacion, y
	 * ademas ya esta escogido el origen de la relacion, entonces se agrega el
	 * nodo destino y el enlace es creado 3. El usuario previamente ha
	 * presionado el boton de creacion de relacion, pero no ha seleccionado el
	 * origen, como no tiene origen, la accion se cancela. 4. El usuario no esta
	 * creando ningun elemento, y solo quiere seleccionarlos, entonces se
	 * verifica la posicion de los elementos y se seleccionan.
	 * 
	 * @param evt
	 *            evento de raton
	 */
	private void mouseHasPressed(final MouseEvent evt) {
		getParent().requestFocus();
		pointClicked = false;

		if ((evt.getModifiers() == MouseEvent.BUTTON1_MASK)
				|| (evt.getModifiers() == MouseEvent.BUTTON1_MASK
						+ MouseEvent.SHIFT_MASK)
						|| (evt.getModifiers() == MouseEvent.BUTTON1_MASK
								+ MouseEvent.CTRL_MASK)) {
			if (evt.getModifiers() == MouseEvent.BUTTON1_MASK) {
				deselectAll();
			}
			if (nuevoNodoVisual != null) {
				nuevoNodoVisual.setLocation(evt.getX(), evt.getY());
				addNewNodo(nuevoNodoVisual);
				nuevoNodoVisual = null;
				updateUI();
			} else if (nuevaLineaVisual != null) {
				if (origen != null) {
					if (nuevaLineaVisual.addPoint(applySnap(new Point(evt
							.getX(), evt.getY())))) {
						hasMoreThan2Points = true;
						nuevaLineaVisual.paint(getGraphics());
						origen.updateUI();
					} else {
						cancelCreacionNodoVisual();
						cancelCreacionLineaVisual();
						updateUI();
					}
				}
			} else {
				final GrLineaVisual linea = getLineaByPunto(evt.getPoint());
				if (linea != null) {
					deselectAll();
					linea.setSelected(true);

					if ((linea.clickedPoint(evt.getPoint()))
							|| (linea.clickedSegment(evt.getPoint()))) {
						pointClicked = true;
					}
				}
				updateUI();
			}
		} else {
			final GrLineaVisual linea = getLineaByPunto(evt.getPoint());
			if (linea != null) {
				deselectAll();
				linea.setSelected(true);
				linea.showMenu(evt.getX(), evt.getY());
			} else {
				if (nuevaLineaVisual != null) {
					updateUI();
				}
				cancelCreacionLineaVisual();
				cancelCreacionNodoVisual();
			}
		}
	}

	/**
	 * Responde al evento de soltado de raton. Cuando se suelta el raton, no se
	 * debe hacer nada del anterior evento, a excepcion de: 1. Cuando el usuario
	 * esta arrastrando algun nodo o linea, el texto de la linea desaparece,
	 * entonces debe reaparecer al soltarse. 2. Cuando se repinta una linea, se
	 * deben alinear y corregir los puntos.
	 * 
	 * @param evt
	 *            evento de raton
	 */
	private void mouseHasReleased(final MouseEvent evt) {
		if (pointClicked) {
			final GrLineaVisual lineaSelected = getSelectedLinea();
			GrNodoVisual nodo1 = null;
			GrNodoVisual nodo2 = null;
			if (lineaSelected.getFin1() instanceof GrNodoVisual) {
				nodo1 = (GrNodoVisual) lineaSelected.getFin1();
			}
			if (lineaSelected.getFin2() instanceof GrNodoVisual) {
				nodo2 = (GrNodoVisual) lineaSelected.getFin2();
			}
			lineaSelected.alignPoints(nodo1, nodo2);
			lineaSelected.correctLinkAttributePoint();
			lineaSelected.showLabels();
			lineaSelected.updateLogical();
			updateUI();
		}
		setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
	}

	/**
	 * Mouse has entered.
	 * 
	 * @param evt
	 *            the evt
	 */
	private void mouseHasEntered(final MouseEvent evt) {
	}

	/**
	 * Mouse has exited.
	 * 
	 * @param evt
	 *            the evt
	 */
	private void mouseHasExited(final MouseEvent evt) {
	}

	/**
	 * Indica que algun elemento va a ser soltado.
	 */
	public void prepararForSoltar() {
		requestFocus();
		setCursor(new Cursor(Cursor.CROSSHAIR_CURSOR));

	}

	/**
	 * Responde al evento de arrastre del raton sobre el diagrama, moviendo el
	 * punto, de acuerdo al SNAP.
	 * 
	 * @param evt
	 *            evento de raton
	 */
	private void mouseHasDragged(final MouseEvent evt) {
		if (pointClicked) {
			final GrLineaVisual lineaSelected = getSelectedLinea();
			lineaSelected.moveSelectedPoint(applySnap(evt.getPoint()));
			lineaSelected.hideLabels();
			this.repaint();
		}

	}

	/**
	 * Responde al evento de raton, cuando se presiona sobre algun nodo dentro
	 * del diagrama.
	 * 
	 * 1. El usuario previamente ha presionado el boton de crear una relacion,
	 * pero no ha seleccionado el origen de la misma, si es asi, se selecciona
	 * el nodo como origen. 2. El usuario ha seleccionado la creacion de
	 * relacion y ha seleccionado el nodo origen, entonces el nodo seleccionado
	 * sera el nodo destino de la relacion, creando la relacion en el diagrama
	 * 3. El usuario ha seleccionado la creacion de relacion y ha seleccionado
	 * el nodo origen, y el nodo destino es el mismo del origen, pero la
	 * relacion no sera visible
	 * 
	 * @param evt
	 *            evento de raton
	 */
	private void mouseHasPressedInNode(final MouseEvent evt) {
		try {
			boolean hasSelected = false;
			GrNodoVisual node = null;
			if (!evt.isConsumed()) {
				cancelCreacionNodoVisual();
				getParent().requestFocus();
				node = (GrNodoVisual) evt.getSource();
				if (evt.getModifiers() == MouseEvent.BUTTON1_MASK) {
					if (!node.isSelected()) {
						deselectAll();
						node.setSelected(true);
						hasSelected = true;
					}
					if (nuevaLineaVisual != null) {
						node.setDrag(false);
						final Point areapoint = new Point(evt.getPoint());
						areapoint.x += node.getLocation().x;
						areapoint.y += node.getLocation().y;
						nuevaLineaVisual.addPoint(node.getCenter() /*
						 * ,areapoint
						 */);
						if (origen == null) {
							nuevaLineaVisual.setFin1(node);
							origen = node;
							updateUI();
						} else {
							if (!(nuevaLineaVisual.getFin1() == node && !hasMoreThan2Points)) {
								nuevaLineaVisual.setFin2(node);
								addNewLinea(nuevaLineaVisual);
								nuevaLineaVisual = null;
								origen = null;
								reloadGraficador();
							}
						}
					}
					sendEventToSelected(evt);
				} else if ((evt.getModifiers() == MouseEvent.BUTTON1_MASK
						+ MouseEvent.SHIFT_MASK)
						|| (evt.getModifiers() == MouseEvent.BUTTON1_MASK
								+ MouseEvent.CTRL_MASK)) {
					node.setSelected(true);
					node.updateUI();
					sendEventToSelected(evt);
					hasSelected = true;
				} else {
					if (nuevaLineaVisual != null) {
						updateUI();
					}
					cancelCreacionLineaVisual();
					cancelCreacionNodoVisual();
				}
			}
			if (hasSelected && node != null && !(node instanceof Note)) {
				propertyViewer.setModelo(node);
			}
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			if (e.getMessage() != null && !e.getMessage().isEmpty()) {
				AppUtil.showAdvertencia(this, e.getMessage());
			} else {
				AppUtil.showAdvertencia(this, e.toString());
			}
		}
	}

	/**
	 * Delega el evento a todos los elementos seleccionados, esto es usado
	 * cuando multiples elementos son seleccionados, esto quiere decir que deben
	 * moverse en conjunto.
	 * 
	 * @param evt
	 *            evento a ser delegado
	 */
	protected void sendEventToSelected(final MouseEvent evt) {
		if (sendEvents) {
			sendEvents = false;
			final GrNodoVisual nodo = (GrNodoVisual) evt.getSource();
			for (GrNodoVisual grNode : nodos) {
				if (grNode != nodo) {
					if (grNode.isSelected()) {
						SentMouseEvent sme = new SentMouseEvent(grNode, evt);
						grNode.dispatchEvent(sme);
					}
				}
			}
			sendEvents = true;
		}
	}

	/**
	 * Retorna los nodos seleccionados.
	 * 
	 * @return nodos seleccionados
	 */
	public List<GrNodoVisual> getSelectedNodos() {
		final List<GrNodoVisual> out = new ArrayList<GrNodoVisual>();

		for (GrNodoVisual elem : nodos) {
			if (elem.isSelected()) {
				out.add(elem);
			}
		}
		return out;
	}

	/**
	 * Responds to the event of the mouse button being released inside a node.
	 * When the mouse is released, nothing must be done beyond what was already
	 * performed by the mouse pressed, except for one situation: When the user
	 * is dragging this node, the edge texts disappear, and must reappear when
	 * the button is released. This is what is performed in this method.
	 * 
	 * @param evt
	 *            the mouse event
	 */
	private void mouseHasReleasedInNode(final MouseEvent evt) {
		if (!evt.isConsumed()) {
			final GrNodoVisual node = (GrNodoVisual) evt.getSource();
			node.setDrag(true);
			final Collection<GrLineaVisual> edges = getEdgesByEdgeEnd(node);

			if (!edges.isEmpty()) {

				for (GrLineaVisual nextEdge : edges) {
					nextEdge.alignPoints(node);
					nextEdge.correctLinkAttributePoint();
					nextEdge.showLabels();
					nextEdge.updateLogical();
				}
				updateUI();
			}
			sendEventToSelected(evt);
			updateUI();
		}
	}

	/**
	 * Responds to the event of the mouse being dragged inside a node. The node,
	 * and all connecting edges, are then moved to follow the new mouse
	 * position.
	 * 
	 * @param evt
	 *            the mouse event.
	 * 
	 * @see mvcase.graph.VisualGraphNode
	 */
	private void mouseHasDraggedInNode(final MouseEvent evt) {
		if (!evt.isConsumed()) {
			final GrNodoVisual node = (GrNodoVisual) evt.getSource();
			final Collection<GrLineaVisual> edges = getEdgesByEdgeEnd(node);
			if (!edges.isEmpty()) {

				for (GrLineaVisual nextEdge : edges) {
					GrNodoVisual node1 = null;
					if ((nextEdge.getFin1()) instanceof GrNodoVisual) {
						node1 = (GrNodoVisual) nextEdge.getFin1();
					}
					GrNodoVisual node2 = null;
					if ((nextEdge.getFin2()) instanceof GrNodoVisual) {
						node2 = (GrNodoVisual) (nextEdge.getFin2());
					}
					if ((node1 != null && node2 != null)
							&& (node1.isSelected() && node2.isSelected())) {
						nextEdge.movePoints(node);
					} else {
						nextEdge.correctPoint(node);
					}
					nextEdge.hideLabels();
				}
			}
			sendEventToSelected(evt);
			this.repaint();
		}
	}

	/**
	 * Retorna la linea actual seleccionada.
	 * 
	 * @return linea seleccionada
	 */
	public GrLineaVisual getSelectedLinea() {
		for (GrLineaVisual elem : lineas) {
			if (elem.isSelected()) {
				return elem;
			}
		}
		return null;
	}

	/**
	 * Gets an edge that contains the specified point.
	 * 
	 * @param p
	 *            the point
	 * 
	 * @return an edge that contains the specified point, or null if there is no
	 *         edge containing that point.
	 * 
	 * @see mvcase.graph.VisualGraphEdge#containsPoint(Point)
	 */
	public GrLineaVisual getLineaByPunto(final Point p) {
		for (GrLineaVisual elem : lineas) {
			if (elem.containsPoint(p)) {
				return elem;
			}
		}
		return null;
	}

	/**
	 * Gets all nodes that contain the specified point.
	 * 
	 * @param p
	 *            the point
	 * 
	 * @return a Collection of all nodes that contain the specified point, or an
	 *         empty collection if no nodes contain that point.
	 */
	public List<GrNodoVisual> getNodesByPoint(final Point p) {
		final List<GrNodoVisual> ret = new ArrayList<GrNodoVisual>();

		for (GrNodoVisual node : nodos) {
			if (node.getBounds().contains(p)) {
				ret.add(node);
			}
		}
		return ret;
	}

	/**
	 * Gets all edges that has the specified end.
	 * 
	 * @param edgeEnd
	 *            the edge end
	 * 
	 * @return a collection of all edges that connect to the specified edge end.
	 */
	public List<GrLineaVisual> getEdgesByEdgeEnd(final ElementoVisual edgeEnd) {
		final List<GrLineaVisual> ret = new ArrayList<GrLineaVisual>();

		for (GrLineaVisual edge : lineas) {
			if ((edge.getFin1() == edgeEnd) || (edge.getFin2() == edgeEnd)) {
				ret.add(edge);
			}
		}
		return ret;
	}

	/**
	 * Gets the visual correspondent of some logical graph element.
	 * 
	 * @param element
	 *            the graph element.
	 * 
	 * @return the visual correspondent of the specified graph element.
	 */
	public ElementoVisual getElementoVisual(final GraphElement element) {
		for (GrNodoVisual elem : nodos) {
			if (elem.getNodoLogico().equals(element)) {
				return elem;
			}
		}

		for (GrLineaVisual elem : lineas) {
			if (elem.getLineaLogica().equals(element)) {
				return elem;
			}
		}
		log.warn(" RETORNO NULO");
		return null;
	}

	/**
	 * Adds a visual graph node to the diagram, and registers the listeners for
	 * the mouse events.
	 * 
	 * @param node
	 *            the node to be added.
	 * 
	 * @see #addNewGraphNode(VisualGraphNode)
	 */
	private void addNodoVisual(final GrNodoVisual node) {
		node.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(final MouseEvent evt) {
				mouseHasPressedInNode(evt);
			}

			@Override
			public void mouseReleased(final MouseEvent evt) {
				mouseHasReleasedInNode(evt);
			}
		});
		node.addMouseMotionListener(new MouseMotionAdapter() {
			@Override
			public void mouseDragged(final MouseEvent evt) {
				mouseHasDraggedInNode(evt);
			}
		});
		nodos.add(node);
		add(node);
		// FIX solamente debe actualizar el arbol y a si mismo
		changeModel();
	}

	/**
	 * Change model.
	 */
	private void changeModel() {
		if (isUpdateModel()) {
			controller.changeModelo(this);
		}
	}

	/**
	 * Adds a new Visual Graph Node to the diagram, creating the connection
	 * between the logic element and the graph element.
	 * 
	 * @param node
	 *            the node to be added.
	 */
	@SuppressWarnings("unchecked")
	public void addNewNodo(final GrNodoVisual node) {
		addNodoVisual(node);
		node.updateLogical();
		diagrama.getContained().add(node.getNodoLogico());
	}

	/**
	 * Adds the linea visual.
	 * 
	 * @param edge
	 *            the edge
	 */
	private void addLineaVisual(final GrLineaVisual edge) {
		lineas.add(edge);
		changeModel();
	}

	/**
	 * Adds a new Visual Graph Edge to the diagram, creating the connection
	 * between the logic element and the graph element.
	 * 
	 * @param edge
	 *            the edge to be added.
	 * 
	 * @throws Exception
	 *             the exception
	 */
	@SuppressWarnings("unchecked")
	public void addNewLinea(final GrLineaVisual edge) throws Exception {
		GraphElement grEdgeEnd1 = null;
		GraphElement grEdgeEnd2 = null;
		ElementoVisual edge1 = edge.getFin1();
		ElementoVisual edge2 = edge.getFin2();
		if (edge1 instanceof GrNodoVisual) {
			grEdgeEnd1 = ((GrNodoVisual) edge1).getNodoLogico();
		} else if (edge1 instanceof GrLineaVisual) {
			grEdgeEnd1 = ((GrLineaVisual) edge1).getLineaLogica();
		}
		if (edge2 instanceof GrNodoVisual) {
			grEdgeEnd2 = ((GrNodoVisual) edge2).getNodoLogico();
		} else if (edge2 instanceof GrLineaVisual) {
			grEdgeEnd2 = ((GrLineaVisual) edge2).getLineaLogica();
		}

		edge.getLineaLogica().setEdgeEnd1(grEdgeEnd1);
		edge.getLineaLogica().setEdgeEnd2(grEdgeEnd2);

		try {
			edge.finalizeCreacionLinea();
			edge.updateLogical();
			diagrama.getContained().add(edge.getLineaLogica());
			addLineaVisual(edge);
			edge.afterAdd();
		} catch (Exception e) {
			cancelCreacionLineaVisual();
			throw e;
		}
	}

	/**
	 * Cancels the creation of a visual graph node. The node that was created
	 * and was expecting to be inserted to the diagram is deleted.
	 * 
	 * @see #cancelVisualGraphEdgeCreation()
	 */
	public void cancelCreacionNodoVisual() {
		if (nuevoNodoVisual != null) {
			final GraphNode logicalNode = nuevoNodoVisual.getNodoLogico();
			final SemanticModelBridge smb = logicalNode.getSemanticModel();

			if (smb instanceof Uml1SemanticModelBridge) {
				try {
					final Uml1SemanticModelBridge semanticBridge = (Uml1SemanticModelBridge) smb;
					final ModelElement me = semanticBridge.getElement();
					final Namespace namespace = me.getNamespace();
					namespace.getOwnedElement().remove(me);
					me.refDelete();
				} catch (Exception e) {
					log.error("Error cancelling visual graph node creation", e);
				}
			}
			logicalNode.refDelete();
			nuevoNodoVisual = null;
		}
	}

	/**
	 * Cancels the creation of a visual graph edge. The edge that was created
	 * and was expecting to be inserted to the diagram is deleted.
	 * 
	 * @see #cancelVisualGraphNodeCreation()
	 */
	public void cancelCreacionLineaVisual() {
		if (nuevaLineaVisual != null) {
			final GraphEdge logicalEdge = nuevaLineaVisual.getLineaLogica();
			final SemanticModelBridge smb = logicalEdge.getSemanticModel();
			if (smb instanceof Uml1SemanticModelBridge) {
				try {
					final Uml1SemanticModelBridge semanticBridge = (Uml1SemanticModelBridge) smb;
					final ModelElement me = semanticBridge.getElement();
					final Namespace namespace = me.getNamespace();
					namespace.getOwnedElement().remove(me);
					me.refDelete();
				} catch (Exception e) {
					log.error("Error cancelling visual graph edge creation", e);
				}
			}
			logicalEdge.refDelete();
			nuevaLineaVisual = null;
		}
		origen = null;
	}

	/**
	 * Pinta todos los elementos del diagrama.
	 * 
	 * @param g
	 *            the g
	 */
	@Override
	protected void paintChildren(final Graphics g) {
		for (GrLineaVisual edge : lineas) {
			GrNodoVisual node1 = null;
			GrLineaVisual edge1 = null;
			GrNodoVisual node2 = null;
			GrLineaVisual edge2 = null;

			if (edge.getFin1() instanceof GrNodoVisual) {
				node1 = (GrNodoVisual) edge.getFin1();
			} else if (edge.getFin1() instanceof GrLineaVisual) {
				edge1 = (GrLineaVisual) edge.getFin1();
			}
			if (edge.getFin2() instanceof GrNodoVisual) {
				node2 = (GrNodoVisual) edge.getFin2();
			} else if (edge.getFin2() instanceof GrLineaVisual) {
				edge2 = (GrLineaVisual) edge.getFin2();
			}
			if (!(node1 == null && edge1 == null)
					&& !(node2 == null && edge2 == null)) {
				edge.paint(g, node1, edge1, node2, edge2);
			}
		}
		super.paintChildren(g);
	}

	/**
	 * Gets the diagram.
	 * 
	 * @return the diagram
	 */
	public Diagram getDiagram() {
		return diagrama;
	}

	/**
	 * Gets the diagrama.
	 * 
	 * @return the diagrama
	 */
	public Diagram getDiagrama() {
		return diagrama;
	}

	/**
	 * Actualiza el graficador, basandose en el modelo.
	 */
	private void updateGraficador() {
		setUpdateModel(false);
		updateGraficador(diagrama);
		updateReferencias(diagrama);
		setUpdateModel(true);
	}

	/**
	 * *************************************************************************
	 * ****.
	 * 
	 * @param modelo
	 *            the modelo
	 */

	public void setModelo(final Object modelo) {
		if (modelo instanceof Diagram) {
			diagrama = (Diagram) modelo;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see vista.Vista#updateVista()
	 */
	public void updateVista() {
		reloadGraficador();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see vista.Vista#getTitulo()
	 */
	public String getTitulo() {
		return diagrama.getName();
	}

	/**
	 * Update modelo.
	 */
	public void updateModelo() {
	}

	/**
	 * Adds the listener.
	 * 
	 * @param listener
	 *            the listener
	 */
	public void addListener(final EditorListener listener) {
	}

	/**
	 * Checks if is graphical element.
	 * 
	 * @param tipoNodo
	 *            the tipo nodo
	 * 
	 * @return true, if is graphical element
	 */
	private boolean isGraphicalElement(final String tipoNodo) {
		return tipoNodo.contains("vista.graficador");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see vista.Vista#modelPropertyChange(java.beans.PropertyChangeEvent)
	 */
	@Override
	public void modelPropertyChange(final PropertyChangeEvent evt) {
		log.debug(getTitulo() + " updating from "
				+ evt.getSource().getClass().getName() + " "
				+ evt.getPropertyName());
		if (evt.getPropertyName()
				.equals(PropertyType.MODEL_MODIFIED.toString())) {
			if (!evt.getOldValue().equals(this)) {
				log.debug(getTitulo() + " updating from "
						+ evt.getSource().getClass().getName());
				updateVista();
			}
		}
	}

	/**
	 * Gets the controller.
	 * 
	 * @return the controller
	 */
	public ControllerProyecto getController() {
		return controller;
	}

	/**
	 * Sets the controller.
	 * 
	 * @param controller
	 *            the new controller
	 */
	public void setController(final ControllerProyecto controller) {
		this.controller = controller;
	}

	/**
	 * Checks if is update model.
	 * 
	 * @return true, if is update model
	 */
	public boolean isUpdateModel() {
		return updateModel;
	}

	/**
	 * Sets the update model.
	 * 
	 * @param updateModel
	 *            the new update model
	 */
	public void setUpdateModel(final boolean updateModel) {
		this.updateModel = updateModel;
	}

	public DiagramManagementPackage getDiagramManagement() {
		return diagramManagement;
	}

}
