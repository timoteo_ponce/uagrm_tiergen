/*
 * ElementoVisual.java
 *
 * Created on 5 de noviembre de 2008, 10:42
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package vista.graficador;

/**
 * The Interface ElementoVisual.
 * 
 * @author Juan Timoteo Ponce Ortiz
 */
public interface ElementoVisual {
	
	void edit();
	void remove();
	void removeFromModel();	
}
