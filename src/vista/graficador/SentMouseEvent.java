package vista.graficador;

import java.awt.event.MouseEvent;

/**
 * Class that represents a mouse event that is sent from one graph element to
 * others.
 * 
 * @author
 */
public final class SentMouseEvent extends MouseEvent {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /**
     * Instantiates a new sent mouse event.
     * 
     * @param grNode
     *            the gr node
     * @param evt
     *            the evt
     */
    public SentMouseEvent(GrNodoVisual grNode, MouseEvent evt) {
	super(grNode, evt.getID(), evt.getWhen(), evt.getModifiers(), evt
		.getX(), evt.getY(), evt.getClickCount(), evt.isPopupTrigger());
    }
}
