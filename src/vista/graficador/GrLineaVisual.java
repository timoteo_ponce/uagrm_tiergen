/*
 * GrLineaVisual.java
 *
 * Created on 5 de noviembre de 2008, 10:51
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package vista.graficador;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import org.apache.log4j.Logger;
import org.omg.uml.foundation.core.ModelElement;
import org.uml.diagrammanagement.GraphEdge;
import org.uml.diagrammanagement.GraphElement;
import org.uml.diagrammanagement.GraphNode;
import org.uml.diagrammanagement.Property;
import org.uml.diagrammanagement.Vertex;
import org.uml.diagrammanagement.VertexClass;

import util.Constantes;
import util.uml.ElementoDeModeloUtil;
import util.uml.ElementoVisualUtil;
import util.uml.UMLUtil;
import vista.graficador.comun.EdgeText;

/**
 * The Class GrLineaVisual.
 * 
 * @author Juan Timoteo Ponce Ortiz
 */
public abstract class GrLineaVisual implements ElementoVisual {

	/** The log. */
	private static Logger log = Logger.getLogger(GrLineaVisual.class);

	/** The Constant TOPLEFT. */
	public static final int TOPLEFT = 0;

	/** The Constant BOTTOMLEFT. */
	public static final int BOTTOMLEFT = 1;

	/** The Constant BOTTOMRIGHT. */
	public static final int BOTTOMRIGHT = 2;

	/** The Constant TOPRIGHT. */
	public static final int TOPRIGHT = 3;

	/** The Constant LABEL. */
	protected static final int LABEL = 0;

	/** The Constant CARD. */
	protected static final int CARD = 1;

	/** The Constant NAME. */
	protected static final int NAME = 2;

	/** The Constant STEREOTYPE. */
	protected static final int STEREOTYPE = 3;

	/** The Constant GRLABEL. */
	protected static final int GRLABEL = 4;

	/** The Constant START. */
	protected static final int START = 0;

	/** The Constant END. */
	protected static final int END = 1;

	/** The Constant NORTH. */
	protected static final int NORTH = 0;

	/** The Constant EAST. */
	protected static final int EAST = 1;

	/** The Constant SOUTH. */
	protected static final int SOUTH = 2;

	/** The Constant WEST. */
	protected static final int WEST = 3;

	/** The Constant NONE. */
	protected static final int NONE = 4;

	/**
	 * Size of a point of the edge, and also the tolerance for knowing if the
	 * user has clicked some point.
	 */
	protected int QUAD = 3;

	/** Size of an edge's arrowhead. */
	protected int ARROWSIZE = 13;

	/** The graficador. */
	protected final GrDiagrama graficador;

	/** The modelo. */
	protected final GraphEdge modelo;

	/** The textos. */
	private final List<EdgeText> textos;

	/** The puntos. */
	protected final List<Point> puntos;

	/** The is selected. */
	private boolean isSelected;

	/** The forecolor. */
	private Color forecolor = Color.black;

	/** The selectcolor. */
	private Color selectcolor = Color.red;

	/** The paintcolor. */
	private Color paintcolor = Color.black;

	/** The selectedpointcolor. */
	private Color selectedpointcolor = Color.black;

	/** The menu. */
	JPopupMenu menu;

	/** The mi delete from model. */
	JMenuItem miEdit, miDelete, miDeleteFromModel;

	/** The fin1. */
	private ElementoVisual fin1;

	/** The fin2. */
	private ElementoVisual fin2;

	/** Index of the selected vertex. */
	protected int selectedpointindex = -1;

	/** The deltax. */
	protected int deltax = 0;

	/** The deltay. */
	protected int deltay = 0;

	/** The hiddenlabels. */
	protected boolean hiddenlabels = false;

	/** The tracedline. */
	protected boolean tracedline = false;

	/** The arrow1. */
	protected boolean arrow1 = false;

	/** The fatarrow1. */
	protected boolean fatarrow1 = false;

	/** The filledfatarrow1. */
	protected boolean filledfatarrow1 = false;

	/** The arrow2. */
	protected boolean arrow2 = false;

	/** The fatarrow2. */
	protected boolean fatarrow2 = false;

	/** The filledfatarrow2. */
	protected boolean filledfatarrow2 = false;

	/** The agreg1. */
	protected boolean agreg1 = false;

	/** The agreg2. */
	protected boolean agreg2 = false;

	/** The byvalue1. */
	protected boolean byvalue1 = false;

	/** The byvalue2. */
	protected boolean byvalue2 = false;

	/** The sticky. */
	protected boolean sticky = false;

	/** The stickylabels. */
	protected boolean stickylabels = false;

	/** The isgraphical. */
	protected boolean isgraphical = false;

	/**
	 * This method should be overridden by concrete classes if they add some
	 * visual aspects that are not present in this generic class, and that need
	 * to be updated.
	 */
	protected abstract void updateLineaVisual();

	/**
	 * This method should be overridden by concrete classes if they add some
	 * visual aspects that are not present in this generic class, and that need
	 * to be updated.
	 */
	protected abstract void updateLineaLogica();

	/**
	 * Finalize creacion linea.
	 * 
	 * @throws Exception
	 *             the exception
	 */
	protected abstract void finalizeCreacionLinea() throws Exception;

	/**
	 * Creates a new instance of GrLineaVisual.
	 * 
	 * @param diagrama
	 *            the diagrama
	 * @param modelo
	 *            the modelo
	 */
	public GrLineaVisual(final GrDiagrama diagrama, final GraphEdge modelo) {
		graficador = diagrama;
		this.modelo = modelo;
		textos = new ArrayList<EdgeText>();
		puntos = new ArrayList<Point>();
		init();
		setGraphical(false);
		initMenu();
	}

	/**
	 * Inits the.
	 */
	protected void init() {
		textos.clear();
		puntos.clear();

		for (Object elem : modelo.getVertices()) {
			Vertex vert = (Vertex) elem;
			puntos.add(new Point(vert.getX(), vert.getY()));
		}
	}

	/**
	 * Inicializa el menu de la linea, agregando escuchadores.
	 */
	private void initMenu() {
		menu = new JPopupMenu();

		miEdit = new JMenuItem("Editar");
		miDelete = new JMenuItem("Eliminar de diagrama");
		miDeleteFromModel = new JMenuItem("Eliminar de modelo ");

		menu.add(miEdit);
		menu.add(miDelete);
		menu.addSeparator();
		menu.add(miDeleteFromModel);

		miEdit.addActionListener(new ActionListener() {
			public void actionPerformed(final ActionEvent ae) {
				edit();
			}
		});

		miDelete.addActionListener(new ActionListener() {
			public void actionPerformed(final ActionEvent ae) {
				remove();
			}
		});

		miDeleteFromModel.addActionListener(new ActionListener() {
			public void actionPerformed(final ActionEvent ae) {
				removeFromModel();
			}
		});
	}

	/**
	 * Show menu.
	 * 
	 * @param x
	 *            the x
	 * @param y
	 *            the y
	 */
	public void showMenu(final int x, final int y) {
		menu.show(graficador, x, y);
	}

	/**
	 * Adds the menu item.
	 * 
	 * @param item
	 *            the item
	 */
	protected void addMenuItem(final JMenuItem item) {
		menu.add(item);
	}

	/**
	 * Carga el editor de relacion.
	 */
	@Override
	public void edit() {
		ElementoVisualUtil.editarElemento(modelo, graficador.getController(),
				graficador);
		graficador.reloadGraficador();
	}

	/**
	 * Elimina la relacion del diagrama.
	 */
	@Override
	public void remove() {
		ElementoVisualUtil.removeElemento(modelo);
		// removeLineaFromModelo();
		graficador.reloadGraficador();
	}

	/**
	 * Elimina la relacion del modelo y el diagrama.
	 */
	@Override
	public void removeFromModel() {
		ModelElement me = ElementoVisualUtil.getElementoLogico(modelo);
		if (me != null) {
			ElementoDeModeloUtil.removeElemento(me);
		} else {
			remove();
		}
	}

	/**
	 * Retorna la longitud de la linea.
	 * 
	 * @return longitud
	 */
	protected double getLength() {
		double ret = 0;
		if (!puntos.isEmpty()) {
			Point p1 = puntos.get(0);
			Point p2;
			for (int i = 1; i < puntos.size(); i++) {
				p2 = puntos.get(i);
				ret += p1.distance(p2);
				p1 = p2;
			}
		}
		return ret;
	}

	/**
	 * Retorna la distancia entre esta linea y un punto especifico.
	 * 
	 * @param p
	 *            punto a distancia
	 * 
	 * @return distancia al punto
	 */
	protected double getPctDist(final Point p) {
		double length = getLength();
		Point pmin1 = new Point();
		Point pmin2 = new Point();
		double mindist = getMinPoints(p, pmin1, pmin2);

		if (mindist != -1) {
			double dist = 0;
			if (!puntos.isEmpty()) {
				Point p1 = puntos.get(0);
				Point p2;
				for (int i = 1; i < puntos.size(); i++) {
					p2 = puntos.get(i);
					if (pmin1.equals(p1) && pmin2.equals(p2)) {
						double b = p1.distanceSq(p);
						double h = mindist * mindist;
						dist += Math.sqrt(b - h);
						return dist / length;
					}
					dist += p1.distance(p2);
					p1 = p2;
				}
			}
		}
		return 0.5;
	}

	/**
	 * Retorna el angulo de proyeccion de un punto especifico en el segmento de
	 * la linea, al menos el mas cercano.
	 * 
	 * @param p
	 *            punto
	 * 
	 * @return angulo dibujadi por la proyeccion del punto en el segmento de
	 *         esta linea
	 */
	protected double getAngleByLocation(final Point p) {
		Point pmin1 = new Point();
		Point pmin2 = new Point();
		getMinPoints(p, pmin1, pmin2);

		if (pmin1 != null && pmin2 != null) {
			return getAngle(pmin1, pmin2);
		}
		return 0;
	}

	/**
	 * REtorna la posicion relativa a un punto con relacion a esta linea.
	 * 
	 * @param p
	 *            punto
	 * 
	 * @return posicion relativa - TOPLEFT, BOTTOMLEFT, BOTTOMRIGHT, TOPRIGHT
	 */
	protected int getOrientation(final Point p) {
		Point pmin1 = new Point();
		Point pmin2 = new Point();
		getMinPoints(p, pmin1, pmin2);

		if (pmin1 != null && pmin2 != null) {
			double angle = getAngle(pmin1, pmin2);
			double c1 = Point2D.distanceSq(p.x, p.y, pmin1.x, pmin1.y);
			double c2 = Line2D.ptLineDistSq(pmin1.x, pmin1.y, pmin2.x, pmin2.y,
					p.x, p.y);
			double c = Math.sqrt(c1 - c2);
			int projx = (int) (pmin1.x + c * Math.cos(angle));
			int projy = (int) (pmin1.y + c * Math.sin(angle));

			if (projx <= p.x && projy <= p.y) {
				return TOPLEFT;
			} else if (projx <= p.x && projy >= p.y) {
				return BOTTOMLEFT;
			} else if (projx >= p.x && projy >= p.y) {
				return BOTTOMRIGHT;
			} else if (projx >= p.x && projy <= p.y) {
				return TOPRIGHT;
			}
		}
		return 0;
	}

	/**
	 * Retorna los dos puntos subsecuentes, desde esta linea, que son cercanos a
	 * otro punto.
	 * 
	 * @param p
	 *            punto base
	 * @param pmin1
	 *            punto 1
	 * @param pmin2
	 *            punto 2
	 * 
	 * @return distancia entre el punto base y el segmento formado por los p1 p2
	 */
	protected double getMinPoints(final Point p, final Point pmin1, final Point pmin2) {
		double mindist = -1;
		if (!puntos.isEmpty()) {
			Point p1 = puntos.get(0);
			Point p2;
			for (int i = 1; i < puntos.size(); i++) {
				p2 = puntos.get(i);
				double dist = Line2D
				.ptSegDist(p1.x, p1.y, p2.x, p2.y, p.x, p.y);
				if (mindist == -1 || dist < mindist) {
					mindist = dist;
					pmin1.x = p1.x;
					pmin1.y = p1.y;
					pmin2.x = p2.x;
					pmin2.y = p2.y;
				}
				p1 = p2;
			}
		}
		return mindist;
	}

	/**
	 * Calcula el angulo formado por dos puntos, en relacion a uno horizontal.
	 * 
	 * @param p1
	 *            punto 1
	 * @param p2
	 *            punto 2
	 * 
	 * @return angulo entre puntos
	 */
	protected double getAngle(final Point p1, final Point p2) {
		double a = (p2.y - p1.y);
		double b = (p2.x - p1.x);
		double angle = 0;
		if (b == 0) {
			if (a > 0) {
				angle = Math.PI / 2;
			} else {
				angle = 3 * Math.PI / 2;
			}
		} else if (a == 0) {
			angle = 0;
		} else {
			angle = Math.atan(a / b);
		}
		if (b < 0) {
			angle += Math.PI;
		}
		return angle;
	}

	/**
	 * Determina si la linea contiene algun punto, siendo que contener es que
	 * sea parte del segmento.
	 * 
	 * @param p
	 *            punto
	 * 
	 * @return si el punto esta contenido
	 */
	protected boolean containsPoint(final Point p) {
		if (!puntos.isEmpty()) {
			Point p1 = null;
			Point p2;
			for (Point nextPoint : puntos) {
				if (p1 == null) {
					p1 = nextPoint;
				} else {
					p2 = nextPoint;
					if (contains(p, p1, p2)) {
						return true;
					}
					p1 = p2;
				}
			}
		}
		return false;
	}

	/**
	 * Determina si un punto es contenido por un segmento.
	 * 
	 * @param p
	 *            el punto
	 * @param p1
	 *            punto 1 del segmento
	 * @param p2
	 *            punto 2 del segmento
	 * 
	 * @return si el punto esta contenido en el segmento
	 */
	protected boolean contains(final Point p, final Point p1, final Point p2) {
		Line2D line = new Line2D.Double(p1, p2);
		Rectangle r = new Rectangle(p.x - QUAD * 3, p.y - QUAD * 3,
				QUAD * 2 * 3, QUAD * 2 * 3);
		if (line.intersects(r)) {
			return true;
		}
		return false;
	}

	/**
	 * Determina si el punto dato esta presente como vertice de linea, si es
	 * asi, la linea se marca seleccionada.
	 * 
	 * @param p
	 *            punto dado
	 * 
	 * @return si la linea contiene el punto como vertice
	 */
	protected boolean clickedPoint(final Point p) {
		if (!puntos.isEmpty()) {
			selectedpointindex = -1;
			Point paux;
			for (int i = 0; i < puntos.size(); i++) {
				paux = puntos.get(i);
				Rectangle r = new Rectangle(p.x - QUAD, p.y - QUAD, QUAD * 2,
						QUAD * 2);
				if (r.contains(paux)) {
					selectedpointindex = i;
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Determina si el punto dado es parte de un segmento de la linea, si es
	 * cireto, un nuevo vertice es insertado en el segmento, seleccionandolo.
	 * 
	 * @param p
	 *            punto dado
	 * 
	 * @return true, if clicked segment
	 */
	protected boolean clickedSegment(final Point p) {
		selectedpointindex = -1;
		if (!puntos.isEmpty()) {
			Point p1 = puntos.get(0);
			Point p2;
			for (int i = 1; i < puntos.size(); i++) {
				p2 = puntos.get(i);
				if (contains(p, p1, p2)) {
					puntos.add(i, new Point(p.x, p.y));
					selectedpointindex = i;
					return true;
				}
				p1 = p2;
			}
		}
		return false;
	}

	/**
	 * Mueve el punto seleccionado a una nueva posicion.
	 * 
	 * @param p
	 *            nueva posicion
	 */
	protected void moveSelectedPoint(final Point p) {
		if ((selectedpointindex != -1)
				&& (p.x > 0 && p.x < GrDiagrama.ANCHO_MAX && p.y > 0 && p.y < GrDiagrama.ALTO_MAX)) {
			puntos.set(selectedpointindex, new Point(p.x, p.y));
		}
	}

	/**
	 * Elimina un vertice.
	 * 
	 * @param index
	 *            vertice
	 * @param nodo
	 *            nodo
	 * 
	 * @return estado
	 */
	protected boolean erasePoint(final int index, final GrNodoVisual nodo) {
		Point paux = puntos.get(index);
		if (nodo.getBounds().contains(paux.x, paux.y)) {
			puntos.remove(index);
			if (index == selectedpointindex) {
				selectedpointindex = -1;
			}
			if (index <= selectedpointindex) {
				selectedpointindex--;
			}
			return true;
		}
		return false;
	}

	/**
	 * Si una linea tiene tres puntos alineados, se mimetizan.
	 * 
	 * @param index
	 *            vertice a verificar
	 * 
	 * @return estado
	 */
	protected boolean alignPoint(final int index) {
		try {
			Point paux = puntos.get(index);
			Point pbefore = puntos.get(index - 1);
			Point pafter = puntos.get(index + 1);

			if (contains(paux, pbefore, pafter)) {
				puntos.remove(index);
				if (index == selectedpointindex) {
					selectedpointindex = -1;
				}
				if (index <= selectedpointindex) {
					selectedpointindex--;
				}
				return true;
			}
		} catch (ArrayIndexOutOfBoundsException e) {
			log.error("Can't align points: " + e);
		}
		return false;
	}

	/**
	 * Alinea los vertices entre dos nodos.
	 * 
	 * @param nodo1
	 *            nodo 1
	 * @param nodo2
	 *            nodo 2
	 */
	protected void alignPoints(final GrNodoVisual nodo1, final GrNodoVisual nodo2) {
		if (puntos.size() > 2) {
			if (nodo1 != null) {
				if (fin1 == nodo1) {
					erasePoint(1, nodo1);
				}
				if (fin2 == nodo1) {
					erasePoint(puntos.size() - 2, nodo1);
				}
			}
			if (nodo2 != null) {
				if (fin1 == nodo2) {
					erasePoint(1, nodo2);
				}
				if (fin2 == nodo2) {
					erasePoint(puntos.size() - 2, nodo2);
				}
			}
			eraseAlignedPoints();
		}
	}

	/**
	 * Alinea todos los vertices alrededor de un nodo.
	 * 
	 * @param nodo
	 *            nodo
	 */
	protected void alignPoints(final GrNodoVisual nodo) {
		if (puntos.size() > 2) {
			if (nodo != null) {
				if (fin1 == nodo) {
					erasePoint(1, nodo);
				}
				if (fin2 == nodo) {
					erasePoint(puntos.size() - 2, nodo);
				}
			}
			eraseAlignedPoints();
		}
	}

	/**
	 * Revisa todos los vertices, alineandolos, eliminando innecesarios.
	 */
	protected void eraseAlignedPoints() {
		for (int i = 1; i < puntos.size() - 1; i++) {
			if (alignPoint(i)) {
				i--;
			}
		}
	}

	/**
	 * Corrige la posicion de un fin de vertice.
	 * 
	 * @param elemento
	 *            elemento a corregir en posicion
	 */
	protected void correctPoint(final ElementoVisual elemento) {
		if (elemento instanceof GrNodoVisual) {
			GrNodoVisual nodo = (GrNodoVisual) elemento;
			if (fin1 == nodo) {
				puntos.set(0, nodo.getCenter());
			}
			if (fin2 == nodo) {
				puntos.set(puntos.size() - 1, nodo.getCenter());
			}
		} else if (elemento instanceof GrLineaVisual) {
			GrLineaVisual linea = (GrLineaVisual) elemento;
			if (fin1 == linea) {
				puntos.set(0, linea.getMiddlePoint());
			}
			if (fin2 == linea) {
				puntos.set(puntos.size() - 1, linea.getMiddlePoint());
			}
		}
	}

	/**
	 * Mueve uno de los vertices del centro al final del nodo.
	 * 
	 * @param nodo
	 *            nodo
	 */
	protected void movePoints(final GrNodoVisual nodo) {
		if (deltax == 0 && deltay == 0) {
			if (fin1 == nodo) {
				Point pi = puntos.get(0);
				deltax = nodo.getCenter().x - pi.x;
				deltay = nodo.getCenter().y - pi.y;
			}
			if (fin2 == nodo) {
				Point pi = puntos.get(puntos.size() - 1);
				deltax = nodo.getCenter().x - pi.x;
				deltay = nodo.getCenter().y - pi.y;
			}
		}
	}

	/**
	 * Elimina todos los vertices.
	 */
	protected void clearPoints() {
		puntos.clear();
	}

	/**
	 * Dibuja esta linea, este se usa cuando la linea no esta completa, y los
	 * nodos origen destino tampocoDraws this edge.
	 * 
	 * @param g
	 *            the g
	 */
	public void paint(final Graphics g) {
		paint(g, puntos.get(0), puntos.get(puntos.size() - 1));
	}

	/**
	 * Dibuja esta linea, se usa cuando la relacion esta completa.
	 * 
	 * @param g
	 *            the g
	 * @param nodo1
	 *            the nodo1
	 * @param edge1
	 *            the edge1
	 * @param nodo2
	 *            the nodo2
	 * @param edge2
	 *            the edge2
	 */
	protected void paint(final Graphics g, final GrNodoVisual nodo1, final GrLineaVisual edge1,
			final GrNodoVisual nodo2, final GrLineaVisual edge2) {
		Point point1 = new Point();
		Point point2 = new Point();
		ElementoVisual element1 = null;
		ElementoVisual element2 = null;

		if (nodo1 != null) {
			element1 = nodo1;
			point1 = nodo1.getCenter();
		} else if (edge1 != null) {
			element1 = edge1;
			point1 = edge1.getMiddlePoint();
		}
		if (nodo2 != null) {
			element2 = nodo2;
			point2 = nodo2.getCenter();
		} else if (edge2 != null) {
			element2 = edge2;
			point2 = edge2.getMiddlePoint();
		}
		Point borderpoint = new Point();
		Point arrowpoint1 = new Point();
		Point arrowpoint2 = new Point();
		Point agregpoint1 = new Point();
		Point agregpoint2 = new Point();
		Point agregpoint3 = new Point();
		Point agregpoint4 = new Point();
		g.setColor(getPaintColor());
		if (calcPoints(START, element1, borderpoint, arrowpoint1, arrowpoint2,
				agregpoint1, agregpoint2, agregpoint3, agregpoint4) != -1) {
			if (fatarrow1 && !hiddenlabels) {
				Polygon p = new Polygon();
				p.addPoint(borderpoint.x, borderpoint.y);
				p.addPoint(arrowpoint1.x, arrowpoint1.y);
				p.addPoint(arrowpoint2.x, arrowpoint2.y);
				point1 = new Point((arrowpoint1.x + arrowpoint2.x + 1) / 2,
						(arrowpoint1.y + arrowpoint2.y + 1) / 2);
				g.drawPolygon(p);
			} else if (filledfatarrow1 && !hiddenlabels) {
				Polygon p = new Polygon();
				p.addPoint(borderpoint.x, borderpoint.y);
				p.addPoint(arrowpoint1.x, arrowpoint1.y);
				p.addPoint(arrowpoint2.x, arrowpoint2.y);
				point1 = new Point((arrowpoint1.x + arrowpoint2.x + 1) / 2,
						(arrowpoint1.y + arrowpoint2.y + 1) / 2);
				g.fillPolygon(p);
			} else if (arrow1 && !hiddenlabels) {
				g.drawLine(borderpoint.x, borderpoint.y, arrowpoint1.x,
						arrowpoint1.y);
				g.drawLine(borderpoint.x, borderpoint.y, arrowpoint2.x,
						arrowpoint2.y);
				point1 = new Point(borderpoint);
			}
			if (agreg1 && !hiddenlabels) {
				Polygon p = new Polygon();
				p.addPoint(agregpoint1.x, agregpoint1.y);
				p.addPoint(agregpoint2.x, agregpoint2.y);
				p.addPoint(agregpoint3.x, agregpoint3.y);
				p.addPoint(agregpoint4.x, agregpoint4.y);
				point1 = new Point(agregpoint3);
				if (byvalue1) {
					g.fillPolygon(p);
				} else {
					g.drawPolygon(p);
				}
			}
		}
		if (calcPoints(END, element2, borderpoint, arrowpoint1, arrowpoint2,
				agregpoint1, agregpoint2, agregpoint3, agregpoint4) != -1) {
			if (fatarrow2 && !hiddenlabels) {
				Polygon p = new Polygon();
				p.addPoint(borderpoint.x, borderpoint.y);
				p.addPoint(arrowpoint1.x, arrowpoint1.y);
				p.addPoint(arrowpoint2.x, arrowpoint2.y);
				point2 = new Point((arrowpoint1.x + arrowpoint2.x + 1) / 2,
						(arrowpoint1.y + arrowpoint2.y + 1) / 2);
				g.drawPolygon(p);
			} else if (filledfatarrow2 && !hiddenlabels) {
				Polygon p = new Polygon();
				p.addPoint(borderpoint.x, borderpoint.y);
				p.addPoint(arrowpoint1.x, arrowpoint1.y);
				p.addPoint(arrowpoint2.x, arrowpoint2.y);
				point2 = new Point((arrowpoint1.x + arrowpoint2.x + 1) / 2,
						(arrowpoint1.y + arrowpoint2.y + 1) / 2);
				g.fillPolygon(p);
			} else if (arrow2 && !hiddenlabels) {
				g.drawLine(borderpoint.x, borderpoint.y, arrowpoint1.x,
						arrowpoint1.y);
				g.drawLine(borderpoint.x, borderpoint.y, arrowpoint2.x,
						arrowpoint2.y);
				point2 = new Point(borderpoint);
			}
			if (agreg2 && !hiddenlabels) {
				Polygon p = new Polygon();
				p.addPoint(agregpoint1.x, agregpoint1.y);
				p.addPoint(agregpoint2.x, agregpoint2.y);
				p.addPoint(agregpoint3.x, agregpoint3.y);
				p.addPoint(agregpoint4.x, agregpoint4.y);
				point2 = new Point(agregpoint3);
				if (byvalue2) {
					g.fillPolygon(p);
				} else {
					g.drawPolygon(p);
				}
			}
		}
		paint(g, point1, point2);
	}

	/**
	 * Dibuja una linea entre los dos puntos, con todos sus vertices.
	 * 
	 * @param g
	 *            the g
	 * @param ptype1
	 *            the ptype1
	 * @param ptype2
	 *            the ptype2
	 */
	protected void paint(final Graphics g, final Point ptype1, final Point ptype2) {
		if (!puntos.isEmpty()) {
			g.setColor(getPaintColor());
			int[] pointsx = new int[puntos.size()];
			int[] pointsy = new int[puntos.size()];
			int pos = 0;
			Point p;
			for (int i = 0; i < puntos.size(); i++) {
				p = puntos.get(i);
				p.x += deltax;
				p.y += deltay;
				if (isSelected()) {
					if (selectedpointindex == i) {
						g.setColor(getSelectedPointColor());
						g.fillRect(p.x - QUAD, p.y - QUAD, QUAD * 2, QUAD * 2);
					} else {
						g.setColor(getPaintColor());
						g.fillOval(p.x - QUAD, p.y - QUAD, QUAD * 2, QUAD * 2);
					}
				}
				if (pos == 0) {
					pointsx[pos] = ptype1.x;
					pointsy[pos] = ptype1.y;
				} else if (pos == puntos.size() - 1) {
					pointsx[pos] = ptype2.x;
					pointsy[pos] = ptype2.y;
				} else {
					pointsx[pos] = p.x;
					pointsy[pos] = p.y;
				}
				pos++;
			}
			deltax = 0;
			deltay = 0;
			g.setColor(getPaintColor());
			if (!tracedline) {
				g.drawPolyline(pointsx, pointsy, pos);
			} else {
				Point p1 = new Point(pointsx[0], pointsy[0]);
				double interval = 7.0;
				Point p2;
				for (int i = 1; i < puntos.size(); i++) {
					p2 = new Point(pointsx[i], pointsy[i]);

					double alfa = ((p2.getY() - p1.getY()) / (p2.getX() - p1
							.getX()));
					double daux1 = 0.0;
					double daux2 = interval / 2 - 0.5;
					double dlimit = Math.sqrt(Math
							.pow(p1.getX() - p2.getX(), 2)
							+ Math.pow(p1.getY() - p2.getY(), 2));
					double xaux1 = 0, yaux1 = 0, xaux2 = 0, yaux2 = 0;
					daux1 -= interval;
					daux2 -= interval;
					while (daux2 < dlimit) {
						daux1 += interval;
						daux2 += interval;
						if ((p1.x != (int) p2.getX())
								&& (p1.y != (int) p2.getY())) {
							if (p1.y < (int) p2.getY()) {
								yaux1 = (+Math.sqrt((Math.pow(alfa * daux1, 2))
										/ (Math.pow(alfa, 2) + (1))) + p1
										.getY());
								yaux2 = (+Math.sqrt((Math.pow(alfa * daux2, 2))
										/ (Math.pow(alfa, 2) + (1))) + p1
										.getY());
							} else {
								yaux1 = (-Math.sqrt((Math.pow(alfa * daux1, 2))
										/ (Math.pow(alfa, 2) + (1))) + p1
										.getY());
								yaux2 = (-Math.sqrt((Math.pow(alfa * daux2, 2))
										/ (Math.pow(alfa, 2) + (1))) + p1
										.getY());
							}
							xaux1 = ((yaux1 - p1.getY()) / alfa) + p1.getX();
							xaux2 = ((yaux2 - p1.getY()) / alfa) + p1.getX();
						} else if (p1.x == (int) p2.getX()) {
							xaux1 = p1.getX();
							xaux2 = p1.getX();
							if (p1.y < (int) p2.getY()) {
								yaux1 = p1.getY() + daux1;
								yaux2 = p1.getY() + daux2;
							} else {
								yaux1 = p1.getY() - daux1;
								yaux2 = p1.getY() - daux2;
							}
						} else {
							yaux1 = p1.getY();
							yaux2 = p1.getY();
							if (p1.x < p2.getX()) {
								xaux1 = p1.getX() + daux1;
								xaux2 = p1.getX() + daux2;
							} else {
								xaux1 = p1.getX() - daux1;
								xaux2 = p1.getX() - daux2;
							}
						}

						if (daux2 < dlimit) {
							g.drawLine((int) xaux1, (int) yaux1, (int) xaux2,
									(int) yaux2);
						} else if (daux1 < dlimit) {
							g.drawLine((int) xaux1, (int) yaux1, (int) p2
									.getX(), (int) p2.getY());
						}
					}

					p1 = new Point(p2);
				} // Fin for
			}
		}
	}

	/**
	 * Verifica si deberia ser una agregacion(el simbolo).
	 * 
	 * @param side
	 *            the side
	 * 
	 * @return true, if agreg
	 */
	protected boolean agreg(final int side) {
		if (side == START) {
			return agreg1;
		} else if (side == END) {
			return agreg2;
		}
		return false;
	}

	/**
	 * Calcula la posicion de los puntos, de acuerdo a la existencia de
	 * agregaciones y flechas.
	 * 
	 * @param side
	 *            the side
	 * @param edgeEnd
	 *            the edge end
	 * @param borderpoint
	 *            the borderpoint
	 * @param arrowpoint1
	 *            the arrowpoint1
	 * @param arrowpoint2
	 *            the arrowpoint2
	 * @param agregpoint1
	 *            the agregpoint1
	 * @param agregpoint2
	 *            the agregpoint2
	 * @param agregpoint3
	 *            the agregpoint3
	 * @param agregpoint4
	 *            the agregpoint4
	 * 
	 * @return the int
	 */
	protected int calcPoints(final int side, final ElementoVisual edgeEnd,
			final Point borderpoint, final Point arrowpoint1, final Point arrowpoint2,
			final Point agregpoint1, final Point agregpoint2, final Point agregpoint3,
			final Point agregpoint4) {
		if (edgeEnd != null) {
			GrNodoVisual node = null;
			GrLineaVisual edge = null;

			if (edgeEnd instanceof GrNodoVisual) {
				node = (GrNodoVisual) edgeEnd;
			} else if (edgeEnd instanceof GrLineaVisual) {
				edge = (GrLineaVisual) edgeEnd;
			}

			Point p0 = null;
			Point p1 = null;
			if ((side == START) && (fin1 == edgeEnd)) {
				p1 = puntos.get(0);
				p0 = puntos.get(1);
			} else if ((side == END) && (fin2 == edgeEnd)) {
				p1 = puntos.get(puntos.size() - 1);
				p0 = puntos.get(puntos.size() - 2);
			} else {
				return -1;
			}

			if (p0.equals(p1)) {
				return NONE;
			}

			Line2D line = new Line2D.Double(p0, p1);

			double linex1 = line.getX1();
			double liney1 = line.getY1();
			double linex2 = line.getX2();
			double liney2 = line.getY2();

			double h1 = liney2 - liney1;
			double w1 = linex2 - linex1;

			double grtx1 = 0;
			double grty1 = 0;
			double grtx2 = 0;
			double grty2 = 0;

			int grtwidth = 1;
			int grtheight = 1;

			if (node != null) {
				grtx1 = node.getLocation().x;
				grty1 = node.getLocation().y;
				grtx2 = grtx1 + node.getWidth();
				grty2 = grty1 + node.getHeight();
				grtwidth = node.getWidth();
				grtheight = node.getHeight();
			} else if (edge != null) {
				grtx1 = edge.getMiddlePoint().x;
				grty1 = edge.getMiddlePoint().y;
				grtx2 = grtx1;
				grty2 = grty1;
			}

			if (line.intersectsLine(new Line2D.Double(grtx1, grty1, grtx2,
					grty1))) {
				double h0 = liney2 - grty1;
				double w0 = (h0 * w1) / h1;
				double pontoy = grty1;
				double pontox = grtx1 + (grtwidth / 2) - w0;
				borderpoint.setLocation((int) pontox, (int) pontoy);
				double w = borderpoint.x - linex1;
				double h = borderpoint.y - liney1;
				if ((w == 0) && (h == 0)) {
					arrowpoint1.setLocation(borderpoint.x, borderpoint.y);
					arrowpoint2.setLocation(borderpoint.x, borderpoint.y);
					agregpoint1.setLocation(borderpoint.x, borderpoint.y);
					agregpoint2.setLocation(borderpoint.x, borderpoint.y);
					agregpoint3.setLocation(borderpoint.x, borderpoint.y);
					agregpoint4.setLocation(borderpoint.x, borderpoint.y);
					return NORTH;
				}
				if (agreg(side)) {
					agregpoint1.setLocation(borderpoint.x, borderpoint.y);
					double alfa = Math.atan(w / h);

					double alfa1 = alfa + Math.PI / 6;
					double bx = borderpoint.x - ARROWSIZE * Math.sin(alfa1);
					double by = borderpoint.y - ARROWSIZE * Math.cos(alfa1);
					agregpoint2.setLocation((int) bx, (int) by);

					bx = borderpoint.x - (double) 2 * ARROWSIZE
					* Math.sin(alfa);
					by = borderpoint.y - (double) 2 * ARROWSIZE
					* Math.cos(alfa);
					agregpoint3.setLocation((int) bx, (int) by);

					alfa1 = alfa - Math.PI / 6;
					bx = borderpoint.x - ARROWSIZE * Math.sin(alfa1);
					by = borderpoint.y - ARROWSIZE * Math.cos(alfa1);
					agregpoint4.setLocation((int) bx, (int) by);

					borderpoint.setLocation(agregpoint3.x, agregpoint3.y);
				}

				w = borderpoint.x - linex1;
				h = borderpoint.y - liney1;
				if ((w == 0) && (h == 0)) {
					arrowpoint1.setLocation(borderpoint.x, borderpoint.y);
					arrowpoint2.setLocation(borderpoint.x, borderpoint.y);
					return NORTH;
				}

				double alfa = Math.atan(w / h);
				double alfa1 = alfa + Math.PI / 6;
				double bx = borderpoint.x - ARROWSIZE * Math.sin(alfa1);
				double by = borderpoint.y - ARROWSIZE * Math.cos(alfa1);
				arrowpoint1.setLocation((int) bx, (int) by);

				alfa1 = alfa - Math.PI / 6;
				bx = borderpoint.x - ARROWSIZE * Math.sin(alfa1);
				by = borderpoint.y - ARROWSIZE * Math.cos(alfa1);
				arrowpoint2.setLocation((int) bx, (int) by);

				return NORTH;
			} else if (line.intersectsLine(new Line2D.Double(grtx2, grty2,
					grtx1, grty2))) { // borda de baixo
				double h0 = liney2 - grty2;
				double w0 = (h0 * w1) / h1;
				double pontoy = grty2;
				double pontox = grtx1 + (grtwidth / 2) - w0;
				borderpoint.setLocation((int) pontox, (int) pontoy);

				double w = borderpoint.x - linex1;
				double h = borderpoint.y - liney1;
				if ((w == 0) && (h == 0)) {
					arrowpoint1.setLocation(borderpoint.x, borderpoint.y);
					arrowpoint2.setLocation(borderpoint.x, borderpoint.y);
					agregpoint1.setLocation(borderpoint.x, borderpoint.y);
					agregpoint2.setLocation(borderpoint.x, borderpoint.y);
					agregpoint3.setLocation(borderpoint.x, borderpoint.y);
					agregpoint4.setLocation(borderpoint.x, borderpoint.y);
					return SOUTH;
				}
				if (agreg(side)) {
					agregpoint1.setLocation(borderpoint.x, borderpoint.y);
					double alfa = Math.atan(w / h);

					double alfa1 = alfa + Math.PI / 6;
					double bx = borderpoint.x + ARROWSIZE * Math.sin(alfa1);
					double by = borderpoint.y + ARROWSIZE * Math.cos(alfa1);
					agregpoint2.setLocation((int) bx, (int) by);

					bx = borderpoint.x + (double) 2 * ARROWSIZE
					* Math.sin(alfa);
					by = borderpoint.y + (double) 2 * ARROWSIZE
					* Math.cos(alfa);
					agregpoint3.setLocation((int) bx, (int) by);

					alfa1 = alfa - Math.PI / 6;
					bx = borderpoint.x + ARROWSIZE * Math.sin(alfa1);
					by = borderpoint.y + ARROWSIZE * Math.cos(alfa1);
					agregpoint4.setLocation((int) bx, (int) by);

					borderpoint.setLocation(agregpoint3.x, agregpoint3.y);
				}

				w = borderpoint.x - linex1;
				h = borderpoint.y - liney1;
				if ((w == 0) && (h == 0)) {
					arrowpoint1.setLocation(borderpoint.x, borderpoint.y);
					arrowpoint2.setLocation(borderpoint.x, borderpoint.y);
					return SOUTH;
				}

				double alfa = Math.atan(w / h);
				double alfa1 = alfa + Math.PI / 6;
				double bx = borderpoint.x + ARROWSIZE * Math.sin(alfa1);
				double by = borderpoint.y + ARROWSIZE * Math.cos(alfa1);
				arrowpoint1.setLocation((int) bx, (int) by);

				alfa1 = alfa - Math.PI / 6;
				bx = borderpoint.x + ARROWSIZE * Math.sin(alfa1);
				by = borderpoint.y + ARROWSIZE * Math.cos(alfa1);
				arrowpoint2.setLocation((int) bx, (int) by);

				return SOUTH;
			} else if (line.intersectsLine(new Line2D.Double(grtx2, grty1,
					grtx2, grty2))) { // borda da direita
				double w0 = linex2 - grtx2;
				double h0 = (w0 * h1) / w1;
				double pontox = grtx2;
				double pontoy = grty1 + (grtheight / 2) - h0;
				borderpoint.setLocation((int) pontox, (int) pontoy);

				double w = borderpoint.x - linex1;
				double h = borderpoint.y - liney1;
				if ((w == 0) && (h == 0)) {
					arrowpoint1.setLocation(borderpoint.x, borderpoint.y);
					arrowpoint2.setLocation(borderpoint.x, borderpoint.y);
					agregpoint1.setLocation(borderpoint.x, borderpoint.y);
					agregpoint2.setLocation(borderpoint.x, borderpoint.y);
					agregpoint3.setLocation(borderpoint.x, borderpoint.y);
					agregpoint4.setLocation(borderpoint.x, borderpoint.y);
					return EAST;
				}
				if (agreg(side)) {
					agregpoint1.setLocation(borderpoint.x, borderpoint.y);
					double alfa = Math.atan(h / w);

					double alfa1 = alfa + Math.PI / 6;
					double bx = borderpoint.x + ARROWSIZE * Math.cos(alfa1);
					double by = borderpoint.y + ARROWSIZE * Math.sin(alfa1);
					agregpoint2.setLocation((int) bx, (int) by);

					bx = borderpoint.x + (double) 2 * ARROWSIZE
					* Math.cos(alfa);
					by = borderpoint.y + (double) 2 * ARROWSIZE
					* Math.sin(alfa);
					agregpoint3.setLocation((int) bx, (int) by);

					alfa1 = alfa - Math.PI / 6;
					bx = borderpoint.x + ARROWSIZE * Math.cos(alfa1);
					by = borderpoint.y + ARROWSIZE * Math.sin(alfa1);
					agregpoint4.setLocation((int) bx, (int) by);

					borderpoint.setLocation(agregpoint3.x, agregpoint3.y);
				}

				w = borderpoint.x - linex1;
				h = borderpoint.y - liney1;
				if ((w == 0) && (h == 0)) {
					arrowpoint1.setLocation(borderpoint.x, borderpoint.y);
					arrowpoint2.setLocation(borderpoint.x, borderpoint.y);
					return EAST;
				}

				double alfa = Math.atan(h / w);
				double alfa1 = alfa + Math.PI / 6;
				double bx = borderpoint.x + ARROWSIZE
				* Math.cos(alfa1);
				double by = borderpoint.y + ARROWSIZE
				* Math.sin(alfa1);
				arrowpoint1.setLocation((int) bx, (int) by);

				alfa1 = alfa - Math.PI / 6;
				bx = borderpoint.x + ARROWSIZE
				* Math.cos(alfa1);
				by = borderpoint.y + ARROWSIZE
				* Math.sin(alfa1);
				arrowpoint2.setLocation((int) bx, (int) by);

				return EAST;
			} else if (line.intersectsLine(new Line2D.Double(grtx1, grty2,
					grtx1, grty1))) { // borda da esquerda
				double w0 = linex2 - grtx1;
				double h0 = (w0 * h1) / w1;
				double pontox = grtx1;
				double pontoy = grty1 + (grtheight / 2) - h0;
				borderpoint.setLocation((int) pontox, (int) pontoy);

				double w = borderpoint.x - linex1;
				double h = borderpoint.y - liney1;
				if ((w == 0) && (h == 0)) {
					arrowpoint1.setLocation(borderpoint.x, borderpoint.y);
					arrowpoint2.setLocation(borderpoint.x, borderpoint.y);
					agregpoint1.setLocation(borderpoint.x, borderpoint.y);
					agregpoint2.setLocation(borderpoint.x, borderpoint.y);
					agregpoint3.setLocation(borderpoint.x, borderpoint.y);
					agregpoint4.setLocation(borderpoint.x, borderpoint.y);
					return WEST;
				}
				if (agreg(side)) {
					agregpoint1.setLocation(borderpoint.x, borderpoint.y);
					double alfa = Math.atan(h / w);

					double alfa1 = alfa + Math.PI / 6;
					double bx = borderpoint.x - ARROWSIZE
					* Math.cos(alfa1);
					double by = borderpoint.y - ARROWSIZE
					* Math.sin(alfa1);
					agregpoint2.setLocation((int) bx, (int) by);

					bx = borderpoint.x - (double) 2 * ARROWSIZE
					* Math.cos(alfa);
					by = borderpoint.y - (double) 2 * ARROWSIZE
					* Math.sin(alfa);
					agregpoint3.setLocation((int) bx, (int) by);

					alfa1 = alfa - Math.PI / 6;
					bx = borderpoint.x - ARROWSIZE
					* Math.cos(alfa1);
					by = borderpoint.y - ARROWSIZE
					* Math.sin(alfa1);
					agregpoint4.setLocation((int) bx, (int) by);

					borderpoint.setLocation(agregpoint3.x, agregpoint3.y);
				}

				w = borderpoint.x - linex1;
				h = borderpoint.y - liney1;
				if ((w == 0) && (h == 0)) {
					arrowpoint1.setLocation(borderpoint.x, borderpoint.y);
					arrowpoint2.setLocation(borderpoint.x, borderpoint.y);
					return WEST;
				}

				double alfa = Math.atan(h / w);
				double alfa1 = alfa + Math.PI / 6;
				double bx = borderpoint.x - ARROWSIZE
				* Math.cos(alfa1);
				double by = borderpoint.y - ARROWSIZE
				* Math.sin(alfa1);
				arrowpoint1.setLocation((int) bx, (int) by);

				alfa1 = alfa - Math.PI / 6;
				bx = borderpoint.x - ARROWSIZE
				* Math.cos(alfa1);
				by = borderpoint.y - ARROWSIZE
				* Math.sin(alfa1);
				arrowpoint2.setLocation((int) bx, (int) by);

				return WEST;
			} else {
				return NONE;
			}
		}
		return -1;
	}

	/**
	 * Retorna un punto localizado a una distancia definida de la linea, en el
	 * lado provisto.
	 * 
	 * @param ret
	 *            punto a obtener
	 * @param distance
	 *            distancia
	 * @param side
	 *            lado de la linea
	 * 
	 * @return the orientation of the point, in relation to the edge.
	 */
	protected int getPointByDist(final Point ret, final double distance, final int side) {
		int orientation = -1;
		if (puntos != null) {
			Point p1 = new Point();
			Point p2 = null;
			if (side == START) {
				orientation = calcPoints(START, fin1, p1, new Point(),
						new Point(), new Point(), new Point(), new Point(),
						new Point());
				p2 = puntos.get(1);
			} else {
				orientation = calcPoints(END, fin2, p1, new Point(),
						new Point(), new Point(), new Point(), new Point(),
						new Point());
				p2 = puntos.get(puntos.size() - 2);
			}
			if (orientation != -1) {
				double x1 = p1.x;
				double y1 = p1.y;
				double x2 = p2.x;
				double y2 = p2.y;
				double a = Math.sqrt(Math.pow(x2 - x1, 2)
						+ Math.pow(y2 - y1, 2));
				double dx = ((x2 - x1) * distance) / a;
				double dy = ((y2 - y1) * distance) / a;
				ret.setLocation((int) (x1 + dx), (int) (y1 + dy));
			}
		}
		return orientation;
	}

	/**
	 * REtorna el punto medio de la linea.
	 * 
	 * @return the middle point
	 */
	protected Point getMiddlePoint() {
		Point ret = null;
		if (puntos != null) {
			Point pborder1 = new Point();
			Point pborder2 = new Point();
			int ori1 = calcPoints(START, fin1, pborder1, new Point(),
					new Point(), new Point(), new Point(), new Point(),
					new Point());
			if (ori1 == NONE) {
				pborder1 = puntos.get(0);
			}
			int ori2 = calcPoints(END, fin2, pborder2, new Point(),
					new Point(), new Point(), new Point(), new Point(),
					new Point());
			if (ori2 == NONE) {
				pborder2 = puntos.get(puntos.size() - 1);
			}
			if (pborder1.equals(pborder2)) {
				return pborder1;
			}
			if ((ori1 != -1) && (ori2 != -1)) {
				Point lastp = pborder1;
				double distance = 0;
				Point p;
				for (int i = 1; i < puntos.size(); i++) {
					p = null;
					if (i != puntos.size() - 1) {
						p = puntos.get(i);
					} else {
						p = pborder2;
					}
					double x1 = lastp.x;
					double y1 = lastp.y;
					double x2 = p.x;
					double y2 = p.y;
					distance += Math.sqrt(Math.pow(x2 - x1, 2)
							+ Math.pow(y2 - y1, 2));
					lastp = p;
				}
				double middledistance = distance / 2;
				double newdistance = 0;
				lastp = pborder1;
				for (int i = 1; i < puntos.size(); i++) {
					p = null;
					if (i != puntos.size() - 1) {
						p = puntos.get(i);
					} else {
						p = pborder2;
					}
					double x1 = lastp.x;
					double y1 = lastp.y;
					double x2 = p.x;
					double y2 = p.y;
					double b = Math.sqrt(Math.pow(x2 - x1, 2)
							+ Math.pow(y2 - y1, 2));
					newdistance += b;
					if (newdistance > middledistance) {
						double a = newdistance - middledistance;
						double dx = ((x2 - x1) * a) / b;
						double dy = ((y2 - y1) * a) / b;
						ret = new Point((int) (x2 - dx), (int) (y2 - dy));
						i = puntos.size();
					}
					lastp = p;
				}
			}
		}
		return ret;
	}

	/**
	 * Show labels.
	 */
	public void showLabels() {
		hiddenlabels = false;
		for (EdgeText et : textos) {
			setLocation(et);
			et.setVisible(true);
		}
	}

	/**
	 * Hide labels.
	 */
	public void hideLabels() {
		if (!hiddenlabels) {
			hiddenlabels = true;
			for (EdgeText et : textos) {
				et.setVisible(false);
			}
		}
	}

	/**
	 * Utilizado para corregir asociaciones.
	 */
	public void correctLinkAttributePoint() {
		GrLineaVisual edge;
		for (Object nextEdge : graficador.getEdgesByEdgeEnd(this)) {
			edge = (GrLineaVisual) nextEdge;
			if (edge.fin1 == this) {
				edge.correctLinkAttributePoint(START, getMiddlePoint());
			} else if (edge.fin2 == this) {
				edge.correctLinkAttributePoint(END, getMiddlePoint());
			}
			edge.eraseAlignedPoints();
		}
	}

	/**
	 * Utilizado para corregir asociaciones.
	 * 
	 * @param side
	 *            the side
	 * @param newpoint
	 *            the newpoint
	 */
	protected void correctLinkAttributePoint(final int side, final Point newpoint) {
		if (side == START) {
			puntos.set(0, newpoint);
		} else {
			puntos.set(puntos.size() - 1, newpoint);
		}
	}

	/**
	 * Ajusta la ubicacion de una etiqueta.
	 * 
	 * @param edgeText
	 *            the edge text
	 */
	protected void setLocation(final EdgeText edgeText) {
		try {
			if (edgeText != null) {
				int textType = GRLABEL;
				int textIndex = 1;
				GraphNode logicalNode = edgeText.getNodoLogico();
				Map props = ElementoVisualUtil.getPropiedades(logicalNode);
				String nodeType = (String) props
				.get(Constantes.MODEL_NODE_TYPE);
				if ("EdgeText".equals(nodeType)) {
					String edgeTextType = (String) props.get("EdgeText:Type");
					if ("Name".equals(edgeTextType)) {
						textType = NAME;
					} else if ("Stereotype".equals(edgeTextType)) {
						textType = STEREOTYPE;
					} else if ("Label1".equals(edgeTextType)) {
						textType = LABEL;
						textIndex = 1;
					} else if ("Label2".equals(edgeTextType)) {
						textType = LABEL;
						textIndex = 2;
					} else if ("Card1".equals(edgeTextType)) {
						textType = CARD;
						textIndex = 1;
					} else if ("Card2".equals(edgeTextType)) {
						textType = CARD;
						textIndex = 2;
					}
				}

				if (textType == NAME) {
					Point middle = getMiddlePoint();
					edgeText.setLocationWithDiff(middle.x - edgeText.getWidth()
							/ 2, middle.y - edgeText.getHeight() / 2);
				} else if (textType == STEREOTYPE) {
					Point middle = getMiddlePoint();
					edgeText.setLocationWithDiff(middle.x - edgeText.getWidth()
							/ 2, middle.y - edgeText.getHeight() / 2 - 20);
				} else if ((textType == LABEL) || (textType == CARD)) {
					Point point = new Point();
					int orientation = -1;
					if (textIndex == 1) {
						orientation = getPointByDist(point, 15, START);
					} else {
						orientation = getPointByDist(point, 15, END);
					}
					if (textType == CARD) {
						if ((orientation == NORTH) || (orientation == SOUTH)) {
							edgeText.setLocationWithDiff(point.x
									- edgeText.getWidth() - 5, point.y
									- edgeText.getHeight() / 2);
						} else if (orientation == EAST) {
							edgeText.setLocationWithDiff(point.x, point.y
									- edgeText.getHeight() - 5);
						} else if (orientation == WEST) {
							edgeText.setLocationWithDiff(point.x
									- edgeText.getWidth(), point.y
									- edgeText.getHeight() - 5);
						}
					} else {
						if ((orientation == NORTH) || (orientation == SOUTH)) {
							edgeText.setLocationWithDiff(point.x + 5, point.y
									- edgeText.getHeight() / 2);
						} else if (orientation == EAST) {
							edgeText.setLocationWithDiff(point.x, point.y + 5);
						} else if (orientation == WEST) {
							edgeText.setLocationWithDiff(point.x
									- edgeText.getWidth(), point.y + 5);
						}
					}
				} else if (textType == GRLABEL) {
					Point middle = getMiddlePoint();
					edgeText.setLocationWithDiff(middle.x - edgeText.getWidth()
							/ 2, middle.y - edgeText.getHeight() / 2);
				}
			}
		} catch (ArrayIndexOutOfBoundsException e) {
			log.error("Changing location error: " + e);
		}
	}

	/**
	 * Asinga el color a las etiquetas.
	 * 
	 * @param color
	 *            the color
	 */
	protected void setLabelColors(final Color color) {
		for (EdgeText elem : textos) {
			elem.setForeColor(color);
		}
	}

	/**
	 * Sets the selected.
	 * 
	 * @param p_isSelected
	 *            the new selected
	 */
	public void setSelected(final boolean p_isSelected) {
		isSelected = p_isSelected;
		if (!isSelected) {
			selectedpointindex = -1;
		}
		if (isSelected) {
			setPaintColor(getSelectColor());
		} else {
			setPaintColor(getForeColor());
		}
	}

	/**
	 * Checks if is selected.
	 * 
	 * @return true, if is selected
	 */
	public boolean isSelected() {
		return isSelected;
	}

	/**
	 * Sets the tracedline.
	 * 
	 * @param p_tracedline
	 *            the new tracedline
	 */
	protected void setTracedline(final boolean p_tracedline) {
		tracedline = p_tracedline;
	}

	/**
	 * Gets the tracedline.
	 * 
	 * @return the tracedline
	 */
	public boolean getTracedline() {
		return tracedline;
	}

	/**
	 * Sets the arrow1.
	 * 
	 * @param p_arrow1
	 *            the new arrow1
	 */
	protected void setArrow1(final boolean p_arrow1) {
		arrow1 = p_arrow1;
	}

	/**
	 * Sets the arrow2.
	 * 
	 * @param p_arrow2
	 *            the new arrow2
	 */
	protected void setArrow2(final boolean p_arrow2) {
		arrow2 = p_arrow2;
	}

	/**
	 * Sets the fat arrow1.
	 * 
	 * @param p_fatarrow1
	 *            the new fat arrow1
	 */
	protected void setFatArrow1(final boolean p_fatarrow1) {
		fatarrow1 = p_fatarrow1;
	}

	/**
	 * Sets the fat arrow2.
	 * 
	 * @param p_fatarrow2
	 *            the new fat arrow2
	 */
	protected void setFatArrow2(final boolean p_fatarrow2) {
		fatarrow2 = p_fatarrow2;
	}

	/**
	 * Sets the filled fat arrow1.
	 * 
	 * @param p_filledfatarrow1
	 *            the new filled fat arrow1
	 */
	protected void setFilledFatArrow1(final boolean p_filledfatarrow1) {
		filledfatarrow1 = p_filledfatarrow1;
	}

	/**
	 * Sets the filled fat arrow2.
	 * 
	 * @param p_filledfatarrow2
	 *            the new filled fat arrow2
	 */
	protected void setFilledFatArrow2(final boolean p_filledfatarrow2) {
		filledfatarrow2 = p_filledfatarrow2;
	}

	/**
	 * Sets the agreg1.
	 * 
	 * @param p_agreg1
	 *            the new agreg1
	 */
	protected void setAgreg1(final boolean p_agreg1) {
		agreg1 = p_agreg1;
	}

	/**
	 * Sets the agreg2.
	 * 
	 * @param p_agreg2
	 *            the new agreg2
	 */
	protected void setAgreg2(final boolean p_agreg2) {
		agreg2 = p_agreg2;
	}

	/**
	 * Sets the by value1.
	 * 
	 * @param p_byvalue1
	 *            the new by value1
	 */
	protected void setByValue1(final boolean p_byvalue1) {
		byvalue1 = p_byvalue1;
	}

	/**
	 * Sets the by value2.
	 * 
	 * @param p_byvalue2
	 *            the new by value2
	 */
	protected void setByValue2(final boolean p_byvalue2) {
		byvalue2 = p_byvalue2;
	}

	/**
	 * Sets the fore color.
	 * 
	 * @param p_color
	 *            the new fore color
	 */
	public void setForeColor(final Color p_color) {
		forecolor = p_color;
	}

	/**
	 * Gets the fore color.
	 * 
	 * @return the fore color
	 */
	public Color getForeColor() {
		return forecolor;
	}

	/**
	 * Sets the select color.
	 * 
	 * @param p_color
	 *            the new select color
	 */
	public void setSelectColor(final Color p_color) {
		selectcolor = p_color;
	}

	/**
	 * Gets the select color.
	 * 
	 * @return the select color
	 */
	public Color getSelectColor() {
		return selectcolor;
	}

	/**
	 * Sets the paint color.
	 * 
	 * @param p_color
	 *            the new paint color
	 */
	public void setPaintColor(final Color p_color) {
		paintcolor = p_color;
		setLabelColors(paintcolor);
	}

	/**
	 * Gets the paint color.
	 * 
	 * @return the paint color
	 */
	protected Color getPaintColor() {
		return paintcolor;
	}

	/**
	 * Sets the selected point color.
	 * 
	 * @param p_color
	 *            the new selected point color
	 */
	public void setSelectedPointColor(final Color p_color) {
		selectedpointcolor = p_color;
	}

	/**
	 * Gets the selected point color.
	 * 
	 * @return the selected point color
	 */
	public Color getSelectedPointColor() {
		return selectedpointcolor;
	}

	/**
	 * Retorna el texto de una linea.
	 * 
	 * @param tipo
	 *            que texto obtener
	 * 
	 * @return texto de la linea
	 */
	public EdgeText getTextoLinea(final String tipo) {
		for (EdgeText et : textos) {
			GraphNode gn = et.getNodoLogico();
			Map props = ElementoVisualUtil.getPropiedades(gn);
			String edgeTextType = (String) props.get("EdgeText:Type");
			if (tipo.equals(edgeTextType)) {
				return et;
			}
		}
		return null;
	}

	/**
	 * Elimina el texto de la linea.
	 * 
	 * @param tipo
	 *            the tipo
	 */
	public void removeTextoLinea(final String tipo) {
		EdgeText et = getTextoLinea(tipo);
		if (et != null) {
			GraphNode temp = et.getNodoLogico();
			modelo.getContained().remove(temp);
			temp.refDelete();
			graficador.remove(et);
			textos.remove(et);
		}
	}

	/**
	 * Asigna el texto de una parte de la linea.
	 * 
	 * @param newText
	 *            nuevo texto
	 * @param edgeType
	 *            parte de la linea
	 */
	public void setTextoLinea(final String newText, final String edgeType) {
		EdgeText edgeText = getTextoLinea(edgeType);
		if (edgeText == null && (newText == null || newText.trim().equals(""))) {
			return;
		}
		if (edgeText != null && (newText == null || newText.trim().equals(""))) {
			removeTextoLinea(edgeType);
			return;
		}
		if (edgeText == null) {
			GraphNode edgeTextNode = this.graficador.getDiagramManagement()
			.getGraphNode()
			.createGraphNode();
			Property propEdgeTextType = UMLUtil.crearPropiedad("", "");
			propEdgeTextType.setKey(Constantes.MODEL_NODE_TYPE);
			propEdgeTextType.setValue(Constantes.MODEL_EDGE_TEXT);
			Property propType = UMLUtil.crearPropiedad("", "");
			propType.setKey(Constantes.MODEL_EDGE_TEXT + ":Type");
			propType.setValue(edgeType);
			Property propDx = UMLUtil.crearPropiedad("", "");
			propDx.setKey(Constantes.MODEL_EDGE_TEXT + ":dx");
			propDx.setValue("0");
			Property propDy = UMLUtil.crearPropiedad("", "");
			propDy.setKey(Constantes.MODEL_EDGE_TEXT + ":dy");
			propDy.setValue("0");
			edgeTextNode.getProperty().add(propEdgeTextType);
			edgeTextNode.getProperty().add(propType);
			edgeTextNode.getProperty().add(propDx);
			edgeTextNode.getProperty().add(propDy);

			edgeText = new EdgeText(graficador, edgeTextNode);
			if (stickylabels) {
				edgeText.setCanMove(false);
			}
			edgeText.setText(newText);
			setLocation(edgeText);
			graficador.add(edgeText);
			modelo.getContained().add(edgeTextNode);
			textos.add(edgeText);
			setLabelColors(getForeColor());
		} else {
			edgeText.setText(newText);
			setLocation(edgeText);
			edgeText.repaint();
		}
	}

	/**
	 * Eliminar todas las etiquetas del graficador.
	 */
	protected void removeEdgeTextsFromGraph() {
		for (EdgeText elem : textos) {
			graficador.remove(elem);
		}
	}

	/**
	 * Gets the graficador.
	 * 
	 * @return the graficador
	 */
	public GrDiagrama getGraficador() {
		return graficador;
	}

	/**
	 * Sets the sticky.
	 * 
	 * @param p_sticky
	 *            the new sticky
	 */
	public void setSticky(final boolean p_sticky) {
		sticky = p_sticky;
	}

	/**
	 * Gets the sticky.
	 * 
	 * @return the sticky
	 */
	public boolean getSticky() {
		return sticky;
	}

	/**
	 * Sets the sticky labels.
	 * 
	 * @param p_sticky
	 *            the new sticky labels
	 */
	public void setStickyLabels(final boolean p_sticky) {
		stickylabels = p_sticky;
		for (EdgeText elem : textos) {
			elem.setCanMove(stickylabels);
		}
	}

	/**
	 * Gets the sticky labels.
	 * 
	 * @return the sticky labels
	 */
	public boolean getStickyLabels() {
		return stickylabels;
	}

	/**
	 * Sets the graphical.
	 * 
	 * @param isgraph
	 *            the new graphical
	 */
	public void setGraphical(final boolean isgraph) {
		isgraphical = isgraph;
	}

	/**
	 * Checks if is graphical.
	 * 
	 * @return true, if is graphical
	 */
	public boolean isGraphical() {
		return isgraphical;
	}

	/**
	 * Gets the puntos.
	 * 
	 * @return the puntos
	 */
	public List<Point> getPuntos() {
		return puntos;
	}

	/**
	 * Gets the textos.
	 * 
	 * @return the textos
	 */
	public List<EdgeText> getTextos() {
		return textos;
	}

	/**
	 * Contains label.
	 * 
	 * @param text
	 *            the text
	 * 
	 * @return true, if successful
	 */
	protected boolean containsLabel(final EdgeText text) {
		return textos.contains(text);
	}

	/**
	 * Sets the fin1.
	 * 
	 * @param edgeEnd
	 *            the new fin1
	 */
	public void setFin1(final ElementoVisual edgeEnd) {
		fin1 = edgeEnd;
	}

	/**
	 * Sets the fin2.
	 * 
	 * @param edgeEnd
	 *            the new fin2
	 */
	public void setFin2(final ElementoVisual edgeEnd) {
		fin2 = edgeEnd;
	}

	/**
	 * Gets the fin1.
	 * 
	 * @return the fin1
	 */
	public ElementoVisual getFin1() {
		return fin1;
	}

	/**
	 * Gets the fin2.
	 * 
	 * @return the fin2
	 */
	public ElementoVisual getFin2() {
		return fin2;
	}

	/**
	 * Adds the point.
	 * 
	 * @param x
	 *            the x
	 * @param y
	 *            the y
	 */
	public void addPoint(final int x, final int y) {
		puntos.add(new Point(x, y));
	}

	/**
	 * Adds the point.
	 * 
	 * @param p
	 *            the p
	 * 
	 * @return true, if successful
	 */
	public boolean addPoint(final Point p) {
		puntos.add(p);
		return true;
	}

	/**
	 * Actualiza la aparieca visual(ACTUALIZAR).
	 */
	public void updateVisual() {
		puntos.clear();

		for (Object nextVertice : modelo.getVertices()) {
			Vertex v = (Vertex) nextVertice;
			addPoint(new Point(v.getX(), v.getY()));
		}

		removeEdgeTextsFromGraph();
		textos.clear();

		for (Object nextContained : modelo.getContained()) {
			GraphElement ge = (GraphElement) nextContained;
			final Map properties = ElementoVisualUtil.getPropiedades(ge);
			if (ge instanceof GraphNode) {
				final GraphNode gn = (GraphNode) ge;
				final String nodeType = (String) properties
				.get(Constantes.MODEL_NODE_TYPE);
				if ("EdgeText".equals(nodeType)) {
					EdgeText et = new EdgeText(graficador, gn);
					textos.add(et);
					graficador.add(et);
				}
			}
		}
		updateLineaVisual();
	}

	/**
	 * Actualiza el modelo(ACTUALIZAR_MODELO).
	 */
	public void updateLogical() {
		final List<Vertex> delete = new ArrayList<Vertex>();

		for (Object nextVertice : modelo.getVertices()) {
			final Vertex v = (Vertex) nextVertice;
			delete.add(v);
		}

		for (Vertex nextToDelete : delete) {
			nextToDelete.refDelete();
		}

		modelo.getVertices().clear();

		for (Point nextPoint : puntos) {
			final VertexClass vertexProxy = this.graficador
					.getDiagramManagement().getVertex();
			Vertex v = vertexProxy.createVertex();
			v.setX(nextPoint.x);
			v.setY(nextPoint.y);
			modelo.getVertices().add(v);
		}
		updateLineaLogica();

	}

	/**
	 * Gets the linea logica.
	 * 
	 * @return the linea logica
	 */
	public GraphEdge getLineaLogica() {
		return modelo;
	}

	/**
	 * After add.
	 */
	public void afterAdd() {

	}

}
