/*
 * BarraTareasClase.java
 *
 * Created on November 9, 2008, 9:39 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package vista.graficador;

import javax.swing.ImageIcon;
import javax.swing.JButton;

import org.omg.uml.foundation.core.Namespace;
import org.uml.diagrammanagement.DiagramManagementPackage;

import util.Constantes;
import vista.graficador.uml.GrAbstraccion;
import vista.graficador.uml.GrAsociacion;
import vista.graficador.uml.GrClase;
import vista.graficador.uml.GrClaseAsociacion;
import vista.graficador.uml.GrDependencia;
import vista.graficador.uml.GrGeneralizacion;
import vista.graficador.uml.GrInterfaz;

/**
 * The Class BarraTareasClase.
 * 
 * @author Juan Timoteo Ponce Ortiz
 */
public final class BarraTareasClase extends BarraTareas {

	/** The btn clase. */
	private JButton btnClase;

	/** The btn interfaz. */
	private JButton btnInterfaz;

	/** The btn asociacion. */
	private JButton btnAsociacion;

	/** The btn dependencia. */
	private JButton btnDependencia;

	/** The btn generalizacion. */
	private JButton btnGeneralizacion;

	/** The btn abstraccion. */
	private JButton btnAbstraccion;

	/** The btn clase associacion. */
	private JButton btnClaseAssociacion;

	/**
	 * Creates a new instance of BarraTareasClase.
	 * 
	 * @param graficador
	 *            the graficador
	 */
	public BarraTareasClase(final Namespace rootNamespace,
			final GrDiagrama graficador,
			final DiagramManagementPackage diagramManagement) {
		super(rootNamespace, graficador, diagramManagement);
		initComponentes();
		initEventos();
	}

	/**
	 * Inits the componentes.
	 */
	private void initComponentes() {
		btnClase = new JButton("Clase");
		btnClase.setIcon(new ImageIcon(getClass().getResource(
		"/iconos/Clase.png")));
		btnInterfaz = new JButton("Interfaz");
		btnInterfaz.setIcon(new ImageIcon(getClass().getResource(
		"/iconos/Interfaz.png")));
		btnAsociacion = new JButton("Asociacion");
		btnAsociacion.setIcon(new ImageIcon(getClass().getResource(
		"/iconos/Asociacion.png")));
		btnDependencia = new JButton("Dependencia");
		btnDependencia.setIcon(new ImageIcon(getClass().getResource(
		"/iconos/Dependencia.png")));
		btnGeneralizacion = new JButton("Generalizacion");
		btnGeneralizacion.setIcon(new ImageIcon(getClass().getResource(
		"/iconos/Generalizacion.png")));
		btnAbstraccion = new JButton("Abstraccion");
		btnAbstraccion.setIcon(new ImageIcon(getClass().getResource(
		"/iconos/Abstraccion.png")));
		btnClaseAssociacion = new JButton("ClaseAsociacion");
		btnClaseAssociacion.setIcon(new ImageIcon(getClass().getResource(
		"/iconos/ClaseAsociacion.png")));
		add(btnClase);
		add(btnInterfaz);
		add(btnAsociacion);
		add(btnDependencia);
		add(btnGeneralizacion);
		add(btnAbstraccion);
		add(btnClaseAssociacion);
	}

	/**
	 * Inits the eventos.
	 */
	private void initEventos() {
		Namespace namespace = (Namespace) graficador.getDiagrama().getOwner()
		.getElement();

		btnClase.addActionListener(new BarraTareasClaseListener(
				getRootNamespace(), "Class", GrClase.class.getName(), null,
				null, graficador, namespace, Constantes.MODEL_NODE_TYPE_ID));

		btnInterfaz.addActionListener(new BarraTareasClaseListener(
				getRootNamespace(), "Interface", GrInterfaz.class.getName(),
				null, null, graficador, namespace,
				Constantes.MODEL_NODE_TYPE_ID));

		btnAsociacion.addActionListener(new BarraTareasClaseListener(
				getRootNamespace(), "Association",
				GrAsociacion.class.getName(), null, null, graficador,
				namespace, Constantes.MODEL_EDGE_TYPE_ID));

		btnDependencia.addActionListener(new BarraTareasClaseListener(
				getRootNamespace(), "Dependency",
				GrDependencia.class.getName(), null, null, graficador,
				namespace, Constantes.MODEL_EDGE_TYPE_ID));

		btnGeneralizacion.addActionListener(new BarraTareasClaseListener(
				getRootNamespace(), "Generalization", GrGeneralizacion.class
				.getName(), null, null, graficador, namespace,
				Constantes.MODEL_EDGE_TYPE_ID));

		btnAbstraccion.addActionListener(new BarraTareasClaseListener(
				getRootNamespace(), "Abstraction", GrAbstraccion.class
				.getName(), null, null, graficador, namespace,
				Constantes.MODEL_EDGE_TYPE_ID));

		btnClaseAssociacion.addActionListener(new BarraTareasClaseListener(
				getRootNamespace(), "AssociationClass", GrClaseAsociacion.class
				.getName(), null, null, graficador, namespace,
				Constantes.MODEL_EDGE_TYPE_ID));

	}

}
