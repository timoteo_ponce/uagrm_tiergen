/*
 * GrClase.java
 *
 * Created on 5 de noviembre de 2008, 11:12
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package vista.graficador.uml;

import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.omg.uml.foundation.core.Attribute;
import org.omg.uml.foundation.core.Feature;
import org.omg.uml.foundation.core.Operation;
import org.omg.uml.foundation.core.UmlClass;
import org.uml.diagrammanagement.GraphNode;
import org.uml.diagrammanagement.SemanticModelBridge;
import org.uml.diagrammanagement.Uml1SemanticModelBridge;

import util.uml.AtributoUtil;
import util.uml.EstereotipoUtil;
import util.uml.OperacionUtil;
import vista.graficador.GrDiagrama;
import vista.graficador.GrNodoVisual;

/**
 * The Class GrClase.
 * 
 * @author Juan Timoteo Ponce Ortiz
 */
public class GrClase extends GrNodoVisual {

	private static final Logger LOG = Logger.getLogger(GrClase.class);

	/** The Constant ESPACIO_LINEAS. */
	public static final int ESPACIO_LINEAS = 5;

	/** The Constant ANCHO_LINEA. */
	public static final int ANCHO_LINEA = 1;

	/** The Constant COLOR. */
	public static final Color COLOR = new Color(255, 255, 200);

	/** The nombre. */
	private String nombre;

	/** The estereotipo. */
	private String estereotipo;

	/** The padre. */
	private String padre;

	/** The atributos. */
	private final List<String> atributos;

	/** The operaciones. */
	private final List<String> operaciones;

	/**
	 * Creates a new instance of GrClase.
	 * 
	 * @param diagrama
	 *            the diagrama
	 * @param nodo
	 *            the nodo
	 */
	public GrClase(final GrDiagrama diagrama, final GraphNode nodo) {
		super(diagrama, nodo);
		atributos = new ArrayList<String>();
		operaciones = new ArrayList<String>();
		init();
		updateNodoVisual();
	}

	/**
	 * Inits the.
	 */
	public void init() {
		atributos.clear();
		operaciones.clear();
		setBackColor(COLOR);
	}

	/**
	 * Actualizar.
	 */
	public void actualizar() {
		try {
			final SemanticModelBridge temp = modelo.getSemanticModel();

			if (temp instanceof Uml1SemanticModelBridge) {
				final Uml1SemanticModelBridge puente = (Uml1SemanticModelBridge) temp;
				final UmlClass clase = (UmlClass) puente.getElement();
				nombre = clase.getName();
				if (clase.getStereotype().size() > 0) {
					estereotipo = "<<"
						+ EstereotipoUtil.toString(clase.getStereotype())
						+ ">>";
				} else {
					estereotipo = null;
				}

				atributos.clear();
				operaciones.clear();

				for (Object elem : clase.getFeature()) {
					final Feature feat = (Feature) elem;
					if (feat instanceof Attribute) {
						Attribute atrib = (Attribute) feat;
						atributos.add(AtributoUtil.toString(atrib));
					} else if (feat instanceof Operation) {
						Operation oper = (Operation) feat;
						operaciones.add(OperacionUtil.toString(oper));
					}
				}
				updateSize();
				repaint();
			}
		} catch (Exception e) {
			LOG.error(e, e);
		}
	}

	/**
	 * Update size.
	 */
	protected void updateSize() {
		double width = getMinimumWidth();
		double height = ANCHO_LINEA + ESPACIO_LINEAS;

		final FontMetrics fm = getFontMetrics(getFont());

		if (estereotipo != null) {
			Rectangle2D bounds = fm.getStringBounds(estereotipo, getGraphics());
			height += bounds.getHeight() + ESPACIO_LINEAS;
			if (bounds.getWidth() > width) {
				width = bounds.getWidth();
			}
		}
		if (nombre != null) {
			Rectangle2D bounds = fm.getStringBounds(nombre, getGraphics());
			height += bounds.getHeight() + ESPACIO_LINEAS;
			if (bounds.getWidth() > width) {
				width = bounds.getWidth();
			}
		}
		if (padre != null) {
			Rectangle2D bounds = fm.getStringBounds(padre, getGraphics());
			height += bounds.getHeight() + ESPACIO_LINEAS;
			if (bounds.getWidth() > width) {
				width = bounds.getWidth();
			}
		}
		height += ANCHO_LINEA + ESPACIO_LINEAS;

		for (String elem : atributos) {
			Rectangle2D bounds = fm.getStringBounds(elem, getGraphics());
			height += bounds.getHeight() + ESPACIO_LINEAS;
			if (bounds.getWidth() > width) {
				width = bounds.getWidth();
			}
		}

		height += ANCHO_LINEA + ESPACIO_LINEAS;

		for (String elem : operaciones) {
			Rectangle2D bounds = fm.getStringBounds(elem, getGraphics());
			height += bounds.getHeight() + ESPACIO_LINEAS;
			if (bounds.getWidth() > width) {
				width = bounds.getWidth();
			}
		}

		width += ESPACIO_LINEAS * 2 + GrDiagrama.DEFAULT_SNAP_SIZE;
		height += GrDiagrama.DEFAULT_SNAP_SIZE;

		this.setSize((int) width, (int) height);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.JComponent#paintComponent(java.awt.Graphics)
	 */
	@Override
	protected void paintComponent(final Graphics g) {
		int width = getSize().width;
		int height = getSize().height;

		g.setColor(getBackColor());
		g.fillRect(0, 0, width - 1, height - 1);
		if (isSelected()) {
			g.setColor(getSelectColor());
		} else {
			g.setColor(getForeColor());
		}
		g.drawRect(0, 0, width - 1, height - 1);

		g.setColor(getForeColor());

		final FontMetrics fm = g.getFontMetrics();
		int y = ANCHO_LINEA + ESPACIO_LINEAS;

		if (estereotipo != null) {
			Rectangle2D bounds = fm.getStringBounds(estereotipo, g);
			int strWidth = (int) bounds.getWidth();
			int strHeight = (int) bounds.getHeight();
			y += strHeight;
			int x = (width / 2) - (strWidth / 2);
			g.drawString(estereotipo, x, y);
			y += ESPACIO_LINEAS;
		}
		if (nombre != null) {
			Rectangle2D bounds = fm.getStringBounds(nombre, g);
			int strWidth = (int) bounds.getWidth();
			int strHeight = (int) bounds.getHeight();
			y += strHeight;
			int x = (width / 2) - (strWidth / 2);
			g.drawString(nombre, x, y);
			y += ESPACIO_LINEAS;
		}
		if (padre != null) {
			Rectangle2D bounds = fm.getStringBounds(padre, g);
			int strWidth = (int) bounds.getWidth();
			int strHeight = (int) bounds.getHeight();
			y += strHeight;
			int x = (width / 2) - (strWidth / 2);
			g.drawString(padre, x, y);
			y += ESPACIO_LINEAS;
		}
		if (isSelected()) {
			g.setColor(getSelectColor());
		} else {
			g.setColor(getForeColor());
		}
		y += ANCHO_LINEA;
		g.drawLine(0, y, width, y);
		y += ESPACIO_LINEAS;

		g.setColor(getForeColor());

		int x = ANCHO_LINEA + ESPACIO_LINEAS;
		for (String str : atributos) {
			Rectangle2D bounds = fm.getStringBounds(str, getGraphics());
			// int strWidth = (int)bounds.getWidth();
			int strHeight = (int) bounds.getHeight();
			y += strHeight;
			g.drawString(str, x, y);
			y += ESPACIO_LINEAS;
		}

		if (isSelected()) {
			g.setColor(getSelectColor());
		} else {
			g.setColor(getForeColor());
		}

		g.drawLine(0, y, width, y);
		y += ANCHO_LINEA + ESPACIO_LINEAS;

		g.setColor(getForeColor());
		for (String str : operaciones) {
			Rectangle2D bounds = fm.getStringBounds(str, getGraphics());
			// int strWidth = (int)bounds.getWidth();
			int strHeight = (int) bounds.getHeight();
			y += strHeight;
			g.drawString(str, x, y);
			y += ESPACIO_LINEAS;
		}
	}

	/**
	 * Gets the titulo.
	 * 
	 * @return the titulo
	 */
	public String getTitulo() {
		return "";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see vista.graficador.GrNodoVisual#getMinimumWidth()
	 */
	@Override
	protected int getMinimumWidth() {
		return 20;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see vista.graficador.GrNodoVisual#updateNodoVisual()
	 */
	@Override
	protected void updateNodoVisual() {
		actualizar();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see vista.graficador.GrNodoVisual#updateNodoLogico()
	 */
	@Override
	protected void updateNodoLogico() {
	}

}
