/*
 * GrClaseAsociacion.java
 *
 * Created on November 9, 2008, 8:57 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package vista.graficador.uml;

import java.util.Iterator;
import java.util.Map;

import org.omg.uml.foundation.core.AssociationEnd;
import org.omg.uml.foundation.core.ModelElement;
import org.omg.uml.foundation.core.UmlAssociation;
import org.omg.uml.foundation.datatypes.AggregationKindEnum;
import org.omg.uml.foundation.datatypes.VisibilityKindEnum;
import org.uml.diagrammanagement.GraphEdge;
import org.uml.diagrammanagement.GraphElement;
import org.uml.diagrammanagement.GraphNode;
import org.uml.diagrammanagement.Property;
import org.uml.diagrammanagement.SemanticModelBridge;
import org.uml.diagrammanagement.Uml1SemanticModelBridge;
import org.uml.diagrammanagement.Vertex;

import util.Constantes;
import util.uml.ElementoVisualUtil;
import util.uml.MultiplicidadUtil;
import util.uml.UMLUtil;
import vista.graficador.ElementoVisual;
import vista.graficador.GrDiagrama;
import vista.graficador.GrNodoVisual;

/**
 * The Class GrClaseAsociacion.
 * 
 * @author Juan Timoteo Ponce Ortiz
 */
public final class GrClaseAsociacion extends GrAsociacion {

    /** Initial distance between the association and the class. */
    public final static int DISTANCE = 50;

    /**
     * Creates a new instance of GrClaseAsociacion.
     * 
     * @param diagrama
     *            the diagrama
     * @param edge
     *            the edge
     */
    public GrClaseAsociacion(GrDiagrama diagrama, GraphEdge edge) {
	super(diagrama, edge);
	// Util.print( this , "Instancia creada" );
    }

    /**
     * Finalizes the creation of this edge. Complements the functionalities of
     * GrAssociation, by adding a class and a link.
     * 
     * @throws Exception
     *             the exception
     */
    @Override
    protected void finalizeCreacionLinea() throws Exception {
	super.finalizeCreacionLinea();
	addLinkClase();
    }

    /**
     * After this element is dragged from the browser, the class and link are
     * added also.
     */
    public void afterDrag() {
	addLinkClase();
    }

    /*
     * (non-Javadoc)
     * 
     * @see vista.graficador.GrLineaVisual#afterAdd()
     */
    @Override
    public void afterAdd() {
    }

    /**
     * Creates a class and a link, together with the association.
     */
    private void addLinkClase() {
	final GraphNode graphNode = UMLUtil.crearGraphNode();

	final Property prop = UMLUtil.crearPropiedad("", "");
	prop.setKey(Constantes.MODEL_NODE_TYPE);
	prop.setValue(GrClase.class.getName());
	graphNode.getProperty().add(prop);

	final Uml1SemanticModelBridge bridge = UMLUtil
		.crearPuenteSemantico(null);
	bridge.setElement(((Uml1SemanticModelBridge) modelo.getSemanticModel())
		.getElement());
	graphNode.setSemanticModel(bridge);

	final GraphEdge graphEdgeLink = UMLUtil.crearGraphEdge();
	Property prop2 = UMLUtil.crearPropiedad("", "");
	prop2.setKey(Constantes.MODEL_EDGE_TYPE);
	prop2.setValue(GrClaseAsociacionLnk.class.getName());
	graphEdgeLink.getProperty().add(prop2);

	graphEdgeLink.setEdgeEnd1(graphNode);
	graphEdgeLink.setEdgeEnd2(modelo);

	modelo.getContained().add(graphNode);
	modelo.getContained().add(graphEdgeLink);

	graphNode.setX(getMiddlePoint().x);
	graphNode.setY(getMiddlePoint().y + DISTANCE);
	graficador.crearNodoVisual(graphNode);

	final GrNodoVisual visualClassNode = (GrNodoVisual) graficador
		.getElementoVisual(graphNode);
	final Vertex v1 = UMLUtil.crearVertice();
	v1.setX(visualClassNode.getCenter().x);
	v1.setY(visualClassNode.getCenter().y);
	Vertex v2 = UMLUtil.crearVertice();
	v2.setX(getMiddlePoint().x);
	v2.setY(getMiddlePoint().y);
	graphEdgeLink.getVertices().add(v1);
	graphEdgeLink.getVertices().add(v2);

	graficador.crearLineaVisual(graphEdgeLink);
    }

    /**
     * Updates the visual appearance of the association, according to the
     * logical edge.
     */
    protected void updateVisualAssociation() {
	final SemanticModelBridge bridge = modelo.getSemanticModel();
	if (bridge instanceof Uml1SemanticModelBridge) {
	    final Uml1SemanticModelBridge semanticBridge = (Uml1SemanticModelBridge) bridge;
	    final ModelElement me = semanticBridge.getElement();
	    if (me instanceof UmlAssociation) {
		final UmlAssociation association = (UmlAssociation) me;
		final Iterator i = association.getConnection().iterator();
		AssociationEnd ae1 = null;
		AssociationEnd ae2 = null;
		if (i.hasNext())
		    ae1 = (AssociationEnd) i.next();
		if (i.hasNext())
		    ae2 = (AssociationEnd) i.next();
		if (ae1 != null && ae2 != null) {
		    String sexp1 = "";
		    if (ae1.getVisibility() != null
			    && ae1.getVisibility() == VisibilityKindEnum.VK_PUBLIC)
			sexp1 = "+";
		    else if (ae1.getVisibility() != null
			    && ae1.getVisibility() == VisibilityKindEnum.VK_PROTECTED)
			sexp1 = "#";
		    else if (ae1.getVisibility() != null
			    && ae1.getVisibility() == VisibilityKindEnum.VK_PRIVATE)
			sexp1 = "-";
		    if (ae1.getName() == null
			    || ae1.getName().trim().equals(""))
			sexp1 = "";
		    String ae1Name = "";
		    if (ae1.getName() != null)
			ae1Name = ae1.getName();
		    setTextoLinea(sexp1 + ae1Name, "Label1");
		    setTextoLinea(MultiplicidadUtil.toString(ae1
			    .getMultiplicity(), MultiplicidadUtil.COMA),
			    "Card1");
		    setArrow1(ae1.isNavigable());
		    boolean isAgreg1 = false;
		    boolean isByValue1 = false;
		    if (ae1.getAggregation() == AggregationKindEnum.AK_AGGREGATE)
			isAgreg1 = true;
		    if (ae1.getAggregation() == AggregationKindEnum.AK_COMPOSITE)
			isByValue1 = true;
		    setAgreg1(isAgreg1);
		    setByValue1(isByValue1);

		    String sexp2 = "";
		    if (ae2.getVisibility() != null
			    && ae2.getVisibility() == VisibilityKindEnum.VK_PUBLIC)
			sexp2 = "+";
		    else if (ae2.getVisibility() != null
			    && ae2.getVisibility() == VisibilityKindEnum.VK_PROTECTED)
			sexp2 = "#";
		    else if (ae2.getVisibility() != null
			    && ae2.getVisibility() == VisibilityKindEnum.VK_PRIVATE)
			sexp2 = "-";
		    if (ae2.getName() == null
			    || ae2.getName().trim().equals(""))
			sexp2 = "";
		    String ae2Name = "";
		    if (ae2.getName() != null)
			ae2Name = ae2.getName();
		    setTextoLinea(sexp2 + ae2Name, "Label2");
		    setTextoLinea(MultiplicidadUtil.toString(ae2
			    .getMultiplicity(), MultiplicidadUtil.COMA),
			    "Card2");
		    setArrow2(ae2.isNavigable());
		    boolean isAgreg2 = false;
		    boolean isByValue2 = false;
		    if (ae2.getAggregation() == AggregationKindEnum.AK_AGGREGATE)
			isAgreg2 = true;
		    if (ae2.getAggregation() == AggregationKindEnum.AK_COMPOSITE)
			isByValue2 = true;
		    setAgreg2(isAgreg2);
		    setByValue2(isByValue2);
		}
	    }
	}
    }

    /*
     * (non-Javadoc)
     * 
     * @see vista.graficador.uml.GrAsociacion#updateLineaVisual()
     */
    @Override
    protected void updateLineaVisual() {
	super.updateLineaVisual();
	GrClase classNode = getAssociationClassClass();
	if (classNode != null)
	    classNode.updateVisual();
    }

    /**
     * Gets the class that belongs to this association class.
     * 
     * @return the class that belongs to this association class.
     */
    public GrClase getAssociationClassClass() {
	final Iterator<GraphElement> i = modelo.getContained().iterator();

	while (i.hasNext()) {
	    GraphElement ge = i.next();
	    if (ge instanceof GraphNode) {
		GraphNode gn = (GraphNode) ge;
		Map props = ElementoVisualUtil.getPropiedades(gn);
		String nodeType = (String) props
			.get(Constantes.MODEL_NODE_TYPE);
		if (GrClase.class.getName().equals(nodeType)) {
		    ElementoVisual vge = graficador.getElementoVisual(gn);
		    if (vge instanceof GrClase) {
			return (GrClase) vge;
		    }
		}
	    }
	}
	return null;
    }

    /**
     * Gets the link contained by this association class.
     * 
     * @return the link contained by this association class.
     */
    public GrClaseAsociacionLnk getAssociationClassLink() {
	final Iterator<GraphElement> i = modelo.getContained().iterator();
	while (i.hasNext()) {
	    GraphElement ge = i.next();
	    if (ge instanceof GraphEdge) {
		GraphEdge gEdge = (GraphEdge) ge;
		Map props = ElementoVisualUtil.getPropiedades(gEdge);
		String edgeType = (String) props
			.get(Constantes.MODEL_EDGE_TYPE);
		if (GrClaseAsociacionLnk.class.getName().equals(edgeType)) {
		    ElementoVisual vge = graficador.getElementoVisual(gEdge);
		    if (vge instanceof GrClaseAsociacionLnk)
			return (GrClaseAsociacionLnk) vge;
		}
	    }
	}
	return null;
    }

}
