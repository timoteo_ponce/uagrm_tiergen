/*
 * GrPaquete.java
 *
 * Created on 9 de noviembre de 2008, 13:56
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package vista.graficador.uml;

import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Polygon;
import java.awt.geom.Rectangle2D;

import org.omg.uml.modelmanagement.UmlPackage;
import org.uml.diagrammanagement.GraphNode;
import org.uml.diagrammanagement.SemanticModelBridge;
import org.uml.diagrammanagement.Uml1SemanticModelBridge;

import util.uml.EstereotipoUtil;
import vista.graficador.GrDiagrama;
import vista.graficador.GrNodoVisual;

/**
 * The Class GrPaquete.
 * 
 * @author Juan Timoteo Ponce Ortiz
 */
public final class GrPaquete extends GrNodoVisual {

    /** The nombre. */
    private String nombre;

    /** The estereotipo. */
    private String estereotipo;

    /** The padre. */
    private String padre;

    /** Defines the space between the lines, for the text inside the package. */
    public final static int SPACE_BETWEEN_LINES = 5;

    /** The weight of the lines of this class. */
    public final static int LINE_WEIGHT = 1;

    /** The width of the tab on the top of the folder icon. */
    public final static int TAB_WIDTH = 20;

    /**
     * Creates a new instance of GrPaquete.
     * 
     * @param diagrama
     *            the diagrama
     * @param node
     *            the node
     */
    public GrPaquete(GrDiagrama diagrama, GraphNode node) {
	super(diagrama, node);
	init();
	updateNodoVisual();
    }

    /**
     * Inits the.
     */
    private void init() {
	setBackColor(new Color(246, 188, 104, 35));
    }

    /*
     * (non-Javadoc)
     * 
     * @see vista.graficador.GrNodoVisual#getMinimumWidth()
     */
    @Override
    protected int getMinimumWidth() {
	return 20;
    }

    /*
     * (non-Javadoc)
     * 
     * @see vista.graficador.GrNodoVisual#updateNodoVisual()
     */
    @Override
    protected void updateNodoVisual() {
	try {
	    SemanticModelBridge bridge = modelo.getSemanticModel();
	    if (bridge instanceof Uml1SemanticModelBridge) {
		Uml1SemanticModelBridge packageBridge = (Uml1SemanticModelBridge) bridge;
		UmlPackage paquete = (UmlPackage) packageBridge.getElement();

		nombre = paquete.getName();
		if (paquete.getStereotype().size() > 0)
		    estereotipo = "<<"
			    + EstereotipoUtil.toString(paquete.getStereotype())
			    + ">>";
		else
		    estereotipo = null;
	    }
	    updateSize();
	    repaint();
	} catch (Exception e) {
	    System.err.println("Error updating visual graph class" + e);
	}
    }

    /*
     * (non-Javadoc)
     * 
     * @see vista.graficador.GrNodoVisual#updateNodoLogico()
     */
    @Override
    protected void updateNodoLogico() {
    }

    /**
     * Draws this package, in the shape of a folder icon.
     * 
     * @param g
     *            the graphics object where to draw this package.
     */
    @Override
    public void paintComponent(Graphics g) {
	int width = getSize().width;
	int height = getSize().height;

	g.setColor(Color.white);
	g.fillRect(0, 0, width - 1, height - 1);

	Polygon p = new Polygon();
	p.addPoint(0, TAB_WIDTH / 2);
	p.addPoint(TAB_WIDTH / 4, 0);
	p.addPoint(TAB_WIDTH - TAB_WIDTH / 4, 0);
	p.addPoint(TAB_WIDTH, TAB_WIDTH / 2);

	g.setColor(getBackColor());
	g.fillPolygon(p);
	g.fillRect(0, TAB_WIDTH / 2, width - 1, height - 1 - TAB_WIDTH / 2);

	if (isSelected())
	    g.setColor(getSelectColor());
	else
	    g.setColor(getForeColor());

	g.drawRect(0, TAB_WIDTH / 2, width - 1, height - 1 - TAB_WIDTH / 2);
	g.drawPolygon(p);

	g.setColor(getForeColor());

	FontMetrics fm = g.getFontMetrics();
	int y = LINE_WEIGHT * 2 + TAB_WIDTH / 2 + SPACE_BETWEEN_LINES;

	if (estereotipo != null) {
	    Rectangle2D bounds = fm.getStringBounds(estereotipo, g);
	    int strWidth = (int) bounds.getWidth();
	    int strHeight = (int) bounds.getHeight();
	    y += strHeight;
	    int x = (width / 2) - (strWidth / 2);
	    g.drawString(estereotipo, x, y);
	    y += SPACE_BETWEEN_LINES;
	}
	if (nombre != null) {
	    Rectangle2D bounds = fm.getStringBounds(nombre, g);
	    int strWidth = (int) bounds.getWidth();
	    int strHeight = (int) bounds.getHeight();
	    y += strHeight;
	    int x = (width / 2) - (strWidth / 2);
	    g.drawString(nombre, x, y);
	    y += SPACE_BETWEEN_LINES;
	}
	if (padre != null) {
	    Rectangle2D bounds = fm.getStringBounds(padre, g);
	    int strWidth = (int) bounds.getWidth();
	    int strHeight = (int) bounds.getHeight();
	    y += strHeight;
	    int x = (width / 2) - (strWidth / 2);
	    g.drawString(padre, x, y);
	    y += SPACE_BETWEEN_LINES;
	}
    }

    /**
     * Updates the size of this package, to better fit its contents.
     */
    protected void updateSize() {
	double width = getMinimumWidth();
	double height = LINE_WEIGHT * 2 + TAB_WIDTH / 2 + SPACE_BETWEEN_LINES;

	FontMetrics fm = getFontMetrics(getFont());

	if (estereotipo != null) {
	    Rectangle2D bounds = fm.getStringBounds(estereotipo, getGraphics());
	    height += bounds.getHeight() + SPACE_BETWEEN_LINES;
	    if (bounds.getWidth() > width)
		width = bounds.getWidth();
	}
	if (nombre != null) {
	    Rectangle2D bounds = fm.getStringBounds(nombre, getGraphics());
	    height += bounds.getHeight() + SPACE_BETWEEN_LINES;
	    if (bounds.getWidth() > width)
		width = bounds.getWidth();
	}
	if (padre != null) {
	    Rectangle2D bounds = fm.getStringBounds(padre, getGraphics());
	    height += bounds.getHeight() + SPACE_BETWEEN_LINES;
	    if (bounds.getWidth() > width)
		width = bounds.getWidth();
	}

	width += SPACE_BETWEEN_LINES * 2 + GrDiagrama.DEFAULT_SNAP_SIZE;
	height += GrDiagrama.DEFAULT_SNAP_SIZE;

	this.setSize((int) width, (int) height);
    }

}
