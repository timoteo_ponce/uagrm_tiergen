/*
 * GrClaseAsociacionLnk.java
 *
 * Created on 6 de noviembre de 2008, 22:41
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package vista.graficador.uml;

import org.uml.diagrammanagement.GraphEdge;

import vista.graficador.GrDiagrama;
import vista.graficador.GrLineaVisual;

/**
 * The Class GrClaseAsociacionLnk.
 * 
 * @author Juan Timoteo Ponce Ortiz
 */
public class GrClaseAsociacionLnk extends GrLineaVisual {

    /**
     * Creates a new instance of GrClaseAsociacionLnk.
     * 
     * @param diagrama
     *            the diagrama
     * @param edge
     *            the edge
     */
    public GrClaseAsociacionLnk(GrDiagrama diagrama, GraphEdge edge) {
	super(diagrama, edge);
	setTracedline(true);
	setGraphical(true);
    }

    /*
     * (non-Javadoc)
     * 
     * @see vista.graficador.GrLineaVisual#updateLineaVisual()
     */
    protected void updateLineaVisual() {
    }

    /*
     * (non-Javadoc)
     * 
     * @see vista.graficador.GrLineaVisual#updateLineaLogica()
     */
    protected void updateLineaLogica() {
    }

    /*
     * (non-Javadoc)
     * 
     * @see vista.graficador.GrLineaVisual#finalizeCreacionLinea()
     */
    protected void finalizeCreacionLinea() throws Exception {
    }

}
