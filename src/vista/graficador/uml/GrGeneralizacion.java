/*
 * GrGeneralizacion.java
 *
 * Created on November 9, 2008, 9:21 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package vista.graficador.uml;

import org.omg.uml.foundation.core.GeneralizableElement;
import org.omg.uml.foundation.core.Generalization;
import org.omg.uml.foundation.core.ModelElement;
import org.uml.diagrammanagement.GraphEdge;
import org.uml.diagrammanagement.SemanticModelBridge;
import org.uml.diagrammanagement.Uml1SemanticModelBridge;

import util.uml.EstereotipoUtil;
import vista.graficador.GrDiagrama;
import vista.graficador.GrLineaVisual;

/**
 * The Class GrGeneralizacion.
 * 
 * @author Juan Timoteo Ponce Ortiz
 */
public final class GrGeneralizacion extends GrLineaVisual {

    /**
     * Creates a new instance of GrGeneralizacion.
     * 
     * @param graficador
     *            the graficador
     * @param edge
     *            the edge
     */
    public GrGeneralizacion(GrDiagrama graficador, GraphEdge edge) {
	super(graficador, edge);
	setFatArrow2(true);
    }

    /*
     * (non-Javadoc)
     * 
     * @see vista.graficador.GrLineaVisual#updateLineaVisual()
     */
    @Override
    protected void updateLineaVisual() {
	final SemanticModelBridge bridge = modelo.getSemanticModel();
	if (bridge instanceof Uml1SemanticModelBridge) {
	    final Uml1SemanticModelBridge semanticBridge = (Uml1SemanticModelBridge) bridge;
	    final ModelElement me = semanticBridge.getElement();
	    if (me instanceof Generalization) {
		Generalization gen = (Generalization) me;
		setTextoLinea(gen.getName(), "Name");
		String newStereo = "";
		String stereo = EstereotipoUtil.toString(gen.getStereotype());
		if (stereo != null && !stereo.trim().equals(""))
		    newStereo = "<<" + stereo + ">>";
		setTextoLinea(newStereo, "Stereotype");
	    }
	}
    }

    /*
     * (non-Javadoc)
     * 
     * @see vista.graficador.GrLineaVisual#updateLineaLogica()
     */
    @Override
    protected void updateLineaLogica() {
    }

    /*
     * (non-Javadoc)
     * 
     * @see vista.graficador.GrLineaVisual#finalizeCreacionLinea()
     */
    @Override
    protected void finalizeCreacionLinea() throws Exception {
	Generalization logicalGeneralization = null;
	try {
	    logicalGeneralization = (Generalization) ((Uml1SemanticModelBridge) getLineaLogica()
		    .getSemanticModel()).getElement();
	} catch (Exception e) {
	}

	if (logicalGeneralization != null) {
	    GeneralizableElement child = logicalGeneralization.getChild();
	    GeneralizableElement parent = logicalGeneralization.getParent();
	    if (child == null && parent == null) {
		try {
		    child = (GeneralizableElement) ((Uml1SemanticModelBridge) getLineaLogica()
			    .getEdgeEnd1().getSemanticModel()).getElement();
		    parent = (GeneralizableElement) ((Uml1SemanticModelBridge) getLineaLogica()
			    .getEdgeEnd2().getSemanticModel()).getElement();
		} catch (ClassCastException cce) {
		    throw new Exception(
			    "Generalization relationship must involve Generalizable elements only!");
		}
	    }
	    if (child != null && parent != null) {
		logicalGeneralization.setChild(child);
		logicalGeneralization.setParent(parent);
		updateLineaVisual();
	    }
	}
    }

}
