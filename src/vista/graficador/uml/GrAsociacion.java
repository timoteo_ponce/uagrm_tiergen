/*
 * GrAsociacion.java
 *
 * Created on 9 de noviembre de 2008, 15:09
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package vista.graficador.uml;

import java.util.Iterator;

import org.omg.uml.foundation.core.AssociationEnd;
import org.omg.uml.foundation.core.Classifier;
import org.omg.uml.foundation.core.ModelElement;
import org.omg.uml.foundation.core.UmlAssociation;
import org.omg.uml.foundation.datatypes.AggregationKindEnum;
import org.omg.uml.foundation.datatypes.VisibilityKindEnum;
import org.uml.diagrammanagement.GraphEdge;
import org.uml.diagrammanagement.SemanticModelBridge;
import org.uml.diagrammanagement.Uml1SemanticModelBridge;

import util.uml.EstereotipoUtil;
import util.uml.MultiplicidadUtil;
import util.uml.UMLUtil;
import vista.graficador.GrDiagrama;
import vista.graficador.GrLineaVisual;

/**
 * The Class GrAsociacion.
 * 
 * @author Juan Timoteo Ponce Ortiz
 */
public class GrAsociacion extends GrLineaVisual {

    /** The unidireccional. */
    private boolean unidireccional;

    /**
     * Creates a new instance of GrAsociacion.
     * 
     * @param diagrama
     *            the diagrama
     * @param edge
     *            the edge
     * @param unidireccional
     *            the unidireccional
     */
    public GrAsociacion(GrDiagrama diagrama, GraphEdge edge,
	    boolean unidireccional) {
	super(diagrama, edge);
	this.unidireccional = unidireccional;
    }

    /**
     * Instantiates a new gr asociacion.
     * 
     * @param diagrama
     *            the diagrama
     * @param edge
     *            the edge
     */
    public GrAsociacion(GrDiagrama diagrama, GraphEdge edge) {
	this(diagrama, edge, true);
    }

    /*
     * (non-Javadoc)
     * 
     * @see vista.graficador.GrLineaVisual#updateLineaVisual()
     */
    protected void updateLineaVisual() {
	final SemanticModelBridge bridge = modelo.getSemanticModel();
	if (bridge instanceof Uml1SemanticModelBridge) {
	    final Uml1SemanticModelBridge semanticBridge = (Uml1SemanticModelBridge) bridge;
	    final ModelElement me = semanticBridge.getElement();
	    if (me instanceof UmlAssociation) {
		final UmlAssociation association = (UmlAssociation) me;
		setTextoLinea(association.getName(), "Name");
		String newStereo = "";
		String stereo = EstereotipoUtil.toString(association
			.getStereotype());
		if (stereo != null && !stereo.trim().equals(""))
		    newStereo = "<<" + stereo + ">>";
		setTextoLinea(newStereo, "Stereotype");
		final Iterator i = association.getConnection().iterator();
		AssociationEnd ae1 = null;
		AssociationEnd ae2 = null;
		if (i.hasNext())
		    ae1 = (AssociationEnd) i.next();
		if (i.hasNext())
		    ae2 = (AssociationEnd) i.next();
		if (ae1 != null && ae2 != null) {
		    String sexp1 = "";
		    if (ae1.getVisibility() != null
			    && ae1.getVisibility() == VisibilityKindEnum.VK_PUBLIC)
			sexp1 = "+";
		    else if (ae1.getVisibility() != null
			    && ae1.getVisibility() == VisibilityKindEnum.VK_PROTECTED)
			sexp1 = "#";
		    else if (ae1.getVisibility() != null
			    && ae1.getVisibility() == VisibilityKindEnum.VK_PRIVATE)
			sexp1 = "-";
		    if (ae1.getName() == null
			    || ae1.getName().trim().equals(""))
			sexp1 = "";
		    String ae1Name = "";
		    if (ae1.getName() != null)
			ae1Name = ae1.getName();
		    setTextoLinea(sexp1 + ae1Name, "Label1");
		    setTextoLinea(MultiplicidadUtil.toString(ae1
			    .getMultiplicity(), MultiplicidadUtil.COMA),
			    "Card1");
		    setArrow1(ae1.isNavigable());
		    boolean isAgreg1 = false;
		    boolean isByValue1 = false;
		    if (ae1.getAggregation() == AggregationKindEnum.AK_AGGREGATE)
			isAgreg1 = true;
		    if (ae1.getAggregation() == AggregationKindEnum.AK_COMPOSITE) {
			isAgreg1 = true;
			isByValue1 = true;
		    }
		    setAgreg1(isAgreg1);
		    setByValue1(isByValue1);

		    String sexp2 = "";
		    if (ae2.getVisibility() != null
			    && ae2.getVisibility() == VisibilityKindEnum.VK_PUBLIC)
			sexp2 = "+";
		    else if (ae2.getVisibility() != null
			    && ae2.getVisibility() == VisibilityKindEnum.VK_PROTECTED)
			sexp2 = "#";
		    else if (ae2.getVisibility() != null
			    && ae2.getVisibility() == VisibilityKindEnum.VK_PRIVATE)
			sexp2 = "-";
		    if (ae2.getName() == null
			    || ae2.getName().trim().equals(""))
			sexp2 = "";
		    String ae2Name = "";
		    if (ae2.getName() != null)
			ae2Name = ae2.getName();
		    setTextoLinea(sexp2 + ae2Name, "Label2");
		    setTextoLinea(MultiplicidadUtil.toString(ae2
			    .getMultiplicity(), MultiplicidadUtil.COMA),
			    "Card2");
		    if (ae1.isNavigable() && ae2.isNavigable()) {
			setArrow1(false);
			setArrow2(false);
		    } else
			setArrow2(ae2.isNavigable());
		    boolean isAgreg2 = false;
		    boolean isByValue2 = false;
		    if (ae2.getAggregation() == AggregationKindEnum.AK_AGGREGATE)
			isAgreg2 = true;
		    if (ae2.getAggregation() == AggregationKindEnum.AK_COMPOSITE) {
			isAgreg2 = true;
			isByValue2 = true;
		    }
		    setAgreg2(isAgreg2);
		    setByValue2(isByValue2);
		}
	    }
	}
    }

    /*
     * (non-Javadoc)
     * 
     * @see vista.graficador.GrLineaVisual#updateLineaLogica()
     */
    protected void updateLineaLogica() {
    }

    /*
     * (non-Javadoc)
     * 
     * @see vista.graficador.GrLineaVisual#finalizeCreacionLinea()
     */
    protected void finalizeCreacionLinea() throws Exception {
	final UmlAssociation logicalAssociation = (UmlAssociation) ((Uml1SemanticModelBridge) getLineaLogica()
		.getSemanticModel()).getElement();

	if (logicalAssociation != null) {
	    final Iterator i = logicalAssociation.getConnection().iterator();
	    if (!i.hasNext()) {
		Classifier c1 = null;
		Classifier c2 = null;
		try {
		    c1 = (Classifier) ((Uml1SemanticModelBridge) getLineaLogica()
			    .getEdgeEnd1().getSemanticModel()).getElement();
		    c2 = (Classifier) ((Uml1SemanticModelBridge) getLineaLogica()
			    .getEdgeEnd2().getSemanticModel()).getElement();
		} catch (ClassCastException cce) {
		    throw new Exception(
			    "Association must involve Classifiers only!");
		} catch (NullPointerException npe) {
		    throw new Exception(
			    "Association must involve Classifiers only!");
		}
		if (c1 != null && c2 != null) {
		    AssociationEnd ae1 = UMLUtil.crearAsociacionEnd();
		    AssociationEnd ae2 = UMLUtil.crearAsociacionEnd();
		    ae1.setVisibility(VisibilityKindEnum.VK_PUBLIC);
		    ae2.setVisibility(VisibilityKindEnum.VK_PUBLIC);
		    if (unidireccional) {
			ae1.setNavigable(false);
			ae2.setNavigable(true);
		    }
		    ae1.setParticipant(c1);
		    ae2.setParticipant(c2);

		    logicalAssociation.getConnection().add(ae1);
		    logicalAssociation.getConnection().add(ae2);

		    updateLineaVisual();
		}
	    }
	}
    }

}
