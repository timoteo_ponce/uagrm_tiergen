/*
 * GrInterfaz.java
 *
 * Created on 9 de noviembre de 2008, 14:48
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package vista.graficador.uml;

import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.omg.uml.foundation.core.Attribute;
import org.omg.uml.foundation.core.Feature;
import org.omg.uml.foundation.core.Interface;
import org.omg.uml.foundation.core.Operation;
import org.uml.diagrammanagement.GraphNode;
import org.uml.diagrammanagement.SemanticModelBridge;
import org.uml.diagrammanagement.Uml1SemanticModelBridge;

import util.uml.AtributoUtil;
import util.uml.EstereotipoUtil;
import util.uml.OperacionUtil;
import vista.graficador.GrDiagrama;
import vista.graficador.GrNodoVisual;

/**
 * The Class GrInterfaz.
 * 
 * @author Juan Timoteo Ponce Ortiz
 */
public final class GrInterfaz extends GrNodoVisual {

    /** The nombre. */
    private String nombre;

    /** The estereotipo. */
    private String estereotipo;

    /** The padre. */
    private String padre;

    /** The atributos. */
    private final List<String> atributos;

    /** The operaciones. */
    private final List<String> operaciones;

    /** Defines the space between the lines, for the text inside the interface. */
    public final static int SPACE_BETWEEN_LINES = 5;

    /** The weight of the lines of this class. */
    public final static int LINE_WEIGHT = 1;

    /** The size of the circle on the top of the interface. */
    public final static int CIRCLE_SIZE = 20;

    /**
     * Creates a new instance of GrInterfaz.
     * 
     * @param diagrama
     *            the diagrama
     * @param node
     *            the node
     */
    public GrInterfaz(GrDiagrama diagrama, GraphNode node) {
	super(diagrama, node);
	atributos = new ArrayList<String>();
	operaciones = new ArrayList<String>();
	init();
	updateNodoVisual();
    }

    /**
     * Inits the.
     */
    public void init() {
	atributos.clear();
	operaciones.clear();
	setBackColor(new Color(255, 255, 200));
    }

    /*
     * (non-Javadoc)
     * 
     * @see vista.graficador.GrNodoVisual#getMinimumWidth()
     */
    @Override
    protected int getMinimumWidth() {
	return 20;
    }

    /*
     * (non-Javadoc)
     * 
     * @see vista.graficador.GrNodoVisual#updateNodoVisual()
     */
    @Override
    protected void updateNodoVisual() {
	try {
	    final SemanticModelBridge bridge = modelo.getSemanticModel();
	    if (bridge instanceof Uml1SemanticModelBridge) {
		final Uml1SemanticModelBridge interfaceBridge = (Uml1SemanticModelBridge) bridge;
		final Interface umlInterface = (Interface) interfaceBridge
			.getElement();

		nombre = umlInterface.getName();
		if (umlInterface.getStereotype().size() > 0)
		    estereotipo = "<<"
			    + EstereotipoUtil.toString(umlInterface
				    .getStereotype()) + ">>";
		else
		    estereotipo = null;

		atributos.clear();
		operaciones.clear();
		Iterator<Feature> i = umlInterface.getFeature().iterator();

		while (i.hasNext()) {
		    Feature f = i.next();
		    if (f instanceof Attribute) {
			Attribute attr = (Attribute) f;
			atributos.add(AtributoUtil.toString(attr));
		    } else if (f instanceof Operation) {
			Operation op = (Operation) f;
			operaciones.add(OperacionUtil.toString(op));
		    }
		}
	    }
	    updateSize();
	    repaint();
	} catch (Exception e) {
	    System.err.println("Error updating visual graph interface" + e);
	}
    }

    /*
     * (non-Javadoc)
     * 
     * @see vista.graficador.GrNodoVisual#updateNodoLogico()
     */
    @Override
    protected void updateNodoLogico() {
    }

    /**
     * Paints this interface.
     * 
     * @param g
     *            the graphics object where to paint this interface
     */
    @Override
    public void paintComponent(Graphics g) {
	int width = getSize().width;
	int height = getSize().height;

	g.setColor(Color.white);
	g.fillRect(0, 0, width - 1, height - 1);

	g.setColor(getBackColor());
	g.fillOval(width / 2 - CIRCLE_SIZE / 2, 0, CIRCLE_SIZE, CIRCLE_SIZE);

	if (isSelected())
	    g.setColor(getSelectColor());
	else
	    g.setColor(getForeColor());
	g.drawOval(width / 2 - CIRCLE_SIZE / 2, 0, CIRCLE_SIZE, CIRCLE_SIZE);

	g.setColor(getForeColor());

	FontMetrics fm = g.getFontMetrics();
	int y = CIRCLE_SIZE + SPACE_BETWEEN_LINES;

	if (estereotipo != null) {
	    Rectangle2D bounds = fm.getStringBounds(estereotipo, g);
	    int strWidth = (int) bounds.getWidth();
	    int strHeight = (int) bounds.getHeight();
	    y += strHeight;
	    int x = (width / 2) - (strWidth / 2);
	    g.drawString(estereotipo, x, y);
	    y += SPACE_BETWEEN_LINES;
	}
	if (nombre != null) {
	    Rectangle2D bounds = fm.getStringBounds(nombre, g);
	    int strWidth = (int) bounds.getWidth();
	    int strHeight = (int) bounds.getHeight();
	    y += strHeight;
	    int x = (width / 2) - (strWidth / 2);
	    g.drawString(nombre, x, y);
	    y += SPACE_BETWEEN_LINES;
	}
	if (padre != null) {
	    Rectangle2D bounds = fm.getStringBounds(padre, g);
	    int strWidth = (int) bounds.getWidth();
	    int strHeight = (int) bounds.getHeight();
	    y += strHeight;
	    int x = (width / 2) - (strWidth / 2);
	    g.drawString(padre, x, y);
	    y += SPACE_BETWEEN_LINES;
	}

	if (isSelected())
	    g.setColor(getSelectColor());
	else
	    g.setColor(getForeColor());
	y += LINE_WEIGHT;
	g.drawLine(0, y, width, y);
	y += SPACE_BETWEEN_LINES;
	g.setColor(getForeColor());

	int x = LINE_WEIGHT + SPACE_BETWEEN_LINES;

	for (String str : atributos) {
	    Rectangle2D bounds = fm.getStringBounds(str, getGraphics());
	    // int strWidth = (int)bounds.getWidth();
	    int strHeight = (int) bounds.getHeight();
	    y += strHeight;
	    g.drawString(str, x, y);
	    y += SPACE_BETWEEN_LINES;
	}

	if (isSelected())
	    g.setColor(getSelectColor());
	else
	    g.setColor(getForeColor());

	g.drawLine(0, y, width, y);
	y += LINE_WEIGHT + SPACE_BETWEEN_LINES;

	g.setColor(getForeColor());
	x = LINE_WEIGHT + SPACE_BETWEEN_LINES;

	for (String str : operaciones) {
	    Rectangle2D bounds = fm.getStringBounds(str, getGraphics());
	    // int strWidth = (int)bounds.getWidth();
	    int strHeight = (int) bounds.getHeight();
	    y += strHeight;
	    g.drawString(str, x, y);
	    y += SPACE_BETWEEN_LINES;
	}

    }

    /**
     * Updates the size of this interface, to better fit its contents.
     */
    protected void updateSize() {
	double width = getMinimumWidth();
	double height = CIRCLE_SIZE + SPACE_BETWEEN_LINES;

	final FontMetrics fm = getFontMetrics(getFont());

	if (estereotipo != null) {
	    Rectangle2D bounds = fm.getStringBounds(estereotipo, getGraphics());
	    height += bounds.getHeight() + SPACE_BETWEEN_LINES;
	    if (bounds.getWidth() > width)
		width = bounds.getWidth();
	}
	if (nombre != null) {
	    Rectangle2D bounds = fm.getStringBounds(nombre, getGraphics());
	    height += bounds.getHeight() + SPACE_BETWEEN_LINES;
	    if (bounds.getWidth() > width)
		width = bounds.getWidth();
	}
	if (padre != null) {
	    Rectangle2D bounds = fm.getStringBounds(padre, getGraphics());
	    height += bounds.getHeight() + SPACE_BETWEEN_LINES;
	    if (bounds.getWidth() > width)
		width = bounds.getWidth();
	}

	height += LINE_WEIGHT + SPACE_BETWEEN_LINES;

	for (String str : atributos) {
	    Rectangle2D bounds = fm.getStringBounds(str, getGraphics());
	    height += bounds.getHeight() + SPACE_BETWEEN_LINES;
	    if (bounds.getWidth() > width)
		width = bounds.getWidth();
	}

	height += LINE_WEIGHT + SPACE_BETWEEN_LINES;

	for (String str : operaciones) {
	    Rectangle2D bounds = fm.getStringBounds(str, getGraphics());
	    height += bounds.getHeight() + SPACE_BETWEEN_LINES;
	    if (bounds.getWidth() > width)
		width = bounds.getWidth();
	}

	width += SPACE_BETWEEN_LINES * 2 + GrDiagrama.DEFAULT_SNAP_SIZE;
	height += GrDiagrama.DEFAULT_SNAP_SIZE;

	this.setSize((int) width, (int) height);
    }

}
