/*
 * GrAbstraccion.java
 *
 * Created on November 9, 2008, 3:58 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package vista.graficador.uml;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.omg.uml.foundation.core.Abstraction;
import org.omg.uml.foundation.core.ModelElement;
import org.omg.uml.foundation.core.Stereotype;
import org.uml.diagrammanagement.GraphEdge;
import org.uml.diagrammanagement.SemanticModelBridge;
import org.uml.diagrammanagement.Uml1SemanticModelBridge;

import util.uml.EstereotipoUtil;
import vista.graficador.ElementoVisual;
import vista.graficador.GrDiagrama;
import vista.graficador.GrLineaVisual;

/**
 * The Class GrAbstraccion.
 * 
 * @author Juan Timoteo Ponce Ortiz
 */
public final class GrAbstraccion extends GrLineaVisual {

    /**
     * Creates a new instance of GrAbstraccion.
     * 
     * @param diagrama
     *            the diagrama
     * @param edge
     *            the edge
     */
    public GrAbstraccion(GrDiagrama diagrama, GraphEdge edge) {
	super(diagrama, edge);
	setTracedline(true);
	setFatArrow2(true);
    }

    /*
     * (non-Javadoc)
     * 
     * @see vista.graficador.GrLineaVisual#updateLineaVisual()
     */
    protected void updateLineaVisual() {
	final ElementoVisual vge2 = getFin2();
	final SemanticModelBridge bridge = modelo.getSemanticModel();

	if (bridge instanceof Uml1SemanticModelBridge) {
	    final Uml1SemanticModelBridge semanticBridge = (Uml1SemanticModelBridge) bridge;
	    final ModelElement me = semanticBridge.getElement();
	    if (me instanceof Abstraction) {
		final Abstraction abstraction = (Abstraction) me;
		setTextoLinea(abstraction.getName(), "Name");
		String newStereo = "";
		String stereo = EstereotipoUtil.toString(abstraction
			.getStereotype());
		if (vge2 instanceof GrInterfaz)
		    stereo = stereotypesToString(abstraction.getStereotype());
		if (stereo != null && !stereo.trim().equals(""))
		    newStereo = "<<" + stereo + ">>";
		setTextoLinea(newStereo, "Stereotype");
	    }
	}
	if (vge2 instanceof GrInterfaz) {
	    setFatArrow2(false);
	    setTracedline(false);
	} else {
	    setFatArrow2(true);
	    setTracedline(true);
	}
    }

    /*
     * (non-Javadoc)
     * 
     * @see vista.graficador.GrLineaVisual#updateLineaLogica()
     */
    protected void updateLineaLogica() {
    }

    /*
     * (non-Javadoc)
     * 
     * @see vista.graficador.GrLineaVisual#finalizeCreacionLinea()
     */
    protected void finalizeCreacionLinea() throws Exception {
	Abstraction logicalAbstraction = null;
	try {
	    logicalAbstraction = (Abstraction) ((Uml1SemanticModelBridge) getLineaLogica()
		    .getSemanticModel()).getElement();
	} catch (Exception e) {
	}

	if (logicalAbstraction != null) {
	    Iterator iClient = logicalAbstraction.getClient().iterator();
	    Iterator iSupplier = logicalAbstraction.getSupplier().iterator();
	    ModelElement meClient = null;
	    ModelElement meSupplier = null;
	    if (!iClient.hasNext() && !iSupplier.hasNext()) {
		try {
		    meClient = ((Uml1SemanticModelBridge) getLineaLogica()
			    .getEdgeEnd1().getSemanticModel()).getElement();
		    meSupplier = ((Uml1SemanticModelBridge) getLineaLogica()
			    .getEdgeEnd2().getSemanticModel()).getElement();
		} catch (ClassCastException e) {
		    throw new Exception(
			    "Abstractions must involve Model Elements!");
		} catch (NullPointerException npe) {
		    throw new Exception(
			    "Abstractions must involve Model Elements!");
		}
	    }
	    if (meClient != null && meSupplier != null) {
		logicalAbstraction.getClient().add(meClient);
		logicalAbstraction.getSupplier().add(meSupplier);
	    }
	}
	updateLineaVisual();
    }

    /**
     * Converts the stereotypes to a single String, to be shown in this
     * abstraction.
     * 
     * @param stereotypes
     *            the stereotypes
     * 
     * @return the string
     */
    public static String stereotypesToString(Collection<Stereotype> stereotypes) {
	final List<Stereotype> excludingRealizes = new ArrayList<Stereotype>();
	for (Stereotype s : stereotypes) {
	    if (!s.getName().equals("realize"))
		excludingRealizes.add(s);
	}
	final StringBuilder ret = new StringBuilder();
	for (Iterator<Stereotype> it = excludingRealizes.iterator(); it
		.hasNext();) {
	    Stereotype stereo = it.next();
	    ret.append(stereo.getName());
	    if (it.hasNext())
		ret.append(",");
	}
	return ret.toString();
    }

}
