package vista;

import java.util.Collection;
import java.util.List;

import modelo.Proyecto;
import modelo.repositorio.Repositorio;

import org.omg.uml.foundation.core.ModelElement;
import org.omg.uml.foundation.core.Relationship;
import org.uml.diagrammanagement.Diagram;
import org.uml.diagrammanagement.DiagramManagementPackage;
import org.uml.diagrammanagement.GraphEdge;
import org.uml.diagrammanagement.GraphElement;
import org.uml.diagrammanagement.GraphNode;

import util.AppUtil;
import util.gui.CloseableTabbedPane;
import util.gui.PanelGrDiagrama;
import util.uml.ElementoVisualUtil;
import util.uml.RelacionUtil;
import util.uml.UMLUtil;
import vista.graficador.BarraTareas;
import vista.graficador.BarraTareasClase;
import vista.graficador.GrDiagrama;
import vista.graficador.GrLineaVisual;
import vista.graficador.GrNodoVisual;
import controlador.ControllerProyecto;

public final class DiagramContainerImpl implements DiagramContainer {

	private final CloseableTabbedPane tabbedPane;

	private final ControllerProyecto controller;

	private final Vista propertyViewer;

	private final Proyecto project;

	private final Repositorio repository;

	public DiagramContainerImpl(final Repositorio repository,
			final Proyecto project, final CloseableTabbedPane tabbedPane,
			final ControllerProyecto controller, final Vista propertyViewer) {
		this.repository = repository;
		this.project = project;
		this.tabbedPane = tabbedPane;
		this.controller = controller;
		this.propertyViewer = propertyViewer;
	}

	public CloseableTabbedPane getTabbedPane() {
		return tabbedPane;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see vista.DiagramContainer#getGraficador(int)
	 */
	public ControllerProyecto getController() {
		return controller;
	}

	public Vista getPropertyViewer() {
		return propertyViewer;
	}

	@Override
	public GrDiagrama getGraficador(final int index) {
		if (index >= 0 && index <= getTabbedPane().getTabComponentCount()) {
			final PanelGrDiagrama aux = (PanelGrDiagrama) getTabbedPane()
			.getTabComponentAt(index);
			return aux.getDiagrama();
		}
		return null;
	}

	/**
	 * Diagram contains.
	 * 
	 * @param diagrama
	 *            the diagrama
	 * 
	 * @return true, if diagram contains
	 */
	@Override
	public boolean diagramContains(final Diagram diagrama) {
		return (indexOf(diagrama) != -1);
	}

	/**
	 * Adds the graficador.
	 * 
	 * @param diagrama
	 *            the diagrama
	 * 
	 * @return true, if adds the graficador
	 */
	@Override
	public boolean addGraficador(final Diagram diagrama) {
		final CloseableTabbedPane tabCenter = getTabbedPane();

		if (!diagramContains(diagrama)) {
			final DiagramManagementPackage diagramManagement = repository
			.getUmlPackage().getDiagramManagement();
			final GrDiagrama graf = new GrDiagrama(getController(), diagrama,
					diagramManagement);
			graf.setPropertyViewer(propertyViewer);
			final BarraTareas barra = new BarraTareasClase(project.getModelo()
					.getNamespace(), graf, diagramManagement);

			final PanelGrDiagrama panel = PanelGrDiagrama.newInstance(barra,
					graf);
			tabCenter.addTab(graf.getTitulo(), panel);
			tabCenter.setSelectedTab(panel);
			//
			getController().addView(graf);
			graf.updateVista();

			return true;
		}
		return false;
	}

	/**
	 * Removes the graficador.
	 * 
	 * @param diagrama
	 *            the diagrama
	 * 
	 * @return true, if removes the graficador
	 */
	@Override
	public boolean removeGraficador(final Diagram diagrama) {
		final int index = indexOf(diagrama);
		if (index != -1) {
			getController().removeView(getGraficador(index));
			getTabbedPane().remove(index);
			return true;
		}
		return false;
	}

	/**
	 * Checks if is showed.
	 * 
	 * @param diagrama
	 *            the diagrama
	 * 
	 * @return true, if checks if is showed
	 */
	@Override
	public boolean isShowed(final Diagram diagrama) {
		final int index = getTabbedPane().getSelectedIndexTab();
		return (indexOf(diagrama) == index);
	}

	/**
	 * Sets the show.
	 * 
	 * @param diagrama
	 *            the diagrama
	 */
	@Override
	public void setShow(final Diagram diagrama) {
		final int index = indexOf(diagrama);
		if (index != -1) {
			getTabbedPane().setSelectedIndexTab(index);
		} else {
			addGraficador(diagrama);
		}
	}

	/**
	 * Adds the elemento to diagram.
	 * 
	 * @param elemento
	 *            the elemento
	 */
	@Override
	public void addElementoToDiagram(final ModelElement elemento) {
		final PanelGrDiagrama aux = (PanelGrDiagrama) getTabbedPane()
		.getSelectedTab();
		final GrDiagrama diag = aux.getDiagrama();

		diag.cancelCreacionLineaVisual();
		diag.cancelCreacionNodoVisual();

		final GraphNode graphNode = UMLUtil.crearGraphNode(elemento);
		diag.setCrearNodoVisual(graphNode);
	}

	/**
	 * Index of.
	 * 
	 * @param diagrama
	 *            the diagrama
	 * 
	 * @return the int
	 */
	public int indexOf(final Diagram diagrama) {
		final CloseableTabbedPane tabCenter = getTabbedPane();
		for (int i = 0; i < tabCenter.getTabComponentCount(); i++) {
			final PanelGrDiagrama aux = (PanelGrDiagrama) tabCenter
			.getTabComponentAt(i);
			final GrDiagrama diag = aux.getDiagrama();
			if (diag.getDiagrama().equals(diagrama)) {
				return i;
			}

		}
		return -1;
	}

	// TODO TENES QUE USARLO!
	/*
	 * (non-Javadoc)
	 * 
	 * @seevista.DiagramContainer#findRelaciones(org.omg.uml.foundation.core.
	 * ModelElement)
	 */
	@Override
	public void findRelaciones(final ModelElement elemento) throws Exception {
		if (!(elemento instanceof Relationship)
				&& getTabbedPane().getSelectedIndexTab() != -1) {
			boolean modified = false;
			final GrDiagrama diagramer = getGraficador(getTabbedPane()
					.getSelectedIndexTab());

			final List<Relationship> relations = RelacionUtil
			.getAllRelaciones();
			final Diagram current = diagramer.getDiagram();
			final GraphNode elementNode = (GraphNode) ElementoVisualUtil
			.getGraphElement(elemento, current);
			final GrNodoVisual nodoVisual = (GrNodoVisual) diagramer
			.getElementoVisual(elementNode);

			diagramer.setUpdateModel(false);

			for (final Relationship rel : relations) {
				if (!contains(current, rel)) {
					final ModelElement end1 = RelacionUtil
					.getAssociationParticipant(rel, 0);
					final ModelElement end2 = RelacionUtil
					.getAssociationParticipant(rel, 1);

					if (end1 != null && end1.equals(elemento)) {
						GraphNode grNode = (GraphNode) ElementoVisualUtil
						.getGraphElement(end2, current);
						GrNodoVisual node = null;
						if (grNode == null) {
							grNode = UMLUtil.crearGraphNode(end2);
							node = UMLUtil.instanceGrNodoVisual(diagramer,
									grNode);
							diagramer.addNewNodo(node);
						}
						if (node == null) {
							node = (GrNodoVisual) diagramer
							.getElementoVisual(grNode);
						}

						final GraphEdge edge = UMLUtil.crearGraphEdge(rel);
						edge.setEdgeEnd1(elementNode);
						edge.setEdgeEnd2(grNode);

						final GrLineaVisual line = UMLUtil
						.instanceGrLineaVisual(diagramer, edge);
						line.setFin1(nodoVisual);
						line.setFin2(node);
						line.addPoint(nodoVisual.getCenter());
						line.addPoint(node.getCenter());
						diagramer.addNewLinea(line);
						modified = true;
					} else if (end2 != null && end2.equals(elemento)
							&& contains(current, end1)) {
						GraphNode grNode = (GraphNode) ElementoVisualUtil
						.getGraphElement(end1, current);
						GrNodoVisual node = null;
						if (grNode == null) {
							grNode = UMLUtil.crearGraphNode(end1);
							node = UMLUtil.instanceGrNodoVisual(diagramer,
									grNode);
							diagramer.addNewNodo(node);
						}
						if (node == null) {
							node = (GrNodoVisual) diagramer
							.getElementoVisual(grNode);
						}

						final GraphEdge edge = UMLUtil.crearGraphEdge(rel);
						edge.setEdgeEnd1(grNode);
						edge.setEdgeEnd2(elementNode);

						final GrLineaVisual line = UMLUtil
						.instanceGrLineaVisual(diagramer, edge);
						line.setFin1(node);
						line.setFin2(nodoVisual);
						line.addPoint(node.getCenter());
						line.addPoint(nodoVisual.getCenter());
						diagramer.addNewLinea(line);
						modified = true;
					}
				}
			}
			if (modified) {
				diagramer.updateVista();
			}
			diagramer.setUpdateModel(true);
		} else {
			AppUtil.showAdvertencia(null, "Debe tener un diagrama activo");
		}

	}

	/**
	 * Contains.
	 * 
	 * @param diagram
	 *            the diagram
	 * @param element
	 *            the element
	 * 
	 * @return true, if successful
	 */
	@SuppressWarnings("unchecked")
	private boolean contains(final Diagram diagram, final ModelElement element) {
		final Collection<GraphElement> graphElements = diagram.getContained();
		for (final GraphElement item : graphElements) {
			if (item instanceof GraphNode) {
				final ModelElement temp = ElementoVisualUtil
				.getElementoLogico(item);
				if (temp.equals(element)) {
					return true;
				}
			}
		}
		return false;
	}

}
