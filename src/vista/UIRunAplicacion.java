package vista;

import java.awt.Container;
import java.awt.Dialog;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.Enumeration;
import java.util.Hashtable;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.SwingWorker;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;

import org.apache.log4j.Logger;
import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.DefaultLogger;
import org.apache.tools.ant.Project;

import util.AppUtil;
import util.Constantes;
import util.PropertyTableModel;
import util.file.FileOperator;

import com.jgoodies.forms.factories.DefaultComponentFactory;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.uif_lite.panel.SimpleInternalFrame;

// TODO: Auto-generated Javadoc
/**
 * The Class UIRunAplicacion.
 * 
 * @author Timo
 */
public class UIRunAplicacion extends JDialog {

    private static Logger log = Logger.getLogger(UIRunAplicacion.class);

    /** The ant project path. */
    private String antProjectPath;

    /** The ant project. */
    private Project antProject;

    /** The table model. */
    private PropertyTableModel tableModel;

    /** The console logger. */
    private DefaultLogger consoleLogger;

    /**
     * Instantiates a new uI run aplicacion.
     * 
     * @param owner
     *            the owner
     */
    public UIRunAplicacion(Frame owner) {
	super(owner);
	initComponents();
    }

    /**
     * Instantiates a new uI run aplicacion.
     * 
     * @param owner
     *            the owner
     */
    public UIRunAplicacion(Dialog owner) {
	super(owner);
	initComponents();
    }

    /**
     * Gets the ant project.
     * 
     * @return the ant project
     */
    public Project getAntProject() {
	return antProject;
    }

    /**
     * Sets the ant project.
     * 
     * @param antProject
     *            the new ant project
     */
    public void setAntProject(Project antProject) {
	this.antProject = antProject;
    }

    /**
     * Gets the ant project path.
     * 
     * @return the ant project path
     */
    public String getAntProjectPath() {
	return antProjectPath;
    }

    /**
     * Sets the ant project path.
     * 
     * @param antProjectPath
     *            the new ant project path
     */
    public void setAntProjectPath(String antProjectPath) {
	this.antProjectPath = antProjectPath;
    }

    /**
     * Inits the.
     */
    public void init() {
	File buildFile = null;
	if (!antProjectPath.endsWith("build.xml"))
	    buildFile = new File(antProjectPath + "/build.xml");
	else
	    buildFile = new File(antProjectPath);
	if (buildFile.exists()) {
	    antProject = FileOperator.loadBuildFile(buildFile);
	}
    }

    /**
     * Load properties.
     */
    public void loadProperties() {
	if (antProject == null) {
	    log.error("Null Ant project");
	    return;
	}
	txtBuildFile.setText(antProjectPath);
	txtLibPath.setText(antProject
		.getProperty(Constantes.COMMON_LIB_PROPERTY));
	txtDescripcion.setText(antProject.getDescription());

	Hashtable props = antProject.getTargets();
	comboTarget.removeAllItems();
	for (Enumeration en = props.keys(); en.hasMoreElements();) {
	    comboTarget.addItem(props.get(en.nextElement()));
	}

	props = antProject.getProperties();
	if (tableModel == null) {
	    tableModel = new PropertyTableModel(props);
	    tableProperties.setModel(tableModel);
	} else {
	    tableModel.setProperties(props);
	}
    }

    /**
     * Btn select build file action performed.
     * 
     * @param e
     *            the e
     */
    private void btnSelectBuildFileActionPerformed() {
	selectBuildFile();
    }

    /**
     * Btn select lib path action performed.
     * 
     * @param e
     *            the e
     */
    private void btnSelectLibPathActionPerformed(ActionEvent e) {
	File path = AppUtil.showAbrirCarpeta(this, "Direccion de librerias",
		getAntProjectPath());
	if (path != null) {
	    txtLibPath.setText(path.getPath());
	}
    }

    /**
     * Btn execute action performed.
     * 
     * @param e
     *            the e
     */
    private void btnExecuteActionPerformed() {
	executeTarget();
    }

    /**
     * Btn close action performed.
     * 
     * @param e
     *            the e
     */
    private void btnCloseActionPerformed() {
	dispose();
    }

    /**
     * Execute target.
     */
    private void executeTarget() {
	txtOutStream.setText("");
	ExecuteThread thread = new ExecuteThread();
	thread.execute();
    }

    /**
     * Inits the components.
     */
    private void initComponents() {
	// JFormDesigner - Component initialization - DO NOT
	// MODIFY//GEN-BEGIN:initComponents
	DefaultComponentFactory compFactory = DefaultComponentFactory
		.getInstance();
	panelFile = new SimpleInternalFrame();
	lblFile = compFactory.createLabel("Fichero");
	txtBuildFile = new JTextField();
	btnSelectBuildFile = new JButton();
	lblLibPath = compFactory.createLabel("Librerias");
	txtLibPath = new JTextField();
	btnSelectLibPath = new JButton();
	panelAnt = new SimpleInternalFrame();
	lblTarget = compFactory.createLabel("Objetivo");
	comboTarget = new JComboBox();
	btnExecute = new JButton();
	lblDescription = compFactory.createLabel("Descripcion");
	scrollPane2 = new JScrollPane();
	txtDescripcion = new JTextArea();
	separatorProperties = compFactory.createSeparator("Propiedades");
	panelTable = new JScrollPane();
	tableProperties = new JTable();
	outputSeparator = compFactory.createSeparator("Salida");
	panelLog = new JScrollPane();
	txtOutStream = new JTextPane();
	panelOperations = new JPanel();
	btnClose = new JButton();
	CellConstraints cc = new CellConstraints();

	// ======== this ========
	setTitle("Ejecutar build file");
	Container contentPane = getContentPane();
	contentPane.setLayout(new FormLayout(
		"12dlu, 301dlu:grow, $lcgap, 9dlu",
		"63dlu, $lgap, 125dlu:grow, $lgap, default"));

	// ======== panelFile ========
	{
	    panelFile.setTitle("Build file");
	    Container panelFileContentPane = panelFile.getContentPane();
	    panelFileContentPane.setLayout(new FormLayout(
		    "53dlu, $lcgap, 239dlu, $lcgap, 22dlu",
		    "default, $lgap, default"));

	    // ---- lblFile ----
	    lblFile.setLabelFor(txtBuildFile);
	    panelFileContentPane.add(lblFile, cc.xywh(1, 1, 1, 1,
		    CellConstraints.RIGHT, CellConstraints.DEFAULT));

	    // ---- txtBuildFile ----
	    txtBuildFile.setEditable(false);
	    panelFileContentPane.add(txtBuildFile, cc.xy(3, 1));

	    // ---- btnSelectBuildFile ----
	    btnSelectBuildFile.setIcon(new ImageIcon(getClass().getResource(
		    "/iconos/search-small.png")));
	    btnSelectBuildFile.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
		    btnSelectBuildFileActionPerformed();
		}
	    });
	    panelFileContentPane.add(btnSelectBuildFile, cc.xy(5, 1));

	    // ---- lblLibPath ----
	    lblLibPath.setLabelFor(txtLibPath);
	    panelFileContentPane.add(lblLibPath, cc.xywh(1, 3, 1, 1,
		    CellConstraints.RIGHT, CellConstraints.DEFAULT));

	    // ---- txtLibPath ----
	    txtLibPath.setEditable(false);
	    panelFileContentPane.add(txtLibPath, cc.xy(3, 3));

	    // ---- btnSelectLibPath ----
	    btnSelectLibPath.setIcon(new ImageIcon(getClass().getResource(
		    "/iconos/search-small.png")));
	    btnSelectLibPath.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
		    btnSelectLibPathActionPerformed(e);
		}
	    });
	    panelFileContentPane.add(btnSelectLibPath, cc.xy(5, 3));
	}
	contentPane.add(panelFile, cc.xywh(2, 1, 1, 1, CellConstraints.DEFAULT,
		CellConstraints.FILL));

	// ======== panelAnt ========
	{
	    panelAnt.setTitle("Ant tasks");
	    Container panelAntContentPane = panelAnt.getContentPane();
	    panelAntContentPane
		    .setLayout(new FormLayout(
			    "5dlu, $lcgap, 53dlu, $lcgap, 125dlu, $lcgap, 19dlu:grow, $lcgap, 69dlu, $lcgap, 9dlu",
			    "default, $lgap, 25dlu, $lgap, default, $lgap, 71dlu, $lgap, default, $lgap, 74dlu"));

	    // ---- lblTarget ----
	    lblTarget.setLabelFor(comboTarget);
	    panelAntContentPane.add(lblTarget, cc.xywh(3, 1, 1, 1,
		    CellConstraints.RIGHT, CellConstraints.DEFAULT));
	    panelAntContentPane.add(comboTarget, cc.xy(5, 1));

	    // ---- btnExecute ----
	    btnExecute.setText("Ejecutar");
	    btnExecute.setIcon(new ImageIcon(getClass().getResource(
		    "/iconos/dialog-apply.png")));
	    btnExecute.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
		    btnExecuteActionPerformed();
		}
	    });
	    panelAntContentPane.add(btnExecute, cc.xy(9, 1));

	    // ---- lblDescription ----
	    lblDescription.setLabelFor(txtDescripcion);
	    panelAntContentPane.add(lblDescription, cc.xywh(3, 3, 1, 1,
		    CellConstraints.RIGHT, CellConstraints.DEFAULT));

	    // ======== scrollPane2 ========
	    {

		// ---- txtDescripcion ----
		txtDescripcion.setEditable(false);
		scrollPane2.setViewportView(txtDescripcion);
	    }
	    panelAntContentPane.add(scrollPane2, cc.xywh(5, 3, 5, 1,
		    CellConstraints.DEFAULT, CellConstraints.FILL));
	    panelAntContentPane.add(separatorProperties, cc.xywh(3, 5, 7, 1));

	    // ======== panelTable ========
	    {
		panelTable.setViewportView(tableProperties);
	    }
	    panelAntContentPane.add(panelTable, cc.xywh(3, 7, 7, 1));
	    panelAntContentPane.add(outputSeparator, cc.xywh(3, 9, 7, 1));

	    // ======== panelLog ========
	    {
		panelLog.setViewportView(txtOutStream);
	    }
	    panelAntContentPane.add(panelLog, cc.xywh(3, 11, 7, 1,
		    CellConstraints.FILL, CellConstraints.FILL));
	}
	contentPane.add(panelAnt, cc.xywh(2, 3, 1, 1, CellConstraints.DEFAULT,
		CellConstraints.FILL));

	// ======== panelOperations ========
	{
	    panelOperations.setLayout(new FormLayout(
		    "default:grow, $lcgap, 107dlu, $lcgap, default:grow",
		    "17dlu"));

	    // ---- btnClose ----
	    btnClose.setText("Cerrar");
	    btnClose.setIcon(new ImageIcon(getClass().getResource(
		    "/iconos/window-close.png")));
	    btnClose.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
		    btnCloseActionPerformed();
		}
	    });
	    panelOperations.add(btnClose, cc.xywh(3, 1, 1, 1,
		    CellConstraints.FILL, CellConstraints.DEFAULT));
	}
	contentPane.add(panelOperations, cc.xywh(1, 5, 4, 1));
	setSize(555, 545);
	setLocationRelativeTo(getOwner());
	// initialization//GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT
    // MODIFY//GEN-BEGIN:variables
    private SimpleInternalFrame panelFile;
    private JLabel lblFile;
    private JTextField txtBuildFile;
    private JButton btnSelectBuildFile;
    private JLabel lblLibPath;
    private JTextField txtLibPath;
    private JButton btnSelectLibPath;
    private SimpleInternalFrame panelAnt;
    private JLabel lblTarget;
    private JComboBox comboTarget;
    private JButton btnExecute;
    private JLabel lblDescription;
    private JScrollPane scrollPane2;
    private JTextArea txtDescripcion;
    private JComponent separatorProperties;
    private JScrollPane panelTable;
    private JTable tableProperties;
    private JComponent outputSeparator;
    private JScrollPane panelLog;
    private JTextPane txtOutStream;
    private JPanel panelOperations;
    private JButton btnClose;

    // JFormDesigner - End of variables declaration//GEN-END:variables
    /**
     * Select build file.
     */
    private void selectBuildFile() {
	File f = AppUtil.showAbrirCarpeta(this, "Seleccionar build file", "");
	if (f != null) {
	    setAntProjectPath(f.getPath());
	    init();
	    loadProperties();
	}
    }

    /**
     * Update text pane.
     * 
     * @param text
     *            the text
     */
    private void updateTextPane(final String text) {
	/*
	 * SwingUtilities.invokeLater(new Runnable() { public void run() {
	 */
	Document doc = txtOutStream.getDocument();
	try {
	    doc.insertString(doc.getLength(), text, null);
	} catch (BadLocationException e) {
	    throw new RuntimeException(e);
	}
	txtOutStream.setCaretPosition(doc.getLength() - 1);
	/*
	 * } });
	 */
    }

    /**
     * The Class ExecuteThread.
     */
    class ExecuteThread extends SwingWorker<Void, Void> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.SwingWorker#doInBackground()
	 */
	@Override
	protected Void doInBackground() throws Exception {
	    String target = comboTarget.getSelectedItem().toString();
	    if (consoleLogger == null) {
		consoleLogger = new DefaultLogger();
		OutputStream os = new OutputStream() {

		    @Override
		    public void write(final int b) throws IOException {
			updateTextPane(String.valueOf((char) b));
		    }

		    @Override
		    public void write(byte[] b, int off, int len)
			    throws IOException {
			updateTextPane(new String(b, off, len));
		    }

		    @Override
		    public void write(byte[] b) throws IOException {
			write(b, 0, b.length);
		    }
		};
		consoleLogger.setErrorPrintStream(new PrintStream(os));
		consoleLogger.setOutputPrintStream(new PrintStream(os));
		consoleLogger.setMessageOutputLevel(Project.MSG_VERBOSE);
		antProject.addBuildListener(consoleLogger);

		try {
		    antProject.executeTarget(target);
		    antProject.fireBuildFinished(null);
		} catch (BuildException e) {
		    antProject.fireBuildFinished(e);
		}
	    }
	    consoleLogger = null;
	    return null;
	}
    };

}
