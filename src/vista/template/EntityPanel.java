/*
 * Created by JFormDesigner on Thu May 28 10:43:46 BOT 2009
 */

package vista.template;

import java.awt.*;
import javax.swing.*;
import com.jgoodies.forms.layout.*;
import com.jgoodies.uif_lite.panel.*;

/**
 * @author Timoteo Ponce
 */
public class EntityPanel extends JPanel {
	public EntityPanel() {
		initComponents();
		initBindings();        
        updateTable();
	}

	private void updateTable() {
		// TODO Auto-generated method stub
		
	}

	private void initBindings() {
		// TODO Auto-generated method stub
		
	}

	private void initComponents() {
		// JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
		panelAttributes = new SimpleInternalFrame();
		panelTable = new JScrollPane();
		table = new JTable();
		CellConstraints cc = new CellConstraints();

		//======== this ========
		setLayout(new FormLayout(
			"default, $lcgap, default:grow, $lcgap, default",
			"default, $lgap, fill:default:grow, $lgap, default:grow, $lgap, default"));

		//======== panelAttributes ========
		{
			panelAttributes.setTitle("Atributos");
			Container panelAttributesContentPane = panelAttributes.getContentPane();
			panelAttributesContentPane.setLayout(new FlowLayout());
		}
		add(panelAttributes, cc.xy(3, 3));

		//======== panelTable ========
		{
			panelTable.setViewportView(table);
		}
		add(panelTable, cc.xy(3, 5));
		// JFormDesigner - End of component initialization  //GEN-END:initComponents
	}

	// JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
	private SimpleInternalFrame panelAttributes;
	private JScrollPane panelTable;
	private JTable table;
	// JFormDesigner - End of variables declaration  //GEN-END:variables
}
