/*
 * Created by JFormDesigner on Sun Jun 07 19:58:29 BOT 2009
 */

package vista;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileFilter;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JToolBar;
import javax.swing.WindowConstants;

import modelo.types.TokenMakerType;

import org.apache.log4j.Logger;

import util.AppUtil;
import util.file.FileOperator;

import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import com.sun.jedit.syntax.JEditTextArea;
import com.sun.jedit.syntax.TokenMarker;

/**
 * The Class UICodeViewer.
 * 
 * @author Juan Timoteo Ponce Ortiz
 */
public class UICodeViewer extends JFrame {

	private static final Logger LOG = Logger.getLogger(UICodeViewer.class);

	/** The root folder. */
	private String rootFolder;

	/** The type. */
	private TokenMakerType type;

	/** The token maker. */
	private TokenMarker tokenMaker;

	/** The text area. */
	private final JEditTextArea textArea = new JEditTextArea();

	/**
	 * Instantiates a new uI code viewer.
	 */
	private UICodeViewer() {
		initComponents();
		init();
		CellConstraints cc = new CellConstraints();
		mainPanel.add(textArea, cc.xy(1, 1));
	}

	public static UICodeViewer newInstance() {
		return new UICodeViewer();
	}

	/**
	 * Inits the.
	 */
	private void init() {
	}

	/**
	 * Btn path action performed.
	 * 
	 * @param e
	 *            the e
	 */
	private void btnPathActionPerformed() {
		setPath();
	}

	/**
	 * Sets the path.
	 */
	private void setPath() {
		File file = AppUtil.showAbrirCarpeta(this, this.getClass()
				.getSimpleName(), null);
		if (file != null) {
			setRootFolder(file.getPath());
		}
	}

	/**
	 * Btn load action performed.
	 * 
	 * @param e
	 *            the e
	 */
	private void btnLoadActionPerformed() {
		reloadFiles();
	}

	/**
	 * Reload files.
	 */
	private void reloadFiles() {
		loadFiles();
	}

	/**
	 * File combo action performed.
	 * 
	 * @param e
	 *            the e
	 */
	private void fileComboActionPerformed() {
		selectDocument(fileCombo.getSelectedIndex());
	}

	/**
	 * Inits the components.
	 */
	private void initComponents() {
		// JFormDesigner - Component initialization - DO NOT MODIFY
		// //GEN-BEGIN:initComponents
		toolBar = new JToolBar();
		txtPath = new JTextField();
		btnPath = new JButton();
		btnLoad = new JButton();
		fileCombo = new JComboBox();
		mainPanel = new JPanel();
		CellConstraints cc = new CellConstraints();

		// ======== this ========
		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		setTitle("Visor de c\u00f3digo");
		Container contentPane = getContentPane();
		contentPane.setLayout(new FormLayout(
				"default, $lcgap, 95dlu:grow, $lcgap, default",
		"2*(default, $lgap), default:grow"));

		// ======== toolBar ========
		{

			// ---- txtPath ----
			txtPath.setEditable(false);
			toolBar.add(txtPath);

			// ---- btnPath ----
			btnPath.setIcon(new ImageIcon(getClass().getResource(
			"/iconos/fileopen.png")));
			btnPath.addActionListener(new ActionListener() {
				public void actionPerformed(final ActionEvent e) {
					btnPathActionPerformed();
				}
			});
			toolBar.add(btnPath);

			// ---- btnLoad ----
			btnLoad.setIcon(new ImageIcon(getClass().getResource(
			"/iconos/gnome-session-switch.png")));
			btnLoad.addActionListener(new ActionListener() {
				public void actionPerformed(final ActionEvent e) {
					btnLoadActionPerformed();
				}
			});
			toolBar.add(btnLoad);
		}
		contentPane.add(toolBar, cc.xy(3, 1));

		// ---- fileCombo ----
		fileCombo.addActionListener(new ActionListener() {
			public void actionPerformed(final ActionEvent e) {
				fileComboActionPerformed();
			}
		});
		contentPane.add(fileCombo, cc.xy(3, 3));

		// ======== mainPanel ========
		{
			mainPanel.setLayout(new FormLayout("default:grow",
			"fill:default:grow"));
		}
		contentPane.add(mainPanel, cc.xywh(3, 5, 2, 1, CellConstraints.FILL,
				CellConstraints.FILL));
		setSize(560, 455);
		setLocationRelativeTo(getOwner());
		// //GEN-END:initComponents
	}

	// JFormDesigner - Variables declaration - DO NOT MODIFY
	// //GEN-BEGIN:variables
	private JToolBar toolBar;
	private JTextField txtPath;
	private JButton btnPath;
	private JButton btnLoad;
	private JComboBox fileCombo;
	private JPanel mainPanel;

	// JFormDesigner - End of variables declaration //GEN-END:variables

	/**
	 * Gets the root folder.
	 * 
	 * @return the root folder
	 */
	public String getRootFolder() {
		return rootFolder;
	}

	/**
	 * Sets the root folder.
	 * 
	 * @param rootFolder
	 *            the new root folder
	 */
	public void setRootFolder(final String rootFolder) {
		this.rootFolder = rootFolder;
		txtPath.setText(rootFolder);
	}

	/**
	 * Gets the type.
	 * 
	 * @return the type
	 */
	//public TokenMakerType getType() {
	//	return type;
	//}

	/**
	 * Sets the type.
	 * 
	 * @param type
	 *            the new type
	 */
	public void setType(final TokenMakerType type) {
		this.type = type;
		init();
	}

	/**
	 * Gets the token marker.
	 * 
	 * @return the token marker
	 */
	public TokenMarker getTokenMarker() {
		if (tokenMaker == null) {
			try {
				tokenMaker = (TokenMarker) AppUtil.classForName(
						type.toString(), null);
			} catch (Exception e) {
				LOG.error(e, e);
			}
		}
		return tokenMaker;
	}

	/**
	 * Load files.
	 */
	public void loadFiles() {
		FileFilter filter = AppUtil.createFileFilter(type.getExtension());
		assert rootFolder != null;

		fileCombo.removeAllItems();
		File folder = new File(rootFolder);
		addFile(folder, filter);
		if (fileCombo.getItemCount() > 0) {
			selectDocument(0);
		}
	}

	/**
	 * Select document.
	 * 
	 * @param index
	 *            the index
	 */
	private void selectDocument(final int index) {
		String fileName = fileCombo.getItemAt(index).toString();
		String content = FileOperator.loadFile(fileName);
		textArea.setTokenMarker(getTokenMarker());
		textArea.setText(content);
		textArea.updateUI();
	}

	/**
	 * Adds the file.
	 * 
	 * @param file
	 *            the file
	 */
	private void addFile(final File file, final FileFilter filter) {
		if (file.isDirectory()) {
			File[] files = file.listFiles(filter);

			for (File itemFile : files) {
				addFile(itemFile, filter);
			}
		} else {
			fileCombo.addItem(file.getPath());
		}
	}

	/*
	 * public static void main(String[] args) { JFrame frame = new JFrame();
	 * UICodeViewer dialog = new UICodeViewer();
	 * dialog.setType(TokenMakerType.JAVA); dialog.setVisible(true); }
	 */
}
