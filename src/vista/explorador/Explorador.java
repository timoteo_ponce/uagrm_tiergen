package vista.explorador;

/*
 * Explorador.java
 *
 * Created on 20 de octubre de 2008, 14:32
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.List;

import javax.jmi.reflect.RefObject;
import javax.swing.ImageIcon;
import javax.swing.JTree;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;

import modelo.repositorio.Repositorio;
import modelo.types.PropertyType;

import org.apache.log4j.Logger;
import org.omg.uml.behavioralelements.commonbehavior.Action;
import org.omg.uml.behavioralelements.statemachines.Event;
import org.omg.uml.foundation.core.AssociationClass;
import org.omg.uml.foundation.core.Classifier;
import org.omg.uml.foundation.core.Feature;
import org.omg.uml.foundation.core.Method;
import org.omg.uml.foundation.core.ModelElement;
import org.omg.uml.foundation.core.Namespace;
import org.omg.uml.foundation.core.Relationship;
import org.omg.uml.foundation.core.Stereotype;
import org.omg.uml.foundation.core.TagDefinition;
import org.omg.uml.modelmanagement.Model;
import org.uml.diagrammanagement.Diagram;

import util.uml.RelacionUtil;
import vista.DiagramContainer;
import vista.Vista;
import controlador.ControllerProyecto;

/**
 * Arbol de exploracion para los elementos del modelo.
 * 
 * @author Juan Timoteo Ponce Ortiz
 */
public final class Explorador extends JTree implements Vista {

	/** The log. */
	private static Logger log = Logger.getLogger(Explorador.class);

	/** Lista utilizada para recordar el estado del arbol. */
	private List<Object> elementosExpandidos;

	/** Modelo a representar. */
	private Model modelo;

	/** The contenedor. */
	private DiagramContainer contenedor;

	/** The controller. */
	private ControllerProyecto controller;

	private final Repositorio repository;

	/**
	 * Creates a new instance of Explorador.
	 * 
	 * @param controller the controller
	 * @param contenedor the contenedor
	 */
	/**
	 * Constructor por defecto.
	 * 
	 * @see #initEvents()
	 */
	public Explorador(final Repositorio repository,
			final ControllerProyecto controller,
			final DiagramContainer contenedor) {
		this.repository = repository;
		this.contenedor = contenedor;
		this.controller = controller;
		setEditable(false);
		setModel(new DefaultTreeModel(null));
		setCellRenderer(new ExploradorRenderer());
		init();
	}

	/**
	 * Inicializa el componente, y sus elementos.
	 */
	private void init() {
		addMouseListener(new ExploradorListener());
	}

	/**
	 * Actualiza todos los nodos del arbol.
	 */
	public void actualizarTodo() {
		final NodoExplorador root = (NodoExplorador) getModel().getRoot();
		final Enumeration<NodoExplorador> nodos = root.preorderEnumeration();
		while (nodos.hasMoreElements()) {
			final NodoExplorador nodo = nodos.nextElement();
			nodo.actualizarTexto();
			((DefaultTreeModel) getModel()).nodeChanged(nodo);
		}
	}

	/**
	 * Registra cada nodo expandido en el arbol, esto se hace para recordar su
	 * estado en la proxima actualizacion.
	 */
	private void guardarExpandidos() {
		final Enumeration e = getExpandedDescendants(getPathForRow(0));
		if (e != null) {
			while (e.hasMoreElements()) {
				TreePath tp = (TreePath) e.nextElement();
				final NodoExplorador nodo = (NodoExplorador) tp.getLastPathComponent();
				final ModelElement me = nodo.getElemento();
				if (me != null) {
					elementosExpandidos.add(me);
				}
			}
		}
	}

	/**
	 * Restaura el estado de un nodo especifico(si esta expandido o no).
	 * 
	 * @param nodo Nodo a recordar
	 */
	private void recordarExpandidos(final NodoExplorador nodo) {
		try {
			final Enumeration<NodoExplorador> hijo = nodo.children();
			if (hijo != null) {
				while (hijo.hasMoreElements()) {
					final NodoExplorador tempNodo = hijo.nextElement();
					final ModelElement tempEle = tempNodo.getElemento();
					if (tempEle != null && elementosExpandidos.contains(tempEle)) {
						final TreePath tp = new TreePath(((DefaultTreeModel) getModel()).getPathToRoot(tempNodo));
						expandPath(tp);
						recordarExpandidos(tempNodo);
					}
				}
			}
		} catch (Exception e) {
			log.error(e.getMessage());
		}
	}

	/**
	 * Busca el nodo representando un elemento especifico.
	 * 
	 * @param root nodo raiz
	 * @param elemento elemento buscado
	 * 
	 * @return nodo representador del elemento.
	 */
	public NodoExplorador getNodo(final NodoExplorador root,final  Object elemento) {
		final Enumeration<NodoExplorador> nodos = root.depthFirstEnumeration();
		while (nodos.hasMoreElements()) {
			final NodoExplorador nodo = nodos.nextElement();
			if (elemento instanceof ModelElement) {
				if (nodo.getElemento() != null && nodo.getElemento().equals(elemento)) {
					return nodo;
				}
			} else if (elemento instanceof Diagram) {
				if (nodo.getDiagrama() != null && nodo.getDiagrama().equals(elemento)) {
					return nodo;
				}
			}
		}
		return null;
	}

	/**
	 * Actualiza un nodo, agregando sus hojas y subhojas, una vez agregado, se
	 * agregan las propiedades de este como subhojas(no completado). El elemento
	 * se agrega de forma diferente que una relacion
	 * 
	 * @param elemento elemento a agregar
	 * @param padre nodo padre
	 */
	private void actualizar(final ModelElement elemento,final  NodoExplorador padre) {
		if (filtrar(elemento)) {
			final NodoExplorador nodo = new NodoExplorador(elemento, this);
			if (elemento instanceof Relationship) {
				final NodoExplorador temp = new NodoExplorador(elemento, this);
				RelacionUtil.addNodosRelacion(temp);
				if (elemento instanceof AssociationClass) {
					padre.add(temp);
				} else {
					padre.addRelacion(temp);
				}
			} else {
				padre.add(nodo);
			}
			if (elemento instanceof Classifier) {
				final Classifier classifier = (Classifier) elemento;				

				for (final Object elem : classifier.getFeature()) {
					final Feature feature = (Feature) elem;
					if (!(feature instanceof Method)) {
						final NodoExplorador temp = new NodoExplorador(feature, this);
						nodo.add(temp);
					}
				}
			}
			if (elemento instanceof Namespace) {
				final Namespace namespaceElement = (Namespace) elemento;

				for (final Object elem : namespaceElement.getOwnedElement()) {
					if (elem instanceof ModelElement) {
						actualizar((ModelElement) elem, nodo);
					}
				}
			}
		}
	}

	/**
	 * Retorna el elemento raiz del modelo(de proyecto).
	 * 
	 * @return modelo raiz
	 */
	protected Namespace getElementoRoot() {
		return modelo;
	}

	/**
	 * Gets the contenedor.
	 * 
	 * @return the contenedor
	 */
	public DiagramContainer getContenedor() {
		return contenedor;
	}

	/**
	 * Filtra los elementos que seran incluidos en el arbol.
	 * 
	 * @param ro elemento a filtrar
	 * 
	 * @return su estado de inclusion.
	 */
	protected boolean filtrar(final RefObject ro) {
		if (ro instanceof Stereotype || ro instanceof TagDefinition || ro instanceof Action || ro instanceof Event) {
			return false;
		}
		return true;
	}

	/**
	 * Asigna el modelo a representar, heredado de vista.
	 * 
	 * @param modelo the modelo
	 */
	public<T> void setModelo(final T modelo) {
		if (modelo instanceof Model) {
			this.modelo = (Model) modelo;
		}
	}

	/**
	 * Actualiza el arbol removiendo todos los elementos y agregandolos de
	 * nuevo, pero antes de ser borrados se recuerda su estado para volver a ser
	 * expandidos al cargar.
	 * 
	 * @see #storeExpanded()
	 * @see #rememberExpanded(NodoExplorador)
	 */
	public void updateVista() {
		// Util.print( "Actualizando arbol" );
		if (getElementoRoot() != null) {
			elementosExpandidos = new ArrayList<Object>();
			guardarExpandidos();

			final Namespace elementoRoot = getElementoRoot();
			final NodoExplorador root = new NodoExplorador(elementoRoot, this);

			for (final Object elem : elementoRoot.getOwnedElement()) {
				if (elem instanceof ModelElement) {
					actualizar((ModelElement) elem, root);
				}
			}

			final Collection<Diagram> diagramas = repository.getAllDiagrams();
			for (Object elem : diagramas) {
				final Diagram diagrama= (Diagram) elem;
				if (filtrar(diagrama)) {
					final NodoExplorador nodo = new NodoExplorador(diagrama, this);
					final ModelElement duenho = diagrama.getOwner().getElement();
					final NodoExplorador nodoDuenho = getNodo(root, duenho);
					if (nodoDuenho != null) {
						nodoDuenho.add(nodo);
					} else {
						root.add(nodo);
					}
				}
			}

			((DefaultTreeModel) getModel()).setRoot(root);
			((DefaultTreeModel) getModel()).reload(root);

			recordarExpandidos(root);
			if (elementosExpandidos != null) {
				elementosExpandidos.clear();
				elementosExpandidos = null;
			}
		} else {
			log.warn("Root element is null");
		}
	}

	/* (non-Javadoc)
	 * @see vista.Vista#getTitulo()
	 */
	public String getTitulo() {
		return "Arbol";
	}

	/**
	 * Clase que maneja los eventos del arbol.
	 */
	class ExploradorListener extends MouseAdapter {

		/**
		 * Mouse clicked.
		 * 
		 * @param evt the evt
		 */
		@Override
		public void mouseClicked(final MouseEvent evt) {
			if (evt.getModifiers() == MouseEvent.BUTTON3_MASK) {
				clickDerecho(evt);
			} else if (evt.getModifiers() == MouseEvent.BUTTON1_MASK) {
				if (evt.getClickCount() == 1) {
					click(evt);
				} else {
					dobleClick(evt);
				}
			}
		}

		/**
		 * Mouse pressed.
		 * 
		 * @param evt the evt
		 */
		@Override
		public void mousePressed(final MouseEvent evt) {
			if (evt.getModifiers() == MouseEvent.BUTTON3_MASK) {
				ratonRightPressed(evt);
			} else {
				ratonPressed(evt);
			}
		}

		/**
		 * Mouse released.
		 * 
		 * @param evt the evt
		 */
		@Override
		public void mouseReleased(final java.awt.event.MouseEvent evt) {
			if (evt.getModifiers() == MouseEvent.BUTTON3_MASK) {
				ratonRightReleased(evt);
			}
		}

		/**
		 * Raton pressed.
		 * 
		 * @param evt the evt
		 */
		protected void ratonPressed(final MouseEvent evt) {
			setSelectionPath(getPathForLocation(evt.getX(), evt.getY()));
			if (getSelectionPath() != null) {
				final NodoExplorador nodo = (NodoExplorador) getSelectionPath().getLastPathComponent();
				if (nodo != null) {
				}
			}
		}

		/**
		 * Raton right pressed.
		 * 
		 * @param evt the evt
		 */
		protected void ratonRightPressed(final MouseEvent evt) {
		}

		/**
		 * Raton right released.
		 * 
		 * @param evt the evt
		 */
		protected void ratonRightReleased(final MouseEvent evt) {
		}

	}

	/**
	 * Click.
	 * 
	 * @param evt the evt
	 */
	protected void click(final MouseEvent evt) {
	}

	/**
	 * Doble click.
	 * 
	 * @param evt the evt
	 */
	protected void dobleClick(final MouseEvent evt) { 
		setSelectionPath(getPathForLocation(evt.getX(), evt.getY()));
		if (getSelectionPath() != null) {

			final NodoExplorador nodo = (NodoExplorador) getSelectionPath().getLastPathComponent();
			if (nodo != null) {
				nodo.dobleClick();
			}
		}
	}

	/**
	 * Click derecho.
	 * 
	 * @param evt the evt
	 */
	private void clickDerecho(final MouseEvent evt) {
		setSelectionPath(getPathForLocation(evt.getX(), evt.getY()));
		if (getSelectionPath() != null) {
			NodoExplorador nodo = (NodoExplorador) getSelectionPath().getLastPathComponent();
			if (nodo != null) {
				nodo.clickDerecho(evt.getX(), evt.getY(), this);
			}
		}
	}

	/* (non-Javadoc)
	 * @see vista.Vista#modelPropertyChange(java.beans.PropertyChangeEvent)
	 */
	public void modelPropertyChange(final PropertyChangeEvent evt) {
		if (evt.getPropertyName().equals(PropertyType.MODEL_MODIFIED.toString())) {
			if (!evt.getOldValue().equals(this)) {
				log.debug(getTitulo() + " updating from: "
						+ evt.getOldValue().getClass().getName());
				updateVista();
			}
		}
	}

	/**
	 * The Class ExploradorRenderer.
	 */
	static class ExploradorRenderer extends DefaultTreeCellRenderer {

		/** The relaciones. */
		final ImageIcon relaciones;

		/** The clase asociacion. */
		final ImageIcon claseAsociacion;

		/** The fin asociacion. */
		final ImageIcon finAsociacion;

		/** The atributo. */
		final ImageIcon atributo;

		/** The dependencia. */
		final ImageIcon dependencia;

		/** The generalizacion. */
		final ImageIcon generalizacion;

		/** The abstraccion. */
		final ImageIcon abstraccion;

		/** The interfaz. */
		final ImageIcon interfaz;

		/** The operacion. */
		final ImageIcon operacion;

		/** The parametro. */
		final ImageIcon parametro;

		/** The asociacion. */
		final ImageIcon asociacion;

		/** The clase. */
		final ImageIcon clase;

		/** The paquete. */
		final ImageIcon paquete;

		/** The diagrama clase. */
		final ImageIcon diagramaClase;

		/**
		 * Instantiates a new explorador renderer.
		 */
		public ExploradorRenderer() {
			relaciones = new ImageIcon(getClass().getResource("/iconos/Relaciones.png"));
			claseAsociacion = new ImageIcon(getClass().getResource("/iconos/ClaseAsociacion.png"));
			finAsociacion = new ImageIcon(getClass().getResource("/iconos/FinAsociacion.png"));
			atributo = new ImageIcon(getClass().getResource("/iconos/Atributo.png"));
			dependencia = new ImageIcon(getClass().getResource("/iconos/Dependencia.png"));
			generalizacion = new ImageIcon(getClass().getResource("/iconos/Generalizacion.png"));
			abstraccion = new ImageIcon(getClass().getResource("/iconos/Abstraccion.png"));
			interfaz = new ImageIcon(getClass().getResource("/iconos/Interfaz.png"));
			operacion = new ImageIcon(getClass().getResource("/iconos/Operacion.png"));
			parametro = new ImageIcon(getClass().getResource("/iconos/Parametro.png"));
			asociacion = new ImageIcon(getClass().getResource("/iconos/Asociacion.png"));
			clase = new ImageIcon(getClass().getResource("/iconos/Clase.png"));
			paquete = new ImageIcon(getClass().getResource("/iconos/Paquete.png"));
			diagramaClase = new ImageIcon(getClass().getResource("/iconos/DiagramaClase.png"));
		}

		/* (non-Javadoc)
		 * @see javax.swing.tree.DefaultTreeCellRenderer#getTreeCellRendererComponent(javax.swing.JTree, java.lang.Object, boolean, boolean, boolean, int, boolean)
		 */
		@Override
		public Component getTreeCellRendererComponent(final JTree tree, final java.lang.Object value, final boolean sel, final boolean expanded, final boolean leaf, final int row,
				final boolean hasFocus) {
			super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row, hasFocus);
			if (value instanceof NodoExplorador) {
				NodoExplorador bn = (NodoExplorador) value;
				ModelElement me = bn.getElemento();
				if (me != null) {
					Class<?> elementType = me.getClass();
					String className = elementType.getName();
					if (className.startsWith("org.omg.uml.foundation.core.AssociationClass")) {
						this.setIcon(claseAsociacion);
					} else if (className.startsWith("org.omg.uml.foundation.core.AssociationEnd")) {
						this.setIcon(finAsociacion);
					} else if (className.startsWith("org.omg.uml.foundation.core.Attribute")) {
						this.setIcon(atributo);
					} else if (className.startsWith("org.omg.uml.foundation.core.Dependency")) {
						this.setIcon(dependencia);
					} else if (className.startsWith("org.omg.uml.foundation.core.Generalization")) {
						this.setIcon(generalizacion);
					} else if (className.startsWith("org.omg.uml.foundation.core.Abstraction")) {
						this.setIcon(abstraccion);
					} else if (className.startsWith("org.omg.uml.foundation.core.Interface")) {
						this.setIcon(interfaz);
					} else if (className.startsWith("org.omg.uml.foundation.core.Operation")) {
						this.setIcon(operacion);
					} else if (className.startsWith("org.omg.uml.foundation.core.Parameter")) {
						this.setIcon(parametro);
					} else if (className.startsWith("org.omg.uml.foundation.core.UmlAssociation")) {
						this.setIcon(asociacion);
					} else if (className.startsWith("org.omg.uml.foundation.core.UmlClass")) {
						this.setIcon(clase);
					} else if (className.startsWith("org.omg.uml.modelmanagement.UmlPackage")) {
						this.setIcon(paquete);
					}
				} else {
					Diagram diagram = bn.getDiagrama();
					if (diagram != null) {
						this.setIcon(diagramaClase);
					} else {
						if (bn.getUserObject() instanceof String) {
							String text = (String) bn.getUserObject();
							if (text.equals("Relaciones")) {
								this.setIcon(relaciones);
							}
						}
					}
				}
			}
			return this;
		}
	}

	/**
	 * Gets the controller.
	 * 
	 * @return the controller
	 */
	public ControllerProyecto getController() {
		return controller;
	}

	/**
	 * Sets the controller.
	 * 
	 * @param controller the new controller
	 */
	public void setController(final ControllerProyecto controller) {
		this.controller = controller;
	}

	/**
	 * Sets the contenedor.
	 * 
	 * @param contenedor the new contenedor
	 */
	public void setContenedor(final DiagramContainer contenedor) {
		this.contenedor = contenedor;
	}

}
