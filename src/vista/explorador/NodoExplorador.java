/*
 * NodoExplorador.java
 *
 * Created on 20 de octubre de 2008, 14:32
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package vista.explorador;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.tree.DefaultMutableTreeNode;

import org.omg.uml.foundation.core.Interface;
import org.omg.uml.foundation.core.ModelElement;
import org.omg.uml.foundation.core.Namespace;
import org.omg.uml.foundation.core.Stereotype;
import org.omg.uml.foundation.core.UmlClass;
import org.omg.uml.modelmanagement.UmlPackage;
import org.uml.diagrammanagement.Diagram;
import org.uml.diagrammanagement.Property;
import org.uml.diagrammanagement.Uml1SemanticModelBridge;

import util.AppUtil;
import util.uml.ElementoDeModeloUtil;
import util.uml.ElementoVisualUtil;
import util.uml.UMLUtil;

/**
 * Componente hoja de un JTree especializado para representar elementos UML y
 * diagrama.
 * 
 * @author Juan Timoteo Ponce Ortiz
 */
public final class NodoExplorador extends DefaultMutableTreeNode {

    /** Elemento a representar. */
    public final ModelElement elemento;

    /** Diagrama a representar. */
    private final Diagram diagrama;

    /** The relaciones. */
    private NodoExplorador relaciones;

    /** Arbol padre. */
    private final Explorador padre;

    /**
     * Constructor normal, nada raro.
     * 
     * @param texto
     *            texto del nodo
     * @param padre
     *            arbol padre
     */
    public NodoExplorador(String texto, Explorador padre) {
	super(texto);
	this.padre = padre;
	elemento = null;
	diagrama = null;
    }

    /**
     * Constructor para representar un diagrama.
     * 
     * @param diagrama
     *            the diagrama
     * @param padre
     *            the padre
     */
    public NodoExplorador(Diagram diagrama, Explorador padre) {
	super();
	elemento = null;
	this.diagrama = diagrama;
	this.padre = padre;
	actualizarTexto();
    }

    /**
     * Constructor para representar un elemento.
     * 
     * @param elemento
     *            the elemento
     * @param padre
     *            the padre
     */
    public NodoExplorador(ModelElement elemento, Explorador padre) {
	super();
	this.elemento = elemento;
	diagrama = null;
	this.padre = padre;
	actualizarTexto();
    }

    /**
     * Instantiates a new nodo explorador.
     * 
     * @param text
     *            the text
     * @param element
     *            the element
     * @param browser
     *            the browser
     * @param readonly
     *            the readonly
     */
    public NodoExplorador(String text, ModelElement element,
	    Explorador browser, boolean readonly) {
	super(text);
	elemento = element;
	diagrama = null;
	padre = browser;

    }

    /**
     * Gets the value that must be shown when the user is editing the node in
     * the browser.
     * 
     * @return a String containing the value that will be seen by the user when
     *         he is editing this node.
     * 
     * @see mvcase.core.UmlBrowser.UmlBrowserEditor
     */
    public String getValorEnEdicion() {
	if (elemento != null) {
	    return elemento.getName();
	}
	if (diagrama != null) {
	    return diagrama.getName();
	}
	return (String) getUserObject();
    }

    /**
     * Sets the user object.
     * 
     * @param newobject
     *            the newobject
     */
    @Override
    public void setUserObject(java.lang.Object newobject) {
	if (elemento != null) {
	    super.setUserObject(newobject);
	} else if (diagrama != null) {
	    super.setUserObject(newobject);
	}
    }

    /**
     * Asigna el nombre del elemento o del diagrama.
     * 
     * @param nuevo
     *            nuevo nombre
     */
    public void setNombreElemento(String nuevo) {
	if (elemento != null) {
	    elemento.setName(nuevo);
	} else if (diagrama != null) {
	    diagrama.setName(nuevo);
	}
    }

    /**
     * Actualiza el texto del nodo, ya sea elemento o diagrama.
     */
    public void actualizarTexto() {
	if (elemento != null) {
	    String estereotipo = "";
	    for (Object stereo : elemento.getStereotype()) {
		estereotipo += ((Stereotype) stereo).getName() + ",";
	    }
	    if (estereotipo.endsWith(","))
		estereotipo = estereotipo
			.substring(0, estereotipo.length() - 1);

	    if (!estereotipo.trim().equals(""))
		estereotipo += "<<" + estereotipo + ">> ";
	    if (elemento.getName() == null
		    || elemento.getName().trim().equals(""))
		estereotipo += "sin_nombre";
	    else
		estereotipo += elemento.getName();
	    setUserObject(estereotipo);
	} else if (diagrama != null) {
	    String texto = "";
	    if (diagrama.getName() == null
		    || diagrama.getName().trim().equals(""))
		texto = "sin_nombre";
	    else
		texto = diagrama.getName();
	    setUserObject(texto);
	}
    }

    /**
     * Elimina este nodo.
     */
    public void remove() {
	if (elemento != null) {
	    ElementoDeModeloUtil.removeElemento(elemento);
	} else if (diagrama != null) {
	    if (AppUtil.showConfirmar(null, "Eliminar",
		    "Desea eliminar este diagrama?")) {
		padre.getContenedor().removeGraficador(diagrama);
		ElementoVisualUtil.removeDiagrama(diagrama);
	    }
	}
	padre.getController().changeModelo(this);
    }

    /**
     * Agrega una relacion al nodo como subnodo.
     * 
     * @param nodo
     *            the nodo
     */
    public void addRelacion(NodoExplorador nodo) {
	if (relaciones == null) {
	    relaciones = new NodoExplorador("Relaciones", padre);
	    add(relaciones);
	}
	relaciones.add(nodo);
    }

    /**
     * Gets the elemento.
     * 
     * @return the elemento
     */
    public ModelElement getElemento() {
	return elemento;
    }

    /**
     * Gets the diagrama.
     * 
     * @return the diagrama
     */
    public Diagram getDiagrama() {
	return diagrama;
    }

    /**
     * Gets the padre.
     * 
     * @return the padre
     */
    public Explorador getPadre() {
	return padre;
    }

    /**
     * Responde al evento del click derecho sobre este nodo * muestra el menu de
     * opciones.
     * 
     * @param x
     *            posicion x
     * @param y
     *            posicion y
     * @param comp
     *            componente padre
     */
    public void clickDerecho(int x, int y, Component comp) {
	if (elemento != null) {
	    final NodoExploradorMenu menu = new NodoExploradorMenu(this);
	    menu.show(comp, x, y);
	} else if (diagrama != null) {
	    final NodoExploradorMenu menu = new NodoExploradorMenu(this);
	    menu.show(comp, x, y);
	}
    }

    /**
     * Doble click.
     */
    public void dobleClick() {
	if (diagrama != null) {
	    if (padre.getContenedor().diagramContains(diagrama)) {
		padre.getContenedor().setShow(diagrama);
	    } else {
		padre.getContenedor().addGraficador(diagrama);
	    }
	}
    }

    /**
     * The Class MenuNodoExplorador.
     */
    final class NodoExploradorMenu extends JPopupMenu {

	/** The nodo. */
	private final NodoExplorador nodo;

	/**
	 * Instantiates a new menu nodo explorador.
	 * 
	 * @param nodo
	 *            the nodo
	 */
	public NodoExploradorMenu(NodoExplorador nodo) {
	    super();
	    this.nodo = nodo;
	    init();
	}

	/**
	 * Inits the.
	 */
	public void init() {
	    if (nodo.getElemento() != null) {
		addItemsComunes();
		addSeparator();
		addItemsElemento();
		addSeparator();
	    }
	    if (nodo.getDiagrama() != null) {
		addItemsComunes();
		addSeparator();
		addItemsElemento();
	    }
	}

	/**
	 * Agrega los items comunes a todos los elementos.
	 * 
	 * @param listener
	 *            the listener
	 */
	private void addItemsComunes() {
	    if (elemento instanceof UmlPackage) {
		final JMenuItem mDiagrama = new JMenuItem("Diagrama");
		mDiagrama.setIcon(new ImageIcon(getClass().getResource(
			"/iconos/DiagramaClase.png")));
		mDiagrama.addActionListener(new ActionListener() {

		    @Override
		    public void actionPerformed(ActionEvent e) {
			addDiagrama();
		    }
		});

		final JMenuItem mPaquete = new JMenuItem("Paquete");
		mPaquete.setIcon(new ImageIcon(getClass().getResource(
			"/iconos/Paquete.png")));
		mPaquete.addActionListener(new ActionListener() {

		    @Override
		    public void actionPerformed(ActionEvent e) {
			addPaquete();

		    }
		});

		final JMenuItem mClase = new JMenuItem("Clase");
		mClase.setIcon(new ImageIcon(getClass().getResource(
			"/iconos/Clase.png")));
		mClase.addActionListener(new ActionListener() {

		    @Override
		    public void actionPerformed(ActionEvent e) {
			addClase();
		    }
		});

		final JMenuItem mInterfaz = new JMenuItem("Interfaz");
		mInterfaz.setIcon(new ImageIcon(getClass().getResource(
			"/iconos/Interfaz.png")));
		mInterfaz.addActionListener(new ActionListener() {

		    @Override
		    public void actionPerformed(ActionEvent e) {
			addInterfaz();
		    }
		});
		final JMenu mAdd = new JMenu("Agregar");
		mAdd.add(mDiagrama);
		mAdd.addSeparator();
		mAdd.add(mPaquete);
		mAdd.add(mClase);
		mAdd.add(mInterfaz);
		add(mAdd);
	    }

	    if (elemento instanceof UmlClass || elemento instanceof UmlPackage
		    || elemento instanceof Interface) {
		final JMenuItem mAddModelo = new JMenuItem(
			"Agregar al diagrama");
		mAddModelo.addActionListener(new ActionListener() {

		    @Override
		    public void actionPerformed(ActionEvent e) {
			padre.getContenedor().addElementoToDiagram(elemento);
		    }
		});
		add(mAddModelo);
	    }
	    final JMenuItem mDel = new JMenuItem("Eliminar");
	    mDel.addActionListener(new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
		    nodo.remove();
		}
	    });

	    add(mDel);
	}

	/**
	 * Agrega los elementos de un elementoDemodelo.
	 * 
	 * @param listener
	 *            the listener
	 */
	private void addItemsElemento() {
	    final JMenuItem mEdit = new JMenuItem("Editar");
	    mEdit.addActionListener(new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
		    editElemento();
		}
	    });
	    add(mEdit);
	}
    }

    /**
     * Edits the elemento.
     */
    public void editElemento() {
	if (getElemento() != null) {
	    ElementoDeModeloUtil.editElemento(null, getElemento(), padre
		    .getController(), this);
	} else if (getDiagrama() != null) {
	    final String txt = AppUtil.showInput(null, "Ingrese el nombre",
		    diagrama.getName());
	    diagrama.setName(txt);
	    // Main.getUIPrincipal().actualizarArbol();
	    padre.getController().changeModelo(this);
	}
    }

    /**
     * Adds the diagrama.
     */
    public void addDiagrama() {
	if (elemento != null && elemento instanceof UmlPackage) {
	    final Diagram diagrama = UMLUtil.crearDiagrama("Diagrama");
	    Uml1SemanticModelBridge puenteDiagrama = null;
	    if (getElemento() != null)
		puenteDiagrama = UMLUtil.crearPuenteSemantico(elemento);

	    diagrama.setOwner(puenteDiagrama);
	    final Property diagramaProp = UMLUtil.crearPropiedad("DiagramType",
		    "ClassDiagram");
	    diagrama.getProperty().add(diagramaProp);

	    padre.getController().changeModelo(this);
	} else
	    AppUtil.showAdvertencia(null, "Debe seleccionar un paquete");
    }

    /**
     * Adds the paquete.
     */
    public void addPaquete() {
	if (elemento != null && elemento instanceof UmlPackage) {
	    final UmlPackage pack = UMLUtil.crearPaquete("Nuevo paquete");
	    if (getElemento() != null)
		pack.setNamespace((Namespace) elemento);

	    padre.getController().changeModelo(this);
	} else
	    AppUtil.showAdvertencia(null, "Debe seleccionar un paquete");
    }

    /**
     * Adds the clase.
     */
    public void addClase() {
	if (elemento != null && elemento instanceof UmlPackage) {
	    final UmlClass clase = UMLUtil.crearClase("Nueva clase");
	    if (getElemento() != null)
		clase.setNamespace((Namespace) elemento);

	    padre.getController().changeModelo(this);
	} else
	    AppUtil.showAdvertencia(null, "Debe seleccionar un paquete");
    }

    /**
     * Adds the interfaz.
     */
    public void addInterfaz() {
	if (elemento != null && elemento instanceof UmlPackage) {
	    final Interface inter = UMLUtil.crearInterfaz("Nueva interface");
	    if (getElemento() != null) {
		inter.setNamespace((Namespace) elemento);
	    }
	    padre.getController().changeModelo(this);
	} else
	    AppUtil.showAdvertencia(null, "Debe seleccionar un paquete");
    }

}
